ChangeLog
=========

.. contents:: Releases
   :backlinks: none
   :local:

Unreleased
----------

2.3.0 (2025-02-19)
------------------

* Switch to API 7.0 (#172).
* Support fred-frgal 4 (#160).
* Add ``ContactClient.update`` (#164, #166).
* Add update state methods (#167).
* Add ``snapshot_id`` to ``get`` and ``list`` methods (#168).
* Add auth info methods (#165).
* Add publish flags definition (#169).
* Support string as ``log_entry_id`` (#163).
* Fix wording of ``refs_only`` warning (#170).
* Drop note about an experimental API from 1.6.
* Fix ruff format.

2.2.2 (2025-01-07)
------------------

* Handle empty responses for ``batch_get`` (#162).
* Fix mypy.

2.2.1 (2024-12-04)
------------------

* Fix DnsKey DS record for high alg number (#161).

2.2.0 (2024-11-28)
------------------

* Switch to API 6.0 (#156).
* Add ``batch_get`` methods to ``DomainClient``, ``KeysetClient`` and ``NssetClient`` (#145).
* Add ``domains_count`` to ``Keyset`` and ``Nsset`` (#148).
* Add ``ContactClient.list_merge_candidates`` (#142).
* Add ``ContactClient.merge`` (#143).
* Add ``DomainClient.list_by_contact*`` (#144).
* Add ``DomainClient.list_by_keyset*`` (#144).
* Update ``DomainClient.list_by_nsset*`` (#144).
* Add ``DomainContactRole`` member aliases (#157).
* Add ``KeysetClient.list_by_contact*`` (#144).
* Add ``NssetClient.list_by_contact*`` (#144).
* Add forward compatible methods to clients (#150, #152, #154, #155).

2.1.1 (2024-12-04)
------------------

* Fix DS record for high alg number (#161).

2.1.0 (2024-11-19)
------------------

* Add DS record computation from ``DnsKey`` (#153).
* Add ``list_nssets`` filter for not linked (#151).
* Add support for list results (#146).
* Drop ``aioitertools`` in favor of ``asyncstdlib`` (#149).

2.0.0 (2024-09-24)
------------------

* Upgrade to pydantic 2 (#105).

1.8.1 (2024-09-12)
------------------

* Fix ``InvalidData`` string representation (#136).
* Fix contact create with partially empty addresses (#137).
* Register ``NssetDoesNotExist`` exception for ``list_by_nsset`` (#138).

1.8.0 (2024-09-02)
------------------

* Use API 5.6.0 (#132).
* Add ContactClient.create (#133).
* Switch to fred-frgal, clean warnings (#134).
* Fix failing test.

1.7.0 (2024-07-24)
------------------

* Add decoders for other ref objects (#126).
* Add ``NssetClient.list_nssets`` method (#128).
* Add ``DomainClient.list_by_nsset`` method (#130).
* Add ``ContactClient.list`` method (#131).
* Avoid backports.strenum in python 3.11+ (#127).

1.6.2 (2024-03-18)
------------------

* Add decoder for ``DomainRef`` (#125).

1.6.1 (2024-03-11)
------------------

* **[BREAKING]** Upgrade to API 5.5.1 (#124):
  * Added required argument ``zone`` to ``DomainClient.list``.
* Fix ruff.

1.6.0 (2024-02-28)
------------------

* Upgrade to API 5.5.0 (#121).
* Drop ``_set_sponsoring_registrar`` utility (#116).
* Add warning about change of ``internal`` flag default (#117).
* Add ``list`` to domain client (#120).
* Add ``DomainBlacklist`` client (#118).
* Add ``block_ids`` to ``DomainLifeCycleStageResult`` (#119).
* Update project setup.

1.5.0 (2023-11-27)
------------------

* Add blacklisted domain life cycle stage (#115).
* Update project setup.

1.4.1 (2023-11-14)
------------------

* Fix cache on get_state_flags (#114).

1.4.0 (2023-11-13)
------------------

* Add ``get_state_flags`` methods (#109).
* Add an option to filter state flags (#110).

1.3.0 (2023-10-02)
------------------

* Add pydantic v2 support (#103).
* Add ``Domain.get_life_cycle_stage`` (#107).
* Add support for IDN in FQDNs (#108).
* Fix annotations.
* Disable warn unused ignore.
* Update project setup.

1.2.0 (2023-06-19)
------------------

* Add contact representatives (#92).
* Fix ``get_domains_by_contact`` (#96).
* Add birthdate property to ``AdditionalIdentifier`` (#42).
* Add ``StrEnum`` compat (#94).
* Update project setup.

1.1.4 (2023-05-26)
------------------

* Fix ``get_registrar_certifications`` return value
* Annotate domain admin client return values (#88)

1.1.3 (2023-05-02)
------------------

* Manage domain state with empty domains (#84).
* Fix Dict in annotations (#83).
* Annotate get_domains_by_contact (#85).
* Relax constraint on aioitertools version (#81).

1.1.2 (2023-04-12)
------------------

* Relax ``cryptography`` versions.

1.1.1 (2023-04-06)
------------------

* Fix ``__all__`` export in regal module breaking some static checks

1.1.0 (2023-03-14)
------------------

* Add support for python 3.11 (#70).
* Use ``fred-types`` for base objects (#80).
* Add models and methods for registrar groups and certification
  (#66, #67, #68, #69, #71, #72, #73, #74, #75, #76, #77, #78, #79).
* Implement method ``update_contact_state`` (#64).
* Improve docstring of ``batch_delete_domains`` (#62).
* Update project setup.
* Fix type annotations.

1.0.1 (2023-02-21)
------------------

* Expose DomainContactRole (#61)
* Expose object refs (#63)
* Fix black for 2023

1.0.0 (2023-01-20)
------------------

* **[BREAKING]** Rename domain lifecycle events (#59):

  * All scheduled events now have ``_scheduled_at`` suffix instead of just ``_at``.
  * Events ``outzone_at`` and ``delete_candidate_at`` now correspond to real dates for events that happened in the past.
    If you want scheduled dates, you can use ``outzone_scheduled_at`` and ``delete_candidate_scheduled_at``.

* **[BREAKING]** Use related object refs instead of object ids. Affected model fields (#58):

  * ``Domain.nsset``
  * ``Domain.keyset``
  * ``Domain.registrant``
  * ``Domain.administrative_contacts``
  * ``Keyset.technical_contacts``
  * ``Nsset.technical_contacts``
  * structure returned by ``get_domains_by_contact``

0.8.2 (2023-01-16)
------------------

* Add ``get_domain_life_cycle_stage`` method (#57).

0.8.1 (2021-01-10)
------------------

* Add deleted related domains to ``get_domains_by_contact`` result (#56).

0.8.0 (2022-12-08)
------------------

* Add ``validation_expires_at`` to ``Domain`` (#55).
* Warn on extra kwargs in models (#41).

0.7.0 (2022-12-05)
------------------

* Add ``check_dns_host`` to nsset client (#53).
* Remove deprecated automagical awaitables (#54).

0.6.2 (2022-11-28)
------------------

* Validate SSL certificate fingerprint

0.6.1 (2022-11-23)
------------------

* Catch invalid certificate exception when accessing SSL certificate properties

0.6.0 (2022-11-22)
------------------

* **[BREAKING]** Change type of Domain.expires_at to datetime
* **[BREAKING]** Change ``registrar_handle`` arg to ``registrar_id`` in ``RegistrarAdminClient.update_registrar``
* **[BREAKING]** Change meaning of ``registrar_handle`` in ``RegistrarAdminClient.update_registrar``
  to change registrar handle
* Add registrar id to ``Registrar`` model – default value is the same as registrar handle
* Add domain lifecycle attributes
* Add EPP credentials edit methods
* Add EPP credentials get method
* Add SSL certificate properties
* Reformat code with black

0.5.3 (2022-11-08)
------------------

* Fix annotations

0.5.2 (2022-11-01)
------------------

* Add exception decoding to ``create_registrar`` method of ``RegistrarAdminClient``
* Add ``__str__`` method to ``Address``

0.5.1 (2022-10-10)
------------------

* Add ``update_registrar`` method to ``RegistrarAdminClient``

0.5.0 (2022-09-20)
------------------

* Add ``get_registrar_zone_access_history`` to ``RegistrarClient``
* Add ``create_registrar`` method to ``RegistrarAdminClient``
* Add zone access edit methods (add, update, delete) to ``RegistrarAdminClient``
* Add ``is_internal`` flag to registrar

0.4.1 (2022-06-13)
------------------

* Inherit object id classes from BaseId

0.4.0 (2022-05-09)
------------------

* Add additional domain notify info methods

0.3.2 (2022-05-03)
------------------

* Fix sponsoring registrar validator

0.3.1 (2022-05-02)
------------------

* Fix get_domains_by_contact to return set of roles instead of booleans

0.3.0 (2022-04-28)
------------------

* Add get_domains_by_contact method
* Add sponsoring registrar to all basic object types

0.2.0 (2022-04-14)
------------------

* Add bulk change domain state flags method
* Add bulk delete domains method
* Add get_object_id methods
* Add ObjectDoesNotExist exception
* Add Id and HistoryId types

* Use pydantic models
* Change decoding of unset messages and empty strings
* Replace UUID with str for all object identifiers
* Expose some missing imports

0.1.0 (2021-11-15)
------------------

Initial version.

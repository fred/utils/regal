=====
Regal
=====

Client library for FRED registry backend

Learn more about the project and our community on the `FRED's home page`__

.. _FRED: https://fred.nic.cz

__ FRED_

"""Exceptions module."""

from collections.abc import Sequence


class RegistryException(Exception):
    """Base exception for all registry exceptions."""


class HistoryChangeProhibited(RegistryException):
    """History change is prohibited."""


class InvalidData(RegistryException, ValueError):
    """Invalid data."""

    def __init__(self, fields: Sequence[str]) -> None:  # pragma: no cover
        super().__init__(fields)
        self.fields = fields


class InvalidHistoryInterval(RegistryException):
    """Invalid history interval."""


class InvalidSnapshot(RegistryException):
    """Invalid snapshot."""


class ObjectDoesNotExist(RegistryException):
    """Object does not exist.

    Base class for individual exceptions.
    """


class ContactDoesNotExist(ObjectDoesNotExist):
    """Contact does not exist."""


class ContactAlreadyExists(RegistryException):
    """Contact already exists."""


class ContactHandleInProtectionPeriod(RegistryException):
    """Contact with the same handle was recently deleted."""


class DomainDoesNotExist(ObjectDoesNotExist):
    """Domain does not exist."""


class ZoneDoesNotExist(ObjectDoesNotExist):
    """Zone does not exist."""


class KeysetDoesNotExist(ObjectDoesNotExist):
    """Keyset does not exist."""


class NssetDoesNotExist(ObjectDoesNotExist):
    """Nsset does not exist."""


class RegistrarDoesNotExist(ObjectDoesNotExist):
    """Registrar does not exist."""


class GroupDoesNotExist(ObjectDoesNotExist):
    """Registrar group does not exist."""


class RegistrarCertificationDoesNotExist(ObjectDoesNotExist):
    """Registrar certification does not exist."""


class RegistrarAlreadyExists(RegistryException):
    """Registrar already exists."""


class EppCredentialsDoesNotExist(RegistryException):
    """Registrar EPP credentials does not exist."""


class RepresentativeDoesNotExist(ObjectDoesNotExist):
    """Contact representative does not exist."""


class RepresentativeAlreadyExists(RegistryException):
    """Contact representative already exists."""


class BlockDoesNotExist(ObjectDoesNotExist):
    """Block does not exist."""


class BlockIdAlreadyExists(RegistryException):
    """Block id already exists."""


class NotCurrentVersion(RegistryException):
    """Contact history identification not valid."""


class ExtraArgumentWarning(UserWarning):
    """Extra argument was passed to a model."""


class StateFlagDoesNotExist(RegistryException):
    """State flag does not exist."""

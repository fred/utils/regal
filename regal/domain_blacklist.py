"""Client for registry Domain blacklist service."""

from collections.abc import AsyncIterable
from datetime import datetime
from typing import Optional, TypeVar, cast

from fred_api.registry.domain_blacklist.domain_blacklist_common_types_pb2 import BlockInfo, BlockRef as BlockRefMessage
from fred_api.registry.domain_blacklist.service_domain_blacklist_grpc_pb2 import (
    BlockDoesNotExist as BlockDoesNotExistMessage,
    BlockInfoRequest,
    CreateBlockReply,
    CreateBlockRequest,
    DeleteBlockRequest,
    InvalidData as InvalidDataMessage,
)
from fred_api.registry.domain_blacklist.service_domain_blacklist_grpc_pb2_grpc import BlockStub
from fred_types import BaseModel, BaseObjectId
from frgal import GrpcDecoder
from frgal.aio import AsyncGrpcClient
from google.protobuf.empty_pb2 import Empty
from pydantic import ConfigDict, model_validator

from .exceptions import BlockDoesNotExist, BlockIdAlreadyExists, InvalidData

BlockT = TypeVar("BlockT", "Block", "BlockRef")


class BlockId(BaseObjectId):
    """Block identifier."""


def _validate_label(self: BlockT) -> BlockT:
    """Ensure either fqdn or regex is set."""
    if not self.fqdn and not self.regex:
        raise ValueError("Either fqdn or regex has to be provided.")
    if self.fqdn and self.regex:
        raise ValueError("Only one of fqdn and regex can be provided.")
    return self


class BlockRef(BaseModel):
    """Block reference info."""

    id: Optional[BlockId] = None
    fqdn: str = ""
    regex: str = ""

    # BlockRef can have either fqdn or regex. Make the model immutable, so the data are validated on initialization
    # and we don't have to take care of the updates.
    model_config = ConfigDict(frozen=True)

    _validate_label = model_validator(mode="after")(_validate_label)


class Block(BaseModel):
    """Represents a block."""

    id: Optional[BlockId] = None
    fqdn: str = ""
    regex: str = ""
    requested_by: str = ""
    valid_from: Optional[datetime] = None
    valid_to: Optional[datetime] = None
    reason: str = ""

    # Block can have either fqdn or regex. Make the model immutable, so the data are validated on initialization
    # and we don't have to take care of the updates.
    model_config = ConfigDict(frozen=True)

    _validate_label = model_validator(mode="after")(_validate_label)


class DomainBlacklistDecoder(GrpcDecoder):
    """Domain blacklist service decoder."""

    decode_unset_messages = False
    decode_empty_string_none = False

    def __init__(self) -> None:
        super().__init__()
        self.set_decoder(BlockRefMessage, self._decode_block_ref)
        self.set_decoder(BlockInfo, self._decode_block_info)

        self.set_exception_decoder(InvalidDataMessage, InvalidData)
        self.set_exception_decoder(BlockDoesNotExistMessage, BlockDoesNotExist)
        self.set_exception_decoder(CreateBlockReply.Exception.BlockIdAlreadyExists, BlockIdAlreadyExists)

    def _decode_block_ref(self, value: BlockRefMessage) -> BlockRef:
        """Decode block reference."""
        return BlockRef(**self._decode_message(value))

    def _decode_block_info(self, value: BlockInfo) -> Block:
        """Decode block info."""
        data = self._decode_message(value)
        data["requested_by"] = data.get("requested_by") or ""
        return Block(**data)


class DomainBlacklistClient(AsyncGrpcClient):
    """Domain blacklist service gRPC client."""

    stub_cls = BlockStub
    decoder_cls = DomainBlacklistDecoder
    service = "Block"

    async def get(self, block_id: BlockId) -> Block:
        """Return block object.

        Args:
            block_id: Block identifier.

        Returns:
            Block object.

        Raises:
            BlockDoesNotExist: Identifier doesn't references an existing block.
            InvalidData: Invalid input parameter.
        """
        request = BlockInfoRequest()
        request.id.value = block_id
        return cast(Block, await self.call("get_block_info", request))

    async def list(self) -> AsyncIterable[BlockRef]:
        """Return currently active blocks.

        Returns:
            Iterable over references of currently active blocks.
        """
        async for refs in self.call_stream("list_blocks", Empty()):
            for ref in refs:
                yield ref

    async def create(self, block: Block) -> BlockId:
        """Create block object.

        Args:
            block: Block object to be created.

        Returns:
            Block identifier.

        Raises:
            BlockIdAlreadyExists: Identifier was supplied and references an existing block.
            InvalidData: Invalid input parameter.
        """
        request = CreateBlockRequest()
        if block.id:
            request.block.id.value = block.id
        if block.fqdn:
            request.block.fqdn = block.fqdn
        else:
            request.block.regex = block.regex
        if block.requested_by:
            request.block.requested_by.value = block.requested_by
        if block.valid_from:
            request.block.valid_from.FromDatetime(block.valid_from)
        if block.valid_to:
            request.block.valid_to.FromDatetime(block.valid_to)
        request.block.reason = block.reason
        return BlockId(await self.call("create_block", request))

    async def delete(self, block_id: BlockId, *, requested_by: Optional[str] = None) -> None:
        """Delete block object.

        Args:
            block_id: Block identifier.
            requested_by: Identifier of the application the requested the block.

        Raises:
            BlockDoesNotExist: Identifier doesn't references an existing block.
            InvalidData: Invalid input parameter.
        """
        request = DeleteBlockRequest()
        request.id.value = block_id
        if requested_by:
            request.requested_by.value = requested_by
        await self.call("delete_block", request)

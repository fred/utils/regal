"""Registry auxiliary library."""
# flake8: noqa: F401 - ignore unused imports

from warnings import warn

from fred_types import (
    ContactHistoryId as ContactHistoryId_,
    ContactId as ContactId_,
    ContactRef as ContactRef_,
    DomainHistoryId as DomainHistoryId_,
    DomainId as DomainId_,
    DomainRef as DomainRef_,
    KeysetHistoryId as KeysetHistoryId_,
    KeysetId as KeysetId_,
    KeysetRef as KeysetRef_,
    NssetHistoryId as NssetHistoryId_,
    NssetId as NssetId_,
    NssetRef as NssetRef_,
)

from .common import Address, FileId, FuzzyValue, ListPage, ObjectEvent, ObjectEvents, PaginationReply
from .constants import DELETE
from .contact import (
    AdditionalIdentifier,
    Contact,
    ContactClient,
    ContactIdentifierType,
    MergeCandidate,
    PublishFlags,
    WarningLetterPreference,
)
from .domain import (
    Domain,
    DomainAdminClient,
    DomainByRelation,
    DomainClient,
    DomainContactRole,
    DomainLifeCycleStage,
    NotDeletedReason,
)
from .domain_blacklist import Block, BlockId, BlockRef, DomainBlacklistClient
from .keyset import DnsKey, DSRecord, Keyset, KeysetByRelation, KeysetClient
from .nsset import DnsHost, Nsset, NssetByRelation, NssetClient
from .object import StateUpdate
from .registrar import (
    Registrar,
    RegistrarAdminClient,
    RegistrarCertification,
    RegistrarCertificationId,
    RegistrarClient,
)
from .representative import Representative, RepresentativeClient
from .search import SearchContactClient, SearchDomainClient, SearchKeysetClient, SearchNssetClient, SearchResults

__version__ = "2.3.0"

__all__ = [
    "AdditionalIdentifier",
    "Address",
    "Block",
    "BlockId",
    "BlockRef",
    "Contact",
    "ContactClient",
    "ContactIdentifierType",
    "DELETE",
    "DnsHost",
    "DnsKey",
    "Domain",
    "DomainAdminClient",
    "DomainBlacklistClient",
    "DomainByRelation",
    "DomainClient",
    "DomainContactRole",
    "DomainLifeCycleStage",
    "DSRecord",
    "FileId",
    "FuzzyValue",
    "Keyset",
    "KeysetByRelation",
    "KeysetClient",
    "ListPage",
    "MergeCandidate",
    "NotDeletedReason",
    "Nsset",
    "NssetByRelation",
    "NssetClient",
    "ObjectEvent",
    "ObjectEvents",
    "PaginationReply",
    "PublishFlags",
    "Registrar",
    "RegistrarAdminClient",
    "RegistrarCertification",
    "RegistrarCertificationId",
    "RegistrarClient",
    "Representative",
    "RepresentativeClient",
    "RepresentativeHistoryId",
    "RepresentativeId",
    "SearchContactClient",
    "SearchDomainClient",
    "SearchKeysetClient",
    "SearchNssetClient",
    "SearchResults",
    "StateUpdate",
    "WarningLetterPreference",
    # Deprecated types
    "ContactHistoryId",
    "ContactId",
    "ContactRef",
    "DomainHistoryId",
    "DomainId",
    "DomainRef",
    "KeysetHistoryId",
    "KeysetId",
    "KeysetRef",
    "NssetHistoryId",
    "NssetId",
    "NssetRef",
]

#: Objects in this list should be directly imported from 'fred-types' package
_regal_deprecated = [
    "ContactHistoryId",
    "ContactId",
    "ContactRef",
    "DomainHistoryId",
    "DomainId",
    "DomainRef",
    "KeysetHistoryId",
    "KeysetId",
    "KeysetRef",
    "NssetHistoryId",
    "NssetId",
    "NssetRef",
]


def __getattr__(name):  # type: ignore [no-untyped-def]
    if name in _regal_deprecated:  # pragma: nocover
        warn(f"'{name}' should be imported directly from 'fred-types'", DeprecationWarning, stacklevel=2)
        return globals()[f"{name}_"]
    raise AttributeError(f"module '{__name__!r}' has no attribute '{name!r}'")

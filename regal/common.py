"""Common registry objects and decoder."""

from collections.abc import AsyncIterable, AsyncIterator, Sequence
from datetime import datetime
from typing import (
    Any,
    Generic,
    Optional,
    TypedDict,
    TypeVar,
    Union,
)

from asyncstdlib import anext, islice as aislice, iter as aiter
from fred_api.registry.common_types_pb2 import (
    ContactDoesNotExist as ContactDoesNotExistMessage,
    DomainDoesNotExist as DomainDoesNotExistMessage,
    InvalidData as InvalidDataMessage,
    InvalidSnapshot as InvalidSnapshotMessage,
    KeysetDoesNotExist as KeysetDoesNotExistMessage,
    NssetDoesNotExist as NssetDoesNotExistMessage,
    OrderByDirection,
    PlaceAddress,
    RegistrarDoesNotExist as RegistrarDoesNotExistMessage,
    StateFlag,
    StateFlagDoesNotExist as StateFlagDoesNotExistMessage,
)
from fred_api.registry.contact.contact_common_types_pb2 import ContactAddress, ContactRef as ContactRefMessage
from fred_api.registry.contact.service_search_contact_grpc_pb2 import SearchContactHistoryReply, SearchContactReply
from fred_api.registry.domain.domain_common_types_pb2 import DomainRef as DomainRefMessage
from fred_api.registry.domain.service_search_domain_grpc_pb2 import SearchDomainHistoryReply, SearchDomainReply
from fred_api.registry.keyset.keyset_common_types_pb2 import KeysetRef as KeysetRefMessage
from fred_api.registry.keyset.service_search_keyset_grpc_pb2 import SearchKeysetHistoryReply, SearchKeysetReply
from fred_api.registry.nsset.nsset_common_types_pb2 import NssetRef as NssetRefMessage
from fred_api.registry.nsset.service_search_nsset_grpc_pb2 import SearchNssetHistoryReply, SearchNssetReply
from fred_api.registry.object_events_pb2 import ObjectEvents as ObjectEventsMessage
from fred_api.registry.registrar.registrar_common_types_pb2 import RegistrarRef as RegistrarRefMessage
from fred_types import BaseId, BaseModel, BaseObjectHistoryId, ContactRef, DomainRef, KeysetRef, NssetRef, RegistrarRef
from frgal import GrpcDecoder
from google.protobuf.message import Message
from pydantic import ConfigDict

from .exceptions import (
    ContactDoesNotExist,
    DomainDoesNotExist,
    InvalidData,
    InvalidSnapshot,
    KeysetDoesNotExist,
    NssetDoesNotExist,
    RegistrarDoesNotExist,
    StateFlagDoesNotExist,
)
from .object import ObjectStateFlag
from .utils import AnyIterable

T = TypeVar("T")
FuzzyValueType = Union[
    SearchContactReply.Data.FuzzyValue,
    SearchContactHistoryReply.Data.FuzzyValue,
    SearchDomainReply.Data.FuzzyValue,
    SearchDomainHistoryReply.Data.FuzzyValue,
    SearchKeysetReply.Data.FuzzyValue,
    SearchKeysetHistoryReply.Data.FuzzyValue,
    SearchNssetReply.Data.FuzzyValue,
    SearchNssetHistoryReply.Data.FuzzyValue,
]


class FuzzyValue(BaseModel):
    """Fuzzy value with lower and upper boundary."""

    lower_estimate: int
    upper_estimate: int


class DatetimeRange(BaseModel):
    """Datetime range."""

    start: Optional[datetime] = None
    end: Optional[datetime] = None


class ObjectEvent(BaseModel):
    """Registry object event detail.

    Attributes:
        registrar_handle: Handle of the registrar which triggered the event.
        timestamp: Timestamp of the event. It may not be present according to the API.
    """

    registrar_handle: str
    timestamp: Optional[datetime] = None


class ObjectEvents(BaseModel):
    """Registry object events collection."""

    registered: ObjectEvent
    transferred: ObjectEvent
    updated: Optional[ObjectEvent] = None
    unregistered: Optional[ObjectEvent] = None


class Address(BaseModel):
    """Place address."""

    street: Sequence[str]
    city: str
    state_or_province: str = ""
    postal_code: Optional[str] = None
    country_code: Optional[str] = None
    company: Optional[str] = None

    def __str__(self) -> str:
        parts = (
            self.company,
            ", ".join(self.street),
            self.city,
            self.state_or_province,
            self.postal_code,
            self.country_code,
        )
        return ", ".join(item for item in parts if item)


class FileId(BaseId):
    """File identification."""


class ObjectHistoryItem(BaseModel):
    """Item of registry object history."""

    history_id: BaseObjectHistoryId
    valid_from: datetime
    log_entry_id: Optional[str]


class ObjectStateHistoryItem(BaseModel):
    """Item of registry object state history."""

    valid_from: datetime
    flags: dict[str, bool]


H = TypeVar("H", ObjectHistoryItem, ObjectStateHistoryItem)


class ObjectHistory(BaseModel, Generic[H]):
    """Registry object history."""

    timeline: list[H]
    valid_to: Optional[datetime]


class RegistryDecoder(GrpcDecoder):
    """Decoder for structures shared among multiple services."""

    decode_unset_messages = False
    decode_empty_string_none = False

    def __init__(self) -> None:
        super().__init__()
        self.set_decoder(PlaceAddress, self._decode_address)
        self.set_decoder(ContactAddress, self._decode_address)
        self.set_decoder(ObjectEventsMessage.EventData, self._decode_object_event)
        self.set_decoder(ObjectEventsMessage, self._decode_object_events)

        self.set_decoder(SearchContactReply.Data.FuzzyValue, self._decode_fuzzy_value)
        self.set_decoder(SearchContactHistoryReply.Data.FuzzyValue, self._decode_fuzzy_value)
        self.set_decoder(SearchDomainReply.Data.FuzzyValue, self._decode_fuzzy_value)
        self.set_decoder(SearchDomainHistoryReply.Data.FuzzyValue, self._decode_fuzzy_value)
        self.set_decoder(SearchKeysetReply.Data.FuzzyValue, self._decode_fuzzy_value)
        self.set_decoder(SearchKeysetHistoryReply.Data.FuzzyValue, self._decode_fuzzy_value)
        self.set_decoder(SearchNssetReply.Data.FuzzyValue, self._decode_fuzzy_value)
        self.set_decoder(SearchNssetHistoryReply.Data.FuzzyValue, self._decode_fuzzy_value)
        self.set_decoder(StateFlag.Traits, self._decode_traits)

        self.set_decoder(ContactRefMessage, self._decode_contact_ref)
        self.set_decoder(DomainRefMessage, self._decode_domain_ref)
        self.set_decoder(KeysetRefMessage, self._decode_keyset_ref)
        self.set_decoder(NssetRefMessage, self._decode_nsset_ref)
        self.set_decoder(RegistrarRefMessage, self._decode_registrar_ref)

        self.set_exception_decoder(InvalidDataMessage, InvalidData)
        self.set_exception_decoder(InvalidSnapshotMessage, InvalidSnapshot)
        self.set_exception_decoder(ContactDoesNotExistMessage, ContactDoesNotExist)
        self.set_exception_decoder(DomainDoesNotExistMessage, DomainDoesNotExist)
        self.set_exception_decoder(KeysetDoesNotExistMessage, KeysetDoesNotExist)
        self.set_exception_decoder(NssetDoesNotExistMessage, NssetDoesNotExist)
        self.set_exception_decoder(RegistrarDoesNotExistMessage, RegistrarDoesNotExist)
        self.set_exception_decoder(StateFlagDoesNotExistMessage, StateFlagDoesNotExist)

    def _decode_fuzzy_value(self, value: FuzzyValueType) -> FuzzyValue:
        """Fuzzy value decoder."""
        return FuzzyValue(lower_estimate=value.lower_estimate, upper_estimate=value.upper_estimate)

    def _decode_address(self, value: Any) -> Address:
        """Address decoder."""
        return Address(**self._decode_message(value))

    def _decode_object_event(self, value: ObjectEventsMessage.EventData) -> Optional[ObjectEvent]:
        """Object event decoder."""
        registrar_handle = self.decode(value.registrar_handle)
        timestamp = self.decode(value.timestamp)
        if registrar_handle or timestamp:
            return ObjectEvent(registrar_handle=registrar_handle, timestamp=timestamp)
        else:
            return None

    def _decode_object_events(self, value: ObjectEventsMessage) -> ObjectEvents:
        """Object events decoder."""
        return ObjectEvents(
            registered=self.decode(value.registered),
            transferred=self.decode(value.transferred),
            updated=self.decode(value.updated),
            unregistered=self.decode(value.unregistered),
        )

    def _decode_traits(self, value: StateFlag.Traits) -> ObjectStateFlag:
        """Decode state flag traits."""
        return ObjectStateFlag(
            manual=(value.how_to_set == StateFlag.Manipulation.Enum.manual),
            internal=(value.visibility == StateFlag.Visibility.Enum.internal),
        )

    def _decode_contact_ref(self, value: ContactRefMessage) -> ContactRef:
        """Decode ContactRefs."""
        return ContactRef(**self._decode_message(value))

    def _decode_domain_ref(self, value: DomainRefMessage) -> DomainRef:
        """Decode DomainRefs."""
        return DomainRef(**self._decode_message(value))

    def _decode_keyset_ref(self, value: KeysetRefMessage) -> KeysetRef:
        """Decode KeysetRefs."""
        return KeysetRef(**self._decode_message(value))

    def _decode_nsset_ref(self, value: NssetRefMessage) -> NssetRef:
        """Decode NssetRefs."""
        return NssetRef(**self._decode_message(value))

    def _decode_registrar_ref(self, value: RegistrarRefMessage) -> RegistrarRef:
        """Decode RegistrarRefs."""
        return RegistrarRef(**self._decode_message(value))


class PaginationResult(TypedDict):
    """Type representing a pagination result.

    Deprecated, will be replaced by PaginationReply in a future version.
    """

    next_page_token: str
    items_left: int


def set_request_pagination(
    request: Message,
    *,
    page_token: Optional[str] = None,
    page_size: Optional[int] = None,
) -> None:
    """Set pagination on request."""
    if page_token is not None:
        request.pagination.page_token = page_token  # type: ignore[attr-defined]
    if page_size is not None:
        request.pagination.page_size = page_size  # type: ignore[attr-defined]


class PaginationReply(BaseModel):
    """Model representing a pagination result.

    Will be renamed to PaginationResult in a future version.
    """

    model_config = ConfigDict(frozen=True)

    next_page_token: str
    items_left: int


class ListPage(BaseModel, Generic[T]):
    """Model with a single page of list result.

    This is a serializable version of `ListResult`.
    """

    model_config = ConfigDict(frozen=True)

    results: tuple[T, ...]
    pagination: Optional[PaginationReply] = None


async def _extract_pagination(stream: AsyncIterable[dict[str, Any]]) -> Optional[PaginationReply]:
    """Utility to extract pagination data from a stream.

    The stream is consumed and thus not usable for other uses.
    """
    first_chunk = await anext(aislice(stream, 0, None), None)
    if first_chunk is not None and first_chunk.get("pagination"):
        return PaginationReply(**first_chunk["pagination"])
    return None


_M = TypeVar("_M", bound=Message)


async def _encode_order_by(order_by: AnyIterable[str], message: type[_M]) -> AsyncIterator[_M]:
    """Encode order_by string into iterable of order by messages."""
    order_by_field = message.DESCRIPTOR.fields_by_name["field"].enum_type

    # XXX: Bug in python 3.9 coverage
    async for name in aiter(order_by):  # pragma: no cover
        order_direction = OrderByDirection.order_by_direction_ascending
        for direction, sign in (
            (OrderByDirection.order_by_direction_ascending, "+"),
            (OrderByDirection.order_by_direction_descending, "-"),
        ):
            if name.startswith(sign):
                name = name.removeprefix(sign)
                order_direction = direction
                break

        full_name = "order_by_field_" + name
        if full_name not in order_by_field.values_by_name:
            raise ValueError("Unknown order_by field: {!r}".format(name))
        else:
            field = order_by_field.values_by_name[full_name].index
            # Enum's value 0 should be only used for 'unspecified' as per protobuf recommendation.
            if not field:
                raise ValueError("Ordering by {!r} is forbidden.".format(name))
        yield message(field=field, direction=order_direction)

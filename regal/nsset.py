"""Nsset client for registry service."""

import warnings
from collections.abc import AsyncIterable, AsyncIterator, Sequence
from datetime import datetime
from ipaddress import IPv4Address, IPv6Address, ip_address
from typing import Any, Literal, Optional, Union, cast, overload

import grpc
from asyncstdlib import batched as abatched, enumerate as aenumerate, tuple as atuple
from fred_api.registry.nsset.add_nsset_auth_info_types_pb2 import AddNssetAuthInfoRequest
from fred_api.registry.nsset.list_nssets_by_contact_types_pb2 import ListNssetsByContactRequest
from fred_api.registry.nsset.list_nssets_types_pb2 import ListNssetsRequest
from fred_api.registry.nsset.nsset_history_types_pb2 import NssetHistoryReply, NssetHistoryRequest
from fred_api.registry.nsset.nsset_info_types_pb2 import (
    BatchNssetInfoRequest,
    CheckDnsHostRequest,
    DnsHost as DnsHostMessage,
    NssetIdRequest,
    NssetInfoReply,
    NssetInfoRequest,
)
from fred_api.registry.nsset.nsset_state_history_types_pb2 import NssetStateHistoryReply, NssetStateHistoryRequest
from fred_api.registry.nsset.nsset_state_types_pb2 import NssetStateRequest
from fred_api.registry.nsset.service_nsset_grpc_pb2_grpc import NssetStub
from fred_api.registry.nsset.update_nsset_state_types_pb2 import UpdateNssetStateRequest
from fred_types import BaseModel, ContactId, ContactRef, NssetHistoryId, NssetId, NssetRef, SnapshotId
from google.protobuf.empty_pb2 import Empty
from pydantic import ConfigDict

from .common import (
    ListPage,
    ObjectEvents,
    ObjectHistory,
    ObjectHistoryItem,
    ObjectStateHistoryItem,
    PaginationReply,
    RegistryDecoder,
    _encode_order_by,
    set_request_pagination,
)
from .exceptions import InvalidHistoryInterval
from .object import ObjectStateFlag, RegistryObjectClient, StateUpdate
from .utils import AnyIterable, _idna_encode


class DnsHost(BaseModel):
    """Dns host info."""

    fqdn: str
    ip_addresses: list[Union[IPv4Address, IPv6Address]]


class Nsset(BaseModel):
    """Nsset info."""

    nsset_id: NssetId
    nsset_handle: str
    nsset_history_id: Optional[NssetHistoryId] = None
    snapshot_id: Optional[SnapshotId] = None
    sponsoring_registrar: Optional[str] = None
    dns_hosts: Sequence[DnsHost] = []
    technical_contacts: Sequence[ContactRef] = []
    technical_check_level: Optional[int] = None
    auth_info: Optional[str] = None
    events: Optional[ObjectEvents] = None
    domains_count: Optional[int] = None


class NssetByRelation(BaseModel):
    """Model representing a nsset returned as a result of a relation."""

    model_config = ConfigDict(frozen=True)

    nsset: NssetRef
    nsset_history_id: Optional[NssetHistoryId] = None
    is_deleted: bool = False


class NssetDecoder(RegistryDecoder):
    """Nsset service decoder."""

    def __init__(self) -> None:
        super().__init__()
        self.set_decoder(DnsHostMessage, self._decode_dns_host)
        self.set_decoder(NssetInfoReply.Data, self._decode_nsset_info)
        # Decode exceptions in batch info response
        self.set_decoder(NssetInfoReply.Exception, self.decode_exception)

        self.set_exception_decoder(NssetHistoryReply.Exception.InvalidHistoryInterval, InvalidHistoryInterval)
        self.set_exception_decoder(NssetStateHistoryReply.Exception.InvalidHistoryInterval, InvalidHistoryInterval)

    def _decode_dns_host(self, value: DnsHostMessage) -> DnsHost:
        """Decode DNS host."""
        return DnsHost(
            fqdn=self.decode(value.fqdn),
            ip_addresses=[ip_address(address) for address in self.decode(value.ip_addresses)],
        )

    def _decode_nsset_info(self, value: NssetInfoReply.Data) -> Nsset:
        """Decode NssetInfoReply.Data."""
        data = self._decode_message(value)
        data.pop("technical_contacts_old", None)
        return Nsset(**data)


class NssetClient(RegistryObjectClient[NssetId, NssetHistoryId]):
    """Nsset service gRPC client."""

    stub_cls = NssetStub
    decoder_cls = NssetDecoder
    service = "Nsset"
    object_id = "nsset_id"
    object_history_id = "nsset_history_id"
    object_snapshot_id = "nsset_snapshot_id"
    object_handle = "nsset_handle"

    def __init__(self, netloc: str, credentials: Optional[grpc.ChannelCredentials] = None, **kwargs: Any) -> None:
        """Initialize nsset client instance.

        Args:
            netloc: Netloc of nsset service server.
            credentials: Optional credentials.
            kwargs: Other client arguments.
        """
        super().__init__(netloc, credentials=credentials, **kwargs)

    async def list(self, *, exclude_not_linked: bool = False) -> AsyncIterable[NssetRef]:
        """Iterate through all nssets in domain registry.

        Args:
            exclude_not_linked: Whether to exclude nssets without domains.

        Returns:
            Async iterable over domain refs.
        """
        request = ListNssetsRequest()
        request.exclude_nssets_without_domain = exclude_not_linked
        # XXX: Bug in python 3.9 coverage
        async for refs in self.call_stream("list_nssets", request):  # pragma: no cover
            for ref in refs:
                yield ref

    async def get_id(self, handle: str) -> NssetId:
        """Get nsset id.

        Args:
            handle: Nsset handle.

        Raises:
            NssetDoesNotExist: If nsset does not exist.
        """
        return NssetId(await self.get_object_id(request=NssetIdRequest(), method="get_nsset_id", handle=handle))

    async def get_nsset_id(self, handle: str) -> NssetId:
        """Get nsset id.

        Args:
            handle: Nsset handle.

        Raises:
            NssetDoesNotExist: If nsset does not exist.
        """
        warnings.warn("Method get_nsset_id is deprecated in favor of get_id.", DeprecationWarning, stacklevel=2)
        return await self.get_id(handle)

    async def get(
        self,
        nsset_id: NssetId,
        history_id: Optional[NssetHistoryId] = None,
        *,
        snapshot_id: Optional[SnapshotId] = None,
        include_domains_count: bool = False,
    ) -> Nsset:
        """Get nsset info.

        Args:
            nsset_id: Nsset ID.
            history_id: Nsset history ID.
            snapshot_id: Snapshot identification.
            include_domains_count: Whether to include domains count in results.

        Raises:
            NssetDoesNotExist: If nsset does not exist.
            InvalidSnapshot: If snapshot_id is provided, but not longer valid.
        """
        request = NssetInfoRequest()
        request.nsset_id.uuid.value = nsset_id
        if history_id is not None:
            request.nsset_history_id.uuid.value = history_id
        if snapshot_id is not None:
            request.nsset_snapshot_id.value = snapshot_id
        request.include_domains_count = include_domains_count
        return cast(Nsset, await self.call("get_nsset_info", request))

    async def _make_batch_get_requests(
        self,
        nsset_ids: AnyIterable[NssetId],
        *,
        snapshot_id: Optional[SnapshotId] = None,
        include_domains_count: bool = False,
        batch_size: int = 100,
    ) -> AsyncIterator[BatchNssetInfoRequest]:
        async for batch in abatched(nsset_ids, n=batch_size):
            request = BatchNssetInfoRequest()
            for id in batch:
                r = request.requests.add()
                r.nsset_id.uuid.value = id
                if snapshot_id is not None:
                    r.nsset_snapshot_id.value = snapshot_id
                r.include_domains_count = include_domains_count
            yield request

    async def batch_get(
        self,
        nsset_ids: AnyIterable[NssetId],
        *,
        snapshot_id: Optional[SnapshotId] = None,
        include_domains_count: bool = False,
        errors: Literal["ignore", "raise"] = "ignore",
        batch_size: int = 100,
    ) -> AsyncIterator[Nsset]:
        """Batch get nsset info.

        Args:
            nsset_ids: Iterable of nsset identifiers.
            snapshot_id: Snapshot identification.
            include_domains_count: Whether to include domains count in results.
            errors: How to handle errors. If 'ignore' (default) nssets with errors are not returned.
                    If 'raise' the first error is raised as an exception.
            batch_size: Number of items in a single stream request.

        Raises:
            NssetDoesNotExist: If nsset does not exist.
            InvalidSnapshot: If provided snapshot_id is no longer valid.
        """
        if errors not in ("ignore", "raise"):
            raise ValueError("Unknown errors value: {!r}".format(errors))

        requests = self._make_batch_get_requests(
            nsset_ids, snapshot_id=snapshot_id, include_domains_count=include_domains_count, batch_size=batch_size
        )
        # XXX: Bug in python 3.9 coverage
        async for obj in self._process_batch_info_stream(
            self.call_stream("batch_nsset_info", requests), errors
        ):  # pragma: no cover
            yield obj

    async def get_nsset_info(
        self, nsset_id: NssetId, history_id: Optional[NssetHistoryId] = None, *, include_domains_count: bool = False
    ) -> Nsset:
        """Get nsset info.

        Args:
            nsset_id: Nsset ID.
            history_id: Nsset history ID.
            include_domains_count: Whether to include domains count in results.

        Raises:
            NssetDoesNotExist: If nsset does not exist.
        """
        warnings.warn("Method get_nsset_info is deprecated in favor of get.", DeprecationWarning, stacklevel=2)
        return await self.get(nsset_id, history_id, include_domains_count=include_domains_count)

    async def get_state(
        self, nsset_id: NssetId, *, internal: bool = False, snapshot_id: Optional[SnapshotId] = None
    ) -> frozenset[str]:
        """Get nsset state consisting of active state flags.

        Args:
            nsset_id: Nsset ID.
            internal: Whether to include internal states in the result.
            snapshot_id: Snapshot identification.

        Returns:
            Set of active state flags.

        Raises:
            NssetDoesNotExist: If nsset does not exist.
            InvalidSnapshot: If snapshot_id is provided, but not longer valid.
        """
        state = await self.get_object_state(
            request=NssetStateRequest(),
            method="get_nsset_state",
            object_id=nsset_id,
            internal=internal,
            snapshot_id=snapshot_id,
        )
        return frozenset(name for (name, enabled) in state.items() if enabled is True)

    async def get_nsset_state(self, nsset_id: NssetId, *, internal: Optional[bool] = None) -> dict[str, bool]:
        """Get nsset state consisting of state flags and their boolean values.

        Args:
            nsset_id: Nsset ID.
            internal: Whether to include internal states in the result.

        Raises:
            NssetDoesNotExist: If nsset does not exist.
        """
        warnings.warn("Method get_nsset_state is deprecated in favor of get_state.", DeprecationWarning, stacklevel=2)
        if internal is None:  # pragma: no cover
            warnings.warn(
                "Default value of internal flag will change to False in regal 2.0.", DeprecationWarning, stacklevel=2
            )
            internal = True

        return await self.get_object_state(
            request=NssetStateRequest(), method="get_nsset_state", object_id=nsset_id, internal=internal
        )

    async def get_history(
        self,
        nsset_id: NssetId,
        start: Optional[Union[NssetHistoryId, datetime]] = None,
        end: Optional[Union[NssetHistoryId, datetime]] = None,
    ) -> ObjectHistory[ObjectHistoryItem]:
        """Get nsset history timeline.

        Args:
            nsset_id: Nsset ID.
            start: Datetime or history_id of history interval start.
            end: Datetime or history_id of history interval end.

        Raises:
            NssetDoesNotExist: If nsset does not exist.
            InvalidHistoryInterval: In case of invalid history interval.
        """
        return await self.get_object_history(
            request=NssetHistoryRequest(),
            method="get_nsset_history",
            object_id=nsset_id,
            start=start,
            end=end,
        )

    async def get_nsset_history(
        self,
        nsset_id: NssetId,
        start: Optional[Union[NssetHistoryId, datetime]] = None,
        end: Optional[Union[NssetHistoryId, datetime]] = None,
    ) -> dict[str, Any]:
        """Get nsset history timeline.

        Args:
            nsset_id: Nsset ID.
            start: Datetime or history_id of history interval start.
            end: Datetime or history_id of history interval end.

        Raises:
            NssetDoesNotExist: If nsset does not exist.
            InvalidHistoryInterval: In case of invalid history interval.
        """
        warnings.warn(
            "Method get_nsset_history is deprecated in favor of get_history.", DeprecationWarning, stacklevel=2
        )
        return await self.get_history_data(
            request=NssetHistoryRequest(),
            method="get_nsset_history",
            object_id=nsset_id,
            start=start,
            end=end,
        )

    async def get_state_history(
        self,
        nsset_id: NssetId,
        start: Optional[Union[NssetHistoryId, datetime]] = None,
        end: Optional[Union[NssetHistoryId, datetime]] = None,
    ) -> ObjectHistory[ObjectStateHistoryItem]:
        """Get nsset state history timeline.

        Args:
            nsset_id: Nsset ID.
            start: Datetime or history_id of history interval start.
            end: Datetime or history_id of history interval end.

        Raises:
            NssetDoesNotExist: If nsset does not exist.
            InvalidHistoryInterval: In case of invalid history interval.
        """
        result = await self.get_object_state_history(
            request=NssetStateHistoryRequest(),
            method="get_nsset_state_history",
            object_id=nsset_id,
            start=start,
            end=end,
        )
        result.pop(self.object_id, None)
        return ObjectHistory[ObjectStateHistoryItem](**result)

    async def get_nsset_state_history(
        self,
        nsset_id: NssetId,
        start: Optional[Union[NssetHistoryId, datetime]] = None,
        end: Optional[Union[NssetHistoryId, datetime]] = None,
    ) -> dict[str, Any]:
        """Get nsset state history timeline.

        Args:
            nsset_id: Nsset ID.
            start: Datetime or history_id of history interval start.
            end: Datetime or history_id of history interval end.

        Raises:
            NssetDoesNotExist: If nsset does not exist.
            InvalidHistoryInterval: In case of invalid history interval.
        """
        warnings.warn(
            "Method get_nsset_state_history is deprecated in favor of get_state_history.",
            DeprecationWarning,
            stacklevel=2,
        )
        return await self.get_object_state_history(
            request=NssetStateHistoryRequest(),
            method="get_nsset_state_history",
            object_id=nsset_id,
            start=start,
            end=end,
        )

    async def get_state_flags(self) -> dict[str, ObjectStateFlag]:
        """Get object state flags and their properties."""
        return await self.get_object_state_flags(method="get_nsset_state_flags")  # type: ignore[call-overload]

    async def list_by_contact(
        self,
        contact_id: ContactId,
        *,
        snapshot_id: Optional[SnapshotId] = None,
        aggregate_entire_history: bool = False,
        order_by: AnyIterable[str] = (),
    ) -> AsyncIterator[NssetByRelation]:
        """Return nssets associated with contact.

        Args:
            contact_id: Contact ID.
            snapshot_id: Snapshot identification.
            aggregate_entire_history: Include nssets connected to the contact in the past.
            order_by: Comma separated list of fields to order by.
                Each field can be prefixed with `+` (asc) or `-` (desc). `+` is optional.

        Returns:
            Iterator of nssets.

        Raises:
            ContactDoesNotExist: If contact does not exist.
            InvalidData: Invalid input parameters.
            InvalidSnapshot: If snapshot_id is provided, but not longer valid.
        """
        request = ListNssetsByContactRequest()
        request.contact_id.uuid.value = contact_id
        if snapshot_id is not None:
            request.contact_snapshot_id.value = snapshot_id
        request.aggregate_entire_history = aggregate_entire_history
        request.order_by.extend(await atuple(_encode_order_by(order_by, ListNssetsByContactRequest.OrderBy)))

        # XXX: Bug in python 3.9 coverage
        async for chunk in self.call_stream("list_nssets_by_contact", request):  # pragma: no cover
            for data in chunk["nssets"]:
                yield NssetByRelation(**data)

    async def list_by_contact_page(
        self,
        contact_id: ContactId,
        *,
        snapshot_id: Optional[SnapshotId] = None,
        aggregate_entire_history: bool = False,
        order_by: AnyIterable[str] = (),
        page_token: Optional[str] = None,
        page_size: Optional[int] = None,
    ) -> ListPage[NssetByRelation]:
        """Return nssets associated with contact.

        Args:
            contact_id: Contact ID.
            snapshot_id: Snapshot identification.
            aggregate_entire_history: Include nssets connected to the contact in the past.
            order_by: Comma separated list of fields to order by.
                Each field can be prefixed with `+` (asc) or `-` (desc). `+` is optional.
            page_token: Page token for pagination.
            page_size: Page size for pagination.

        Returns:
            Nssets and pagination info.

        Raises:
            ContactDoesNotExist: If contact does not exist.
            InvalidData: Invalid input parameters.
            InvalidSnapshot: If snapshot_id is provided, but not longer valid.
        """
        request = ListNssetsByContactRequest()
        request.contact_id.uuid.value = contact_id
        if snapshot_id is not None:
            request.contact_snapshot_id.value = snapshot_id
        request.aggregate_entire_history = aggregate_entire_history
        request.order_by.extend(await atuple(_encode_order_by(order_by, ListNssetsByContactRequest.OrderBy)))
        set_request_pagination(request, page_token=page_token, page_size=page_size)

        results = []
        pagination = None
        # XXX: Bug in python 3.9 coverage
        async for chunk in self.call_stream("list_nssets_by_contact", request):  # pragma: no cover
            if pagination is None and chunk.get("pagination"):
                pagination = PaginationReply(**chunk.get("pagination"))
            for data in chunk["nssets"]:
                results.append(NssetByRelation(**data))

        return ListPage(results=tuple(results), pagination=pagination)

    async def _make_update_state_requests(
        self,
        updates: AnyIterable[StateUpdate[NssetId]],
        *,
        snapshot_id: Optional[SnapshotId] = None,
        log_entry_id: Optional[str] = None,
        batch_size: int = 100,
    ) -> AsyncIterator[UpdateNssetStateRequest]:
        async for i, batch in aenumerate(abatched(updates, n=batch_size)):
            request = UpdateNssetStateRequest()
            for update in batch:
                message = request.updates.add()
                message.nsset_id.uuid.value = update.id
                message.set_flags.extend(update.set_flags)
                message.unset_flags.extend(update.unset_flags)
            if i == 0:
                if snapshot_id is not None:
                    request.nsset_snapshot_id.value = snapshot_id
                if log_entry_id is not None:
                    request.log_entry_id.value = log_entry_id
            yield request

    async def update_state(
        self,
        updates: AnyIterable[StateUpdate[NssetId]],
        *,
        snapshot_id: Optional[SnapshotId] = None,
        log_entry_id: Optional[str] = None,
        batch_size: int = 100,
    ) -> None:
        """Update nssets' states.

        Args:
            updates: Iterable of changes.
            snapshot_id: The snapshot identification.
            log_entry_id: Identification of logger entry for the action.
            batch_size: Number of objects in a single stream request.

        Raises:
            NssetDoesNotExist: Nsset not found.
            InvalidSnapshot: Snapshot is not valid.
            StateFlagDoesNotExist: State flag not found.
            InvalidData: Invalid input parameters.
        """
        await atuple(
            self.call_stream(
                "update_nsset_state",
                self._make_update_state_requests(
                    updates, snapshot_id=snapshot_id, log_entry_id=log_entry_id, batch_size=batch_size
                ),
            )
        )

    async def check_dns_host(self, fqdn: str) -> bool:
        """Return whether DNS host exists.

        This method returns `True` if there is at least one registered nsset containing nameserver with `fqdn`.

        Args:
            fqdn: DNS host's fqdn.
        """
        request = CheckDnsHostRequest(fqdn=_idna_encode(fqdn))
        reply = await self.call("check_dns_host", request)
        return cast(bool, reply)

    @overload
    async def add_auth_info(
        self,
        nsset_id: NssetId,
        *,
        snapshot_id: Optional[SnapshotId] = None,
        ttl: int = 0,
        log_entry_id: Optional[str] = None,
        by_registrar: Optional[str] = None,
    ) -> str: ...

    @overload
    async def add_auth_info(
        self,
        nsset_id: NssetId,
        *,
        snapshot_id: Optional[SnapshotId] = None,
        auth_info: str,
        ttl: int = 0,
        log_entry_id: Optional[str] = None,
        by_registrar: Optional[str] = None,
    ) -> None: ...

    async def add_auth_info(
        self,
        nsset_id: NssetId,
        *,
        snapshot_id: Optional[SnapshotId] = None,
        auth_info: Optional[str] = None,
        ttl: int = 0,
        log_entry_id: Optional[str] = None,
        by_registrar: Optional[str] = None,
    ) -> Optional[str]:
        """Add new auth info to the object.

        Args:
            nsset_id: Nsset ID.
            snapshot_id: The snapshot identification.
            auth_info: Auth info to set. Will be generated if not provided.
            ttl: Lifetime of the auth info in seconds. If 0 (default), server default is used.
            log_entry_id: Identification of logger entry for the action.
            by_registrar: Handle of the registrar upon whose regard the action is performed.

        Raises:
            NssetDoesNotExist: If nsset does not exist.
            InvalidData: Invalid input parameters.
            InvalidSnapshot: If snapshot_id is provided, but not longer valid.
            RegistrarDoesNotExist: Provided registrar does not exist.
        """
        request = AddNssetAuthInfoRequest()
        request.nsset_id.uuid.value = nsset_id
        if snapshot_id is not None:
            request.nsset_snapshot_id.value = snapshot_id
        if auth_info is not None:
            request.auth_info = auth_info
        else:
            request.generate.CopyFrom(Empty())
        request.ttl = ttl
        if log_entry_id is not None:
            request.log_entry_id.value = log_entry_id
        if by_registrar is not None:
            request.by_registrar.value = by_registrar
        result = cast(Optional[str], await self.call("add_nsset_auth_info", request))
        return result

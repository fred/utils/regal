"""Registry object common."""

from abc import ABC, abstractmethod
from collections.abc import AsyncIterator
from datetime import datetime
from typing import TYPE_CHECKING, Any, Generic, Literal, Optional, TypeVar, Union, cast

from asyncstdlib import cache
from fred_api.registry.contact.contact_info_types_pb2 import ContactIdRequest
from fred_api.registry.domain.domain_info_types_pb2 import DomainIdRequest
from fred_api.registry.keyset.keyset_info_types_pb2 import KeysetIdRequest
from fred_api.registry.nsset.nsset_history_types_pb2 import NssetHistoryRequest
from fred_api.registry.nsset.nsset_info_types_pb2 import NssetIdRequest, NssetInfoRequest
from fred_api.registry.nsset.nsset_state_history_types_pb2 import NssetStateHistoryRequest
from fred_api.registry.nsset.nsset_state_types_pb2 import NssetStateRequest
from fred_types import BaseModel, BaseObjectHistoryId, BaseObjectId, SnapshotId
from frgal.aio import AsyncGrpcClient
from google.protobuf.empty_pb2 import Empty
from pydantic import ConfigDict

from regal.utils import AbcAttribute

if TYPE_CHECKING:
    from .common import ObjectHistory, ObjectHistoryItem

ObjectIdT = TypeVar("ObjectIdT", bound=BaseObjectId)
ObjectHistoryIdT = TypeVar("ObjectHistoryIdT", bound=BaseObjectHistoryId)


class ObjectStateFlag(BaseModel):
    """Represents properties of an object state flag.

    Attributes:
        manual: Whether the state flag is set manually.
        internal: Whether the state flag is internal.
    """

    manual: bool
    internal: bool


class StateUpdate(BaseModel, Generic[ObjectIdT]):
    """Represents an update of object state."""

    model_config = ConfigDict(frozen=True)

    id: ObjectIdT
    set_flags: tuple[str, ...] = ()
    unset_flags: tuple[str, ...] = ()


class RegistryObjectClient(ABC, AsyncGrpcClient, Generic[ObjectIdT, ObjectHistoryIdT]):
    """Abstract service gRPC client for registry objects."""

    service: str = AbcAttribute
    object_id: str = AbcAttribute
    object_history_id: str = AbcAttribute
    object_snapshot_id: str = AbcAttribute
    object_handle: str = AbcAttribute

    async def get_object_id(
        self,
        request: Union[ContactIdRequest, DomainIdRequest, KeysetIdRequest, NssetIdRequest],
        method: str,
        handle: str,
    ) -> str:
        """Get registry object id."""
        getattr(request, self.object_handle).value = handle
        result = await self.call(method, request)
        return cast(str, result[self.object_id])

    async def get_object_info(
        self,
        request: Union[NssetInfoRequest],
        method: str,
        object_id: ObjectIdT,
        history_id: Optional[ObjectHistoryIdT] = None,
        snapshot_id: Optional[SnapshotId] = None,
    ) -> dict[str, Any]:
        """Get registry object info."""
        getattr(request, self.object_id).uuid.value = object_id
        if history_id is not None:
            getattr(request, self.object_history_id).uuid.value = history_id
        if snapshot_id is not None:
            getattr(request, self.object_snapshot_id).value = snapshot_id
        result = await self.call(method, request)
        return cast(dict[str, Any], result)

    async def _process_batch_info_stream(
        self, stream: AsyncIterator[list[dict[str, Any]]], errors: Literal["ignore", "raise"] = "ignore"
    ) -> AsyncIterator[Any]:
        if errors not in ("ignore", "raise"):  # pragma: no cover
            raise ValueError("Unknown errors value: {!r}".format(errors))

        # XXX: Bug in python 3.9 coverage
        async for chunk in stream:  # pragma: no cover
            for reply in chunk or ():
                if "error" in reply:
                    if errors == "ignore":
                        continue
                    else:
                        assert errors == "raise"  # noqa: S101
                        raise reply["error"]["exception"]
                else:
                    assert "data" in reply  # noqa: S101
                    yield reply["data"]

    async def get_object_state(
        self,
        request: Union[NssetStateRequest],
        method: str,
        object_id: ObjectIdT,
        *,
        internal: bool = True,
        snapshot_id: Optional[SnapshotId] = None,
    ) -> dict[str, bool]:
        """Get object history state.

        Arguments:
            request: Basic gRPC request message.
            method: Name of the method to be called.
            object_id: Object identifier.
            internal: Whether to include internal states in the result.
            snapshot_id: Snapshot identification
        """
        getattr(request, self.object_id).uuid.value = object_id
        if snapshot_id is not None:
            getattr(request, self.object_snapshot_id).value = snapshot_id
        result = await self.call(method, request)
        state = cast(dict[str, bool], result["state"])
        if not internal:
            flags = await self.get_state_flags()
            state = {k: v for k, v in state.items() if not flags[k].internal}
        return state

    async def get_history_data(
        self,
        request: Union[NssetHistoryRequest],
        method: str,
        object_id: ObjectIdT,
        start: Optional[Union[ObjectHistoryIdT, datetime]] = None,
        end: Optional[Union[ObjectHistoryIdT, datetime]] = None,
    ) -> dict[str, Any]:
        """Get registry object history data."""
        getattr(request, self.object_id).uuid.value = object_id
        self._set_history_limits(request, start, end)

        result = await self.call(method, request)
        return cast(dict[str, Any], result)

    async def get_object_history(
        self,
        request: Union[NssetHistoryRequest],
        method: str,
        object_id: ObjectIdT,
        start: Optional[Union[ObjectHistoryIdT, datetime]] = None,
        end: Optional[Union[ObjectHistoryIdT, datetime]] = None,
    ) -> "ObjectHistory[ObjectHistoryItem]":
        """Get registry object history."""
        from .common import ObjectHistory, ObjectHistoryItem

        result = await self.get_history_data(request, method, object_id, start, end)
        data = {
            "timeline": [
                {
                    "history_id": i[self.object_history_id],
                    "valid_from": i["valid_from"],
                    "log_entry_id": i["log_entry_id"],
                }
                for i in result["timeline"]
            ],
            "valid_to": result["valid_to"],
        }
        return ObjectHistory[ObjectHistoryItem](**data)

    async def get_object_state_history(
        self,
        request: Union[NssetStateHistoryRequest],
        method: str,
        object_id: ObjectIdT,
        start: Optional[Union[ObjectHistoryIdT, datetime]] = None,
        end: Optional[Union[ObjectHistoryIdT, datetime]] = None,
    ) -> dict[str, Any]:
        """Get registry object state history."""
        getattr(request, self.object_id).uuid.value = object_id
        self._set_history_limits(request, start, end)

        result = await self.call(method, request)
        return cast(dict[str, Any], {self.object_id: result[self.object_id], **result["history"]})

    def _set_history_limits(
        self,
        request: Union[NssetHistoryRequest, NssetStateHistoryRequest],
        start: Optional[Union[ObjectHistoryIdT, datetime]] = None,
        end: Optional[Union[ObjectHistoryIdT, datetime]] = None,
    ) -> None:
        """Set history limits to request.

        Args:
            request: Request with `history` item of ContactHistoryInterval type.
            start: Datetime or history_id of history interval start.
            end: Datetime or history_id of history interval end.
        """
        if start is not None:
            if isinstance(start, datetime):
                request.history.lower_limit.timestamp.FromDatetime(start)
            else:
                getattr(request.history.lower_limit, self.object_history_id).uuid.value = start
        if end is not None:
            if isinstance(end, datetime):
                request.history.upper_limit.timestamp.FromDatetime(end)
            else:
                getattr(request.history.upper_limit, self.object_history_id).uuid.value = end

    @cache
    async def get_object_state_flags(
        self,
        method: str,
    ) -> dict[str, ObjectStateFlag]:
        """Get object state flags and their properties."""
        return cast(dict[str, ObjectStateFlag], await self.call(method, Empty()))

    @abstractmethod
    async def get_state_flags(self) -> dict[str, ObjectStateFlag]:
        """Get object state flags and their properties."""

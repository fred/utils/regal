"""Domain client for registry Domain service."""

import warnings
from collections.abc import AsyncIterable, AsyncIterator, Container, Mapping, Sequence
from datetime import datetime
from enum import IntEnum, unique
from typing import (
    Any,
    Literal,
    NamedTuple,
    Optional,
    TypedDict,
    Union,
    cast,
    overload,
)

import grpc
from asyncstdlib import batched as abatched, enumerate as aenumerate, tuple as atuple
from fred_api.registry.domain.add_domain_auth_info_types_pb2 import AddDomainAuthInfoRequest
from fred_api.registry.domain.domain_common_types_pb2 import DomainContactRole as ProtoDomainContactRole
from fred_api.registry.domain.domain_history_types_pb2 import DomainHistoryReply, DomainHistoryRequest
from fred_api.registry.domain.domain_info_types_pb2 import (
    BatchDomainInfoRequest,
    DomainIdRequest,
    DomainInfoReply,
    DomainInfoRequest,
)
from fred_api.registry.domain.domain_life_cycle_stage_types_pb2 import DomainLifeCycleStageRequest
from fred_api.registry.domain.domain_state_history_types_pb2 import DomainStateHistoryReply, DomainStateHistoryRequest
from fred_api.registry.domain.domain_state_types_pb2 import DomainStateRequest
from fred_api.registry.domain.domains_by_contact_types_pb2 import DomainsByContactReply, DomainsByContactRequest
from fred_api.registry.domain.list_domains_by_contact_types_pb2 import (
    ListDomainsByContactReply,
    ListDomainsByContactRequest,
)
from fred_api.registry.domain.list_domains_by_keyset_types_pb2 import ListDomainsByKeysetRequest
from fred_api.registry.domain.list_domains_by_nsset_types_pb2 import ListDomainsByNssetRequest
from fred_api.registry.domain.list_domains_types_pb2 import (
    ListDomainsRequest,
    ZoneDoesNotExist as ZoneDoesNotExistMessage,
)
from fred_api.registry.domain.service_admin_grpc_pb2 import (
    BatchDeleteDomainsReply as BatchDeleteDomainsReplyMessage,
    BatchDeleteDomainsRequest,
    DomainContactInfo,
    GetDomainsDeleteNotifyInfoRequest,
    GetDomainsOutzoneNotifyInfoRequest,
    ManageDomainStateFlagsReply as ManageDomainStateFlagsReplyMessage,
    ManageDomainStateFlagsRequest,
    UpdateDomainsAdditionalNotifyInfoReply,
    UpdateDomainsDeleteAdditionalNotifyInfoRequest,
    UpdateDomainsOutzoneAdditionalNotifyInfoRequest,
)
from fred_api.registry.domain.service_admin_grpc_pb2_grpc import AdminStub
from fred_api.registry.domain.service_domain_grpc_pb2_grpc import DomainStub
from fred_api.registry.domain.update_domain_state_types_pb2 import UpdateDomainStateRequest
from fred_types import (
    BaseModel,
    ContactId,
    ContactRef,
    DomainHistoryId,
    DomainId,
    DomainRef,
    KeysetId,
    KeysetRef,
    NssetId,
    NssetRef,
    SnapshotId,
)
from frgal.aio import AsyncGrpcClient
from google.protobuf.empty_pb2 import Empty
from google.protobuf.message import Message
from pydantic import ConfigDict
from typing_extensions import NotRequired

from .common import (
    DatetimeRange,
    ListPage,
    ObjectEvents,
    ObjectHistory,
    ObjectHistoryItem,
    ObjectStateHistoryItem,
    PaginationReply,
    PaginationResult,
    RegistryDecoder,
    _encode_order_by,
    set_request_pagination,
)
from .domain_blacklist import BlockId
from .exceptions import (
    DomainDoesNotExist,
    InvalidHistoryInterval,
    ZoneDoesNotExist,
)
from .object import ObjectStateFlag, RegistryObjectClient, StateUpdate
from .utils import AnyIterable, _idna_encode

try:
    from enum import StrEnum  # type: ignore[attr-defined, unused-ignore]
except ImportError:  # pragma: no cover
    from backports.strenum import StrEnum  # type: ignore[assignment, no-redef, unused-ignore]


UpdateDomainsNotifyInfoRequest = Union[
    UpdateDomainsDeleteAdditionalNotifyInfoRequest,
    UpdateDomainsOutzoneAdditionalNotifyInfoRequest,
]


class Domain(BaseModel):
    """Domain info.

    Attributes:
        expiration_warning_scheduled_at: An expected date of an expiration warning for the domain.
        outzone_unguarded_warning_scheduled_at: An expected date of an outzone warning for the domain.
        outzone_scheduled_at: An expected date of the domain removal from the zone.
        delete_warning_scheduled_at: An expected date of a deletion warning for the domain.
        delete_candidate_scheduled_at: An expected date of the domain deletion.
        validation_expires_at: Timestamp of validation expiration for ENUM domains. Expected date if in future.
        outzone_at: Actual date of the domain removal from the zone. None if domain is in the zone.
        delete_candidate_at: Actual date of the domain deletion. None if domain is not deleted.
    """

    domain_id: DomainId
    fqdn: str
    domain_history_id: Optional[DomainHistoryId] = None
    snapshot_id: Optional[SnapshotId] = None
    registrant: Optional[ContactRef] = None
    sponsoring_registrar: Optional[str] = None
    nsset: Optional[NssetRef] = None
    keyset: Optional[KeysetRef] = None
    administrative_contacts: Sequence[ContactRef] = []
    auth_info: Optional[str] = None
    events: Optional[ObjectEvents] = None
    # Domain life cycle attributes.
    expires_at: Optional[datetime] = None
    outzone_at: Optional[datetime] = None
    delete_candidate_at: Optional[datetime] = None
    expiration_warning_scheduled_at: Optional[datetime] = None
    outzone_unguarded_warning_scheduled_at: Optional[datetime] = None
    outzone_scheduled_at: Optional[datetime] = None
    delete_warning_scheduled_at: Optional[datetime] = None
    delete_candidate_scheduled_at: Optional[datetime] = None
    validation_expires_at: Optional[datetime] = None


# TODO: Return unique when aliases are removed.
# @unique
class DomainContactRole(StrEnum):
    """Role of contact linked to domain."""

    # Annotations forced because of backports.strenum. Remove when not needed.
    HOLDER: "DomainContactRole" = "holder"  # type: ignore[assignment, misc]
    ADMIN: "DomainContactRole" = "admin"  # type: ignore[assignment, misc]
    KEYSET_TECH: "DomainContactRole" = "keyset_tech_contact"  # type: ignore[assignment, misc]
    NSSET_TECH: "DomainContactRole" = "nsset_tech_contact"  # type: ignore[assignment, misc]

    # Aliases to support naming from API 6.0.
    REGISTRANT: "DomainContactRole" = HOLDER  # type: ignore[assignment, misc]
    ADMINISTRATIVE: "DomainContactRole" = ADMIN  # type: ignore[assignment, misc]
    KEYSET_TECHNICAL: "DomainContactRole" = KEYSET_TECH  # type: ignore[assignment, misc]
    NSSET_TECHNICAL: "DomainContactRole" = NSSET_TECH  # type: ignore[assignment, misc]


@unique
class NotDeletedReason(IntEnum):
    """Enum with reasons for domain not to be deleted."""

    UNDEFINED = 0
    DOMAIN_BLOCKED = 1
    DOMAIN_DOES_NOT_EXIST = 2
    DOMAIN_HISTORY_ID_MISMATCH = 3
    INSUFFICIENT_PERMISSIONS = 4


@unique
class ContactInfoType(StrEnum):
    """Contact information type."""

    # Annotations forced because of backports.strenum. Remove when not needed.
    EMAIL: "ContactInfoType" = "email"  # type: ignore[assignment, misc]
    TELEPHONE: "ContactInfoType" = "telephone"  # type: ignore[assignment, misc]


class ContactInfo(BaseModel):
    """Contact information."""

    type: ContactInfoType
    value: str


class _DomainsByContactDomain(TypedDict):
    """Type representing a single item of get_domains_by_contact."""

    domain: DomainRef
    domain_history_id: DomainHistoryId
    roles: set[DomainContactRole]
    is_deleted: bool


class DomainsByContactResult(TypedDict):
    """Type representing a result of get_domains_by_contact."""

    domains: list[_DomainsByContactDomain]
    pagination: NotRequired[PaginationResult]


class DomainByRelation(BaseModel):
    """Model representing a domain returned as a result of a relation."""

    model_config = ConfigDict(frozen=True)

    domain: DomainRef
    domain_history_id: Optional[DomainHistoryId] = None
    is_deleted: bool = False
    roles: frozenset[DomainContactRole] = frozenset()


@unique
class DomainLifeCycleStage(StrEnum):
    """Enum of domain life cycle stages."""

    # Annotations forced because of backports.strenum. Remove when not needed.
    REGISTERED: "DomainLifeCycleStage" = "registered"  # type: ignore[assignment, misc]
    DELETE_CANDIDATE: "DomainLifeCycleStage" = "delete_candidate"  # type: ignore[assignment, misc]
    NOT_REGISTERED: "DomainLifeCycleStage" = "not_registered"  # type: ignore[assignment, misc]
    IN_AUCTION: "DomainLifeCycleStage" = "in_auction"  # type: ignore[assignment, misc]
    BLACKLISTED: "DomainLifeCycleStage" = "blacklisted"  # type: ignore[assignment, misc]


class DomainLifeCycleStageResult(NamedTuple):
    """Represents result of get_life_cycle_stage."""

    stage: DomainLifeCycleStage
    id: Optional[DomainId] = None
    block_ids: tuple[BlockId, ...] = ()


class BatchDeleteDomainsResult(TypedDict):
    """Type representing a result of batch_delete_domains.

    Deprecated, will be replaced by BatchDeleteDomainsReply in a future version.
    """

    domain_id: DomainId
    reason: NotDeletedReason


class BatchDeleteDomainsReply(BaseModel):
    """Type representing a result of batch_delete_domains.

    Will be renamed to BatchDeleteDomainsResult in a future version.
    """

    domain_id: DomainId
    reason: NotDeletedReason


class ManageDomainStateFlagsResult(TypedDict):
    """Type representing a result of manage_domain_state_flags.

    Deprecated, will be replaced by ManageDomainStateFlagsReply in a future version.
    """

    domain_id: DomainId
    request_id: str
    state_flag: str


class ManageDomainStateFlagsReply(BaseModel):
    """Type representing a result of manage_domain_state_flags.

    Will be renamed to ManageDomainStateFlagsResult in a future version.
    """

    domain_id: DomainId
    request_id: str
    state_flag: str


class _GetDomainsNotifyInfoDomain(TypedDict):
    """Type representing a single item of get_domains_notify_info."""

    domain_id: DomainId
    fqdn: str
    registrant_id: ContactId
    registrant_handle: str
    registrant_name: str
    registrant_organization: str
    contacts: list[ContactInfo]
    additional_contacts: list[ContactInfo]


class GetDomainsNotifyInfoResult(TypedDict):
    """Type representing a result of get_domains_notify_info."""

    domains: list[_GetDomainsNotifyInfoDomain]
    pagination: NotRequired[PaginationResult]


class NotifyInfoDomain(BaseModel):
    """Type representing a single item of get_notify_info."""

    domain_id: DomainId
    fqdn: str
    registrant_id: ContactId
    registrant_handle: str
    registrant_name: str
    registrant_organization: str
    contacts: list[ContactInfo]
    additional_contacts: list[ContactInfo]


class GetDomainsNotifyInfoReply(BaseModel):
    """Type representing a result of get_notify_info."""

    domains: list[NotifyInfoDomain]
    pagination: Optional[PaginationReply] = None


class DomainDecoder(RegistryDecoder):
    """Domain service decoder."""

    def __init__(self) -> None:
        super().__init__()
        self.set_decoder(DomainsByContactReply.Data.Domain, self._decode_domain_by_contact)
        self.set_decoder(ListDomainsByContactReply.Data.Domain, self._decode_list_by_contact_domain)
        self.set_decoder(DomainInfoReply.Data, self._decode_domain_info)
        # Decode exceptions in batch info response
        self.set_decoder(DomainInfoReply.Exception, self.decode_exception)

        self.set_exception_decoder(DomainHistoryReply.Exception.InvalidHistoryInterval, InvalidHistoryInterval)
        self.set_exception_decoder(DomainStateHistoryReply.Exception.InvalidHistoryInterval, InvalidHistoryInterval)
        self.set_exception_decoder(DomainsByContactReply.Exception.InvalidOrderBy, ValueError)
        self.set_exception_decoder(ZoneDoesNotExistMessage, ZoneDoesNotExist)

    def _decode_domain_info(self, value: DomainInfoReply.Data) -> Domain:
        """Decode DomainInfoReply.Data."""
        data = self._decode_message(value)
        data.pop("registrant_old", None)
        data.pop("nsset_old", None)
        data.pop("keyset_old", None)
        data.pop("administrative_contacts_old", None)
        return Domain(**data)

    def _decode_domain_by_contact(self, value: DomainsByContactReply.Data.Domain) -> _DomainsByContactDomain:
        """Transform booleans to DomainContactRole set."""
        roles = set()
        if value.is_holder:
            roles.add(DomainContactRole.HOLDER)
        if value.is_admin_contact:
            roles.add(DomainContactRole.ADMIN)
        if value.is_keyset_tech_contact:
            roles.add(DomainContactRole.KEYSET_TECH)
        if value.is_nsset_tech_contact:
            roles.add(DomainContactRole.NSSET_TECH)
        return {
            "domain": self.decode(value.domain),
            "domain_history_id": DomainHistoryId(self.decode(value.domain_history_id)),
            "roles": roles,
            "is_deleted": value.is_deleted,
        }

    def _decode_list_by_contact_domain(self, value: ListDomainsByContactReply.Data.Domain) -> dict[str, Any]:
        """Decode roles in ListDomainsByContactReply.Data.Domain."""
        # TODO: Replace by enum decoder when available.
        roles = set()
        for role in value.roles:
            if role == ProtoDomainContactRole.role_registrant:
                roles.add(DomainContactRole.HOLDER)
            elif role == ProtoDomainContactRole.role_administrative:
                roles.add(DomainContactRole.ADMIN)
            if role == ProtoDomainContactRole.role_keyset_technical:
                roles.add(DomainContactRole.KEYSET_TECH)
            if role == ProtoDomainContactRole.role_nsset_technical:
                roles.add(DomainContactRole.NSSET_TECH)
        result = cast(dict[str, Any], self._decode_message(value))
        result["roles"] = roles
        return result


class DomainClient(RegistryObjectClient[DomainId, DomainHistoryId]):
    """Domain service gRPC client."""

    stub_cls = DomainStub
    decoder_cls = DomainDecoder
    service = "Domain"
    object_id = "domain_id"
    object_history_id = "domain_history_id"
    object_snapshot_id = "domain_snapshot_id"
    object_handle = "fqdn"

    def __init__(self, netloc: str, credentials: Optional[grpc.ChannelCredentials] = None, **kwargs: Any) -> None:
        """Initialize domain client instance.

        Args:
            netloc: Netloc of Domain service server.
            credentials: Optional credentials.
            kwargs: Other client arguments.
        """
        super().__init__(netloc, credentials=credentials, **kwargs)

    async def get_id(self, fqdn: str) -> DomainId:
        """Get domain id.

        Args:
            fqdn: Domain FQDN.

        Raises:
            DomainDoesNotExist: If domain does not exist.
        """
        handle = _idna_encode(fqdn)
        return DomainId(await self.get_object_id(request=DomainIdRequest(), method="get_domain_id", handle=handle))

    async def get_domain_id(self, fqdn: str) -> DomainId:
        """Get domain id.

        Args:
            fqdn: Domain FQDN.

        Raises:
            DomainDoesNotExist: If domain does not exist.
        """
        warnings.warn("Method get_domain_id is deprecated in favor of get_id.", DeprecationWarning, stacklevel=2)
        return await self.get_id(fqdn)

    async def get(
        self,
        domain_id: DomainId,
        history_id: Optional[DomainHistoryId] = None,
        *,
        snapshot_id: Optional[SnapshotId] = None,
    ) -> Domain:
        """Get domain info.

        Args:
            domain_id: Domain ID.
            history_id: Domain history ID.
            snapshot_id: Snapshot identification.

        Raises:
            DomainDoesNotExist: If domain does not exist.
            InvalidSnapshot: If snapshot_id is provided, but not longer valid.
        """
        return cast(
            Domain,
            await self.get_object_info(
                request=DomainInfoRequest(),
                method="get_domain_info",
                object_id=domain_id,
                history_id=history_id,
                snapshot_id=snapshot_id,
            ),
        )

    async def _make_batch_get_requests(
        self, domain_ids: AnyIterable[DomainId], *, snapshot_id: Optional[SnapshotId] = None, batch_size: int = 100
    ) -> AsyncIterator[BatchDomainInfoRequest]:
        async for batch in abatched(domain_ids, n=batch_size):
            request = BatchDomainInfoRequest()
            for id in batch:
                r = request.requests.add()
                r.domain_id.uuid.value = id
                if snapshot_id is not None:
                    r.domain_snapshot_id.value = snapshot_id
            yield request

    async def batch_get(
        self,
        domain_ids: AnyIterable[DomainId],
        *,
        snapshot_id: Optional[SnapshotId] = None,
        errors: Literal["ignore", "raise"] = "ignore",
        batch_size: int = 100,
    ) -> AsyncIterator[Domain]:
        """Batch get domain info.

        Args:
            domain_ids: Iterable of domain identifiers.
            snapshot_id: Snapshot identification.
            errors: How to handle errors. If 'ignore' (default) domains with errors are not returned.
                    If 'raise' the first error is raised as an exception.
            batch_size: Number of items in a single stream request.

        Raises:
            DomainDoesNotExist: If domain does not exist.
            InvalidSnapshot: If provided snapshot_id is no longer valid.
        """
        if errors not in ("ignore", "raise"):
            raise ValueError("Unknown errors value: {!r}".format(errors))

        requests = self._make_batch_get_requests(domain_ids, snapshot_id=snapshot_id, batch_size=batch_size)
        # XXX: Bug in python 3.9 coverage
        async for obj in self._process_batch_info_stream(
            self.call_stream("batch_domain_info", requests), errors
        ):  # pragma: no cover
            yield obj

    async def get_domain_info(self, domain_id: DomainId, history_id: Optional[DomainHistoryId] = None) -> Domain:
        """Get domain info.

        Args:
            domain_id: Domain ID.
            history_id: Domain history ID.

        Raises:
            DomainDoesNotExist: If domain does not exist.
        """
        warnings.warn("Method get_domain_info is deprecated in favor of get.", DeprecationWarning, stacklevel=2)
        return await self.get(domain_id, history_id)

    async def get_state(
        self, domain_id: DomainId, *, internal: bool = False, snapshot_id: Optional[SnapshotId] = None
    ) -> frozenset[str]:
        """Get domain state consisting of active state flags.

        Args:
            domain_id: Domain ID.
            internal: Whether to include internal states in the result.
            snapshot_id: Snapshot identification.

        Returns:
            Set of active state flags.

        Raises:
            DomainDoesNotExist: If domain does not exist.
            InvalidSnapshot: If snapshot_id is provided, but not longer valid.
        """
        state = await self.get_object_state(
            request=DomainStateRequest(),
            method="get_domain_state",
            object_id=domain_id,
            internal=internal,
            snapshot_id=snapshot_id,
        )
        return frozenset(name for (name, enabled) in state.items() if enabled is True)

    async def get_domain_state(self, domain_id: DomainId, *, internal: Optional[bool] = None) -> dict[str, bool]:
        """Get domain state consisting of state flags and their boolean values.

        Args:
            domain_id: Domain ID.
            internal: Whether to include internal states in the result.

        Raises:
            DomainDoesNotExist: If domain does not exist.
        """
        warnings.warn("Method get_domain_state is deprecated in favor of get_state.", DeprecationWarning, stacklevel=2)
        if internal is None:  # pragma: no cover
            warnings.warn(
                "Default value of internal flag will change to False in regal 2.0.", DeprecationWarning, stacklevel=2
            )
            internal = True

        return await self.get_object_state(
            request=DomainStateRequest(), method="get_domain_state", object_id=domain_id, internal=internal
        )

    async def get_history(
        self,
        domain_id: DomainId,
        start: Optional[Union[DomainHistoryId, datetime]] = None,
        end: Optional[Union[DomainHistoryId, datetime]] = None,
    ) -> ObjectHistory[ObjectHistoryItem]:
        """Get domain history timeline.

        Args:
            domain_id: Domain ID.
            start: Datetime or history_id of history interval start.
            end: Datetime or history_id of history interval end.

        Raises:
            DomainDoesNotExist: If domain does not exist.
            InvalidHistoryInterval: In case of invalid history interval.
        """
        return await self.get_object_history(
            request=DomainHistoryRequest(),
            method="get_domain_history",
            object_id=domain_id,
            start=start,
            end=end,
        )

    async def get_domain_history(
        self,
        domain_id: DomainId,
        start: Optional[Union[DomainHistoryId, datetime]] = None,
        end: Optional[Union[DomainHistoryId, datetime]] = None,
    ) -> dict[str, Any]:
        """Get domain history timeline.

        Args:
            domain_id: Domain ID.
            start: Datetime or history_id of history interval start.
            end: Datetime or history_id of history interval end.

        Raises:
            DomainDoesNotExist: If domain does not exist.
            InvalidHistoryInterval: In case of invalid history interval.
        """
        warnings.warn(
            "Method get_domain_history is deprecated in favor of get_history.", DeprecationWarning, stacklevel=2
        )
        return await self.get_history_data(
            request=DomainHistoryRequest(),
            method="get_domain_history",
            object_id=domain_id,
            start=start,
            end=end,
        )

    async def get_state_history(
        self,
        domain_id: DomainId,
        start: Optional[Union[DomainHistoryId, datetime]] = None,
        end: Optional[Union[DomainHistoryId, datetime]] = None,
    ) -> ObjectHistory[ObjectStateHistoryItem]:
        """Get domain state history timeline.

        Args:
            domain_id: Domain ID.
            start: Datetime or history_id of history interval start.
            end: Datetime or history_id of history interval end.

        Raises:
            DomainDoesNotExist: If domain does not exist.
            InvalidHistoryInterval: In case of invalid history interval.
        """
        result = await self.get_object_state_history(
            request=DomainStateHistoryRequest(),
            method="get_domain_state_history",
            object_id=domain_id,
            start=start,
            end=end,
        )
        result.pop(self.object_id, None)
        return ObjectHistory[ObjectStateHistoryItem](**result)

    async def get_domain_state_history(
        self,
        domain_id: DomainId,
        start: Optional[Union[DomainHistoryId, datetime]] = None,
        end: Optional[Union[DomainHistoryId, datetime]] = None,
    ) -> dict[str, Any]:
        """Get domain state history timeline.

        Args:
            domain_id: Domain ID.
            start: Datetime or history_id of history interval start.
            end: Datetime or history_id of history interval end.

        Raises:
            DomainDoesNotExist: If domain does not exist.
            InvalidHistoryInterval: In case of invalid history interval.
        """
        warnings.warn(
            "Method get_domain_state_history is deprecated in favor of get_state_history.",
            DeprecationWarning,
            stacklevel=2,
        )
        return await self.get_object_state_history(
            request=DomainStateHistoryRequest(),
            method="get_domain_state_history",
            object_id=domain_id,
            start=start,
            end=end,
        )

    async def get_state_flags(self) -> dict[str, ObjectStateFlag]:
        """Get object state flags and their properties."""
        return await self.get_object_state_flags(method="get_domain_state_flags")  # type: ignore[call-overload]

    async def list(self, zone: str) -> AsyncIterable[DomainRef]:
        """Iterate through all registered domains.

        Returns:
            Async iterable over domain refs.
        """
        request = ListDomainsRequest(zone=zone)
        # XXX: Bug in python 3.9 coverage
        async for refs in self.call_stream("list_domains", request):  # pragma: no cover
            for ref in refs:
                yield ref

    async def list_by_contact(
        self,
        contact_id: ContactId,
        *,
        snapshot_id: Optional[SnapshotId] = None,
        aggregate_entire_history: bool = False,
        fqdn_filter: str = "",
        roles: Container[DomainContactRole] = frozenset(),
        order_by: AnyIterable[str] = (),
    ) -> AsyncIterator[DomainByRelation]:
        """Return domains associated with contact.

        Args:
            contact_id: Contact ID.
            snapshot_id: Snapshot identification.
            aggregate_entire_history: Include domains connected to the contact in the past.
            fqdn_filter: Filter for domain names.
                `*` means any sequence of characters.
                Pattern is matched against the whole domain name.
            roles: Set of roles in which contact is linked to domain.
            order_by: Comma separated list of fields to order by.
                Each field can be prefixed with `+` (asc) or `-` (desc). `+` is optional.

        Returns:
            Iterator of domains.

        Raises:
            ContactDoesNotExist: If contact does not exist.
            InvalidData: Invalid input parameters.
            InvalidSnapshot: If snapshot_id is provided, but not longer valid.
        """
        request = ListDomainsByContactRequest()
        request.contact_id.uuid.value = contact_id
        if snapshot_id is not None:
            request.contact_snapshot_id.value = snapshot_id
        request.aggregate_entire_history = aggregate_entire_history
        request.fqdn_filter = fqdn_filter

        if DomainContactRole.HOLDER in roles:
            request.role_filters.append(ProtoDomainContactRole.role_registrant)
        if DomainContactRole.ADMIN in roles:
            request.role_filters.append(ProtoDomainContactRole.role_administrative)
        if DomainContactRole.KEYSET_TECH in roles:
            request.role_filters.append(ProtoDomainContactRole.role_keyset_technical)
        if DomainContactRole.NSSET_TECH in roles:
            request.role_filters.append(ProtoDomainContactRole.role_nsset_technical)

        request.order_by.extend(await atuple(_encode_order_by(order_by, ListDomainsByContactRequest.OrderBy)))

        # XXX: Bug in python 3.9 coverage
        async for chunk in self.call_stream("list_domains_by_contact", request):  # pragma: no cover
            for data in chunk["domains"]:
                yield DomainByRelation(**data)

    async def list_by_contact_page(
        self,
        contact_id: ContactId,
        *,
        snapshot_id: Optional[SnapshotId] = None,
        aggregate_entire_history: bool = False,
        fqdn_filter: str = "",
        roles: Container[DomainContactRole] = frozenset(),
        order_by: AnyIterable[str] = (),
        page_token: Optional[str] = None,
        page_size: Optional[int] = None,
    ) -> ListPage[DomainByRelation]:
        """Return domains associated with contact.

        Args:
            contact_id: Contact ID.
            snapshot_id: Snapshot identification.
            aggregate_entire_history: Include domains connected to the contact in the past.
            fqdn_filter: Filter for domain names.
                `*` means any sequence of characters.
                Pattern is matched against the whole domain name.
            roles: Set of roles in which contact is linked to domain.
            order_by: Comma separated list of fields to order by.
                Each field can be prefixed with `+` (asc) or `-` (desc). `+` is optional.
            page_token: Page token for pagination.
            page_size: Page size for pagination.

        Returns:
            Domains and pagination info.

        Raises:
            ContactDoesNotExist: If contact does not exist.
            InvalidData: Invalid input parameters.
            InvalidSnapshot: If snapshot_id is provided, but not longer valid.
        """
        request = ListDomainsByContactRequest()
        request.contact_id.uuid.value = contact_id
        if snapshot_id is not None:
            request.contact_snapshot_id.value = snapshot_id
        request.aggregate_entire_history = aggregate_entire_history
        request.fqdn_filter = fqdn_filter

        if DomainContactRole.HOLDER in roles:
            request.role_filters.append(ProtoDomainContactRole.role_registrant)
        if DomainContactRole.ADMIN in roles:
            request.role_filters.append(ProtoDomainContactRole.role_administrative)
        if DomainContactRole.KEYSET_TECH in roles:
            request.role_filters.append(ProtoDomainContactRole.role_keyset_technical)
        if DomainContactRole.NSSET_TECH in roles:
            request.role_filters.append(ProtoDomainContactRole.role_nsset_technical)

        request.order_by.extend(await atuple(_encode_order_by(order_by, ListDomainsByContactRequest.OrderBy)))
        set_request_pagination(request, page_token=page_token, page_size=page_size)

        results = []
        pagination = None
        # XXX: Bug in python 3.9 coverage
        async for chunk in self.call_stream("list_domains_by_contact", request):  # pragma: no cover
            if pagination is None and chunk.get("pagination"):
                pagination = PaginationReply(**chunk.get("pagination"))
            for data in chunk["domains"]:
                results.append(DomainByRelation(**data))

        return ListPage(results=tuple(results), pagination=pagination)

    async def list_by_keyset(
        self,
        keyset_id: KeysetId,
        *,
        snapshot_id: Optional[SnapshotId] = None,
        aggregate_entire_history: bool = False,
        order_by: AnyIterable[str] = (),
    ) -> AsyncIterator[DomainByRelation]:
        """Return domains associated with keyset.

        Args:
            keyset_id: Keyset ID.
            snapshot_id: Snapshot identification.
            aggregate_entire_history: Include domains connected to the keyset in the past.
            order_by: Comma separated list of fields to order by.
                Each field can be prefixed with `+` (asc) or `-` (desc). `+` is optional.

        Returns:
            Iterator of domains.

        Raises:
            KeysetDoesNotExist: If keyset does not exist.
            InvalidData: Invalid input parameters.
            InvalidSnapshot: If snapshot_id is provided, but not longer valid.
        """
        request = ListDomainsByKeysetRequest()
        request.keyset_id.uuid.value = keyset_id
        if snapshot_id is not None:
            request.keyset_snapshot_id.value = snapshot_id
        request.aggregate_entire_history = aggregate_entire_history
        request.order_by.extend(await atuple(_encode_order_by(order_by, ListDomainsByKeysetRequest.OrderBy)))

        # XXX: Bug in python 3.9 coverage
        async for chunk in self.call_stream("list_domains_by_keyset", request):  # pragma: no cover
            for data in chunk["domains"]:
                yield DomainByRelation(**data)

    async def list_by_keyset_page(
        self,
        keyset_id: KeysetId,
        *,
        snapshot_id: Optional[SnapshotId] = None,
        aggregate_entire_history: bool = False,
        order_by: AnyIterable[str] = (),
        page_token: Optional[str] = None,
        page_size: Optional[int] = None,
    ) -> ListPage[DomainByRelation]:
        """Return domains associated with keyset.

        Args:
            keyset_id: Keyset ID.
            snapshot_id: Snapshot identification.
            aggregate_entire_history: Include domains connected to the keyset in the past.
            order_by: Comma separated list of fields to order by.
                Each field can be prefixed with `+` (asc) or `-` (desc). `+` is optional.
            page_token: Page token for pagination.
            page_size: Page size for pagination.

        Returns:
            Domains and pagination info.

        Raises:
            KeysetDoesNotExist: If keyset does not exist.
            InvalidData: Invalid input parameters.
            InvalidSnapshot: If snapshot_id is provided, but not longer valid.
        """
        request = ListDomainsByKeysetRequest()
        request.keyset_id.uuid.value = keyset_id
        if snapshot_id is not None:
            request.keyset_snapshot_id.value = snapshot_id
        request.aggregate_entire_history = aggregate_entire_history
        request.order_by.extend(await atuple(_encode_order_by(order_by, ListDomainsByKeysetRequest.OrderBy)))
        set_request_pagination(request, page_token=page_token, page_size=page_size)

        results = []
        pagination = None
        # XXX: Bug in python 3.9 coverage
        async for chunk in self.call_stream("list_domains_by_keyset", request):  # pragma: no cover
            if pagination is None and chunk.get("pagination"):
                pagination = PaginationReply(**chunk.get("pagination"))
            for data in chunk["domains"]:
                results.append(DomainByRelation(**data))

        return ListPage(results=tuple(results), pagination=pagination)

    @overload
    async def list_by_nsset(
        self,
        nsset_id: NssetId,
        *,
        snapshot_id: Optional[SnapshotId] = None,
        aggregate_entire_history: bool = False,
        order_by: AnyIterable[str] = (),
        refs_only: Literal[False],
    ) -> AsyncIterator[DomainByRelation]:
        ...
        yield  # type: ignore[misc]

    @overload
    async def list_by_nsset(
        self,
        nsset_id: NssetId,
        *,
        snapshot_id: Optional[SnapshotId] = None,
        aggregate_entire_history: bool = False,
        order_by: AnyIterable[str] = (),
        refs_only: Literal[True] = True,
    ) -> AsyncIterator[DomainRef]:
        ...
        yield  # type: ignore[misc]

    async def list_by_nsset(  # type: ignore[misc]
        self,
        nsset_id: NssetId,
        *,
        snapshot_id: Optional[SnapshotId] = None,
        aggregate_entire_history: bool = False,
        order_by: AnyIterable[str] = (),
        refs_only: Optional[bool] = None,
    ) -> Union[AsyncIterator[DomainByRelation], AsyncIterator[DomainRef]]:
        """Iterate through all domains associated with nsset.

        Args:
            nsset_id: Nsset ID.
            snapshot_id: Snapshot identification.
            aggregate_entire_history: Include domains connected to the nsset in the past.
            order_by: Comma separated list of fields to order by.
                Each field can be prefixed with `+` (asc) or `-` (desc). `+` is optional.
            refs_only: If true (default) return only references and not additional domain details.

        Returns:
            Iterator of domains.

        Raises:
            NssetDoesNotExist: If nsset does not exist.
            InvalidData: Invalid input parameters.
            InvalidSnapshot: If snapshot_id is provided, but not longer valid.
        """
        if refs_only is None:
            warnings.warn(
                "Argument refs_only will be removed and method will act as False in the future release.",
                DeprecationWarning,
                stacklevel=2,
            )
            refs_only = True
        request = ListDomainsByNssetRequest()
        request.nsset_id.uuid.value = nsset_id
        if snapshot_id is not None:
            request.nsset_snapshot_id.value = snapshot_id
        request.aggregate_entire_history = aggregate_entire_history
        request.order_by.extend(await atuple(_encode_order_by(order_by, ListDomainsByNssetRequest.OrderBy)))

        # XXX: Bug in python 3.9 coverage
        async for chunk in self.call_stream("list_domains_by_nsset", request):  # pragma: no cover
            for data in chunk["domains"]:
                domain = DomainByRelation(**data)
                if refs_only:
                    yield domain.domain
                else:
                    yield domain

    async def list_by_nsset_page(
        self,
        nsset_id: NssetId,
        *,
        snapshot_id: Optional[SnapshotId] = None,
        aggregate_entire_history: bool = False,
        order_by: AnyIterable[str] = (),
        page_token: Optional[str] = None,
        page_size: Optional[int] = None,
    ) -> ListPage[DomainByRelation]:
        """Return domains associated with nsset.

        Args:
            nsset_id: Nsset ID.
            snapshot_id: Snapshot identification.
            aggregate_entire_history: Include domains connected to the nsset in the past.
            order_by: Comma separated list of fields to order by.
                Each field can be prefixed with `+` (asc) or `-` (desc). `+` is optional.
            page_token: Page token for pagination.
            page_size: Page size for pagination.

        Returns:
            Domains and pagination info.

        Raises:
            NssetDoesNotExist: If nsset does not exist.
            InvalidData: Invalid input parameters.
            InvalidSnapshot: If snapshot_id is provided, but not longer valid.
        """
        request = ListDomainsByNssetRequest()
        request.nsset_id.uuid.value = nsset_id
        if snapshot_id is not None:
            request.nsset_snapshot_id.value = snapshot_id
        request.aggregate_entire_history = aggregate_entire_history
        request.order_by.extend(await atuple(_encode_order_by(order_by, ListDomainsByNssetRequest.OrderBy)))
        set_request_pagination(request, page_token=page_token, page_size=page_size)

        results = []
        pagination = None
        # XXX: Bug in python 3.9 coverage
        async for chunk in self.call_stream("list_domains_by_nsset", request):  # pragma: no cover
            if pagination is None and chunk.get("pagination"):
                pagination = PaginationReply(**chunk.get("pagination"))
            for data in chunk["domains"]:
                results.append(DomainByRelation(**data))

        return ListPage(results=tuple(results), pagination=pagination)

    async def get_domains_by_contact(
        self,
        contact_id: ContactId,
        roles: set[DomainContactRole],
        *,
        include_deleted: bool = False,
        order_by: Optional[str] = None,
        fqdn_filter: Optional[str] = None,
        page_token: Optional[str] = None,
        page_size: Optional[int] = None,
    ) -> DomainsByContactResult:
        """Get domains linked to contact.

        Args:
            contact_id: Contact ID.
            roles: Set of roles in which is contact linked to domain.
            include_deleted: Include deleted domains that were linked to contact in moment of their deletion.
            order_by: Name of column to order results by.
                Can be prefixed with `+` (asc) or `-` (desc). `+` is optional.
            fqdn_filter: Filter for domain names.
                `*` means any sequence of characters.
                Pattern is matched against the whole domain name.
            page_token: Page token for pagination.
            page_size: Page size for pagination.

        Returns:
            List of domain ids and their appropriate contact roles.

        Raises:
            ContactDoesNotExist: If contact does not exist.
            ValueError: If order_by column name is invalid.
        """
        request = DomainsByContactRequest()
        request.contact_id.uuid.value = contact_id
        set_request_pagination(request, page_token=page_token, page_size=page_size)

        if DomainContactRole.HOLDER in roles:
            request.include_holder = True
        if DomainContactRole.ADMIN in roles:
            request.include_admin_contact = True
        if DomainContactRole.KEYSET_TECH in roles:
            request.include_keyset_tech_contact = True
        if DomainContactRole.NSSET_TECH in roles:
            request.include_nsset_tech_contact = True

        request.include_deleted_domains = include_deleted
        if order_by:
            request.order_by = order_by
        if fqdn_filter:
            request.fqdn_filter = fqdn_filter

        result: DomainsByContactResult = {"domains": []}
        async for message in self.call_stream("get_domains_by_contact", request):  # pragma: no cover
            if pagination := message.get("pagination"):
                result.setdefault("pagination", pagination)
            result["domains"].extend(message.get("domains", []))

        return result

    async def _make_update_state_requests(
        self,
        updates: AnyIterable[StateUpdate[DomainId]],
        *,
        snapshot_id: Optional[SnapshotId] = None,
        log_entry_id: Optional[str] = None,
        batch_size: int = 100,
    ) -> AsyncIterator[UpdateDomainStateRequest]:
        async for i, batch in aenumerate(abatched(updates, n=batch_size)):
            request = UpdateDomainStateRequest()
            for update in batch:
                message = request.updates.add()
                message.domain_id.uuid.value = update.id
                message.set_flags.extend(update.set_flags)
                message.unset_flags.extend(update.unset_flags)
            if i == 0:
                if snapshot_id is not None:
                    request.domain_snapshot_id.value = snapshot_id
                if log_entry_id is not None:
                    request.log_entry_id.value = log_entry_id
            yield request

    async def update_state(
        self,
        updates: AnyIterable[StateUpdate[DomainId]],
        *,
        snapshot_id: Optional[SnapshotId] = None,
        log_entry_id: Optional[str] = None,
        batch_size: int = 100,
    ) -> None:
        """Update domains' states.

        Args:
            updates: Iterable of changes.
            snapshot_id: The snapshot identification.
            log_entry_id: Identification of logger entry for the action.
            batch_size: Number of objects in a single stream request.

        Raises:
            DomainDoesNotExist: Domain not found.
            InvalidSnapshot: Snapshot is not valid.
            StateFlagDoesNotExist: State flag not found.
            InvalidData: Invalid input parameters.
        """
        await atuple(
            self.call_stream(
                "update_domain_state",
                self._make_update_state_requests(
                    updates, snapshot_id=snapshot_id, log_entry_id=log_entry_id, batch_size=batch_size
                ),
            )
        )

    async def get_domain_life_cycle_stage(self, fqdn: str) -> Optional[DomainId]:
        """Return result based on domain status.

        Args:
            fqdn: Domain FQDN.

        Returns:
            * Domain ID in case the domain is registered.
            * None if domain is in delete candidate state or have been deleted as such today.

        Raises:
            DomainDoesNotExist: If domain is not registered.
        """
        request = DomainLifeCycleStageRequest()
        request.fqdn.value = _idna_encode(fqdn)
        result = await self.call("get_domain_life_cycle_stage", request=request)
        if "domain_id" in result:
            return cast(DomainId, result["domain_id"])
        elif "delete_candidate" in result:
            return None
        else:
            raise DomainDoesNotExist

    async def get_life_cycle_stage(self, fqdn: str) -> DomainLifeCycleStageResult:
        """Return information about domain life cycle stage.

        Args:
            fqdn: Domain FQDN.

        Returns:
            A tuple with the life cycle stage and the domain ID, if available.
        """
        request = DomainLifeCycleStageRequest()
        request.fqdn.value = _idna_encode(fqdn)
        result = await self.call("get_domain_life_cycle_stage", request=request)
        if not result:
            return DomainLifeCycleStageResult(DomainLifeCycleStage.NOT_REGISTERED)
        elif "domain_id" in result:
            return DomainLifeCycleStageResult(DomainLifeCycleStage.REGISTERED, cast(DomainId, result["domain_id"]))
        elif "blacklisted" in result:
            return DomainLifeCycleStageResult(DomainLifeCycleStage.BLACKLISTED, block_ids=tuple(result["blacklisted"]))
        else:
            return DomainLifeCycleStageResult(DomainLifeCycleStage(tuple(result)[0]))

    @overload
    async def add_auth_info(
        self,
        domain_id: DomainId,
        *,
        snapshot_id: Optional[SnapshotId] = None,
        ttl: int = 0,
        log_entry_id: Optional[str] = None,
        by_registrar: Optional[str] = None,
    ) -> str: ...

    @overload
    async def add_auth_info(
        self,
        domain_id: DomainId,
        *,
        snapshot_id: Optional[SnapshotId] = None,
        auth_info: str,
        ttl: int = 0,
        log_entry_id: Optional[str] = None,
        by_registrar: Optional[str] = None,
    ) -> None: ...

    async def add_auth_info(
        self,
        domain_id: DomainId,
        *,
        snapshot_id: Optional[SnapshotId] = None,
        auth_info: Optional[str] = None,
        ttl: int = 0,
        log_entry_id: Optional[str] = None,
        by_registrar: Optional[str] = None,
    ) -> Optional[str]:
        """Add new auth info to the object.

        Args:
            domain_id: Domain ID.
            snapshot_id: The snapshot identification.
            auth_info: Auth info to set. Will be generated if not provided.
            ttl: Lifetime of the auth info in seconds. If 0 (default), server default is used.
            log_entry_id: Identification of logger entry for the action.
            by_registrar: Handle of the registrar upon whose regard the action is performed.

        Raises:
            DomainDoesNotExist: If domain does not exist.
            InvalidData: Invalid input parameters.
            InvalidSnapshot: If snapshot_id is provided, but not longer valid.
            RegistrarDoesNotExist: Provided registrar does not exist.
        """
        request = AddDomainAuthInfoRequest()
        request.domain_id.uuid.value = domain_id
        if snapshot_id is not None:
            request.domain_snapshot_id.value = snapshot_id
        if auth_info is not None:
            request.auth_info = auth_info
        else:
            request.generate.CopyFrom(Empty())
        request.ttl = ttl
        if log_entry_id is not None:
            request.log_entry_id.value = log_entry_id
        if by_registrar is not None:
            request.by_registrar.value = by_registrar
        result = cast(Optional[str], await self.call("add_domain_auth_info", request))
        return result


class DomainAdminDecoder(RegistryDecoder):
    """Domain Admin service decoder."""

    def __init__(self) -> None:
        super().__init__()
        self.set_decoder(DomainContactInfo, self._decode_domain_contact_info)
        self.set_decoder(BatchDeleteDomainsReplyMessage.NotDeleted, self._decode_not_deleted)

        self.set_exception_decoder(ManageDomainStateFlagsReplyMessage.Exception.DomainDoesNotExist, DomainDoesNotExist)
        self.set_exception_decoder(
            ManageDomainStateFlagsReplyMessage.Exception.DomainStateFlagRequestDoesNotExist, ValueError
        )
        self.set_exception_decoder(ManageDomainStateFlagsReplyMessage.Exception.DomainStateFlagDoesNotExist, ValueError)
        self.set_exception_decoder(
            UpdateDomainsAdditionalNotifyInfoReply.Exception.DomainDoesNotExist, DomainDoesNotExist
        )
        self.set_exception_decoder(
            UpdateDomainsAdditionalNotifyInfoReply.Exception.InvalidDomainContactInfo, ValueError
        )

    def _decode_domain_contact_info(self, value: DomainContactInfo) -> Optional[ContactInfo]:
        if value.HasField("email"):
            return ContactInfo(type=ContactInfoType.EMAIL, value=value.email)
        elif value.HasField("telephone"):
            return ContactInfo(type=ContactInfoType.TELEPHONE, value=value.telephone)
        else:
            return None

    def _decode_not_deleted(self, value: BatchDeleteDomainsReplyMessage.NotDeleted) -> dict:
        decoded = cast(dict[str, Any], self._decode_message(value))
        decoded["reason"] = NotDeletedReason(decoded["reason"])
        return decoded


class DomainAdminClient(AsyncGrpcClient):
    """Domain Admin service gRPC client."""

    stub_cls = AdminStub
    decoder_cls = DomainAdminDecoder
    service = "Admin"

    @staticmethod
    async def _make_batch_delete_domains_requests(
        domains: AnyIterable[Domain], *, batch_size: int = 100
    ) -> AsyncIterable[BatchDeleteDomainsRequest]:
        """Request generator for `batch_delete_domains` method."""
        async for chunk in abatched(domains, batch_size):
            request = BatchDeleteDomainsRequest()
            for domain in chunk:
                message = BatchDeleteDomainsRequest.Domain()
                message.domain_id.uuid.value = domain.domain_id
                if domain.domain_history_id is None:
                    raise ValueError("{!r} is missing a domain_history_id")
                message.domain_history_id.uuid.value = domain.domain_history_id
                request.domains.append(message)
            yield request

    async def batch_delete(
        self,
        domains: AnyIterable[Domain],
        *,
        batch_size: int = 100,
    ) -> AsyncIterable[BatchDeleteDomainsReply]:
        """Batch deletion of domains.

        Args:
            domains: Iterable (or async iterable) of domains to be deleted.
            batch_size: Number of domains in single stream request.

        Returns:
            Async iterable with information about domains that haven't been deleted.
        """
        requests = self._make_batch_delete_domains_requests(domains, batch_size=batch_size)
        async for message in self.call_stream("batch_delete_domains", requests):  # pragma: no cover
            for domain in message or ():
                yield BatchDeleteDomainsReply(**domain)

    async def batch_delete_domains(
        self,
        domains: AnyIterable[Domain],
        *,
        batch_size: int = 100,
    ) -> AsyncIterable[BatchDeleteDomainsResult]:
        """Batch deletion of domains.

        Args:
            domains: Iterable (or async iterable) of domains to be deleted.
            batch_size: Number of domains in single stream request.

        Returns:
            Async iterable of dictionaries with following keys:
                domain_id: Domain identification (as a string).
                reason: Reason for not deleting domain (as a NotDeletedReason enumeration).
        """
        warnings.warn(
            "Method batch_delete_domains is deprecated in favor of batch_delete.", DeprecationWarning, stacklevel=2
        )
        requests = self._make_batch_delete_domains_requests(domains, batch_size=batch_size)
        async for message in self.call_stream("batch_delete_domains", requests):  # pragma: no cover
            for domain in message or ():
                yield domain

    @staticmethod
    async def _make_manage_domain_state_flags_requests(
        domains: AnyIterable[Domain],
        state_flags: dict[str, Optional[DatetimeRange]],
        close_requests: AnyIterable[str],
        *,
        batch_size: int = 100,
    ) -> AsyncIterable[ManageDomainStateFlagsRequest]:
        """Request generator for `manage_domain_state_flags` method."""
        async for chunk in abatched(domains, batch_size):
            request = ManageDomainStateFlagsRequest()
            for flag, validity in state_flags.items():
                validity_msg = request.state_flag_requests_to_create.state_flags.get_or_create(flag)
                if validity and validity.start:
                    validity_msg.valid_from.FromDatetime(validity.start)
                if validity and validity.end:
                    validity_msg.valid_to.FromDatetime(validity.end)
            for domain in chunk:
                message = ManageDomainStateFlagsRequest.Domain()
                message.domain_id.uuid.value = domain.domain_id
                if domain.domain_history_id:
                    message.domain_history_id.uuid.value = domain.domain_history_id
                request.state_flag_requests_to_create.domains.append(message)
            yield request

        async for close_chunk in abatched(close_requests, batch_size):  # pragma: no cover # because of py39 coverage
            request = ManageDomainStateFlagsRequest()
            for request_id in close_chunk:
                message = request.state_flag_requests_to_close.add()
                message.uuid.value = request_id
            yield request

    @overload
    def manage_state_flags(
        self,
        *,
        domains: AnyIterable[Domain],
        state_flags: Mapping[str, Optional[DatetimeRange]],
        close_requests: Optional[AnyIterable[str]] = None,
        batch_size: int = 100,
    ) -> AsyncIterable[ManageDomainStateFlagsReply]: ...

    @overload
    def manage_state_flags(
        self,
        *,
        close_requests: Optional[AnyIterable[str]] = None,
        batch_size: int = 100,
    ) -> AsyncIterable[ManageDomainStateFlagsReply]: ...

    async def manage_state_flags(
        self,
        *,
        domains: Optional[AnyIterable[Domain]] = None,
        state_flags: Optional[Mapping[str, Optional[DatetimeRange]]] = None,
        close_requests: Optional[AnyIterable[str]] = None,
        batch_size: int = 100,
    ) -> AsyncIterable[ManageDomainStateFlagsReply]:
        """Manage domains state flags.

        Args:
            domains: Iterable (or async iterable) of domains to be changed.
            state_flags: Dictionary. Keys are state flags, values are datetime ranges
                (or None if datetime range is from NOW to indefinitely).
            close_requests: String ids of domain state flag requests that should be closed.
            batch_size: Number of domains in single stream request.

        Returns:
            Async iterable of with information about state flag requests.

        Raises:
            DomainDoesNotExist: When domain does not exist.
            ValueError: When state flag or state flag request id does not exist.
        """
        if (domains is None) ^ (state_flags is None):
            raise ValueError("Either both `domains` and `state_flags` have to be defined or none of them.")

        requests = self._make_manage_domain_state_flags_requests(
            domains or [],
            cast(dict[str, Optional[DatetimeRange]], state_flags) or {},
            close_requests or [],
            batch_size=batch_size,
        )
        async for message in self.call_stream("manage_domain_state_flags", requests):  # pragma: no cover
            for record in message or []:
                record["domain_id"] = DomainId(record["domain_id"])
                yield ManageDomainStateFlagsReply(**record)

    @overload
    def manage_domain_state_flags(
        self,
        *,
        domains: AnyIterable[Domain],
        state_flags: Mapping[str, Optional[DatetimeRange]],
        close_requests: Optional[AnyIterable[str]] = None,
        batch_size: int = 100,
    ) -> AsyncIterable[ManageDomainStateFlagsResult]: ...

    @overload
    def manage_domain_state_flags(
        self,
        *,
        close_requests: Optional[AnyIterable[str]] = None,
        batch_size: int = 100,
    ) -> AsyncIterable[ManageDomainStateFlagsResult]: ...

    async def manage_domain_state_flags(
        self,
        *,
        domains: Optional[AnyIterable[Domain]] = None,
        state_flags: Optional[Mapping[str, Optional[DatetimeRange]]] = None,
        close_requests: Optional[AnyIterable[str]] = None,
        batch_size: int = 100,
    ) -> AsyncIterable[ManageDomainStateFlagsResult]:
        """Manage domains state flags.

        Args:
            domains: Iterable (or async iterable) of domains to be changed.
            state_flags: Dictionary. Keys are state flags, values are datetime ranges
                (or None if datetime range is from NOW to indefinitely).
            close_requests: String ids of domain state flag requests that should be closed.
            batch_size: Number of domains in single stream request.

        Returns:
            Async iterable of dictionaries with following keys:
                request_id: Contains ID of flag state request. It can be used to close the request later.
                domain_id: Domain ID.
                state_flag: Name of state flag that was set.

        Raises:
            DomainDoesNotExist: When domain does not exist.
            ValueError: When state flag or state flag request id does not exist.
        """
        warnings.warn(
            "Method manage_domain_state_flags is deprecated in favor of manage_state_flags.",
            DeprecationWarning,
            stacklevel=2,
        )
        if (domains is None) ^ (state_flags is None):
            raise ValueError("Either both `domains` and `state_flags` have to be defined or none of them.")

        requests = self._make_manage_domain_state_flags_requests(
            domains or [],
            cast(dict[str, Optional[DatetimeRange]], state_flags) or {},
            close_requests or [],
            batch_size=batch_size,
        )
        async for message in self.call_stream("manage_domain_state_flags", requests):  # pragma: no cover
            for record in message or []:
                record["domain_id"] = DomainId(record["domain_id"])
                yield record

    async def _get_domains_notify_info(
        self, request: Union[GetDomainsDeleteNotifyInfoRequest, GetDomainsOutzoneNotifyInfoRequest], method: str
    ) -> GetDomainsNotifyInfoResult:
        """Get domains notify info."""
        result: GetDomainsNotifyInfoResult = {"domains": []}
        async for message in self.call_stream(method, request):  # pragma: no cover
            if pagination := message.get("pagination"):
                result.setdefault("pagination", pagination)
            for domain in message.get("domains", []):
                domain["domain_id"] = DomainId(domain.get("domain_id"))
                domain["registrant_id"] = ContactId(domain.get("registrant_id"))
                result["domains"].append(domain)

        return result

    async def get_outzone_notify_info(
        self,
        start: datetime,
        end: datetime,
        *,
        page_size: Optional[int] = None,
        page_token: Optional[str] = None,
    ) -> GetDomainsNotifyInfoReply:
        """Get domains outzone notify info.

        Args:
            start: Start of the interval in which the domain is expected to stop being generated to zone.
            end: End of the interval in which the domain is expected to stop being generated to zone.
            page_token: Page token for pagination.
            page_size: Page size for pagination.
        """
        request = GetDomainsOutzoneNotifyInfoRequest()
        request.outzone_from.FromDatetime(start)
        request.outzone_to.FromDatetime(end)
        set_request_pagination(request, page_token=page_token, page_size=page_size)

        return GetDomainsNotifyInfoReply(
            **await self._get_domains_notify_info(request, "get_domains_outzone_notify_info")  # type: ignore[arg-type]
        )

    async def get_delete_notify_info(
        self,
        start: datetime,
        end: datetime,
        *,
        page_size: Optional[int] = None,
        page_token: Optional[str] = None,
    ) -> GetDomainsNotifyInfoReply:
        """Get domains delete notify info.

        Args:
            start: Start of the interval in which the domain is expected to be deleted.
            end: End of the interval in which the domain is expected to be deleted.
            page_token: Page token for pagination.
            page_size: Page size for pagination.
        """
        request = GetDomainsDeleteNotifyInfoRequest()
        request.delete_from.FromDatetime(start)
        request.delete_to.FromDatetime(end)
        set_request_pagination(request, page_token=page_token, page_size=page_size)

        return GetDomainsNotifyInfoReply(
            **await self._get_domains_notify_info(request, "get_domains_delete_notify_info")  # type: ignore[arg-type]
        )

    async def get_domains_outzone_notify_info(
        self,
        start: datetime,
        end: datetime,
        *,
        page_size: Optional[int] = None,
        page_token: Optional[str] = None,
    ) -> GetDomainsNotifyInfoResult:
        """Get domains outzone notify info.

        Args:
            start: Start of the interval in which the domain is expected to stop being generated to zone.
            end: End of the interval in which the domain is expected to stop being generated to zone.
            page_token: Page token for pagination.
            page_size: Page size for pagination.
        """
        warnings.warn(
            "Method get_domains_outzone_notify_info is deprecated in favor of get_outzone_notify_info.",
            DeprecationWarning,
            stacklevel=2,
        )
        request = GetDomainsOutzoneNotifyInfoRequest()
        request.outzone_from.FromDatetime(start)
        request.outzone_to.FromDatetime(end)
        set_request_pagination(request, page_token=page_token, page_size=page_size)

        return await self._get_domains_notify_info(request, "get_domains_outzone_notify_info")

    async def get_domains_delete_notify_info(
        self,
        start: datetime,
        end: datetime,
        *,
        page_size: Optional[int] = None,
        page_token: Optional[str] = None,
    ) -> GetDomainsNotifyInfoResult:
        """Get domains delete notify info.

        Args:
            start: Start of the interval in which the domain is expected to be deleted.
            end: End of the interval in which the domain is expected to be deleted.
            page_token: Page token for pagination.
            page_size: Page size for pagination.
        """
        warnings.warn(
            "Method get_domains_delete_notify_info is deprecated in favor of get_delete_notify_info.",
            DeprecationWarning,
            stacklevel=2,
        )
        request = GetDomainsDeleteNotifyInfoRequest()
        request.delete_from.FromDatetime(start)
        request.delete_to.FromDatetime(end)
        set_request_pagination(request, page_token=page_token, page_size=page_size)

        return await self._get_domains_notify_info(request, "get_domains_delete_notify_info")

    @staticmethod
    async def _make_update_domains_additional_notify_info_requests(
        request_type: type[UpdateDomainsNotifyInfoRequest],
        info_message: Message,
        domains: Mapping[DomainId, Sequence[ContactInfo]],
        *,
        batch_size: int = 100,
    ) -> AsyncIterable[UpdateDomainsNotifyInfoRequest]:
        """Request generator for `update_domains_(outzone|delete)_additional_notify_info` methods.

        Args:
            request_type: Request message type.
            info_message: Additional info message with prefilled (outzone|delete)_from and (outzone|delete)_to fields.
            domains: Mapping of domains. Keys are domain ids, values are sequences of additional contact info.
            batch_size: Number of domains in single stream request.
        """
        async for chunk in abatched(domains.items(), batch_size):
            chunk_request = request_type()
            for domain_id, additional_info in chunk:
                domain = chunk_request.domains_additional_info.add()
                domain.CopyFrom(info_message)
                domain.domain_id.uuid.value = domain_id
                for contact in additional_info:
                    domain.additional_contacts.add(**{contact.type: contact.value})
            yield chunk_request

    async def update_outzone_additional_notify_info(
        self,
        domains: Mapping[DomainId, Sequence[ContactInfo]],
        *,
        start: datetime,
        end: datetime,
        batch_size: int = 100,
    ) -> None:
        """Update additional notify info for domains that will stop being generated to zone.

        Args:
            domains: Mapping of domains. Keys are domain ids, values are sequences of additional contact info.
            start: Start of the interval in which the domain is expected to stop being generated to zone.
            end: End of the interval in which the domain is expected to stop being generated to zone.
            batch_size: Number of domains in single stream request.

        Raises:
            DomainDoesNotExist: When at least one of the domains does not exist.
            ValueError: When domain contact info format is invalid.
        """
        request = UpdateDomainsOutzoneAdditionalNotifyInfoRequest.DomainAdditionalInfo()
        request.outzone_from.FromDatetime(start)
        request.outzone_to.FromDatetime(end)

        await self.call(
            "update_domains_outzone_additional_notify_info",
            self._make_update_domains_additional_notify_info_requests(
                UpdateDomainsOutzoneAdditionalNotifyInfoRequest,
                request,
                domains,
                batch_size=batch_size,
            ),
        )

    async def update_delete_additional_notify_info(
        self,
        domains: Mapping[DomainId, Sequence[ContactInfo]],
        *,
        start: datetime,
        end: datetime,
        batch_size: int = 100,
    ) -> None:
        """Update additional notify info for domains that are expected to be deleted.

        Args:
            domains: Mapping of domains. Keys are domain ids, values are sequences of additional contact info.
            start: Start of the interval in which the domain is expected to be deleted.
            end: End of the interval in which the domain is expected to be deleted.
            batch_size: Number of domains in single stream request.

        Raises:
            DomainDoesNotExist: When at least one of the domains does not exist.
            ValueError: When domain contact info format is invalid.
        """
        request = UpdateDomainsDeleteAdditionalNotifyInfoRequest.DomainAdditionalInfo()
        request.delete_from.FromDatetime(start)
        request.delete_to.FromDatetime(end)

        await self.call(
            "update_domains_delete_additional_notify_info",
            self._make_update_domains_additional_notify_info_requests(
                UpdateDomainsDeleteAdditionalNotifyInfoRequest,
                request,
                domains,
                batch_size=batch_size,
            ),
        )

    async def update_domains_outzone_additional_notify_info(
        self,
        domains: Mapping[DomainId, Sequence[ContactInfo]],
        *,
        start: datetime,
        end: datetime,
        batch_size: int = 100,
    ) -> None:
        """Update additional notify info for domains that will stop being generated to zone.

        Args:
            domains: Mapping of domains. Keys are domain ids, values are sequences of additional contact info.
            start: Start of the interval in which the domain is expected to stop being generated to zone.
            end: End of the interval in which the domain is expected to stop being generated to zone.
            batch_size: Number of domains in single stream request.

        Raises:
            DomainDoesNotExist: When at least one of the domains does not exist.
            ValueError: When domain contact info format is invalid.
        """
        warnings.warn(
            "Method update_domains_outzone_additional_notify_info is deprecated in favor of update_outzone_additional_notify_info.",
            DeprecationWarning,
            stacklevel=2,
        )
        await self.update_outzone_additional_notify_info(domains, start=start, end=end, batch_size=batch_size)

    async def update_domains_delete_additional_notify_info(
        self,
        domains: Mapping[DomainId, Sequence[ContactInfo]],
        *,
        start: datetime,
        end: datetime,
        batch_size: int = 100,
    ) -> None:
        """Update additional notify info for domains that are expected to be deleted.

        Args:
            domains: Mapping of domains. Keys are domain ids, values are sequences of additional contact info.
            start: Start of the interval in which the domain is expected to be deleted.
            end: End of the interval in which the domain is expected to be deleted.
            batch_size: Number of domains in single stream request.

        Raises:
            DomainDoesNotExist: When at least one of the domains does not exist.
            ValueError: When domain contact info format is invalid.
        """
        warnings.warn(
            "Method update_domains_delete_additional_notify_info is deprecated in favor of update_delete_additional_notify_info.",
            DeprecationWarning,
            stacklevel=2,
        )
        await self.update_delete_additional_notify_info(domains, start=start, end=end, batch_size=batch_size)

"""Keyset client for registry service."""

import base64
import hashlib
import warnings
from collections.abc import AsyncIterator, Sequence
from datetime import datetime
from typing import Any, Literal, Optional, Union, cast, overload

import grpc
from asyncstdlib import batched as abatched, enumerate as aenumerate, tuple as atuple
from fred_api.registry.keyset.add_keyset_auth_info_types_pb2 import AddKeysetAuthInfoRequest
from fred_api.registry.keyset.keyset_history_types_pb2 import KeysetHistoryReply, KeysetHistoryRequest
from fred_api.registry.keyset.keyset_info_types_pb2 import (
    BatchKeysetInfoRequest,
    DnsKey as DnsKeyMessage,
    KeysetIdRequest,
    KeysetInfoReply,
    KeysetInfoRequest,
)
from fred_api.registry.keyset.keyset_state_history_types_pb2 import KeysetStateHistoryReply, KeysetStateHistoryRequest
from fred_api.registry.keyset.keyset_state_types_pb2 import KeysetStateRequest
from fred_api.registry.keyset.list_keysets_by_contact_types_pb2 import ListKeysetsByContactRequest
from fred_api.registry.keyset.service_keyset_grpc_pb2_grpc import KeysetStub
from fred_api.registry.keyset.update_keyset_state_types_pb2 import UpdateKeysetStateRequest
from fred_types import (
    BaseModel,
    ContactId,
    ContactRef,
    DnskeyAlgorithm,
    KeysetHistoryId,
    KeysetId,
    KeysetRef,
    SnapshotId,
)
from google.protobuf.empty_pb2 import Empty
from pydantic import ConfigDict

from .common import (
    ListPage,
    ObjectEvents,
    ObjectHistory,
    ObjectHistoryItem,
    ObjectStateHistoryItem,
    PaginationReply,
    RegistryDecoder,
    _encode_order_by,
    set_request_pagination,
)
from .exceptions import InvalidHistoryInterval
from .object import ObjectStateFlag, RegistryObjectClient, StateUpdate
from .utils import AnyIterable


class DSRecord(BaseModel):
    """Delegation signer resource record."""

    keytag: int
    alg: int
    digest_type: int
    digest: str


class DnsKey(BaseModel):
    """Dns key info."""

    flags: int
    protocol: int = 3
    alg: int
    key: str

    def ds_record(self, fqdn: str) -> DSRecord:
        """Compute DS record for DNS key and given domain name."""
        digtype, digest = self._dsrecord_digest(fqdn)
        return DSRecord(alg=self.alg, keytag=self._ds_record_keytag(), digest_type=digtype, digest=digest)

    def _ds_record_keytag(self) -> int:
        """Compute keytag from RRDATA of DNSKEY RR according to appendix B of RFC 4034."""
        key = base64.b64decode(self.key)
        if self.alg == DnskeyAlgorithm.RSAMD5:
            if len(key) < 4:
                return 0
            else:
                return (key[len(key) - 4] << 8) + key[len(key) - 3]
        else:
            sum = self.flags + (self.protocol << 8) + self.alg
            for i in range(0, len(key)):
                if i & 1:
                    sum += key[i]
                else:
                    sum += key[i] << 8
            sum += (sum >> 16) & 0xFFFF
            return sum & 0xFFFF

    def _dsrecord_digest(self, fqdn: str) -> tuple[int, str]:
        """Compute digest from FQDN and RRDATA of DNSKEY RR according to RFC 4034 (4509).

        There are several possible digest types, but we're just using SHA256 for the time being.
        In the future, there'll probably be a single source of truth about this in FRED backend.

        Returns:
            Tuple of digest type and digest.
        """
        hash = hashlib.sha256()
        labels = fqdn.split(".")
        buffer = ""
        for label in labels:
            buffer += chr(len(label)) + label
        buffer += chr(0) + chr(self.flags >> 8) + chr(self.flags & 255) + chr(self.protocol)
        buffer += chr(self.alg)
        hash.update(buffer.encode() + base64.b64decode(self.key))
        return (2, hash.hexdigest().upper())


class Keyset(BaseModel):
    """Keyset info."""

    keyset_id: KeysetId
    keyset_handle: str
    keyset_history_id: Optional[KeysetHistoryId] = None
    snapshot_id: Optional[SnapshotId] = None
    sponsoring_registrar: Optional[str] = None
    dns_keys: Sequence[DnsKey] = []
    technical_contacts: Sequence[ContactRef] = []
    auth_info: Optional[str] = None
    events: Optional[ObjectEvents] = None
    domains_count: Optional[int] = None


class KeysetByRelation(BaseModel):
    """Model representing a keyset returned as a result of a relation."""

    model_config = ConfigDict(frozen=True)

    keyset: KeysetRef
    keyset_history_id: Optional[KeysetHistoryId] = None
    is_deleted: bool = False


class KeysetDecoder(RegistryDecoder):
    """Keyset service decoder."""

    def __init__(self) -> None:
        super().__init__()
        self.set_decoder(DnsKeyMessage, self._decode_dns_key)
        self.set_decoder(KeysetInfoReply.Data, self._decode_keyset_info)
        # Decode exceptions in batch info response
        self.set_decoder(KeysetInfoReply.Exception, self.decode_exception)

        self.set_exception_decoder(KeysetHistoryReply.Exception.InvalidHistoryInterval, InvalidHistoryInterval)
        self.set_exception_decoder(KeysetStateHistoryReply.Exception.InvalidHistoryInterval, InvalidHistoryInterval)

    def _decode_dns_key(self, value: DnsKeyMessage) -> DnsKey:
        """Decode DNS key."""
        return DnsKey(flags=value.flags, protocol=value.protocol, alg=value.alg, key=value.key)

    def _decode_keyset_info(self, value: KeysetInfoReply.Data) -> Keyset:
        """Decode KeysetInfoReply.Data."""
        data = self._decode_message(value)
        data.pop("technical_contacts_old", None)
        return Keyset(**data)


class KeysetClient(RegistryObjectClient[KeysetId, KeysetHistoryId]):
    """Keyset service gRPC client."""

    stub_cls = KeysetStub
    decoder_cls = KeysetDecoder
    service = "Keyset"
    object_id = "keyset_id"
    object_history_id = "keyset_history_id"
    object_snapshot_id = "keyset_snapshot_id"
    object_handle = "keyset_handle"

    def __init__(self, netloc: str, credentials: Optional[grpc.ChannelCredentials] = None, **kwargs: Any) -> None:
        """Initialize keyset client instance.

        Args:
            netloc: Netloc of keyset service server.
            credentials: Optional credentials.
            kwargs: Other client arguments.
        """
        super().__init__(netloc, credentials=credentials, **kwargs)

    async def get_id(self, handle: str) -> KeysetId:
        """Get keyset id.

        Args:
            handle: Keyset handle.

        Raises:
            KeysetDoesNotExist: If keyset does not exist.
        """
        return KeysetId(await self.get_object_id(request=KeysetIdRequest(), method="get_keyset_id", handle=handle))

    async def get_keyset_id(self, handle: str) -> KeysetId:
        """Get keyset id.

        Args:
            handle: Keyset handle.

        Raises:
            KeysetDoesNotExist: If keyset does not exist.
        """
        warnings.warn("Method get_keyset_id is deprecated in favor of get_id.", DeprecationWarning, stacklevel=2)
        return await self.get_id(handle)

    async def get(
        self,
        keyset_id: KeysetId,
        history_id: Optional[KeysetHistoryId] = None,
        *,
        snapshot_id: Optional[SnapshotId] = None,
        include_domains_count: bool = False,
    ) -> Keyset:
        """Get keyset info.

        Args:
            keyset_id: Keyset ID.
            history_id: Keyset history ID.
            snapshot_id: Snapshot identification.
            include_domains_count: Whether to include domains count in results.

        Raises:
            KeysetDoesNotExist: If keyset does not exist.
            InvalidSnapshot: If snapshot_id is provided, but not longer valid.
        """
        request = KeysetInfoRequest()
        request.keyset_id.uuid.value = keyset_id
        if history_id is not None:
            request.keyset_history_id.uuid.value = history_id
        if snapshot_id is not None:
            request.keyset_snapshot_id.value = snapshot_id
        request.include_domains_count = include_domains_count
        return cast(Keyset, await self.call("get_keyset_info", request))

    async def _make_batch_get_requests(
        self,
        keyset_ids: AnyIterable[KeysetId],
        *,
        snapshot_id: Optional[SnapshotId] = None,
        include_domains_count: bool = False,
        batch_size: int = 100,
    ) -> AsyncIterator[BatchKeysetInfoRequest]:
        async for batch in abatched(keyset_ids, n=batch_size):
            request = BatchKeysetInfoRequest()
            for id in batch:
                r = request.requests.add()
                r.keyset_id.uuid.value = id
                if snapshot_id is not None:
                    r.keyset_snapshot_id.value = snapshot_id
                r.include_domains_count = include_domains_count
            yield request

    async def batch_get(
        self,
        keyset_ids: AnyIterable[KeysetId],
        *,
        snapshot_id: Optional[SnapshotId] = None,
        include_domains_count: bool = False,
        errors: Literal["ignore", "raise"] = "ignore",
        batch_size: int = 100,
    ) -> AsyncIterator[Keyset]:
        """Batch get keyset info.

        Args:
            keyset_ids: Iterable of keyset identifiers.
            snapshot_id: Snapshot identification.
            include_domains_count: Whether to include domains count in results.
            errors: How to handle errors. If 'ignore' (default) keysets with errors are not returned.
                    If 'raise' the first error is raised as an exception.
            batch_size: Number of items in a single stream request.

        Raises:
            KeysetDoesNotExist: If keyset does not exist.
            InvalidSnapshot: If provided snapshot_id is no longer valid.
        """
        if errors not in ("ignore", "raise"):
            raise ValueError("Unknown errors value: {!r}".format(errors))

        requests = self._make_batch_get_requests(
            keyset_ids, snapshot_id=snapshot_id, include_domains_count=include_domains_count, batch_size=batch_size
        )
        # XXX: Bug in python 3.9 coverage
        async for obj in self._process_batch_info_stream(
            self.call_stream("batch_keyset_info", requests), errors
        ):  # pragma: no cover
            yield obj

    async def get_keyset_info(
        self, keyset_id: KeysetId, history_id: Optional[KeysetHistoryId] = None, *, include_domains_count: bool = False
    ) -> Keyset:
        """Get keyset info.

        Args:
            keyset_id: Keyset ID.
            history_id: Keyset history ID.
            include_domains_count: Whether to include domains count in results.

        Raises:
            KeysetDoesNotExist: If keyset does not exist.
        """
        warnings.warn("Method get_keyset_info is deprecated in favor of get.", DeprecationWarning, stacklevel=2)
        return await self.get(keyset_id, history_id, include_domains_count=include_domains_count)

    async def get_state(
        self, keyset_id: KeysetId, *, internal: bool = False, snapshot_id: Optional[SnapshotId] = None
    ) -> frozenset[str]:
        """Get keyset state consisting of active state flags.

        Args:
            keyset_id: Keyset ID.
            internal: Whether to include internal states in the result.
            snapshot_id: Snapshot identification.

        Returns:
            Set of active state flags.

        Raises:
            KeysetDoesNotExist: If keyset does not exist.
            InvalidSnapshot: If snapshot_id is provided, but not longer valid.
        """
        state = await self.get_object_state(
            request=KeysetStateRequest(),
            method="get_keyset_state",
            object_id=keyset_id,
            internal=internal,
            snapshot_id=snapshot_id,
        )
        return frozenset(name for (name, enabled) in state.items() if enabled is True)

    async def get_keyset_state(self, keyset_id: KeysetId, *, internal: Optional[bool] = None) -> dict[str, bool]:
        """Get keyset state consisting of state flags and their boolean values.

        Args:
            keyset_id: Keyset ID.
            internal: Whether to include internal states in the result.

        Raises:
            KeysetDoesNotExist: If keyset does not exist.
        """
        warnings.warn("Method get_keyset_state is deprecated in favor of get_state.", DeprecationWarning, stacklevel=2)
        if internal is None:  # pragma: no cover
            warnings.warn(
                "Default value of internal flag will change to False in regal 2.0.", DeprecationWarning, stacklevel=2
            )
            internal = True

        return await self.get_object_state(
            request=KeysetStateRequest(), method="get_keyset_state", object_id=keyset_id, internal=internal
        )

    async def get_history(
        self,
        keyset_id: KeysetId,
        start: Optional[Union[KeysetHistoryId, datetime]] = None,
        end: Optional[Union[KeysetHistoryId, datetime]] = None,
    ) -> ObjectHistory[ObjectHistoryItem]:
        """Get keyset history timeline.

        Args:
            keyset_id: Keyset ID.
            start: Datetime or history_id of history interval start.
            end: Datetime or history_id of history interval end.

        Raises:
            KeysetDoesNotExist: If keyset does not exist.
            InvalidHistoryInterval: In case of invalid history interval.
        """
        return await self.get_object_history(
            request=KeysetHistoryRequest(),
            method="get_keyset_history",
            object_id=keyset_id,
            start=start,
            end=end,
        )

    async def get_keyset_history(
        self,
        keyset_id: KeysetId,
        start: Optional[Union[KeysetHistoryId, datetime]] = None,
        end: Optional[Union[KeysetHistoryId, datetime]] = None,
    ) -> dict[str, Any]:
        """Get keyset history timeline.

        Args:
            keyset_id: Keyset ID.
            start: Datetime or history_id of history interval start.
            end: Datetime or history_id of history interval end.

        Raises:
            KeysetDoesNotExist: If keyset does not exist.
            InvalidHistoryInterval: In case of invalid history interval.
        """
        warnings.warn(
            "Method get_keyset_history is deprecated in favor of get_history.", DeprecationWarning, stacklevel=2
        )
        return await self.get_history_data(
            request=KeysetHistoryRequest(),
            method="get_keyset_history",
            object_id=keyset_id,
            start=start,
            end=end,
        )

    async def get_state_flags(self) -> dict[str, ObjectStateFlag]:
        """Get object state flags and their properties."""
        return await self.get_object_state_flags(method="get_keyset_state_flags")  # type: ignore[call-overload]

    async def get_state_history(
        self,
        keyset_id: KeysetId,
        start: Optional[Union[KeysetHistoryId, datetime]] = None,
        end: Optional[Union[KeysetHistoryId, datetime]] = None,
    ) -> ObjectHistory[ObjectStateHistoryItem]:
        """Get keyset state history timeline.

        Args:
            keyset_id: Keyset ID.
            start: Datetime or history_id of history interval start.
            end: Datetime or history_id of history interval end.

        Raises:
            KeysetDoesNotExist: If keyset does not exist.
            InvalidHistoryInterval: In case of invalid history interval.
        """
        result = await self.get_object_state_history(
            request=KeysetStateHistoryRequest(),
            method="get_keyset_state_history",
            object_id=keyset_id,
            start=start,
            end=end,
        )
        result.pop(self.object_id, None)
        return ObjectHistory[ObjectStateHistoryItem](**result)

    async def get_keyset_state_history(
        self,
        keyset_id: KeysetId,
        start: Optional[Union[KeysetHistoryId, datetime]] = None,
        end: Optional[Union[KeysetHistoryId, datetime]] = None,
    ) -> dict[str, Any]:
        """Get keyset state history timeline.

        Args:
            keyset_id: Keyset ID.
            start: Datetime or history_id of history interval start.
            end: Datetime or history_id of history interval end.

        Raises:
            KeysetDoesNotExist: If keyset does not exist.
            InvalidHistoryInterval: In case of invalid history interval.
        """
        warnings.warn(
            "Method get_keyset_state_history is deprecated in favor of get_state_history.",
            DeprecationWarning,
            stacklevel=2,
        )
        return await self.get_object_state_history(
            request=KeysetStateHistoryRequest(),
            method="get_keyset_state_history",
            object_id=keyset_id,
            start=start,
            end=end,
        )

    async def list_by_contact(
        self,
        contact_id: ContactId,
        *,
        snapshot_id: Optional[SnapshotId] = None,
        aggregate_entire_history: bool = False,
        order_by: AnyIterable[str] = (),
    ) -> AsyncIterator[KeysetByRelation]:
        """Return keysets associated with contact.

        Args:
            contact_id: Contact ID.
            snapshot_id: Snapshot identification.
            aggregate_entire_history: Include keysets connected to the contact in the past.
            order_by: Comma separated list of fields to order by.
                Each field can be prefixed with `+` (asc) or `-` (desc). `+` is optional.

        Returns:
            Iterator of keysets.

        Raises:
            ContactDoesNotExist: If contact does not exist.
            InvalidData: Invalid input parameters.
            InvalidSnapshot: If snapshot_id is provided, but not longer valid.
        """
        request = ListKeysetsByContactRequest()
        request.contact_id.uuid.value = contact_id
        if snapshot_id is not None:
            request.contact_snapshot_id.value = snapshot_id
        request.aggregate_entire_history = aggregate_entire_history
        request.order_by.extend(await atuple(_encode_order_by(order_by, ListKeysetsByContactRequest.OrderBy)))

        # XXX: Bug in python 3.9 coverage
        async for chunk in self.call_stream("list_keysets_by_contact", request):  # pragma: no cover
            for data in chunk["keysets"]:
                yield KeysetByRelation(**data)

    async def list_by_contact_page(
        self,
        contact_id: ContactId,
        *,
        snapshot_id: Optional[SnapshotId] = None,
        aggregate_entire_history: bool = False,
        order_by: AnyIterable[str] = (),
        page_token: Optional[str] = None,
        page_size: Optional[int] = None,
    ) -> ListPage[KeysetByRelation]:
        """Return keysets associated with contact.

        Args:
            contact_id: Contact ID.
            snapshot_id: Snapshot identification.
            aggregate_entire_history: Include keysets connected to the contact in the past.
            order_by: Comma separated list of fields to order by.
                Each field can be prefixed with `+` (asc) or `-` (desc). `+` is optional.
            page_token: Page token for pagination.
            page_size: Page size for pagination.

        Returns:
            Keysets and pagination info.

        Raises:
            ContactDoesNotExist: If contact does not exist.
            InvalidData: Invalid input parameters.
            InvalidSnapshot: If snapshot_id is provided, but not longer valid.
        """
        request = ListKeysetsByContactRequest()
        request.contact_id.uuid.value = contact_id
        if snapshot_id is not None:
            request.contact_snapshot_id.value = snapshot_id
        request.aggregate_entire_history = aggregate_entire_history
        request.order_by.extend(await atuple(_encode_order_by(order_by, ListKeysetsByContactRequest.OrderBy)))
        set_request_pagination(request, page_token=page_token, page_size=page_size)

        results = []
        pagination = None
        # XXX: Bug in python 3.9 coverage
        async for chunk in self.call_stream("list_keysets_by_contact", request):  # pragma: no cover
            if pagination is None and chunk.get("pagination"):
                pagination = PaginationReply(**chunk.get("pagination"))
            for data in chunk["keysets"]:
                results.append(KeysetByRelation(**data))

        return ListPage(results=tuple(results), pagination=pagination)

    async def _make_update_state_requests(
        self,
        updates: AnyIterable[StateUpdate[KeysetId]],
        *,
        snapshot_id: Optional[SnapshotId] = None,
        log_entry_id: Optional[str] = None,
        batch_size: int = 100,
    ) -> AsyncIterator[UpdateKeysetStateRequest]:
        async for i, batch in aenumerate(abatched(updates, n=batch_size)):
            request = UpdateKeysetStateRequest()
            for update in batch:
                message = request.updates.add()
                message.keyset_id.uuid.value = update.id
                message.set_flags.extend(update.set_flags)
                message.unset_flags.extend(update.unset_flags)
            if i == 0:
                if snapshot_id is not None:
                    request.keyset_snapshot_id.value = snapshot_id
                if log_entry_id is not None:
                    request.log_entry_id.value = log_entry_id
            yield request

    async def update_state(
        self,
        updates: AnyIterable[StateUpdate[KeysetId]],
        *,
        snapshot_id: Optional[SnapshotId] = None,
        log_entry_id: Optional[str] = None,
        batch_size: int = 100,
    ) -> None:
        """Update keysets' states.

        Args:
            updates: Iterable of changes.
            snapshot_id: The snapshot identification.
            log_entry_id: Identification of logger entry for the action.
            batch_size: Number of objects in a single stream request.

        Raises:
            KeysetDoesNotExist: Keyset not found.
            InvalidSnapshot: Snapshot is not valid.
            StateFlagDoesNotExist: State flag not found.
            InvalidData: Invalid input parameters.
        """
        await atuple(
            self.call_stream(
                "update_keyset_state",
                self._make_update_state_requests(
                    updates, snapshot_id=snapshot_id, log_entry_id=log_entry_id, batch_size=batch_size
                ),
            )
        )

    @overload
    async def add_auth_info(
        self,
        keyset_id: KeysetId,
        *,
        snapshot_id: Optional[SnapshotId] = None,
        ttl: int = 0,
        log_entry_id: Optional[str] = None,
        by_registrar: Optional[str] = None,
    ) -> str: ...

    @overload
    async def add_auth_info(
        self,
        keyset_id: KeysetId,
        *,
        snapshot_id: Optional[SnapshotId] = None,
        auth_info: str,
        ttl: int = 0,
        log_entry_id: Optional[str] = None,
        by_registrar: Optional[str] = None,
    ) -> None: ...

    async def add_auth_info(
        self,
        keyset_id: KeysetId,
        *,
        snapshot_id: Optional[SnapshotId] = None,
        auth_info: Optional[str] = None,
        ttl: int = 0,
        log_entry_id: Optional[str] = None,
        by_registrar: Optional[str] = None,
    ) -> Optional[str]:
        """Add new auth info to the object.

        Args:
            keyset_id: Keyset ID.
            snapshot_id: The snapshot identification.
            auth_info: Auth info to set. Will be generated if not provided.
            ttl: Lifetime of the auth info in seconds. If 0 (default), server default is used.
            log_entry_id: Identification of logger entry for the action.
            by_registrar: Handle of the registrar upon whose regard the action is performed.

        Raises:
            KeysetDoesNotExist: If keyset does not exist.
            InvalidData: Invalid input parameters.
            InvalidSnapshot: If snapshot_id is provided, but not longer valid.
            RegistrarDoesNotExist: Provided registrar does not exist.
        """
        request = AddKeysetAuthInfoRequest()
        request.keyset_id.uuid.value = keyset_id
        if snapshot_id is not None:
            request.keyset_snapshot_id.value = snapshot_id
        if auth_info is not None:
            request.auth_info = auth_info
        else:
            request.generate.CopyFrom(Empty())
        request.ttl = ttl
        if log_entry_id is not None:
            request.log_entry_id.value = log_entry_id
        if by_registrar is not None:
            request.by_registrar.value = by_registrar
        result = cast(Optional[str], await self.call("add_keyset_auth_info", request))
        return result

"""Contact client for registry service."""

import sys
import warnings
from collections.abc import AsyncIterable, AsyncIterator, Mapping, Sequence
from datetime import date, datetime
from enum import IntEnum, unique
from typing import Any, Optional, Union, cast, overload

import grpc
from asyncstdlib import batched as abatched, enumerate as aenumerate, iter as aiter, tuple as atuple
from fred_api.registry.contact.add_contact_auth_info_types_pb2 import AddContactAuthInfoRequest
from fred_api.registry.contact.contact_common_types_pb2 import (
    ContactAdditionalIdentifier as AdditionalIdentifierMessage,
    WarningLetter as WarningLetterMessage,
)
from fred_api.registry.contact.contact_history_types_pb2 import ContactHistoryReply, ContactHistoryRequest
from fred_api.registry.contact.contact_info_types_pb2 import ContactIdRequest, ContactInfoRequest
from fred_api.registry.contact.contact_state_history_types_pb2 import (
    ContactStateHistoryReply,
    ContactStateHistoryRequest,
)
from fred_api.registry.contact.contact_state_types_pb2 import ContactStateRequest
from fred_api.registry.contact.create_contact_types_pb2 import (
    ContactAlreadyExists as ContactAlreadyExistsMessage,
    ContactHandleInProtectionPeriod as ContactHandleInProtectionPeriodMessage,
    CreateContactRequest,
)
from fred_api.registry.contact.list_merge_candidates_types_pb2 import ListMergeCandidatesRequest
from fred_api.registry.contact.merge_contacts_types_pb2 import MergeContactsRequest
from fred_api.registry.contact.service_contact_grpc_pb2_grpc import ContactStub
from fred_api.registry.contact.update_contact_state_stream_types_pb2 import UpdateContactStateStreamRequest
from fred_api.registry.contact.update_contact_state_types_pb2 import (
    UpdateContactStateReply,
    UpdateContactStateRequest,
)
from fred_api.registry.contact.update_contact_types_pb2 import UpdateContactRequest
from fred_types import BaseModel, ContactHistoryId, ContactId, ContactRef, RegistrarRef, SnapshotId
from google.protobuf.empty_pb2 import Empty
from google.protobuf.message import Message
from pydantic import ConfigDict, Field

from .common import (
    Address,
    ListPage,
    ObjectEvents,
    ObjectHistory,
    ObjectHistoryItem,
    ObjectStateHistoryItem,
    PaginationReply,
    RegistryDecoder,
    _encode_order_by,
    set_request_pagination,
)
from .constants import DELETE, DeleteType
from .exceptions import (
    ContactAlreadyExists,
    ContactHandleInProtectionPeriod,
    InvalidHistoryInterval,
    NotCurrentVersion,
)
from .object import ObjectStateFlag, RegistryObjectClient, StateUpdate
from .utils import AnyIterable

try:
    from enum import StrEnum  # type: ignore[attr-defined, unused-ignore]
except ImportError:  # pragma: no cover
    from backports.strenum import StrEnum  # type: ignore[assignment, no-redef, unused-ignore]

# Pydantic requires TypedDict from typing_extensions in python < 3.12.
if sys.version_info < (3, 12):  # pragma: no cover
    from typing_extensions import TypedDict
else:  # pragma: no cover
    from typing import TypedDict


@unique
class WarningLetterPreference(IntEnum):
    """Enum with warning letter results."""

    TO_SEND = 0
    NOT_TO_SEND = 1
    NOT_SPECIFIED = 2


@unique
class ContactIdentifierType(StrEnum):
    """Contact identifier type enum."""

    # Annotations forced because of backports.strenum. Remove when not needed.
    NATIONAL_IDENTITY_NUMBER: "ContactIdentifierType" = "national_identity_number"  # type: ignore[assignment, misc]
    NATIONAL_IDENTITY_CARD: "ContactIdentifierType" = "national_identity_card"  # type: ignore[assignment, misc]
    PASSPORT_NUMBER: "ContactIdentifierType" = "passport_number"  # type: ignore[assignment, misc]
    COMPANY_REGISTRATION_NUMBER: "ContactIdentifierType" = "company_registration_number"  # type: ignore[assignment, misc]
    SOCIAL_SECURITY_NUMBER: "ContactIdentifierType" = "social_security_number"  # type: ignore[assignment, misc]
    BIRTHDATE: "ContactIdentifierType" = "birthdate"  # type: ignore[assignment, misc]


class AdditionalIdentifier(BaseModel):
    """Additional contact identifier."""

    type: ContactIdentifierType
    value: str

    @property
    def birthdate(self) -> Optional[date]:
        """Return birthdate as a date if possible.

        Returns: Birthdate as a date object or None if the identifier is not a birthdate.

        Raises:
            ValueError: If the identifier has birthdate type, but invalid value.
                        Legacy data in the registry may not have a valid format.
                        Such cases must be handled separately, according to the application.
        """
        if self.type != ContactIdentifierType.BIRTHDATE or not self.value:
            return None
        try:
            return date.fromisoformat(self.value)
        except ValueError as error:
            raise ValueError("{!r} is not a valid date.".format(self.value)) from error


class PublishFlags(TypedDict, total=False):
    """Represents publish flags."""

    name: bool
    organization: bool
    place: bool
    telephone: bool
    fax: bool
    emails: bool
    notify_emails: bool
    vat_identification_number: bool
    additional_identifier: bool


class Contact(BaseModel):
    """Contact info."""

    contact_id: ContactId
    contact_handle: str
    contact_history_id: Optional[ContactHistoryId] = None
    snapshot_id: Optional[SnapshotId] = None
    sponsoring_registrar: Optional[str] = None
    name: str = ""
    organization: str = ""
    place: Optional[Address] = None
    telephone: Optional[str] = None
    fax: Optional[str] = None
    emails: list[str] = []
    notify_emails: list[str] = []
    vat_identification_number: Optional[str] = None
    additional_identifier: Optional[AdditionalIdentifier] = None
    publish: PublishFlags = {}
    mailing_address: Optional[Address] = None
    billing_address: Optional[Address] = None
    shipping_address1: Optional[Address] = None
    shipping_address2: Optional[Address] = None
    shipping_address3: Optional[Address] = None
    warning_letter: WarningLetterPreference = WarningLetterPreference.NOT_SPECIFIED
    auth_info: Optional[str] = None
    events: Optional[ObjectEvents] = None


class MergeCandidate(BaseModel):
    """Candidate for a contact merge."""

    model_config = ConfigDict(frozen=True)

    contact: ContactRef
    registrar: RegistrarRef = Field(validation_alias="sponsoring_registrar")
    domain_count: int = 0
    keyset_count: int = 0
    nsset_count: int = 0


class ContactDecoder(RegistryDecoder):
    """Contact service decoder."""

    def __init__(self) -> None:
        super().__init__()
        self.set_decoder(AdditionalIdentifierMessage, self._decode_additional_identifier)
        self.set_decoder(WarningLetterMessage, self._decode_warning_letter)
        self.set_exception_decoder(ContactAlreadyExistsMessage, ContactAlreadyExists)
        self.set_exception_decoder(ContactHandleInProtectionPeriodMessage, ContactHandleInProtectionPeriod)
        self.set_exception_decoder(ContactHistoryReply.Exception.InvalidHistoryInterval, InvalidHistoryInterval)
        self.set_exception_decoder(ContactStateHistoryReply.Exception.InvalidHistoryInterval, InvalidHistoryInterval)
        self.set_exception_decoder(UpdateContactStateReply.Exception.NotCurrentVersion, NotCurrentVersion)

    def _decode_additional_identifier(self, value: AdditionalIdentifierMessage) -> Optional[AdditionalIdentifier]:
        """Decode additional identifier."""
        field = value.WhichOneof("Variant")
        if field is not None:
            return AdditionalIdentifier(type=ContactIdentifierType(field), value=self.decode(getattr(value, field)))
        else:
            return None

    def _decode_warning_letter(self, value: WarningLetterMessage) -> WarningLetterPreference:
        """Decode WarningLetter."""
        return WarningLetterPreference(value.preference)


def _set_address_message(message: Message, address: Optional[Address]) -> None:
    if address:
        message.street.extend(address.street)  # type: ignore[attr-defined]
        message.city = address.city  # type: ignore[attr-defined]
        message.state_or_province = address.state_or_province  # type: ignore[attr-defined]
        if address.postal_code:
            message.postal_code.value = address.postal_code  # type: ignore[attr-defined]
        if address.country_code:
            message.country_code.value = address.country_code  # type: ignore[attr-defined]
        if address.company:
            message.company = address.company  # type: ignore[attr-defined]


class ContactClient(RegistryObjectClient[ContactId, ContactHistoryId]):
    """Contact service gRPC client."""

    stub_cls = ContactStub
    decoder_cls = ContactDecoder
    service = "Contact"
    object_id = "contact_id"
    object_history_id = "contact_history_id"
    object_snapshot_id = "contact_snapshot_id"
    object_handle = "contact_handle"

    def __init__(self, netloc: str, credentials: Optional[grpc.ChannelCredentials] = None, **kwargs: Any) -> None:
        """Initialize contact client instance.

        Args:
            netloc: Netloc of contact service server.
            credentials: Optional credentials.
            kwargs: Other client arguments.
        """
        super().__init__(netloc, credentials=credentials, **kwargs)

    async def get_id(self, handle: str) -> ContactId:
        """Get contact id.

        Args:
            handle: Contact handle.

        Raises:
            ContactDoesNotExist: If contact does not exist.
        """
        return ContactId(await self.get_object_id(request=ContactIdRequest(), method="get_contact_id", handle=handle))

    async def get_contact_id(self, handle: str) -> ContactId:
        """Get contact id.

        Args:
            handle: Contact handle.

        Raises:
            ContactDoesNotExist: If contact does not exist.
        """
        warnings.warn("Method get_contact_id is deprecated in favor of get_id.", DeprecationWarning, stacklevel=2)
        return await self.get_id(handle)

    async def get(
        self,
        contact_id: ContactId,
        history_id: Optional[ContactHistoryId] = None,
        *,
        snapshot_id: Optional[SnapshotId] = None,
    ) -> Contact:
        """Get contact info.

        Args:
            contact_id: Contact ID.
            history_id: Contact history ID.
            snapshot_id: Snapshot identification.

        Raises:
            ContactDoesNotExist: If contact does not exist.
            InvalidSnapshot: If snapshot_id is provided, but not longer valid.
        """
        result = await self.get_object_info(
            request=ContactInfoRequest(),
            method="get_contact_info",
            object_id=contact_id,
            history_id=history_id,
            snapshot_id=snapshot_id,
        )
        return Contact(**result)

    async def get_contact_info(self, contact_id: ContactId, history_id: Optional[ContactHistoryId] = None) -> Contact:
        """Get contact info.

        Args:
            contact_id: Contact ID.
            history_id: Contact history ID.

        Raises:
            ContactDoesNotExist: If contact does not exist.
        """
        warnings.warn("Method get_contact_info is deprecated in favor of get.", DeprecationWarning, stacklevel=2)
        return await self.get(contact_id, history_id)

    async def get_state(
        self, contact_id: ContactId, *, internal: bool = False, snapshot_id: Optional[SnapshotId] = None
    ) -> frozenset[str]:
        """Get contact state consisting of active state flags.

        Args:
            contact_id: Contact ID.
            internal: Whether to include internal states in the result.
            snapshot_id: Snapshot identification.

        Returns:
            Set of active state flags.

        Raises:
            ContactDoesNotExist: If contact does not exist.
            InvalidSnapshot: If snapshot_id is provided, but not longer valid.
        """
        state = await self.get_object_state(
            request=ContactStateRequest(),
            method="get_contact_state",
            object_id=contact_id,
            internal=internal,
            snapshot_id=snapshot_id,
        )
        return frozenset(name for (name, enabled) in state.items() if enabled is True)

    async def get_contact_state(self, contact_id: ContactId, *, internal: Optional[bool] = None) -> dict[str, bool]:
        """Get contact state consisting of state flags and their boolean values.

        Args:
            contact_id: Contact ID.
            internal: Whether to include internal states in the result.

        Raises:
            ContactDoesNotExist: If contact does not exist.
        """
        warnings.warn("Method get_contact_state is deprecated in favor of get_state.", DeprecationWarning, stacklevel=2)
        if internal is None:  # pragma: no cover
            warnings.warn(
                "Default value of internal flag will change to False in regal 2.0.", DeprecationWarning, stacklevel=2
            )
            internal = True

        return await self.get_object_state(
            request=ContactStateRequest(), method="get_contact_state", object_id=contact_id, internal=internal
        )

    async def get_history(
        self,
        contact_id: ContactId,
        start: Optional[Union[ContactHistoryId, datetime]] = None,
        end: Optional[Union[ContactHistoryId, datetime]] = None,
    ) -> ObjectHistory[ObjectHistoryItem]:
        """Get contact history timeline.

        Args:
            contact_id: Contact ID.
            start: Datetime or history_id of history interval start.
            end: Datetime or history_id of history interval end.

        Raises:
            ContactDoesNotExist: If contact does not exist.
            InvalidHistoryInterval: In case of invalid history interval.
        """
        return await self.get_object_history(
            request=ContactHistoryRequest(),
            method="get_contact_history",
            object_id=contact_id,
            start=start,
            end=end,
        )

    async def get_contact_history(
        self,
        contact_id: ContactId,
        start: Optional[Union[ContactHistoryId, datetime]] = None,
        end: Optional[Union[ContactHistoryId, datetime]] = None,
    ) -> dict[str, Any]:
        """Get contact history timeline.

        Args:
            contact_id: Contact ID.
            start: Datetime or history_id of history interval start.
            end: Datetime or history_id of history interval end.

        Raises:
            ContactDoesNotExist: If contact does not exist.
            InvalidHistoryInterval: In case of invalid history interval.
        """
        warnings.warn(
            "Method get_contact_history is deprecated in favor of get_history.", DeprecationWarning, stacklevel=2
        )
        return await self.get_history_data(
            request=ContactHistoryRequest(),
            method="get_contact_history",
            object_id=contact_id,
            start=start,
            end=end,
        )

    async def get_state_history(
        self,
        contact_id: ContactId,
        start: Optional[Union[ContactHistoryId, datetime]] = None,
        end: Optional[Union[ContactHistoryId, datetime]] = None,
    ) -> ObjectHistory[ObjectStateHistoryItem]:
        """Get contact state history timeline.

        Args:
            contact_id: Contact ID.
            start: Datetime or history_id of history interval start.
            end: Datetime or history_id of history interval end.

        Raises:
            ContactDoesNotExist: If contact does not exist.
            InvalidHistoryInterval: In case of invalid history interval.
        """
        result = await self.get_object_state_history(
            request=ContactStateHistoryRequest(),
            method="get_contact_state_history",
            object_id=contact_id,
            start=start,
            end=end,
        )
        result.pop(self.object_id, None)
        return ObjectHistory[ObjectStateHistoryItem](**result)

    async def get_contact_state_history(
        self,
        contact_id: ContactId,
        start: Optional[Union[ContactHistoryId, datetime]] = None,
        end: Optional[Union[ContactHistoryId, datetime]] = None,
    ) -> dict[str, Any]:
        """Get contact state history timeline.

        Args:
            contact_id: Contact ID.
            start: Datetime or history_id of history interval start.
            end: Datetime or history_id of history interval end.

        Raises:
            ContactDoesNotExist: If contact does not exist.
            InvalidHistoryInterval: In case of invalid history interval.
        """
        warnings.warn(
            "Method get_contact_state_history is deprecated in favor of get_state_history.",
            DeprecationWarning,
            stacklevel=2,
        )
        return await self.get_object_state_history(
            request=ContactStateHistoryRequest(),
            method="get_contact_state_history",
            object_id=contact_id,
            start=start,
            end=end,
        )

    async def get_state_flags(self) -> dict[str, ObjectStateFlag]:
        """Get object state flags and their properties."""
        return await self.get_object_state_flags(method="get_contact_state_flags")  # type: ignore[call-overload]

    async def list(self) -> AsyncIterable[ContactRef]:
        """Iterate through all registered contacts.

        Returns:
            Async iterable over contact refs.
        """
        # XXX: Bug in python 3.9 coverage
        async for refs in self.call_stream("list_contacts", Empty()):  # pragma: no cover
            for ref in refs:
                yield ref

    async def create(
        self, contact: Contact, *, log_entry_id: Optional[str] = None, by_registrar: Optional[str] = None
    ) -> ContactId:
        """Create a new contact.

        Args:
            contact: Contact to be created.
            log_entry_id: An identifier of the related log entry.
            by_registrar: Handle of the registrar upon whose regard the creation is performed.

        Returns:
            Contact identifier

        Raises:
            ContactAlreadyExists: Contact with the specified handle already exist.
            InvalidData: Invalid input parameter.
            ContactHandleInProtectionPeriod: Contact with the same handle was recently deleted.
            RegistrarDoesNotExist: Registrar does not exist
        """
        request = CreateContactRequest()
        request.contact_handle.value = contact.contact_handle
        request.name = contact.name
        request.organization = contact.organization
        _set_address_message(request.place, contact.place)
        if contact.telephone:
            request.telephone.value = contact.telephone
        if contact.fax:
            request.fax.value = contact.fax
        for email in contact.emails:
            request.emails.add(value=email)
        for email in contact.notify_emails:
            request.notify_emails.add(value=email)
        if contact.vat_identification_number:
            request.vat_identification_number.value = contact.vat_identification_number
        if contact.additional_identifier:
            getattr(
                request.additional_identifier, contact.additional_identifier.type
            ).value = contact.additional_identifier.value
        request.disclose.update(contact.publish)
        request.warning_letter.preference = contact.warning_letter.value
        _set_address_message(request.mailing_address, contact.mailing_address)
        _set_address_message(request.billing_address, contact.billing_address)
        _set_address_message(request.shipping_address1, contact.shipping_address1)
        _set_address_message(request.shipping_address2, contact.shipping_address2)
        _set_address_message(request.shipping_address3, contact.shipping_address3)
        if log_entry_id:
            request.log_entry_id.value = log_entry_id
        if by_registrar:
            request.by_registrar.value = by_registrar
        return cast(ContactId, await self.call("create_contact", request))

    async def update(  # noqa: C901
        self,
        contact_id: ContactId,
        *,
        snapshot_id: Optional[SnapshotId] = None,
        name: Optional[str] = None,
        organization: Optional[str] = None,
        place: Optional[Address] = None,
        telephone: Optional[str] = None,
        fax: Optional[str] = None,
        emails: Optional[Sequence[str]] = None,
        notify_emails: Optional[Sequence[str]] = None,
        vat_identification_number: Optional[str] = None,
        additional_identifier: Optional[AdditionalIdentifier] = None,
        publish: Optional[PublishFlags] = None,
        mailing_address: Optional[Union[Address, DeleteType]] = None,
        billing_address: Optional[Union[Address, DeleteType]] = None,
        shipping_address1: Optional[Union[Address, DeleteType]] = None,
        shipping_address2: Optional[Union[Address, DeleteType]] = None,
        shipping_address3: Optional[Union[Address, DeleteType]] = None,
        warning_letter: Optional[WarningLetterPreference] = None,
        log_entry_id: Optional[str] = None,
        by_registrar: Optional[str] = None,
    ) -> ContactHistoryId:
        """Update a contact.

        Args:
            contact_id: Contact identification.
            snapshot_id: Contact history identification.
            name: Set new name.
            organization: Set new organization.
            place: Set new address.
            telephone: Set new phone number.
            fax: Set new fax number.
            emails: Set new list of emails.
            notify_emails: Set new list of notify emails.
            vat_identification_number: Set new VAT ID.
            additional_identifier: Set new identifier.
            publish: Set disclose flags.
            mailing_address: Set new mailing address or request delete by `DELETE` singleton.
            billing_address: Set new billing address or request delete by `DELETE` singleton.
            shipping_address1: Set new shipping address 1 or request delete by `DELETE` singleton.
            shipping_address2: Set new shipping address 2 or request delete by `DELETE` singleton.
            shipping_address3: Set new shipping address 3 or request delete by `DELETE` singleton.
            warning_letter: Set warning letter preference.
            log_entry_id: Identifier of logger entry for the action.
            by_registrar: Handle of the registrar upon whose regard the operation is performed.

        Returns:
            History identifier of the updated contact.

        Raises:
            ContactDoesNotExist: Contact not found.
            InvalidSnapshot: Snapshot is not valid.
            RegistrarDoesNotExist: Registrar not found.
        """
        request = UpdateContactRequest()
        request.contact_id.uuid.value = contact_id
        if snapshot_id is not None:
            request.contact_snapshot_id.value = snapshot_id
        if name is not None:
            request.set_name = name
        if organization is not None:
            request.set_organization = organization
        _set_address_message(request.set_place, place)
        if telephone is not None:
            request.set_telephone.value = telephone
        if fax is not None:
            request.set_fax.value = fax
        for email in emails or ():
            request.set_emails.values.add(value=email)
        for email in notify_emails or ():
            request.set_notify_emails.values.add(value=email)
        if vat_identification_number is not None:
            request.set_vat_identification_number.value = vat_identification_number
        if additional_identifier is not None:
            getattr(request.set_additional_identifier, additional_identifier.type).value = additional_identifier.value
        if publish is not None:
            request.set_publish.update(publish)
        if mailing_address is DELETE:
            request.delete_mailing_address.CopyFrom(Empty())
        elif mailing_address is not None:
            _set_address_message(request.set_mailing_address, mailing_address)
        if billing_address is DELETE:
            request.delete_billing_address.CopyFrom(Empty())
        elif billing_address is not None:
            _set_address_message(request.set_billing_address, billing_address)
        if shipping_address1 is DELETE:
            request.delete_shipping_address1.CopyFrom(Empty())
        elif shipping_address1 is not None:
            _set_address_message(request.set_shipping_address1, shipping_address1)
        if shipping_address2 is DELETE:
            request.delete_shipping_address2.CopyFrom(Empty())
        elif shipping_address2 is not None:
            _set_address_message(request.set_shipping_address2, shipping_address2)
        if shipping_address3 is DELETE:
            request.delete_shipping_address3.CopyFrom(Empty())
        elif shipping_address3 is not None:
            _set_address_message(request.set_shipping_address3, shipping_address3)
        if warning_letter is not None:
            request.set_warning_letter.preference = warning_letter.value
        if log_entry_id is not None:
            request.log_entry_id.value = log_entry_id
        if by_registrar is not None:
            request.by_registrar.value = by_registrar
        data = await self.call("update_contact", request)
        return ContactHistoryId(data["contact_history_id"])

    async def _make_update_state_requests(
        self,
        updates: AnyIterable[StateUpdate[ContactId]],
        *,
        snapshot_id: Optional[SnapshotId] = None,
        log_entry_id: Optional[str] = None,
        batch_size: int = 100,
    ) -> AsyncIterator[UpdateContactStateStreamRequest]:
        async for i, batch in aenumerate(abatched(updates, n=batch_size)):
            request = UpdateContactStateStreamRequest()
            for update in batch:
                message = request.updates.add()
                message.contact_id.uuid.value = update.id
                message.set_flags.extend(update.set_flags)
                message.unset_flags.extend(update.unset_flags)
            if i == 0:
                if snapshot_id is not None:
                    request.contact_snapshot_id.value = snapshot_id
                if log_entry_id is not None:
                    request.log_entry_id.value = log_entry_id
            yield request

    @overload
    async def update_state(
        self,
        contact_id: ContactId,
        history_id: ContactHistoryId,
        set_flags: Mapping[str, bool],
        *,
        log_entry_id: Optional[str] = None,
    ) -> None: ...

    @overload
    async def update_state(
        self,
        updates: AnyIterable[StateUpdate[ContactId]],
        *,
        snapshot_id: Optional[SnapshotId] = None,
        log_entry_id: Optional[str] = None,
        batch_size: int = 100,
    ) -> None: ...

    async def update_state(  # type: ignore[misc]
        self,
        contact_id: Optional[ContactId] = None,
        history_id: Optional[ContactHistoryId] = None,
        set_flags: Optional[Mapping[str, bool]] = None,
        *,
        updates: Optional[AnyIterable[StateUpdate[ContactId]]] = None,
        snapshot_id: Optional[SnapshotId] = None,
        log_entry_id: Optional[str] = None,
        batch_size: int = 100,
    ) -> None:
        """Update contact's state.

        Args:
            updates: Iterable of changes.
            snapshot_id: The snapshot identification.
            log_entry_id: Identification of logger entry for the action.
            batch_size: Number of objects in a single stream request.
            contact_id: Contact identification. This argument is deprecated in favor of `updates`.
            history_id: Contact history identification. This argument is deprecated in favor of `snapshot_id`.
            set_flags: Set of flags (name and new value) to update. This argument is deprecated in favor of `updates`.

        Raises:
            ContactDoesNotExist: Contact not found.
            InvalidSnapshot: Snapshot is not valid.
            StateFlagDoesNotExist: State flag not found.
            InvalidData: Invalid input parameters.
            NotCurrentVersion: Contact history identification not valid.
        """
        if isinstance(contact_id, ContactId):
            warnings.warn(
                "Argument `contact_id` is deprecated in favor of `StateUpdate` object.",
                DeprecationWarning,
                stacklevel=2,
            )
            assert history_id is not None  # noqa: S101
            assert set_flags is not None  # noqa: S101
            request = UpdateContactStateRequest()
            request.contact_id.uuid.value = contact_id
            request.contact_history_id.uuid.value = history_id
            for key, value in set_flags.items():
                request.set_flags[key] = value
            if log_entry_id is not None:
                request.log_entry_id.value = log_entry_id
            await self.call("update_contact_state", request)
        else:
            if contact_id is not None and updates is None:  # type: ignore[unreachable]
                # Updates passed as first arg.
                updates = contact_id  # type: ignore[unreachable]
            assert updates is not None  # noqa: S101
            await atuple(
                self.call_stream(
                    "update_contact_state_stream",
                    self._make_update_state_requests(
                        updates, snapshot_id=snapshot_id, log_entry_id=log_entry_id, batch_size=batch_size
                    ),
                )
            )

    async def update_contact_state(
        self,
        contact_id: ContactId,
        history_id: ContactHistoryId,
        set_flags: Mapping[str, bool],
        log_entry_id: Optional[str] = None,
    ) -> None:
        """Update contact's state.

        Args:
            contact_id: Contact identification.
            history_id: Contact history identification.
            set_flags: Set of flags (name and new value) to update.
            log_entry_id: Identification of logger entry for the action.

        Returns:
            none: Operation successfully finished.

        Raises:
            ContactDoesNotExist: Contact not found.
            NotCurrentVersion: Contact history identification not valid.
        """
        warnings.warn(
            "Method update_contact_state is deprecated in favor of update_state.", DeprecationWarning, stacklevel=2
        )
        await self.update_state(contact_id, history_id, set_flags, log_entry_id=log_entry_id)

    @overload
    async def add_auth_info(
        self,
        contact_id: ContactId,
        *,
        snapshot_id: Optional[SnapshotId] = None,
        ttl: int = 0,
        log_entry_id: Optional[str] = None,
        by_registrar: Optional[str] = None,
    ) -> str: ...

    @overload
    async def add_auth_info(
        self,
        contact_id: ContactId,
        *,
        snapshot_id: Optional[SnapshotId] = None,
        auth_info: str,
        ttl: int = 0,
        log_entry_id: Optional[str] = None,
        by_registrar: Optional[str] = None,
    ) -> None: ...

    async def add_auth_info(
        self,
        contact_id: ContactId,
        *,
        snapshot_id: Optional[SnapshotId] = None,
        auth_info: Optional[str] = None,
        ttl: int = 0,
        log_entry_id: Optional[str] = None,
        by_registrar: Optional[str] = None,
    ) -> Optional[str]:
        """Add new auth info to the object.

        Args:
            contact_id: Contact ID.
            snapshot_id: The snapshot identification.
            auth_info: Auth info to set. Will be generated if not provided.
            ttl: Lifetime of the auth info in seconds. If 0 (default), server default is used.
            log_entry_id: Identification of logger entry for the action.
            by_registrar: Handle of the registrar upon whose regard the action is performed.

        Raises:
            ContactDoesNotExist: If contact does not exist.
            InvalidData: Invalid input parameters.
            InvalidSnapshot: If snapshot_id is provided, but not longer valid.
            RegistrarDoesNotExist: Provided registrar does not exist.
        """
        request = AddContactAuthInfoRequest()
        request.contact_id.uuid.value = contact_id
        if snapshot_id is not None:
            request.contact_snapshot_id.value = snapshot_id
        if auth_info is not None:
            request.auth_info = auth_info
        else:
            request.generate.CopyFrom(Empty())
        request.ttl = ttl
        if log_entry_id is not None:
            request.log_entry_id.value = log_entry_id
        if by_registrar is not None:
            request.by_registrar.value = by_registrar
        result = cast(Optional[str], await self.call("add_contact_auth_info", request))
        return result

    async def list_merge_candidates(
        self,
        contact_id: ContactId,
        *,
        order_by: AnyIterable[str] = (),
    ) -> AsyncIterator[MergeCandidate]:
        """Return candidates for contact merge.

        Args:
            contact_id: Contact ID of the target contact.
            order_by: Comma separated list of fields to order by.
                Each field can be prefixed with `+` (asc) or `-` (desc). `+` is optional.

        Returns:
            Iterator of merge candidates.

        Raises:
            ContactDoesNotExist: If contact does not exist.
            InvalidData: Invalid input parameters.
        """
        request = ListMergeCandidatesRequest()
        request.contact_id.uuid.value = contact_id
        request.order_by.extend(await atuple(_encode_order_by(order_by, ListMergeCandidatesRequest.OrderBy)))

        # XXX: Bug in python 3.9 coverage
        async for chunk in self.call_stream("list_merge_candidates", request):  # pragma: no cover
            for candidate in chunk["merge_candidates"]:
                yield MergeCandidate(**candidate)

    async def list_merge_candidates_page(
        self,
        contact_id: ContactId,
        *,
        order_by: AnyIterable[str] = (),
        page_token: Optional[str] = None,
        page_size: Optional[int] = None,
    ) -> ListPage[MergeCandidate]:
        """Return candidates for contact merge.

        Args:
            contact_id: Contact ID of the target contact.
            order_by: Comma separated list of fields to order by.
                Each field can be prefixed with `+` (asc) or `-` (desc). `+` is optional.
            page_token: Page token for pagination.
            page_size: Page size for pagination.

        Returns:
            Merge candidates and pagination info.

        Raises:
            ContactDoesNotExist: If contact does not exist.
            InvalidData: Invalid input parameters.
        """
        request = ListMergeCandidatesRequest()
        request.contact_id.uuid.value = contact_id
        request.order_by.extend(await atuple(_encode_order_by(order_by, ListMergeCandidatesRequest.OrderBy)))
        set_request_pagination(request, page_token=page_token, page_size=page_size)

        results = []
        pagination = None
        # XXX: Bug in python 3.9 coverage
        async for chunk in self.call_stream("list_merge_candidates", request):  # pragma: no cover
            if pagination is None and chunk.get("pagination"):
                pagination = PaginationReply(**chunk.get("pagination"))
            for candidate in chunk["merge_candidates"]:
                results.append(MergeCandidate(**candidate))

        return ListPage(results=tuple(results), pagination=pagination)

    async def merge(
        self, target: ContactId, sources: AnyIterable[ContactId], *, log_entry_id: Optional[str] = None
    ) -> None:
        """Merge source contacts into the target contact.

        Args:
            target: Identifier of the target contact.
            sources: Identifiers of the source contacts.
            log_entry_id: An identifier of the related log entry.

        Raises:
            ContactDoesNotExist: If the target contact doesn't exist.
            InvalidData: Invalid input parameter.
        """
        request = MergeContactsRequest()
        request.target_contact_id.uuid.value = target
        # XXX: Bug in python 3.9 coverage
        async for source in aiter(sources):  # pragma: no cover
            source_msg = request.source_contact_ids.add()
            source_msg.uuid.value = source
        if log_entry_id is not None:
            request.log_entry_id.value = log_entry_id
        await self.call("merge_contacts", request)

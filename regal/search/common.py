"""Search clients for registry service."""

from abc import ABC
from collections.abc import Sequence
from datetime import datetime
from typing import Generic, Optional, TypeVar, Union, cast

from fred_api.registry.contact.service_search_contact_grpc_pb2 import (
    SearchContactHistoryReply,
    SearchContactHistoryRequest,
    SearchContactReply,
    SearchContactRequest,
)
from fred_api.registry.domain.service_search_domain_grpc_pb2 import (
    SearchDomainHistoryReply,
    SearchDomainHistoryRequest,
    SearchDomainReply,
    SearchDomainRequest,
)
from fred_api.registry.keyset.service_search_keyset_grpc_pb2 import (
    SearchKeysetHistoryReply,
    SearchKeysetHistoryRequest,
    SearchKeysetReply,
    SearchKeysetRequest,
)
from fred_api.registry.nsset.service_search_nsset_grpc_pb2 import (
    SearchNssetHistoryReply,
    SearchNssetHistoryRequest,
    SearchNssetReply,
    SearchNssetRequest,
)
from fred_types import BaseModel
from frgal.aio import AsyncGrpcClient

from regal.common import FuzzyValue, RegistryDecoder
from regal.exceptions import InvalidHistoryInterval
from regal.object import ObjectHistoryIdT, ObjectIdT
from regal.utils import AbcAttribute

SearchCurrentRequestType = Union[SearchContactRequest, SearchDomainRequest, SearchKeysetRequest, SearchNssetRequest]
SearchCurrentReplyType = Union[SearchContactReply, SearchDomainReply, SearchKeysetReply, SearchNssetReply]
SearchHistoryRequestType = Union[
    SearchContactHistoryRequest,
    SearchDomainHistoryRequest,
    SearchKeysetHistoryRequest,
    SearchNssetHistoryRequest,
]
SearchHistoryReplyType = Union[
    SearchContactHistoryReply,
    SearchDomainHistoryReply,
    SearchKeysetHistoryReply,
    SearchNssetHistoryReply,
]


class HistorySearchPeriod(BaseModel, Generic[ObjectHistoryIdT]):
    """History search period."""

    valid_from: datetime
    valid_to: Optional[datetime] = None
    matched_items: Sequence[str] = []
    history_ids: Sequence[ObjectHistoryIdT] = []

    @classmethod
    def from_message_dict(cls, data: dict) -> "HistorySearchPeriod":
        """Create HistorySearchPeriod from grpc message dictionary."""
        for ids_type in ("contact_history_ids", "domain_history_ids", "keyset_history_ids", "nsset_history_ids"):
            if ids_type in data:
                history_ids = data.pop(ids_type)
                return cls(**data, history_ids=history_ids)
        return cls(**data)


class BaseSearchResult(BaseModel, Generic[ObjectIdT]):
    """Base search result."""

    object_id: ObjectIdT


class SearchCurrentResult(BaseSearchResult[ObjectIdT], Generic[ObjectIdT]):
    """Generic search result."""

    matched_items: Sequence[str] = []


class SearchHistoryResult(BaseSearchResult[ObjectIdT], Generic[ObjectIdT, ObjectHistoryIdT]):
    """Generic search history result."""

    histories: Sequence[HistorySearchPeriod[ObjectHistoryIdT]] = []


SearchResultT = TypeVar("SearchResultT", bound=BaseSearchResult)


class SearchResults(BaseModel, Generic[SearchResultT]):
    """Set of search results."""

    results: Sequence[SearchResultT] = []
    searched_items: Sequence[str] = []
    estimated_total: Optional[FuzzyValue] = None


class SearchDecoder(RegistryDecoder, Generic[ObjectIdT, ObjectHistoryIdT]):
    """Search service decoder."""

    def __init__(self) -> None:
        super().__init__()
        self.set_exception_decoder(SearchContactHistoryReply.Exception.ChronologyViolation, InvalidHistoryInterval)
        self.set_exception_decoder(SearchDomainHistoryReply.Exception.ChronologyViolation, InvalidHistoryInterval)
        self.set_exception_decoder(SearchKeysetHistoryReply.Exception.ChronologyViolation, InvalidHistoryInterval)
        self.set_exception_decoder(SearchNssetHistoryReply.Exception.ChronologyViolation, InvalidHistoryInterval)

        self.set_decoder(SearchContactReply.Data, self._decode_current_search)
        self.set_decoder(SearchDomainReply.Data, self._decode_current_search)
        self.set_decoder(SearchKeysetReply.Data, self._decode_current_search)
        self.set_decoder(SearchNssetReply.Data, self._decode_current_search)

        self.set_decoder(SearchContactHistoryReply.Data, self._decode_history_search)
        self.set_decoder(SearchDomainHistoryReply.Data, self._decode_history_search)
        self.set_decoder(SearchKeysetHistoryReply.Data, self._decode_history_search)
        self.set_decoder(SearchNssetHistoryReply.Data, self._decode_history_search)

    def _decode_current_search(self, value: SearchCurrentReplyType) -> SearchResults[SearchCurrentResult[ObjectIdT]]:
        return SearchResults(
            results=[
                SearchCurrentResult(
                    object_id=result.object_id.uuid.value,
                    matched_items=self.decode(result.matched_items),
                )
                for result in value.results
            ],
            searched_items=self.decode(value.searched_items),
            estimated_total=self.decode(value.estimated_total),
        )

    def _decode_history_search(
        self, value: SearchHistoryReplyType
    ) -> SearchResults[SearchHistoryResult[ObjectIdT, ObjectHistoryIdT]]:
        results: list[SearchHistoryResult[ObjectIdT, ObjectHistoryIdT]] = []
        for result in value.results:
            results.append(
                SearchHistoryResult(
                    object_id=result.object_id.uuid.value,
                    histories=[
                        HistorySearchPeriod.from_message_dict(self.decode(history)) for history in result.histories
                    ],
                )
            )
        return SearchResults(
            results=results,
            searched_items=self.decode(value.searched_items),
            estimated_total=self.decode(value.estimated_total),
        )


class BaseSearchClient(ABC, AsyncGrpcClient, Generic[ObjectIdT, ObjectHistoryIdT]):
    """Base search service gRPC client."""

    service: str = AbcAttribute
    limit_name: str = AbcAttribute  # Limit name, e.x. `max_number_of_contacts`
    search_method: str = AbcAttribute
    search_history_method: str = AbcAttribute
    current_request_cls: SearchCurrentRequestType = AbcAttribute
    history_request_cls: SearchHistoryRequestType = AbcAttribute

    async def search(
        self,
        query_values: Sequence[str],
        limit: Optional[int] = None,
        searched_items: Optional[Sequence[str]] = None,
    ) -> SearchResults[SearchCurrentResult[ObjectIdT]]:
        """Search for registry objects based on query.

        Args:
            query_values: List of searched patterns.
            limit: Maximum number of returned best matches.
            searched_items: List of object fields that should be included in search.
                If ommited, all the fields will be searched.
        """
        request = self.current_request_cls()
        request.query_values[:] = query_values
        if limit is not None:
            request.limit = limit
        if searched_items is not None:
            request.searched_items[:] = searched_items

        result = await self.call(self.search_method, request)
        return cast(SearchResults[SearchCurrentResult], result)

    async def search_history(
        self,
        query_values: Sequence[str],
        limit: Optional[int] = None,
        searched_items: Optional[Sequence[str]] = None,
        start: Optional[datetime] = None,
        end: Optional[datetime] = None,
    ) -> SearchResults[SearchHistoryResult[ObjectIdT, ObjectHistoryIdT]]:
        """Search for registry objects history versions based on query.

        Args:
            query_values: List of searched patterns.
            limit: Maximum number of returned best matches.
            searched_items: List of object fields that should be included in search.
                If ommited, all the fields will be searched.
            start: Search objects throughout history starting with this datetime.
                If ommited, there is no lower limit on data age.
            end: Search objects throughout history ending with this datetime.
                If ommited, search all the way to current data.

        Raises:
            InvalidHistoryInterval: In case of invalid history interval.
        """
        request = self.history_request_cls()
        request.query_values[:] = query_values
        if limit is not None:
            setattr(request, self.limit_name, limit)
        if searched_items is not None:
            request.searched_items[:] = searched_items
        if start is not None:
            request.data_valid_from.FromDatetime(start)
        if end is not None:
            request.data_valid_to.FromDatetime(end)

        result = await self.call(self.search_history_method, request)
        return cast(SearchResults[SearchHistoryResult[ObjectIdT, ObjectHistoryIdT]], result)

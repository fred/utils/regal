"""Client for SearchNsset service."""

import warnings
from collections.abc import Sequence
from datetime import datetime
from typing import Optional

from fred_api.registry.nsset.service_search_nsset_grpc_pb2 import SearchNssetHistoryRequest, SearchNssetRequest
from fred_api.registry.nsset.service_search_nsset_grpc_pb2_grpc import SearchNssetStub
from fred_types import NssetHistoryId, NssetId

from .common import BaseSearchClient, SearchCurrentResult, SearchDecoder, SearchHistoryResult, SearchResults


class SearchNssetClient(BaseSearchClient[NssetId, NssetHistoryId]):
    """SearchNsset service gRPC client."""

    stub_cls = SearchNssetStub
    decoder_cls = SearchDecoder[NssetId, NssetHistoryId]
    service = "SearchNsset"
    limit_name = "max_number_of_nssets"
    search_method = "search_nsset"
    search_history_method = "search_nsset_history"
    current_request_cls = SearchNssetRequest
    history_request_cls = SearchNssetHistoryRequest

    async def search_nsset(
        self, query_values: Sequence[str], limit: Optional[int] = None, searched_items: Optional[Sequence[str]] = None
    ) -> SearchResults[SearchCurrentResult[NssetId]]:
        """Search for nssets based on query.

        Args:
            query_values: List of strings with searched values. Values can be substrings of some nssets fields.
            limit: Maximum number of returned best results.
            searched_items: List of nsset fields that should be included in search.
                If ommited, all the fields will be searched.
        """
        warnings.warn("Method search_nsset is deprecated in favor of search.", DeprecationWarning, stacklevel=2)
        return await self.search(
            query_values=query_values,
            limit=limit,
            searched_items=searched_items,
        )

    async def search_nsset_history(
        self,
        query_values: Sequence[str],
        limit: Optional[int] = None,
        searched_items: Optional[Sequence[str]] = None,
        start: Optional[datetime] = None,
        end: Optional[datetime] = None,
    ) -> SearchResults[SearchHistoryResult[NssetId, NssetHistoryId]]:
        """Search for nssets history versions based on query.

        Args:
            query_values: List of strings with searched values. Values can be substrings of some nsset fields.
            limit: Maximum number of returned best results.
            searched_items: List of nsset fields that should be included in search.
                If ommited, all the fields will be searched.
            start: Search nsset throughout history starting with this datetime.
                If ommited, there is no lower limit on data age.
            end: Search nsset throughout history ending with this datetime.
                If ommited, search all the way to current data.

        Raises:
            InvalidHistoryInterval: In case of invalid history interval.
        """
        warnings.warn(
            "Method search_nsset_history is deprecated in favor of search_history.", DeprecationWarning, stacklevel=2
        )
        return await self.search_history(
            query_values=query_values,
            limit=limit,
            searched_items=searched_items,
            start=start,
            end=end,
        )

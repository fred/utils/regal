"""Client for SearchContact service."""

import warnings
from collections.abc import Sequence
from datetime import datetime
from typing import Optional

from fred_api.registry.contact.service_search_contact_grpc_pb2 import SearchContactHistoryRequest, SearchContactRequest
from fred_api.registry.contact.service_search_contact_grpc_pb2_grpc import SearchContactStub
from fred_types import ContactHistoryId, ContactId

from .common import BaseSearchClient, SearchCurrentResult, SearchDecoder, SearchHistoryResult, SearchResults


class SearchContactClient(BaseSearchClient[ContactId, ContactHistoryId]):
    """SearchContact service gRPC client."""

    stub_cls = SearchContactStub
    decoder_cls = SearchDecoder[ContactId, ContactHistoryId]
    service = "SearchContact"
    limit_name = "max_number_of_contacts"
    search_method = "search_contact"
    search_history_method = "search_contact_history"
    current_request_cls = SearchContactRequest
    history_request_cls = SearchContactHistoryRequest

    async def search_contact(
        self, query_values: Sequence[str], limit: Optional[int] = None, searched_items: Optional[Sequence[str]] = None
    ) -> SearchResults[SearchCurrentResult[ContactId]]:
        """Search for contacts based on query.

        Args:
            query_values: List of strings with searched values. Values can be substrings of some contacts fields.
            limit: Maximum number of returned best results.
            searched_items: List of contact fields that should be included in search.
                If ommited, all the fields will be searched.
        """
        warnings.warn("Method search_contact is deprecated in favor of search.", DeprecationWarning, stacklevel=2)
        return await self.search(
            query_values=query_values,
            limit=limit,
            searched_items=searched_items,
        )

    async def search_contact_history(
        self,
        query_values: Sequence[str],
        limit: Optional[int] = None,
        searched_items: Optional[Sequence[str]] = None,
        start: Optional[datetime] = None,
        end: Optional[datetime] = None,
    ) -> SearchResults[SearchHistoryResult[ContactId, ContactHistoryId]]:
        """Search for contacts history versions based on query.

        Args:
            query_values: List of strings with searched values. Values can be substrings of some contact fields.
            limit: Maximum number of returned best results.
            searched_items: List of contact fields that should be included in search.
                If ommited, all the fields will be searched.
            start: Search contact throughout history starting with this datetime.
                If ommited, there is no lower limit on data age.
            end: Search contact throughout history ending with this datetime.
                If ommited, search all the way to current data.

        Raises:
            InvalidHistoryInterval: In case of invalid history interval.
        """
        warnings.warn(
            "Method search_contact_history is deprecated in favor of search_history.", DeprecationWarning, stacklevel=2
        )
        return await self.search_history(
            query_values=query_values,
            limit=limit,
            searched_items=searched_items,
            start=start,
            end=end,
        )

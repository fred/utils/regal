"""Client for SearchKeyset service."""

import warnings
from collections.abc import Sequence
from datetime import datetime
from typing import Optional

from fred_api.registry.keyset.service_search_keyset_grpc_pb2 import SearchKeysetHistoryRequest, SearchKeysetRequest
from fred_api.registry.keyset.service_search_keyset_grpc_pb2_grpc import SearchKeysetStub

from regal.keyset import KeysetHistoryId, KeysetId

from .common import BaseSearchClient, SearchCurrentResult, SearchDecoder, SearchHistoryResult, SearchResults


class SearchKeysetClient(BaseSearchClient[KeysetId, KeysetHistoryId]):
    """SearchKeyset service gRPC client."""

    stub_cls = SearchKeysetStub
    decoder_cls = SearchDecoder[KeysetId, KeysetHistoryId]
    service = "SearchKeyset"
    limit_name = "max_number_of_keysets"
    search_method = "search_keyset"
    search_history_method = "search_keyset_history"
    current_request_cls = SearchKeysetRequest
    history_request_cls = SearchKeysetHistoryRequest

    async def search_keyset(
        self, query_values: Sequence[str], limit: Optional[int] = None, searched_items: Optional[Sequence[str]] = None
    ) -> SearchResults[SearchCurrentResult[KeysetId]]:
        """Search for keysets based on query.

        Args:
            query_values: List of strings with searched values. Values can be substrings of some keysets fields.
            limit: Maximum number of returned best results.
            searched_items: List of keyset fields that should be included in search.
                If ommited, all the fields will be searched.
        """
        warnings.warn("Method search_keyset is deprecated in favor of search.", DeprecationWarning, stacklevel=2)
        return await self.search(
            query_values=query_values,
            limit=limit,
            searched_items=searched_items,
        )

    async def search_keyset_history(
        self,
        query_values: Sequence[str],
        limit: Optional[int] = None,
        searched_items: Optional[Sequence[str]] = None,
        start: Optional[datetime] = None,
        end: Optional[datetime] = None,
    ) -> SearchResults[SearchHistoryResult[KeysetId, KeysetHistoryId]]:
        """Search for keysets history versions based on query.

        Args:
            query_values: List of strings with searched values. Values can be substrings of some keyset fields.
            limit: Maximum number of returned best results.
            searched_items: List of keyset fields that should be included in search.
                If ommited, all the fields will be searched.
            start: Search keyset throughout history starting with this datetime.
                If ommited, there is no lower limit on data age.
            end: Search keyset throughout history ending with this datetime.
                If ommited, search all the way to current data.

        Raises:
            InvalidHistoryInterval: In case of invalid history interval.
        """
        warnings.warn(
            "Method search_keyset_history is deprecated in favor of search_history.", DeprecationWarning, stacklevel=2
        )
        return await self.search_history(
            query_values=query_values,
            limit=limit,
            searched_items=searched_items,
            start=start,
            end=end,
        )

"""Client for SearchDomain service."""

import warnings
from collections.abc import Sequence
from datetime import datetime
from typing import Optional

from fred_api.registry.domain.service_search_domain_grpc_pb2 import SearchDomainHistoryRequest, SearchDomainRequest
from fred_api.registry.domain.service_search_domain_grpc_pb2_grpc import SearchDomainStub
from fred_types import DomainHistoryId, DomainId

from .common import BaseSearchClient, SearchCurrentResult, SearchDecoder, SearchHistoryResult, SearchResults


class SearchDomainClient(BaseSearchClient[DomainId, DomainHistoryId]):
    """SearchDomain service gRPC client."""

    stub_cls = SearchDomainStub
    decoder_cls = SearchDecoder[DomainId, DomainHistoryId]
    service = "SearchDomain"
    limit_name = "max_number_of_domains"
    search_method = "search_domain"
    search_history_method = "search_domain_history"
    current_request_cls = SearchDomainRequest
    history_request_cls = SearchDomainHistoryRequest

    async def search_domain(
        self, query_values: Sequence[str], limit: Optional[int] = None, searched_items: Optional[Sequence[str]] = None
    ) -> SearchResults[SearchCurrentResult[DomainId]]:
        """Search for domains based on query.

        Args:
            query_values: List of strings with searched values. Values can be substrings of domain fqdn.
            limit: Maximum number of returned best results.
            searched_items: List of domain fields that should be included in search.
                If ommited, all the fields will be searched.
                The only valid field for domain is `fqdn`.
        """
        warnings.warn("Method search_domain is deprecated in favor of search.", DeprecationWarning, stacklevel=2)
        return await self.search(
            query_values=query_values,
            limit=limit,
            searched_items=searched_items,
        )

    async def search_domain_history(
        self,
        query_values: Sequence[str],
        limit: Optional[int] = None,
        searched_items: Optional[Sequence[str]] = None,
        start: Optional[datetime] = None,
        end: Optional[datetime] = None,
    ) -> SearchResults[SearchHistoryResult[DomainId, DomainHistoryId]]:
        """Search for domains history versions based on query.

        Args:
            query_values: List of strings with searched values. Values can be substrings of domain fqdn.
            limit: Maximum number of returned best results.
            searched_items: List of domain fields that should be included in search.
                If ommited, all the fields will be searched.
                The only valid field for domain is `fqdn`.
            start: Search domain throughout history starting with this datetime.
                If ommited, there is no lower limit on data age.
            end: Search domain throughout history ending with this datetime.
                If ommited, search all the way to current data.

        Raises:
            InvalidHistoryInterval: In case of invalid history interval.
        """
        warnings.warn(
            "Method search_domain_history is deprecated in favor of search_history.", DeprecationWarning, stacklevel=2
        )
        return await self.search_history(
            query_values=query_values,
            limit=limit,
            searched_items=searched_items,
            start=start,
            end=end,
        )

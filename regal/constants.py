"""Registry constants."""

from enum import Enum, unique
from typing import Any, Final, Literal

# Define NOOP singleton to use as a default method argument.
NOOP: Any = object()


@unique
class _Sentinel(Enum):
    """Enum of sentinels.

    Based on https://stackoverflow.com/a/60605919/2440346
    """

    DELETE = object()


DELETE: Final = _Sentinel.DELETE
DeleteType = Literal[_Sentinel.DELETE]

"""Various regal utils."""

from collections.abc import AsyncIterable, Iterable
from typing import Any, TypeVar, Union

import idna

X = TypeVar("X")
AnyIterable = Union[Iterable[X], AsyncIterable[X]]


class AbcAttributeType:
    """Abstract attribute type.

    Use instance of this class as default value of ABC
    attribute in order to enforce class attribute definition.
    """

    __isabstractmethod__ = True


# Abstract attribute singleton
AbcAttribute: Any = AbcAttributeType()


def _idna_encode(fqdn: str) -> str:
    """Encode FQDN to punycode.

    FQDNs which can't be encoded are returned unmodified.
    """
    try:
        # Use idna library to support RFC 5891. Standard library only supports RFC 3490 and RFC 3492.
        return idna.encode(fqdn).decode()
    except UnicodeError:
        # Bad input. Return it and let server deal with it.
        return fqdn

"""Registrar client for registry service."""

import logging
import warnings
from collections.abc import AsyncIterator, Sequence
from datetime import datetime, timedelta, timezone
from decimal import Decimal
from functools import cached_property
from typing import Any, Optional, cast

import grpc
import pytz
from asyncstdlib import list as alist
from cryptography import x509
from cryptography.hazmat.primitives.hashes import MD5
from fred_api.registry.registrar.registrar_certification_types_pb2 import (
    GetRegistrarCertificationsReply,
    GetRegistrarCertificationsRequest,
)
from fred_api.registry.registrar.registrar_common_types_pb2 import (
    Credit,
    RegistrarCertification as RegistrarCertificationMessage,
    RegistrarEppCredentialsId,
    SslCertificate as SslCertificateMessage,
)
from fred_api.registry.registrar.registrar_credit_types_pb2 import RegistrarCreditReply, RegistrarCreditRequest
from fred_api.registry.registrar.registrar_epp_credentials_types_pb2 import (
    GetRegistrarEppCredentialsReply,
    GetRegistrarEppCredentialsRequest,
)
from fred_api.registry.registrar.registrar_group_types_pb2 import (
    GetRegistrarGroupsMembershipReply,
    GetRegistrarGroupsMembershipRequest,
)
from fred_api.registry.registrar.registrar_info_types_pb2 import RegistrarInfoReply, RegistrarInfoRequest
from fred_api.registry.registrar.registrar_zone_access_types_pb2 import (
    RegistrarZoneAccessReply,
    RegistrarZoneAccessRequest,
)
from fred_api.registry.registrar.service_admin_grpc_pb2 import (
    AddRegistrarCertificationReply,
    AddRegistrarCertificationRequest,
    AddRegistrarEppCredentialsReply,
    AddRegistrarEppCredentialsRequest,
    AddRegistrarGroupMembershipReply,
    AddRegistrarGroupMembershipRequest,
    AddRegistrarZoneAccessReply,
    AddRegistrarZoneAccessRequest,
    CreateRegistrarReply,
    CreateRegistrarRequest,
    DeleteRegistrarCertificationReply,
    DeleteRegistrarCertificationRequest,
    DeleteRegistrarEppCredentialsReply,
    DeleteRegistrarEppCredentialsRequest,
    DeleteRegistrarGroupMembershipReply,
    DeleteRegistrarGroupMembershipRequest,
    DeleteRegistrarZoneAccessReply,
    DeleteRegistrarZoneAccessRequest,
    UpdateRegistrarCertificationReply,
    UpdateRegistrarCertificationRequest,
    UpdateRegistrarEppCredentialsReply,
    UpdateRegistrarEppCredentialsRequest,
    UpdateRegistrarReply,
    UpdateRegistrarRequest,
    UpdateRegistrarZoneAccessReply,
    UpdateRegistrarZoneAccessRequest,
)
from fred_api.registry.registrar.service_admin_grpc_pb2_grpc import AdminStub
from fred_api.registry.registrar.service_registrar_grpc_pb2_grpc import RegistrarStub
from fred_types import BaseId, BaseModel, RegistrarId
from frgal.aio import AsyncGrpcClient
from google.protobuf.empty_pb2 import Empty
from pydantic import field_validator, model_validator

from .common import Address, FileId, RegistryDecoder
from .constants import NOOP
from .exceptions import (
    EppCredentialsDoesNotExist,
    GroupDoesNotExist,
    HistoryChangeProhibited,
    InvalidData,
    RegistrarAlreadyExists,
    RegistrarCertificationDoesNotExist,
    RegistrarDoesNotExist,
)

logger = logging.getLogger(__name__)


class Registrar(BaseModel):
    """Registrar info.

    Attributes:
        is_internal: Whether the registrar is internal.
    """

    registrar_id: RegistrarId
    registrar_handle: str
    name: str
    organization: str = ""
    place: Optional[Address] = None

    telephone: Optional[str] = None
    fax: Optional[str] = None
    emails: Sequence[str] = []
    url: Optional[str] = None
    company_registration_number: Optional[str] = None
    vat_identification_number: Optional[str] = None

    is_system_registrar: bool = False
    is_internal: bool = False

    variable_symbol: str = ""
    payment_memo_regex: str = ""
    is_vat_payer: bool = False

    @model_validator(mode="before")
    @classmethod
    def default_registrar_id(cls, data: Any) -> Any:
        """Prefill registrar id with handle if it's not defined."""
        if isinstance(data, dict):
            if "registrar_id" not in data:
                data["registrar_id"] = RegistrarId(data.get("registrar_handle"))
        return data


class ZoneAccess(BaseModel):
    """Registrar zone access."""

    zone: str
    valid_from: Optional[datetime] = None
    valid_to: Optional[datetime] = None

    def is_active(self) -> bool:
        """Return whether zone access is currently active.

        If `valid_from` is not set, there is no lower bound.
        If `valid_to` is not set, there is no upper bound.
        """
        now = datetime.now(tz=timezone.utc)
        valid_from = self.valid_from or now
        valid_to = self.valid_to or now + timedelta(seconds=1)

        if valid_from <= now < valid_to:
            return True
        else:
            return False


class EppCredentialsId(BaseId):
    """EPP credentials ID."""


class SslCertificate(BaseModel):
    """SSL certificate used for registrar access."""

    fingerprint: str  # in hex format with optional colon separator
    cert_data_pem: str = ""

    @field_validator("fingerprint")
    def _check_fingerprint_format(cls, v: str) -> str:
        bytes.fromhex(v.replace(":", ""))
        return v

    @model_validator(mode="after")
    def _check_cert_fingerprint(self) -> "SslCertificate":
        if self.cert_data_pem:
            try:
                cert = x509.load_pem_x509_certificate(self.cert_data_pem.encode("utf-8"))
            except ValueError:
                logger.warning("Invalid certificate data with fingerprint %s", self.fingerprint)
            else:
                computed_fingerprint = cert.fingerprint(MD5())  # noqa: S303
                if self.fingerprint and bytes.fromhex(self.fingerprint.replace(":", "")) != computed_fingerprint:
                    logger.warning(
                        "Fingerprint %s does not match computed fingerprint %s",
                        self.fingerprint,
                        computed_fingerprint.hex(":"),
                    )
        return self

    @classmethod
    def from_pem(cls, cert_data_pem: str) -> "SslCertificate":
        """Create SslCertificate from PEM data.

        Args:
            cert_data_pem: Certificate PEM data.

        Returns:
            SslCertificate with computed fingerprint.

        Raises:
            ValueError: If certificate is invalid.
        """
        cert = x509.load_pem_x509_certificate(cert_data_pem.encode("utf-8"))
        fingerprint = cert.fingerprint(MD5()).hex(":")  # noqa: S303
        return cls(fingerprint=fingerprint, cert_data_pem=cert_data_pem)

    @cached_property
    def _cert(self) -> Optional[x509.Certificate]:
        if self.cert_data_pem:
            try:
                return x509.load_pem_x509_certificate(self.cert_data_pem.encode("utf-8"))
            except ValueError:
                pass  # In case of invalid certificate return None
        return None

    @property
    def not_valid_before(self) -> Optional[datetime]:
        """Return datetime before which the certificate is not valid."""
        return pytz.utc.localize(self._cert.not_valid_before) if self._cert else None

    @property
    def not_valid_after(self) -> Optional[datetime]:
        """Return datetime after which the certificate is not valid."""
        return pytz.utc.localize(self._cert.not_valid_after) if self._cert else None

    @property
    def issuer(self) -> Optional[str]:
        """Return issuer as RFC4514 string."""
        return self._cert.issuer.rfc4514_string() if self._cert else None

    @property
    def subject(self) -> Optional[str]:
        """Return subject as RFC4514 string."""
        return self._cert.subject.rfc4514_string() if self._cert else None


class EppCredentials(BaseModel):
    """Registrar's EPP credentials.

    Attributes:
        id: Unique identifier of EPP credentials
        create_time: Creation date and time
        certificate: SSL certificate for registrar's access
    """

    id: EppCredentialsId
    create_time: datetime
    certificate: SslCertificate


class RegistrarCertificationId(BaseId):
    """Unique identifier of registrar certification."""


class RegistrarCertification(BaseModel):
    """Registrar certification data.

    Attributes:
        certification_id: Unique identifier of certification
        registrar_handle: Handle of the registrar
        valid_from: Start of the certification validity
        valid_to: End of the certification validity ('None' for unlimited)
        classification: Value 0 to 100
        file_id: Document (in PDF) with certification details
    """

    certification_id: RegistrarCertificationId
    registrar_handle: str
    valid_from: datetime
    valid_to: Optional[datetime] = None
    classification: int
    file_id: FileId


class RegistrarDecoder(RegistryDecoder):
    """Registrar service decoder."""

    def __init__(self) -> None:
        super().__init__()
        self.set_decoder(Credit, self._decode_credit)
        self.set_decoder(RegistrarEppCredentialsId, self._decode_epp_credentials_id)
        self.set_decoder(RegistrarCertificationMessage, self._decode_registrar_certification)
        self.set_decoder(SslCertificateMessage, self._decode_ssl_certificate)

        self.set_exception_decoder(RegistrarInfoReply.Exception.RegistrarDoesNotExist, RegistrarDoesNotExist)
        self.set_exception_decoder(RegistrarCreditReply.Exception.RegistrarDoesNotExist, RegistrarDoesNotExist)
        self.set_exception_decoder(RegistrarZoneAccessReply.Exception.RegistrarDoesNotExist, RegistrarDoesNotExist)

        self.set_exception_decoder(AddRegistrarZoneAccessReply.Exception.RegistrarDoesNotExist, RegistrarDoesNotExist)
        self.set_exception_decoder(AddRegistrarZoneAccessReply.Exception.ZoneDoesNotExist, ValueError)
        self.set_exception_decoder(AddRegistrarZoneAccessReply.Exception.InvalidData, InvalidData)
        self.set_exception_decoder(UpdateRegistrarZoneAccessReply.Exception.RegistrarZoneAccessDoesNotExist, ValueError)
        self.set_exception_decoder(
            UpdateRegistrarZoneAccessReply.Exception.HistoryChangeProhibited,
            HistoryChangeProhibited,
        )
        self.set_exception_decoder(UpdateRegistrarZoneAccessReply.Exception.InvalidData, InvalidData)
        self.set_exception_decoder(DeleteRegistrarZoneAccessReply.Exception.RegistrarZoneAccessDoesNotExist, ValueError)
        self.set_exception_decoder(
            DeleteRegistrarZoneAccessReply.Exception.HistoryChangeProhibited,
            HistoryChangeProhibited,
        )
        self.set_exception_decoder(DeleteRegistrarZoneAccessReply.Exception.InvalidData, InvalidData)
        self.set_exception_decoder(CreateRegistrarReply.Exception.RegistrarAlreadyExists, RegistrarAlreadyExists)
        self.set_exception_decoder(CreateRegistrarReply.Exception.InvalidData, InvalidData)
        self.set_exception_decoder(UpdateRegistrarReply.Exception.RegistrarDoesNotExist, RegistrarDoesNotExist)
        self.set_exception_decoder(UpdateRegistrarReply.Exception.InvalidData, InvalidData)
        self.set_exception_decoder(
            GetRegistrarEppCredentialsReply.Exception.RegistrarDoesNotExist,
            RegistrarDoesNotExist,
        )
        self.set_exception_decoder(
            AddRegistrarEppCredentialsReply.Exception.RegistrarDoesNotExist,
            RegistrarDoesNotExist,
        )
        self.set_exception_decoder(
            AddRegistrarEppCredentialsReply.Exception.RegistrarEppCredentialsDoesNotExist,
            EppCredentialsDoesNotExist,
        )
        self.set_exception_decoder(AddRegistrarEppCredentialsReply.Exception.InvalidData, InvalidData)
        self.set_exception_decoder(
            UpdateRegistrarEppCredentialsReply.Exception.RegistrarEppCredentialsDoesNotExist,
            EppCredentialsDoesNotExist,
        )
        self.set_exception_decoder(UpdateRegistrarEppCredentialsReply.Exception.InvalidData, InvalidData)
        self.set_exception_decoder(
            DeleteRegistrarEppCredentialsReply.Exception.RegistrarEppCredentialsDoesNotExist,
            EppCredentialsDoesNotExist,
        )
        self.set_exception_decoder(DeleteRegistrarEppCredentialsReply.Exception.InvalidData, InvalidData)
        self.set_exception_decoder(
            GetRegistrarGroupsMembershipReply.Exception.RegistrarDoesNotExist,
            RegistrarDoesNotExist,
        )
        self.set_exception_decoder(
            AddRegistrarGroupMembershipReply.Exception.RegistrarDoesNotExist,
            RegistrarDoesNotExist,
        )
        self.set_exception_decoder(
            AddRegistrarGroupMembershipReply.Exception.GroupDoesNotExist,
            GroupDoesNotExist,
        )
        self.set_exception_decoder(
            DeleteRegistrarGroupMembershipReply.Exception.RegistrarDoesNotExist,
            RegistrarDoesNotExist,
        )
        self.set_exception_decoder(
            DeleteRegistrarGroupMembershipReply.Exception.GroupDoesNotExist,
            GroupDoesNotExist,
        )
        self.set_exception_decoder(
            GetRegistrarCertificationsReply.Exception.RegistrarDoesNotExist,
            RegistrarDoesNotExist,
        )
        self.set_exception_decoder(
            AddRegistrarCertificationReply.Exception.RegistrarDoesNotExist,
            RegistrarDoesNotExist,
        )
        self.set_exception_decoder(AddRegistrarCertificationReply.Exception.InvalidData, InvalidData)
        self.set_exception_decoder(
            UpdateRegistrarCertificationReply.Exception.RegistrarCertificationDoesNotExist,
            RegistrarCertificationDoesNotExist,
        )
        self.set_exception_decoder(UpdateRegistrarCertificationReply.Exception.InvalidData, InvalidData)
        self.set_exception_decoder(
            DeleteRegistrarCertificationReply.Exception.RegistrarCertificationDoesNotExist,
            RegistrarCertificationDoesNotExist,
        )
        self.set_exception_decoder(DeleteRegistrarCertificationReply.Exception.InvalidData, InvalidData)

    def _decode_credit(self, value: Credit) -> Optional[Decimal]:
        """Decode registrar credit."""
        if not value.decimal_value:
            return None
        return Decimal(value.decimal_value)

    def _decode_epp_credentials_id(self, value: RegistrarEppCredentialsId) -> EppCredentialsId:
        """Decode EPP credentials ID."""
        return EppCredentialsId(value.value)

    def _decode_registrar_certification(self, value: RegistrarCertificationMessage) -> RegistrarCertification:
        """Decode registrar certification."""
        return RegistrarCertification(**self._decode_message(value))

    def _decode_ssl_certificate(self, value: SslCertificateMessage) -> SslCertificate:
        """Decode SSL certificate."""
        return SslCertificate(fingerprint=value.fingerprint.hex(":"), cert_data_pem=value.cert_data_pem)


class RegistrarClient(AsyncGrpcClient):
    """Registrar service gRPC client."""

    stub_cls = RegistrarStub
    decoder_cls = RegistrarDecoder
    service = "Registrar"

    def __init__(self, netloc: str, credentials: Optional[grpc.ChannelCredentials] = None, **kwargs: Any) -> None:
        """Initialize Registrar client instance.

        Args:
            netloc: Netloc of registrar service server.
            credentials: Optional credentials.
            kwargs: Other client arguments.
        """
        super().__init__(netloc, credentials=credentials, **kwargs)

    async def list_handles(self) -> AsyncIterator[str]:
        """Get list of all registrar handles."""
        for handle in await self.call("get_registrar_handles", Empty()):
            yield handle

    async def get_registrar_handles(self) -> list[str]:
        """Get list of all registrar handles."""
        warnings.warn(
            "Method get_registrar_handles is deprecated in favor of list_handles.", DeprecationWarning, stacklevel=2
        )
        return await alist(self.list_handles())

    async def get(self, registrar_handle: str) -> Registrar:
        """Get registrar info by handle."""
        request = RegistrarInfoRequest()
        request.registrar_handle.value = registrar_handle
        result = await self.call("get_registrar_info", request)
        return Registrar(**result)

    async def get_registrar_info(self, registrar_handle: str) -> Registrar:
        """Get registrar info by handle."""
        warnings.warn("Method get_registrar_info is deprecated in favor of get.", DeprecationWarning, stacklevel=2)
        return await self.get(registrar_handle)

    async def get_credit(self, registrar_handle: str) -> dict[str, Decimal]:
        """Get registrar credit by handle.

        Args:
            registrar_handle: Registrar handle.

        Returns:
            Dictionary with (zone, credit in zone) pairs.
        """
        request = RegistrarCreditRequest()
        request.registrar_handle.value = registrar_handle
        result = await self.call("get_registrar_credit", request)
        return cast(dict[str, Decimal], result["credit_by_zone"])

    async def get_registrar_credit(self, registrar_handle: str) -> dict[str, Decimal]:
        """Get registrar credit by handle.

        Args:
            registrar_handle: Registrar handle.

        Returns:
            Dictionary with (zone, credit in zone) pairs.
        """
        warnings.warn(
            "Method get_registrar_credit is deprecated in favor of get_credit.", DeprecationWarning, stacklevel=2
        )
        return await self.get_credit(registrar_handle)

    async def list_zone_access(self, registrar_handle: str) -> AsyncIterator[ZoneAccess]:
        """Get registrar zone access (with full history).

        Args:
            registrar_handle: Registrar handle.

        Returns:
            Iterator of zone access items.
        """
        request = RegistrarZoneAccessRequest()
        request.registrar_handle.value = registrar_handle
        result = await self.call("get_registrar_zone_access", request)
        for zone, access in result["access_by_zone"].items():
            for i in access["history"]:
                yield ZoneAccess(zone=zone, valid_from=i["valid_from"], valid_to=i["valid_to"])

    async def get_registrar_zone_access(self, registrar_handle: str) -> dict[str, bool]:
        """Get registrar zone access.

        Args:
            registrar_handle: Registrar handle.

        Returns:
            Dictionary with (zone, can access zone) pairs.
        """
        warnings.warn(
            "Method get_registrar_zone_access is deprecated in favor of list_zone_access.",
            DeprecationWarning,
            stacklevel=2,
        )
        request = RegistrarZoneAccessRequest()
        request.registrar_handle.value = registrar_handle
        result = await self.call("get_registrar_zone_access", request)
        return {zone: access["has_access_now"] for zone, access in result["access_by_zone"].items()}

    async def get_registrar_zone_access_history(self, registrar_handle: str) -> dict[str, list[ZoneAccess]]:
        """Get registrar zone access history.

        Args:
            registrar_handle: Registrar handle.

        Returns:
            Dictionary with zones as keys and list of zone accesses as values.
        """
        warnings.warn(
            "Method get_registrar_zone_access_history is deprecated in favor of list_zone_access.",
            DeprecationWarning,
            stacklevel=2,
        )
        request = RegistrarZoneAccessRequest()
        request.registrar_handle.value = registrar_handle
        result = await self.call("get_registrar_zone_access", request)
        zone_access = result["access_by_zone"]
        for zone, access in zone_access.items():
            zone_access[zone] = [
                ZoneAccess(zone=zone, valid_from=acc["valid_from"], valid_to=acc["valid_to"])
                for acc in access["history"]
            ]
        return cast(dict[str, list[ZoneAccess]], zone_access)

    async def list_epp_credentials(self, registrar_handle: str) -> AsyncIterator[EppCredentials]:
        """Get registrar EPP credentials.

        Args:
            registrar_handle: Registrar handle.

        Returns:
            Iterator of EPP credentials records.
        """
        request = GetRegistrarEppCredentialsRequest()
        request.registrar_handle.value = registrar_handle
        result = await self.call("get_registrar_epp_credentials", request)
        for item in result["credentials"]:
            yield EppCredentials(
                id=item["credentials_id"], create_time=item["create_time"], certificate=item["certificate"]
            )

    async def get_registrar_epp_credentials(self, registrar_handle: str) -> list[dict[str, Any]]:
        """Get registrar EPP credentials.

        Args:
            registrar_handle: Registrar handle.

        Returns:
            List of dictionaries with keys `credentials_id`, `create_time` and `certificate`.
        """
        warnings.warn(
            "Method get_registrar_epp_credentials is deprecated in favor of list_epp_credentials.",
            DeprecationWarning,
            stacklevel=2,
        )
        request = GetRegistrarEppCredentialsRequest()
        request.registrar_handle.value = registrar_handle
        result = await self.call("get_registrar_epp_credentials", request)
        return cast(list[dict[str, Any]], result.get("credentials", []))

    async def list_groups(self) -> AsyncIterator[str]:
        """Get list of all available registrar groups.

        Args:
            none

        Returns:
            Iterator of registrar groups (possibly empty).
        """
        for group in await self.call("get_registrar_groups", Empty()) or []:
            yield group

    async def get_registrar_groups(self) -> list[str]:
        """Get list of all available registrar groups.

        Args:
            none

        Returns:
            List of registrar groups (possibly empty).
        """
        warnings.warn(
            "Method get_registrar_groups is deprecated in favor of list_groups.", DeprecationWarning, stacklevel=2
        )
        return await alist(self.list_groups())

    async def list_group_membership(self, registrar_handle: str) -> AsyncIterator[str]:
        """Get list of groups registrar belongs to.

        Args:
            registrar_handle: Registrar handle.

        Returns:
            Iterator of groups registrar belongs to.
        """
        request = GetRegistrarGroupsMembershipRequest()
        request.registrar_handle.value = registrar_handle
        for group in (await self.call("get_registrar_groups_membership", request))["groups"] or []:
            yield group

    async def get_registrar_groups_membership(self, registrar_handle: str) -> list[str]:
        """Get list of groups registrar belongs to.

        Args:
            registrar_handle: Registrar handle.

        Returns:
            List of groups registrar belongs to (possibly empty).
        """
        warnings.warn(
            "Method get_registrar_groups_membership is deprecated in favor of list_group_membership.",
            DeprecationWarning,
            stacklevel=2,
        )
        return await alist(self.list_group_membership(registrar_handle))

    async def list_certifications(self, registrar_handle: str) -> AsyncIterator[RegistrarCertification]:
        """Get list of registrar certifications.

        Args:
            registrar_handle: Registrar handle.

        Returns:
            Iterator of registrar's certifications (possibly empty).
        """
        request = GetRegistrarCertificationsRequest()
        request.registrar_handle.value = registrar_handle

        for certification in (await self.call("get_registrar_certifications", request)) or []:
            yield certification

    async def get_registrar_certifications(self, registrar_handle: str) -> list[RegistrarCertification]:
        """Get list of registrar certifications.

        Args:
            registrar_handle: Registrar handle.

        Returns:
            List of registrar's certifications (possibly empty).
        """
        warnings.warn(
            "Method get_registrar_certifications is deprecated in favor of list_certifications.",
            DeprecationWarning,
            stacklevel=2,
        )
        return await alist(self.list_certifications(registrar_handle))


class RegistrarAdminClient(AsyncGrpcClient):
    """Registrar Admin service gRPC client."""

    stub_cls = AdminStub
    decoder_cls = RegistrarDecoder
    service = "Admin"

    async def create(self, registrar: Registrar) -> None:
        """Create a registrar.

        Args:
            registrar: The registrar object.
        """
        request = CreateRegistrarRequest()
        request.registrar_handle.value = registrar.registrar_handle
        request.name = registrar.name
        request.organization = registrar.organization
        if registrar.place is not None:
            for street in registrar.place.street:
                request.place.street.append(street)
            request.place.city = registrar.place.city
            request.place.state_or_province = registrar.place.state_or_province
            request.place.postal_code.value = registrar.place.postal_code
            request.place.country_code.value = registrar.place.country_code
        if registrar.telephone is not None:
            request.telephone.value = registrar.telephone
        if registrar.fax is not None:
            request.fax.value = registrar.fax
        for email in registrar.emails:
            request.emails.add(value=email)
        if registrar.url is not None:
            request.url.value = registrar.url
        request.is_system_registrar = registrar.is_system_registrar
        if registrar.company_registration_number is not None:
            request.company_registration_number.value = registrar.company_registration_number
        if registrar.vat_identification_number is not None:
            request.vat_identification_number.value = registrar.vat_identification_number
        request.variable_symbol = registrar.variable_symbol
        request.payment_memo_regex = registrar.payment_memo_regex
        request.is_vat_payer = registrar.is_vat_payer
        request.is_internal = registrar.is_internal
        await self.call("create_registrar", request)

    async def create_registrar(self, registrar: Registrar) -> None:
        """Create a registrar.

        Args:
            registrar: The registrar object.
        """
        warnings.warn("Method create_registrar is deprecated in favor of create.", DeprecationWarning, stacklevel=2)
        await self.create(registrar)

    async def update(  # noqa: C901
        self,
        registrar_id: str,
        *,
        registrar_handle: Optional[str] = None,
        name: Optional[str] = None,
        organization: Optional[str] = None,
        place: Optional[Address] = None,
        telephone: Optional[str] = None,
        fax: Optional[str] = None,
        emails: Optional[Sequence[str]] = None,
        url: Optional[str] = None,
        is_system_registrar: Optional[bool] = None,
        company_registration_number: Optional[str] = None,
        vat_identification_number: Optional[str] = None,
        variable_symbol: Optional[str] = None,
        payment_memo_regex: Optional[str] = None,
        is_vat_payer: Optional[bool] = None,
        is_internal: Optional[bool] = None,
    ) -> None:
        """Update a registrar.

        Args:
            registrar_id: Registrar ID.
            registrar_handle: Set new registrar handle.
            name: Set new name.
            organization: Set new organization.
            place: Set new address.
            telephone: Set new phone number.
            fax: Set new fax.
            emails: Set new list of emails.
            url: Set new url.
            is_system_registrar: Set whether registrar is system registrar.
            company_registration_number: Set new company registration number.
            vat_identification_number: Set new VAT identification number.
            variable_symbol: Set new variable symbol.
            payment_memo_regex: Set new payment memo regex.
            is_vat_payer: Set whether registrar is VAT payer.
            is_internal: Set whether registrar is internal.
        """
        request = UpdateRegistrarRequest()
        request.registrar_handle.value = registrar_id
        if registrar_handle is not None:
            request.set_registrar_handle.value = registrar_handle
        if name is not None:
            request.set_name = name
        if organization is not None:
            request.set_organization = organization
        if place is not None:
            request.set_place.street[:] = place.street
            request.set_place.city = place.city
            request.set_place.state_or_province = place.state_or_province
            if place.postal_code is not None:
                request.set_place.postal_code.value = place.postal_code
            if place.country_code is not None:
                request.set_place.country_code.value = place.country_code
        if telephone is not None:
            request.set_telephone.value = telephone
        if fax is not None:
            request.set_fax.value = fax
        if emails is not None:
            for email in emails:
                request.set_emails.values.add(value=email)
        if url is not None:
            request.set_url.value = url
        if is_system_registrar is not None:
            request.set_is_system_registrar = is_system_registrar
        if company_registration_number is not None:
            request.set_company_registration_number.value = company_registration_number
        if vat_identification_number is not None:
            request.set_vat_identification_number.value = vat_identification_number
        if variable_symbol is not None:
            request.set_variable_symbol = variable_symbol
        if payment_memo_regex is not None:
            request.set_payment_memo_regex = payment_memo_regex
        if is_vat_payer is not None:
            request.set_is_vat_payer = is_vat_payer
        if is_internal is not None:
            request.set_is_internal = is_internal
        await self.call("update_registrar", request)

    async def update_registrar(  # noqa: C901
        self,
        registrar_id: str,
        *,
        registrar_handle: Optional[str] = None,
        name: Optional[str] = None,
        organization: Optional[str] = None,
        place: Optional[Address] = None,
        telephone: Optional[str] = None,
        fax: Optional[str] = None,
        emails: Optional[Sequence[str]] = None,
        url: Optional[str] = None,
        is_system_registrar: Optional[bool] = None,
        company_registration_number: Optional[str] = None,
        vat_identification_number: Optional[str] = None,
        variable_symbol: Optional[str] = None,
        payment_memo_regex: Optional[str] = None,
        is_vat_payer: Optional[bool] = None,
        is_internal: Optional[bool] = None,
    ) -> None:
        """Update a registrar.

        Args:
            registrar_id: Registrar ID.
            registrar_handle: Set new registrar handle.
            name: Set new name.
            organization: Set new organization.
            place: Set new address.
            telephone: Set new phone number.
            fax: Set new fax.
            emails: Set new list of emails.
            url: Set new url.
            is_system_registrar: Set whether registrar is system registrar.
            company_registration_number: Set new company registration number.
            vat_identification_number: Set new VAT identification number.
            variable_symbol: Set new variable symbol.
            payment_memo_regex: Set new payment memo regex.
            is_vat_payer: Set whether registrar is VAT payer.
            is_internal: Set whether registrar is internal.
        """
        warnings.warn("Method update_registrar is deprecated in favor of update.", DeprecationWarning, stacklevel=2)
        await self.update(
            registrar_id,
            registrar_handle=registrar_handle,
            name=name,
            organization=organization,
            place=place,
            telephone=telephone,
            fax=fax,
            emails=emails,
            url=url,
            is_system_registrar=is_system_registrar,
            company_registration_number=company_registration_number,
            vat_identification_number=vat_identification_number,
            variable_symbol=variable_symbol,
            payment_memo_regex=payment_memo_regex,
            is_vat_payer=is_vat_payer,
            is_internal=is_internal,
        )

    async def add_zone_access(self, registrar_handle: str, access: ZoneAccess) -> ZoneAccess:
        """Add registrar zone access.

        Args:
            registrar_handle: Registrar handle.
            access: Zone access to add.

        Returns:
            Added zone access.
        """
        request = AddRegistrarZoneAccessRequest()
        request.registrar_handle.value = registrar_handle
        request.zone = access.zone
        if access.valid_from:
            request.valid_from.FromDatetime(access.valid_from)
        if access.valid_to:
            request.valid_to.FromDatetime(access.valid_to)
        result = await self.call("add_registrar_zone_access", request)
        result.pop("registrar_handle", None)
        return ZoneAccess(**result)

    async def update_zone_access(
        self,
        registrar_handle: str,
        access: ZoneAccess,
        *,
        valid_from: Optional[datetime] = NOOP,
        valid_to: Optional[datetime] = NOOP,
    ) -> ZoneAccess:
        """Update registrar zone access.

        Args:
            registrar_handle: Registrar handle.
            access: Zone access to update. `valid_from` has to be defined.
            valid_from: Future `valid_from` that replaces current value. If None, current date will be used.
            valid_to: New `valid_to` that replaces current value. If None, validity is not limited.

        Returns:
            Updated zone access.
        """
        request = UpdateRegistrarZoneAccessRequest()
        request.registrar_handle.value = registrar_handle
        request.zone = access.zone
        request.valid_from.FromDatetime(access.valid_from)

        if isinstance(valid_from, datetime):
            request.set_valid_from.FromDatetime(valid_from)
        elif valid_from is None:
            request.set_valid_from.SetInParent()

        if isinstance(valid_to, datetime):
            request.set_valid_to.FromDatetime(valid_to)
        elif valid_to is None:
            request.set_valid_to.SetInParent()

        result = await self.call("update_registrar_zone_access", request)
        result.pop("registrar_handle", None)
        return ZoneAccess(**result)

    async def delete_zone_access(self, registrar_handle: str, access: ZoneAccess) -> None:
        """Delete registrar zone access.

        Args:
            registrar_handle: Registrar handle.
            access: Zone access to update. `valid_from` has to be defined.
        """
        request = DeleteRegistrarZoneAccessRequest()
        request.registrar_handle.value = registrar_handle
        request.zone = access.zone
        request.valid_from.FromDatetime(access.valid_from)
        await self.call("delete_registrar_zone_access", request)

    async def add_epp_credentials(
        self,
        registrar_handle: str,
        certificate: SslCertificate,
        *,
        password: Optional[str] = None,
        template_credentials_id: Optional[EppCredentialsId] = None,
    ) -> EppCredentialsId:
        """Add registrar EPP credentials.

        Args:
            registrar_handle: Registrar handle.
            certificate: SSL certificate.
            password: Password for EPP access.
            template_credentials_id: EPP credentials ID to copy password hash from.

        Arguments `password` and `template_credentials_id` are mutually exclusive.
        """
        if (password is not None) and (template_credentials_id is not None):
            raise ValueError("`password` and `template_credentials_id` args are mutually exclusive")
        request = AddRegistrarEppCredentialsRequest()
        request.registrar_handle.value = registrar_handle
        request.certificate.fingerprint = bytes.fromhex(certificate.fingerprint.replace(":", ""))
        request.certificate.cert_data_pem = certificate.cert_data_pem
        if password is not None:
            request.password = password
        if template_credentials_id is not None:
            request.template_credentials_id.value = template_credentials_id

        return cast(EppCredentialsId, await self.call("add_registrar_epp_credentials", request))

    async def update_epp_credentials(self, credentials_id: EppCredentialsId, *, password: str) -> None:
        """Update registrar EPP credentials.

        Args:
            credentials_id: EPP credentials ID.
            password: Password for EPP access.
        """
        request = UpdateRegistrarEppCredentialsRequest()
        request.credentials_id.value = credentials_id
        request.password = password

        await self.call("update_registrar_epp_credentials", request)

    async def delete_epp_credentials(self, credentials_id: EppCredentialsId) -> None:
        """Delete registrar EPP credentials.

        Args:
            credentials_id: EPP credentials ID.
        """
        request = DeleteRegistrarEppCredentialsRequest()
        request.credentials_id.value = credentials_id

        await self.call("delete_registrar_epp_credentials", request)

    async def add_group_membership(self, registrar_handle: str, group: str) -> None:
        """Add registrar to group.

        Args:
            registrar_handle: Registrar handle.
            group: Registrar group.
        """
        request = AddRegistrarGroupMembershipRequest()
        request.registrar_handle.value = registrar_handle
        request.group = group

        await self.call("add_registrar_group_membership", request)

    async def add_registrar_group_membership(self, registrar_handle: str, group: str) -> list[str]:
        """Add registrar to group.

        Args:
            registrar_handle: Registrar handle.
            group: Registrar group.

        Returns:
            List of groups registrar belongs to.
        """
        warnings.warn(
            "Method add_registrar_group_membership is deprecated in favor of add_group_membership.",
            DeprecationWarning,
            stacklevel=2,
        )
        request = AddRegistrarGroupMembershipRequest()
        request.registrar_handle.value = registrar_handle
        request.group = group

        result = await self.call("add_registrar_group_membership", request)

        return result["groups"] or []

    async def delete_group_membership(self, registrar_handle: str, group: str) -> None:
        """Delete registrar from group.

        Args:
            registrar_handle: Registrar handle.
            group: Registrar group.
        """
        request = DeleteRegistrarGroupMembershipRequest()
        request.registrar_handle.value = registrar_handle
        request.group = group

        await self.call("delete_registrar_group_membership", request)

    async def delete_registrar_group_membership(self, registrar_handle: str, group: str) -> list[str]:
        """Delete registrar from group.

        Args:
            registrar_handle: Registrar handle.
            group: Registrar group.

        Returns:
            List of groups registrar belongs to (possibly empty).
        """
        warnings.warn(
            "Method delete_registrar_group_membership is deprecated in favor of delete_group_membership.",
            DeprecationWarning,
            stacklevel=2,
        )
        request = DeleteRegistrarGroupMembershipRequest()
        request.registrar_handle.value = registrar_handle
        request.group = group

        result = await self.call("delete_registrar_group_membership", request)

        return result["groups"] or []

    async def add_certification(
        self,
        registrar_handle: str,
        classification: int,
        file_id: str,
        *,
        valid_from: Optional[datetime] = None,
        valid_to: Optional[datetime] = None,
    ) -> None:
        """Add registrar's certification.

        Args:
            registrar_handle: Registrar handle.
            classification: Classification of registrar (value 0 to 100).
            file_id: Identification of certification file (in PDF).
            valid_from: Start of certification validity (defaults to today).
            valid_to: End of certification validity (defaults to unlimited).

        Raises:
            RegistrarNotFound: Registrar handle identifies no known registrar.
            InvalidData: Invalid input parameters.
        """
        request = AddRegistrarCertificationRequest()
        request.registrar_handle.value = registrar_handle
        if valid_from:
            request.valid_from.FromDatetime(valid_from)
        if valid_to:
            request.valid_to.FromDatetime(valid_to)
        request.classification = classification
        request.file_id.value = file_id

        await self.call("add_registrar_certification", request)

    async def add_registrar_certification(
        self,
        registrar_handle: str,
        classification: int,
        file_id: str,
        valid_from: Optional[datetime] = None,
        valid_to: Optional[datetime] = None,
    ) -> None:
        """Add registrar's certification.

        Args:
            registrar_handle: Registrar handle.
            classification: Classification of registrar (value 0 to 100).
            file_id: Identification of certification file (in PDF).
            valid_from: Start of certification validity (defaults to today).
            valid_to: End of certification validity (defaults to unlimited).

        Raises:
            RegistrarNotFound: Registrar handle identifies no known registrar.
            InvalidData: Invalid input parameters.
        """
        warnings.warn(
            "Method add_registrar_certification is deprecated in favor of add_certification.",
            DeprecationWarning,
            stacklevel=2,
        )
        await self.add_certification(
            registrar_handle, classification, file_id, valid_from=valid_from, valid_to=valid_to
        )

    async def update_certification(
        self,
        certification_id: str,
        *,
        classification: Optional[int] = None,
        file_id: Optional[str] = None,
        valid_to: Optional[datetime] = None,
    ) -> None:
        """Update registrar's certification.

        Args:
            certification_id: Identification of certification.
            classification: Classification of registrar.
            file_id: Identification of file with certification details (in PDF).
            valid_to: End of certificate validity.

        Returns:
            none

        Raises:
            RegistrarCertificationDoesNotExist: Certification with provided identification does not exist.
            InvalidData: Invalid input parameter.
        """
        request = UpdateRegistrarCertificationRequest()
        request.certification_id.value = certification_id
        if classification is not None:
            request.set_classification = classification
        if file_id is not None:
            request.set_file_id.value = file_id
        if valid_to:
            request.set_valid_to.FromDatetime(valid_to)

        await self.call("update_registrar_certification", request)

    async def update_registrar_certification(
        self,
        certification_id: str,
        classification: Optional[int] = None,
        file_id: Optional[str] = None,
        valid_to: Optional[datetime] = None,
    ) -> None:
        """Update registrar's certification.

        Args:
            certification_id: Identification of certification.
            classification: Classification of registrar.
            file_id: Identification of file with certification details (in PDF).
            valid_to: End of certificate validity.

        Returns:
            none

        Raises:
            RegistrarCertificationDoesNotExist: Certification with provided identification does not exist.
            InvalidData: Invalid input parameter.
        """
        warnings.warn(
            "Method update_registrar_certification is deprecated in favor of update_certification.",
            DeprecationWarning,
            stacklevel=2,
        )
        await self.update_certification(
            certification_id, classification=classification, file_id=file_id, valid_to=valid_to
        )

    async def delete_certification(self, certification_id: str) -> None:
        """Delete registrar certification.

        Args:
            certification_id: Identification of certification.

        Returns:
            none

        Raises:
            RegistrarCertificationDoesNotExist: Certification with provided identification does not exist.
            InvalidData: Invalid input parameter.
        """
        request = DeleteRegistrarCertificationRequest()
        request.certification_id.value = certification_id

        await self.call("delete_registrar_certification", request)

    async def delete_registrar_certification(self, certification_id: str) -> None:
        """Delete registrar certification.

        Args:
            certification_id: Identification of certification.

        Returns:
            none

        Raises:
            RegistrarCertificationDoesNotExist: Certification with provided identification does not exist.
            InvalidData: Invalid input parameter.
        """
        warnings.warn(
            "Method delete_registrar_certification is deprecated in favor of delete_certification.",
            DeprecationWarning,
            stacklevel=2,
        )
        await self.delete_certification(certification_id)

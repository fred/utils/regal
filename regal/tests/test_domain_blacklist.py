from collections.abc import Iterable
from datetime import datetime
from typing import Any
from unittest import IsolatedAsyncioTestCase, TestCase
from unittest.mock import call, sentinel

import pytz
from asyncstdlib import iter as aiter, tuple as atuple
from fred_api.registry.domain_blacklist.domain_blacklist_common_types_pb2 import BlockInfo, BlockRef as BlockRefMessage
from fred_api.registry.domain_blacklist.service_domain_blacklist_grpc_pb2 import (
    BlockInfoReply,
    BlockInfoRequest,
    CreateBlockReply,
    CreateBlockRequest,
    DeleteBlockReply,
    DeleteBlockRequest,
    ListBlocksReply,
)
from fred_types import BaseModel
from frgal.utils import AsyncTestClientMixin
from google.protobuf.empty_pb2 import Empty

from regal import Block, BlockId, BlockRef, DomainBlacklistClient
from regal.domain_blacklist import DomainBlacklistDecoder

from .utils import make_awaitable


class BlockRefTest(TestCase):
    model: type[BaseModel] = BlockRef

    def test_label_valid(self):
        data: Iterable[dict[str, Any]] = (
            {"fqdn": "example.org"},
            {"regex": "example.org"},
        )
        for input in data:
            with self.subTest(input=input):
                block = self.model(**input)
                self.assertEqual(block.model_dump(exclude_unset=True), input)

    def test_label_invalid(self):
        data: Iterable[tuple[dict[str, Any], str]] = (
            ({}, "Either fqdn or regex has to be provided."),
            ({"fqdn": "example.org", "regex": "example.org"}, "Only one of fqdn and regex can be provided."),
        )
        for input, msg in data:
            with self.subTest(input=input):
                with self.assertRaisesRegex(ValueError, msg):
                    self.model(**input)


class BlockTest(BlockRefTest):
    model = Block


class DomainBlacklistDecoderTest(TestCase):
    def setUp(self):
        self.decoder = DomainBlacklistDecoder()

    def test_decode_block_ref_fqdn(self):
        block_ref = BlockRefMessage()
        block_ref.id.value = "42"
        block_ref.fqdn = "example.org"
        self.assertEqual(self.decoder.decode(block_ref), BlockRef(id=BlockId("42"), fqdn="example.org"))

    def test_decode_block_ref_regex(self):
        block_ref = BlockRefMessage()
        block_ref.id.value = "42"
        block_ref.regex = r".*\.example.org"
        self.assertEqual(self.decoder.decode(block_ref), BlockRef(id=BlockId("42"), regex=r".*\.example.org"))

    def test_decode_block_info_fqdn(self):
        block_info = BlockInfo()
        block_info.id.value = "42"
        block_info.fqdn = "example.org"
        self.assertEqual(self.decoder.decode(block_info), Block(id=BlockId("42"), fqdn="example.org"))

    def test_decode_block_info_regex(self):
        block_info = BlockInfo()
        block_info.id.value = "42"
        block_info.regex = r".*\.example.org"
        self.assertEqual(self.decoder.decode(block_info), Block(id=BlockId("42"), regex=r".*\.example.org"))

    def test_decode_block_info_full(self):
        block_info = BlockInfo()
        block_info.id.value = "42"
        block_info.fqdn = "example.org"
        block_info.requested_by.value = "KRYTEN"
        block_info.valid_from.FromDatetime(datetime(1988, 2, 15, 20, tzinfo=pytz.utc))
        block_info.valid_to.FromDatetime(datetime(2020, 4, 9, 22, tzinfo=pytz.utc))
        block_info.reason = "Fuchal"
        result = Block(
            id=BlockId("42"),
            fqdn="example.org",
            requested_by="KRYTEN",
            valid_from=datetime(1988, 2, 15, 20, tzinfo=pytz.utc),
            valid_to=datetime(2020, 4, 9, 22, tzinfo=pytz.utc),
            reason="Fuchal",
        )
        self.assertEqual(self.decoder.decode(block_info), result)


class TestDomainBlacklistClient(AsyncTestClientMixin, DomainBlacklistClient):
    """Test DomainBlacklistClient."""


class DomainBlacklistClientTest(IsolatedAsyncioTestCase):
    def setUp(self):
        self.client = TestDomainBlacklistClient(netloc=sentinel.netloc)

    async def test_get(self):
        reply = BlockInfoReply()
        reply.data.block.id.value = "42"
        reply.data.block.fqdn = "example.org"
        reply.data.block.requested_by.value = "KRYTEN"
        reply.data.block.valid_from.FromDatetime(datetime(1988, 2, 15, 20, tzinfo=pytz.utc))
        reply.data.block.valid_to.FromDatetime(datetime(2020, 4, 9, 22, tzinfo=pytz.utc))
        reply.data.block.reason = "Fuchal"
        self.client.mock.return_value = make_awaitable(reply)

        block_id = BlockId("42")
        block = await self.client.get(block_id)

        result = Block(
            id=BlockId("42"),
            fqdn="example.org",
            requested_by="KRYTEN",
            valid_from=datetime(1988, 2, 15, 20, tzinfo=pytz.utc),
            valid_to=datetime(2020, 4, 9, 22, tzinfo=pytz.utc),
            reason="Fuchal",
        )
        self.assertEqual(block, result)

        request = BlockInfoRequest()
        request.id.value = "42"
        grpc_call = call(request, method="/Fred.Registry.Api.DomainBlacklist.Block/get_block_info", timeout=None)
        self.assertEqual(self.client.mock.mock_calls, [grpc_call])

    async def test_list_empty_stream(self):
        self.client.stream_mock.return_value = aiter(())

        blocks = await atuple(self.client.list())

        self.assertEqual(blocks, ())

        grpc_call = call(Empty(), method="/Fred.Registry.Api.DomainBlacklist.Block/list_blocks", timeout=None)
        self.assertEqual(self.client.stream_mock.mock_calls, [grpc_call])

    async def test_list_empty_response(self):
        reply = ListBlocksReply()
        reply.data.blocks.extend([])
        self.client.stream_mock.return_value = aiter([reply])

        blocks = await atuple(self.client.list())

        self.assertEqual(blocks, ())

        grpc_call = call(Empty(), method="/Fred.Registry.Api.DomainBlacklist.Block/list_blocks", timeout=None)
        self.assertEqual(self.client.stream_mock.mock_calls, [grpc_call])

    async def test_list_fqdn(self):
        reply = ListBlocksReply()
        block_ref = reply.data.blocks.add()
        block_ref.id.value = "42"
        block_ref.fqdn = "example.org"
        self.client.stream_mock.return_value = aiter([reply])

        blocks = await atuple(self.client.list())

        self.assertEqual(blocks, (BlockRef(id=BlockId("42"), fqdn="example.org"),))

        grpc_call = call(Empty(), method="/Fred.Registry.Api.DomainBlacklist.Block/list_blocks", timeout=None)
        self.assertEqual(self.client.stream_mock.mock_calls, [grpc_call])

    async def test_list_regex(self):
        reply = ListBlocksReply()
        block_ref = reply.data.blocks.add()
        block_ref.id.value = "42"
        block_ref.regex = r".*\.example.org"
        self.client.stream_mock.return_value = aiter([reply])

        blocks = await atuple(self.client.list())

        self.assertEqual(blocks, (BlockRef(id=BlockId("42"), regex=r".*\.example.org"),))

        grpc_call = call(Empty(), method="/Fred.Registry.Api.DomainBlacklist.Block/list_blocks", timeout=None)
        self.assertEqual(self.client.stream_mock.mock_calls, [grpc_call])

    async def test_list_flatten(self):
        # Test responses are properly flattened.
        reply_1 = ListBlocksReply()
        block_ref_1 = reply_1.data.blocks.add()
        block_ref_1.id.value = "42"
        block_ref_1.fqdn = "1.example.org"
        block_ref_2 = reply_1.data.blocks.add()
        block_ref_2.id.value = "43"
        block_ref_2.fqdn = "2.example.org"
        reply_2 = ListBlocksReply()
        block_ref_3 = reply_2.data.blocks.add()
        block_ref_3.id.value = "44"
        block_ref_3.fqdn = "3.example.org"
        block_ref_4 = reply_2.data.blocks.add()
        block_ref_4.id.value = "45"
        block_ref_4.fqdn = "4.example.org"
        self.client.stream_mock.return_value = aiter([reply_1, reply_2])

        blocks = await atuple(self.client.list())

        result = (
            BlockRef(id=BlockId("42"), fqdn="1.example.org"),
            BlockRef(id=BlockId("43"), fqdn="2.example.org"),
            BlockRef(id=BlockId("44"), fqdn="3.example.org"),
            BlockRef(id=BlockId("45"), fqdn="4.example.org"),
        )
        self.assertEqual(blocks, result)

        grpc_call = call(Empty(), method="/Fred.Registry.Api.DomainBlacklist.Block/list_blocks", timeout=None)
        self.assertEqual(self.client.stream_mock.mock_calls, [grpc_call])

    async def test_create_fqdn(self):
        reply = CreateBlockReply()
        reply.data.id.value = "42"
        self.client.mock.return_value = make_awaitable(reply)

        block = Block(fqdn="example.org")
        block_id = await self.client.create(block)

        self.assertEqual(block_id, BlockId("42"))

        request = CreateBlockRequest()
        request.block.fqdn = "example.org"
        grpc_call = call(request, method="/Fred.Registry.Api.DomainBlacklist.Block/create_block", timeout=None)
        self.assertEqual(self.client.mock.mock_calls, [grpc_call])

    async def test_create_regex(self):
        reply = CreateBlockReply()
        reply.data.id.value = "42"
        self.client.mock.return_value = make_awaitable(reply)

        block = Block(regex=r".*\.example.org")
        block_id = await self.client.create(block)

        self.assertEqual(block_id, BlockId("42"))

        request = CreateBlockRequest()
        request.block.regex = r".*\.example.org"
        grpc_call = call(request, method="/Fred.Registry.Api.DomainBlacklist.Block/create_block", timeout=None)
        self.assertEqual(self.client.mock.mock_calls, [grpc_call])

    async def test_create_full(self):
        reply = CreateBlockReply()
        reply.data.id.value = "42"
        self.client.mock.return_value = make_awaitable(reply)

        block = Block(
            id=BlockId("42"),
            fqdn="example.org",
            requested_by="KRYTEN",
            valid_from=datetime(1988, 2, 15, 20, tzinfo=pytz.utc),
            valid_to=datetime(2020, 4, 9, 22, tzinfo=pytz.utc),
            reason="Fuchal",
        )
        block_id = await self.client.create(block)

        self.assertEqual(block_id, BlockId("42"))

        request = CreateBlockRequest()
        request.block.id.value = "42"
        request.block.fqdn = "example.org"
        request.block.requested_by.value = "KRYTEN"
        request.block.valid_from.FromDatetime(datetime(1988, 2, 15, 20, tzinfo=pytz.utc))
        request.block.valid_to.FromDatetime(datetime(2020, 4, 9, 22, tzinfo=pytz.utc))
        request.block.reason = "Fuchal"
        grpc_call = call(request, method="/Fred.Registry.Api.DomainBlacklist.Block/create_block", timeout=None)
        self.assertEqual(self.client.mock.mock_calls, [grpc_call])

    async def test_delete(self):
        reply = DeleteBlockReply()
        self.client.mock.return_value = make_awaitable(reply)

        block_id = BlockId("42")
        await self.client.delete(block_id)

        request = DeleteBlockRequest()
        request.id.value = "42"
        grpc_call = call(request, method="/Fred.Registry.Api.DomainBlacklist.Block/delete_block", timeout=None)
        self.assertEqual(self.client.mock.mock_calls, [grpc_call])

    async def test_delete_requested_by(self):
        reply = DeleteBlockReply()
        self.client.mock.return_value = make_awaitable(reply)

        block_id = BlockId("42")
        await self.client.delete(block_id, requested_by="KRYTEN")

        request = DeleteBlockRequest()
        request.id.value = "42"
        request.requested_by.value = "KRYTEN"
        grpc_call = call(request, method="/Fred.Registry.Api.DomainBlacklist.Block/delete_block", timeout=None)
        self.assertEqual(self.client.mock.mock_calls, [grpc_call])

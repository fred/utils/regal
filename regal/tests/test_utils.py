from unittest import TestCase

from regal.utils import _idna_encode


class IdnaEncodeTest(TestCase):
    def test_encode(self):
        data = (
            # fqdn, result
            ("example.org", "example.org"),
            ("háčkyčárky.cz", "xn--hkyrky-ptac70bc.cz"),
            ("xn--hkyrky-ptac70bc.cz", "xn--hkyrky-ptac70bc.cz"),
            # Check invalid inputs are left as is.
            ("xn--háčky", "xn--háčky"),
        )
        for fqdn, result in data:
            with self.subTest(fqdn=fqdn):
                self.assertEqual(_idna_encode(fqdn), result)

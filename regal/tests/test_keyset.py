import base64
from typing import Any, Optional
from unittest import IsolatedAsyncioTestCase, TestCase
from unittest.mock import call, sentinel

from asyncstdlib import iter as aiter, tuple as atuple
from fred_api.registry.common_types_pb2 import KeysetDoesNotExist as KeysetDoesNotExistMessage, OrderByDirection
from fred_api.registry.keyset.add_keyset_auth_info_types_pb2 import AddKeysetAuthInfoReply, AddKeysetAuthInfoRequest
from fred_api.registry.keyset.keyset_history_types_pb2 import (
    KeysetHistoryRecord,
    KeysetHistoryReply,
    KeysetHistoryRequest,
)
from fred_api.registry.keyset.keyset_info_types_pb2 import (
    BatchKeysetInfoReply,
    BatchKeysetInfoRequest,
    DnsKey as DnsKeyMessage,
    KeysetIdReply,
    KeysetIdRequest,
    KeysetInfoReply,
    KeysetInfoRequest,
)
from fred_api.registry.keyset.keyset_state_flags_types_pb2 import KeysetStateFlagsReply
from fred_api.registry.keyset.keyset_state_history_types_pb2 import (
    KeysetStateHistory,
    KeysetStateHistoryReply,
    KeysetStateHistoryRequest,
)
from fred_api.registry.keyset.keyset_state_types_pb2 import KeysetStateReply, KeysetStateRequest
from fred_api.registry.keyset.list_keysets_by_contact_types_pb2 import (
    ListKeysetsByContactReply,
    ListKeysetsByContactRequest,
)
from fred_api.registry.keyset.update_keyset_state_types_pb2 import UpdateKeysetStateRequest
from fred_types import ContactId, KeysetHistoryId, KeysetId, KeysetRef, SnapshotId
from frgal.utils import AsyncTestClientMixin
from google.protobuf.empty_pb2 import Empty

from regal.common import ListPage, PaginationReply
from regal.exceptions import KeysetDoesNotExist
from regal.keyset import DnsKey, DSRecord, Keyset, KeysetByRelation, KeysetClient
from regal.object import StateUpdate
from regal.tests.common import ObjectClientTestMixin

from .utils import make_awaitable


class TestKeysetClient(AsyncTestClientMixin, KeysetClient):
    """Test KeysetClient."""

    exhaust_iterables = True


class KeysetClientTest(ObjectClientTestMixin[TestKeysetClient], IsolatedAsyncioTestCase):
    """Test KeysetClient's methods."""

    grpc_namespace = "/Fred.Registry.Api.Keyset.Keyset"
    get_state_request = KeysetStateRequest
    get_state_reply = KeysetStateReply
    get_state_method = "get_keyset_state"
    get_history_request = KeysetHistoryRequest
    get_history_reply = KeysetHistoryReply
    get_history_method = "get_keyset_history"
    get_history_record = KeysetHistoryRecord
    get_state_history_request = KeysetStateHistoryRequest
    get_state_history_reply = KeysetStateHistoryReply
    get_state_history_method = "get_keyset_state_history"
    get_state_history_record = KeysetStateHistory.Record
    get_state_flags_method = "get_keyset_state_flags"
    get_state_flags_reply_cls = KeysetStateFlagsReply

    def setUp(self):
        self.client = TestKeysetClient(netloc=sentinel.netloc)

    async def test_get_id(self):
        keyset_id = KeysetId("kryten")
        reply = KeysetIdReply()
        reply.data.keyset_id.uuid.value = keyset_id
        self.client.mock.return_value = make_awaitable(reply)

        response = await self.client.get_id("HANDLE")
        self.assertEqual(response, keyset_id)
        self.assertIsInstance(response, KeysetId)

        request = KeysetIdRequest()
        request.keyset_handle.value = "HANDLE"
        self.client.mock.assert_called_once_with(
            request,
            method="/Fred.Registry.Api.Keyset.Keyset/get_keyset_id",
            timeout=None,
        )

    async def test_get_keyset_id(self):
        keyset_id = KeysetId("kryten")
        reply = KeysetIdReply()
        reply.data.keyset_id.uuid.value = keyset_id
        self.client.mock.return_value = make_awaitable(reply)

        with self.assertWarnsRegex(DeprecationWarning, "Method get_keyset_id is deprecated in favor of get_id."):
            response = await self.client.get_keyset_id("HANDLE")
        self.assertEqual(response, keyset_id)
        self.assertIsInstance(response, KeysetId)

        request = KeysetIdRequest()
        request.keyset_handle.value = "HANDLE"
        self.client.mock.assert_called_once_with(
            request,
            method="/Fred.Registry.Api.Keyset.Keyset/get_keyset_id",
            timeout=None,
        )

    async def _test_get(self, request: KeysetInfoRequest, **kwargs: Any) -> None:
        dns_key = DnsKeyMessage()
        dns_key.flags = 256
        dns_key.protocol = 3
        dns_key.alg = 15
        dns_key.key = "some key"
        reply = KeysetInfoReply()
        reply.data.keyset_id.uuid.value = "kryten"
        reply.data.keyset_handle.value = "HANDLE"
        reply.data.dns_keys.extend([dns_key])
        self.client.mock.return_value = make_awaitable(reply)

        response = await self.client.get(KeysetId("kryten"), **kwargs)

        self.assertEqual(
            response,
            Keyset(
                keyset_id=KeysetId("kryten"),
                keyset_handle="HANDLE",
                dns_keys=[DnsKey(flags=256, protocol=3, alg=15, key="some key")],
            ),
        )

        request.keyset_id.uuid.value = "kryten"
        self.client.mock.assert_called_once_with(
            request,
            method="/Fred.Registry.Api.Keyset.Keyset/get_keyset_info",
            timeout=None,
        )

    async def test_get(self):
        request = KeysetInfoRequest()
        await self._test_get(request)

    async def test_get_history_id(self):
        request = KeysetInfoRequest()
        request.keyset_history_id.uuid.value = "nova-5"
        await self._test_get(request, history_id=KeysetHistoryId("nova-5"))

    async def test_get_snapshot_id(self):
        # Test get with a snapshot_id.
        request = KeysetInfoRequest()
        request.keyset_snapshot_id.value = "LAST-THURSDAY"
        await self._test_get(request, snapshot_id=SnapshotId("LAST-THURSDAY"))

    async def test_get_include_domains_count(self):
        # Test include_domains_count is passed to request correctly.
        reply = KeysetInfoReply()
        reply.data.keyset_id.uuid.value = "starbug-42"
        reply.data.keyset_handle.value = "STARBUG"
        self.client.mock.return_value = make_awaitable(reply)

        await self.client.get(KeysetId("STARBUG-42"), include_domains_count=True)

        request = KeysetInfoRequest()
        request.keyset_id.uuid.value = "STARBUG-42"
        request.include_domains_count = True
        self.client.mock.assert_called_once_with(
            request,
            method="/Fred.Registry.Api.Keyset.Keyset/get_keyset_info",
            timeout=None,
        )

    async def _test_get_keyset_info(self, keyset_history_id: Optional[KeysetHistoryId] = None) -> None:
        dns_key = DnsKeyMessage()
        dns_key.flags = 256
        dns_key.protocol = 3
        dns_key.alg = 15
        dns_key.key = "some key"
        reply = KeysetInfoReply()
        reply.data.keyset_id.uuid.value = "kryten"
        if keyset_history_id:
            reply.data.keyset_history_id.uuid.value = keyset_history_id
        reply.data.keyset_handle.value = "HANDLE"
        reply.data.dns_keys.extend([dns_key])
        reply.data.domains_count = 42
        self.client.mock.return_value = make_awaitable(reply)

        with self.assertWarnsRegex(DeprecationWarning, "Method get_keyset_info is deprecated in favor of get."):
            response = await self.client.get_keyset_info(KeysetId("kryten"), history_id=keyset_history_id)

        self.assertEqual(
            response,
            Keyset(
                keyset_id=KeysetId("kryten"),
                keyset_history_id=keyset_history_id,
                keyset_handle="HANDLE",
                dns_keys=[DnsKey(flags=256, protocol=3, alg=15, key="some key")],
                domains_count=42,
            ),
        )

        request = KeysetInfoRequest()
        request.keyset_id.uuid.value = "kryten"
        if keyset_history_id:
            request.keyset_history_id.uuid.value = keyset_history_id
        self.client.mock.assert_called_once_with(
            request,
            method="/Fred.Registry.Api.Keyset.Keyset/get_keyset_info",
            timeout=None,
        )

    async def test_get_keyset_info(self):
        await self._test_get_keyset_info()

    async def test_get_keyset_info_with_history_id(self):
        await self._test_get_keyset_info(keyset_history_id=KeysetHistoryId("nova-5"))

    async def test_get_keyset_info_include_domains_count(self):
        # Test include_domains_count is passed to request correctly.
        reply = KeysetInfoReply()
        reply.data.keyset_id.uuid.value = "starbug-42"
        reply.data.keyset_handle.value = "STARBUG"
        self.client.mock.return_value = make_awaitable(reply)

        with self.assertWarnsRegex(DeprecationWarning, "Method get_keyset_info is deprecated in favor of get."):
            await self.client.get_keyset_info(KeysetId("STARBUG-42"), include_domains_count=True)

        request = KeysetInfoRequest()
        request.keyset_id.uuid.value = "STARBUG-42"
        request.include_domains_count = True
        self.client.mock.assert_called_once_with(
            request,
            method="/Fred.Registry.Api.Keyset.Keyset/get_keyset_info",
            timeout=None,
        )

    async def _test_batch_get(self, keysets: tuple[Keyset, ...]) -> None:
        result = self.client.batch_get((KeysetId("starbug-42"),))

        self.assertEqual(await atuple(result), keysets)
        request = BatchKeysetInfoRequest()
        req_1 = request.requests.add()
        req_1.keyset_id.uuid.value = "starbug-42"
        self.assertEqual(
            self.client.stream_mock.mock_calls,
            [call((request,), method="/Fred.Registry.Api.Keyset.Keyset/batch_keyset_info", timeout=None)],
        )

    async def test_batch_get_empty_stream(self):
        self.client.stream_mock.return_value = aiter(())

        await self._test_batch_get(())

    async def test_batch_get_empty_response(self):
        reply = BatchKeysetInfoReply()
        self.client.stream_mock.return_value = aiter([reply])

        await self._test_batch_get(())

    async def test_batch_get_empty_replies(self):
        reply = BatchKeysetInfoReply()
        reply.data.replies.extend([])
        self.client.stream_mock.return_value = aiter([reply])

        await self._test_batch_get(())

    async def test_batch_get(self):
        reply = BatchKeysetInfoReply()
        keyset_reply = reply.data.replies.add()
        keyset_reply.data.keyset_id.uuid.value = "starbug-42"
        keyset_reply.data.keyset_handle.value = "STARBUG"
        self.client.stream_mock.return_value = aiter([reply])

        keyset = Keyset(keyset_id=KeysetId("starbug-42"), keyset_handle="STARBUG")
        await self._test_batch_get((keyset,))

    async def test_batch_get_snapshot_id(self):
        # Ensure snapshot is passed to each request.
        snapshot_id = SnapshotId("LAST-THURSDAY")
        self.client.stream_mock.return_value = aiter(())

        await atuple(self.client.batch_get((KeysetId("starbug-42"), KeysetId("starbug-43")), snapshot_id=snapshot_id))

        request = BatchKeysetInfoRequest()
        req_1 = request.requests.add()
        req_1.keyset_id.uuid.value = "starbug-42"
        req_1.keyset_snapshot_id.value = snapshot_id
        req_2 = request.requests.add()
        req_2.keyset_id.uuid.value = "starbug-43"
        req_2.keyset_snapshot_id.value = snapshot_id
        self.assertEqual(
            self.client.stream_mock.mock_calls,
            [call((request,), method="/Fred.Registry.Api.Keyset.Keyset/batch_keyset_info", timeout=None)],
        )

    async def test_batch_get_error_ignore(self):
        reply = BatchKeysetInfoReply()
        keyset_reply = reply.data.replies.add()
        keyset_reply.error.exception.keyset_does_not_exist.CopyFrom(KeysetDoesNotExistMessage())
        self.client.stream_mock.return_value = aiter([reply])

        await self._test_batch_get(())

    async def test_batch_get_error_raise(self):
        reply = BatchKeysetInfoReply()
        keyset_reply = reply.data.replies.add()
        keyset_reply.error.exception.keyset_does_not_exist.CopyFrom(KeysetDoesNotExistMessage())
        self.client.stream_mock.return_value = aiter([reply])

        with self.assertRaises(KeysetDoesNotExist):
            await atuple(self.client.batch_get((KeysetId("starbug-42"),), errors="raise"))

    async def test_batch_get_error_invalid(self):
        with self.assertRaisesRegex(ValueError, "Unknown errors value: 'invalid'"):
            await atuple(self.client.batch_get((), errors="invalid"))  # type: ignore[arg-type]

    async def test_batch_get_include_domains_count(self):
        # Test include_domains_count is passed to the request correctly.
        self.client.stream_mock.return_value = aiter(())

        await atuple(self.client.batch_get((KeysetId("starbug-42"),), include_domains_count=True))

        request = BatchKeysetInfoRequest()
        req_1 = request.requests.add()
        req_1.keyset_id.uuid.value = "starbug-42"
        req_1.include_domains_count = True
        self.assertEqual(
            self.client.stream_mock.mock_calls,
            [call((request,), method="/Fred.Registry.Api.Keyset.Keyset/batch_keyset_info", timeout=None)],
        )

    async def test_batch_get_flatten(self):
        # Test responses are properly flattened.
        reply_1 = BatchKeysetInfoReply()
        keyset_reply_1 = reply_1.data.replies.add()
        keyset_reply_1.data.keyset_id.uuid.value = "42"
        keyset_reply_1.data.keyset_handle.value = "STARBUG-42"
        keyset_reply_2 = reply_1.data.replies.add()
        keyset_reply_2.data.keyset_id.uuid.value = "43"
        keyset_reply_2.data.keyset_handle.value = "STARBUG-43"
        reply_2 = BatchKeysetInfoReply()
        keyset_reply_3 = reply_2.data.replies.add()
        keyset_reply_3.data.keyset_id.uuid.value = "44"
        keyset_reply_3.data.keyset_handle.value = "STARBUG-44"
        keyset_reply_4 = reply_2.data.replies.add()
        keyset_reply_4.data.keyset_id.uuid.value = "45"
        keyset_reply_4.data.keyset_handle.value = "STARBUG-45"
        self.client.stream_mock.return_value = aiter([reply_1, reply_2])

        keysets = (
            Keyset(keyset_id=KeysetId("42"), keyset_handle="STARBUG-42"),
            Keyset(keyset_id=KeysetId("43"), keyset_handle="STARBUG-43"),
            Keyset(keyset_id=KeysetId("44"), keyset_handle="STARBUG-44"),
            Keyset(keyset_id=KeysetId("45"), keyset_handle="STARBUG-45"),
        )
        await self._test_batch_get(keysets)

    async def _test_list_by_contact(self, result: tuple[KeysetByRelation, ...]) -> None:
        self.assertEqual(await atuple(self.client.list_by_contact(ContactId("KRYTEN"))), result)

        request = ListKeysetsByContactRequest()
        request.contact_id.uuid.value = "KRYTEN"
        self.assertEqual(
            self.client.stream_mock.mock_calls,
            [call(request, method="/Fred.Registry.Api.Keyset.Keyset/list_keysets_by_contact", timeout=None)],
        )

    async def test_list_by_contact_empty_stream(self):
        self.client.stream_mock.return_value = aiter(())

        await self._test_list_by_contact(())

    async def test_list_by_contact_empty_response(self):
        reply = ListKeysetsByContactReply()
        reply.data.keysets.extend([])
        self.client.stream_mock.return_value = aiter([reply])

        await self._test_list_by_contact(())

    async def test_list_by_contact(self):
        reply = ListKeysetsByContactReply()
        keyset_ref = reply.data.keysets.add()
        keyset_ref.keyset.id.uuid.value = "starbug-42"
        keyset_ref.keyset.handle.value = "STARBUG"
        keyset_ref.keyset_history_id.uuid.value = "Nova 5"
        keyset_ref.is_deleted = True
        self.client.stream_mock.return_value = aiter([reply])

        keyset = KeysetByRelation(
            keyset=KeysetRef(id=KeysetId("starbug-42"), handle="STARBUG"),
            keyset_history_id=KeysetHistoryId("Nova 5"),
            is_deleted=True,
        )
        await self._test_list_by_contact((keyset,))

    async def test_list_by_contact_snapshot_id(self):
        # Test snapshot_id is passed to the request correctly.
        self.client.stream_mock.return_value = aiter(())

        await atuple(self.client.list_by_contact(ContactId("KRYTEN"), snapshot_id=SnapshotId("LAST-THURSDAY")))

        request = ListKeysetsByContactRequest()
        request.contact_id.uuid.value = "KRYTEN"
        request.contact_snapshot_id.value = SnapshotId("LAST-THURSDAY")
        self.assertEqual(
            self.client.stream_mock.mock_calls,
            [call(request, method="/Fred.Registry.Api.Keyset.Keyset/list_keysets_by_contact", timeout=None)],
        )

    async def test_list_by_contact_aggregate(self):
        # Test aggregate_entire_history is passed to the request correctly.
        self.client.stream_mock.return_value = aiter(())

        await atuple(self.client.list_by_contact(ContactId("KRYTEN"), aggregate_entire_history=True))

        request = ListKeysetsByContactRequest()
        request.contact_id.uuid.value = "KRYTEN"
        request.aggregate_entire_history = True
        self.assertEqual(
            self.client.stream_mock.mock_calls,
            [call(request, method="/Fred.Registry.Api.Keyset.Keyset/list_keysets_by_contact", timeout=None)],
        )

    async def test_list_by_contact_order_by(self):
        # Test order_by is passed to the request correctly.
        self.client.stream_mock.return_value = aiter(())

        await atuple(self.client.list_by_contact(ContactId("KRYTEN"), order_by=("-crdate", "handle")))

        request = ListKeysetsByContactRequest()
        request.contact_id.uuid.value = "KRYTEN"
        request.order_by.add(
            field=ListKeysetsByContactRequest.OrderByField.order_by_field_crdate,
            direction=OrderByDirection.order_by_direction_descending,
        )
        request.order_by.add(field=ListKeysetsByContactRequest.OrderByField.order_by_field_handle)
        self.assertEqual(
            self.client.stream_mock.mock_calls,
            [call(request, method="/Fred.Registry.Api.Keyset.Keyset/list_keysets_by_contact", timeout=None)],
        )

    async def test_list_by_contact_flatten(self):
        # Test responses are properly flattened.
        reply_1 = ListKeysetsByContactReply()
        keyset_ref_1 = reply_1.data.keysets.add()
        keyset_ref_1.keyset.id.uuid.value = "42"
        keyset_ref_1.keyset.handle.value = "STARBUG-42"
        keyset_ref_2 = reply_1.data.keysets.add()
        keyset_ref_2.keyset.id.uuid.value = "43"
        keyset_ref_2.keyset.handle.value = "STARBUG-43"
        reply_2 = ListKeysetsByContactReply()
        keyset_ref_3 = reply_2.data.keysets.add()
        keyset_ref_3.keyset.id.uuid.value = "44"
        keyset_ref_3.keyset.handle.value = "STARBUG-44"
        keyset_ref_4 = reply_2.data.keysets.add()
        keyset_ref_4.keyset.id.uuid.value = "45"
        keyset_ref_4.keyset.handle.value = "STARBUG-45"
        self.client.stream_mock.return_value = aiter([reply_1, reply_2])

        keysets = (
            KeysetByRelation(keyset=KeysetRef(id=KeysetId("42"), handle="STARBUG-42")),
            KeysetByRelation(keyset=KeysetRef(id=KeysetId("43"), handle="STARBUG-43")),
            KeysetByRelation(keyset=KeysetRef(id=KeysetId("44"), handle="STARBUG-44")),
            KeysetByRelation(keyset=KeysetRef(id=KeysetId("45"), handle="STARBUG-45")),
        )
        await self._test_list_by_contact(keysets)

    async def _test_list_by_contact_page(self, page: ListPage[KeysetByRelation]) -> None:
        result = await self.client.list_by_contact_page(ContactId("KRYTEN"))

        self.assertEqual(result, page)

        request = ListKeysetsByContactRequest()
        request.contact_id.uuid.value = "KRYTEN"
        self.assertEqual(
            self.client.stream_mock.mock_calls,
            [call(request, method="/Fred.Registry.Api.Keyset.Keyset/list_keysets_by_contact", timeout=None)],
        )

    async def test_list_by_contact_page_empty_stream(self):
        self.client.stream_mock.return_value = aiter(())

        await self._test_list_by_contact_page(ListPage(results=()))

    async def test_list_by_contact_page_empty_response(self):
        reply = ListKeysetsByContactReply()
        reply.data.keysets.extend([])
        self.client.stream_mock.return_value = aiter([reply])

        await self._test_list_by_contact_page(ListPage(results=()))

    async def test_list_by_contact_page_pagination_response(self):
        reply = ListKeysetsByContactReply()
        reply.data.pagination.next_page_token = "quagaars"
        reply.data.pagination.items_left = 42
        self.client.stream_mock.return_value = aiter([reply])

        pagination = PaginationReply(next_page_token="quagaars", items_left=42)
        await self._test_list_by_contact_page(ListPage(results=(), pagination=pagination))

    async def test_list_by_contact_page(self):
        reply = ListKeysetsByContactReply()
        keyset_ref = reply.data.keysets.add()
        keyset_ref.keyset.id.uuid.value = "starbug-42"
        keyset_ref.keyset.handle.value = "STARBUG"
        keyset_ref.keyset_history_id.uuid.value = "Nova 5"
        keyset_ref.is_deleted = True
        self.client.stream_mock.return_value = aiter([reply])

        keyset = KeysetByRelation(
            keyset=KeysetRef(id=KeysetId("starbug-42"), handle="STARBUG"),
            keyset_history_id=KeysetHistoryId("Nova 5"),
            is_deleted=True,
        )
        await self._test_list_by_contact_page(ListPage(results=(keyset,)))

    async def test_list_by_contact_page_pagination_request(self):
        # Test pagination args are passed to the request correctly.
        self.client.stream_mock.return_value = aiter(())

        await self.client.list_by_contact_page(ContactId("KRYTEN"), page_token="quagaars", page_size=42)

        request = ListKeysetsByContactRequest()
        request.contact_id.uuid.value = "KRYTEN"
        request.pagination.page_token = "quagaars"
        request.pagination.page_size = 42
        self.assertEqual(
            self.client.stream_mock.mock_calls,
            [call(request, method="/Fred.Registry.Api.Keyset.Keyset/list_keysets_by_contact", timeout=None)],
        )

    async def test_list_by_contact_page_snapshot_id(self):
        # Test snapshot_id is passed to the request correctly.
        self.client.stream_mock.return_value = aiter(())

        await self.client.list_by_contact_page(ContactId("KRYTEN"), snapshot_id=SnapshotId("LAST-THURSDAY"))

        request = ListKeysetsByContactRequest()
        request.contact_id.uuid.value = "KRYTEN"
        request.contact_snapshot_id.value = SnapshotId("LAST-THURSDAY")
        self.assertEqual(
            self.client.stream_mock.mock_calls,
            [call(request, method="/Fred.Registry.Api.Keyset.Keyset/list_keysets_by_contact", timeout=None)],
        )

    async def test_list_by_contact_page_aggregate(self):
        # Test aggregate_entire_history is passed to the request correctly.
        self.client.stream_mock.return_value = aiter(())

        await self.client.list_by_contact_page(ContactId("KRYTEN"), aggregate_entire_history=True)

        request = ListKeysetsByContactRequest()
        request.contact_id.uuid.value = "KRYTEN"
        request.aggregate_entire_history = True
        self.assertEqual(
            self.client.stream_mock.mock_calls,
            [call(request, method="/Fred.Registry.Api.Keyset.Keyset/list_keysets_by_contact", timeout=None)],
        )

    async def test_list_by_contact_page_order_by(self):
        # Test order_by is passed to the request correctly.
        self.client.stream_mock.return_value = aiter(())

        await self.client.list_by_contact_page(ContactId("KRYTEN"), order_by=("-crdate", "handle"))

        request = ListKeysetsByContactRequest()
        request.contact_id.uuid.value = "KRYTEN"
        request.order_by.add(
            field=ListKeysetsByContactRequest.OrderByField.order_by_field_crdate,
            direction=OrderByDirection.order_by_direction_descending,
        )
        request.order_by.add(field=ListKeysetsByContactRequest.OrderByField.order_by_field_handle)
        self.assertEqual(
            self.client.stream_mock.mock_calls,
            [call(request, method="/Fred.Registry.Api.Keyset.Keyset/list_keysets_by_contact", timeout=None)],
        )

    async def test_list_by_contact_page_flatten(self):
        # Test responses are properly flattened.
        reply_1 = ListKeysetsByContactReply()
        keyset_ref_1 = reply_1.data.keysets.add()
        keyset_ref_1.keyset.id.uuid.value = "42"
        keyset_ref_1.keyset.handle.value = "STARBUG-42"
        keyset_ref_2 = reply_1.data.keysets.add()
        keyset_ref_2.keyset.id.uuid.value = "43"
        keyset_ref_2.keyset.handle.value = "STARBUG-43"
        reply_2 = ListKeysetsByContactReply()
        keyset_ref_3 = reply_2.data.keysets.add()
        keyset_ref_3.keyset.id.uuid.value = "44"
        keyset_ref_3.keyset.handle.value = "STARBUG-44"
        keyset_ref_4 = reply_2.data.keysets.add()
        keyset_ref_4.keyset.id.uuid.value = "45"
        keyset_ref_4.keyset.handle.value = "STARBUG-45"
        self.client.stream_mock.return_value = aiter([reply_1, reply_2])

        keysets = (
            KeysetByRelation(keyset=KeysetRef(id=KeysetId("42"), handle="STARBUG-42")),
            KeysetByRelation(keyset=KeysetRef(id=KeysetId("43"), handle="STARBUG-43")),
            KeysetByRelation(keyset=KeysetRef(id=KeysetId("44"), handle="STARBUG-44")),
            KeysetByRelation(keyset=KeysetRef(id=KeysetId("45"), handle="STARBUG-45")),
        )
        await self._test_list_by_contact_page(ListPage(results=keysets))

    async def test_update_state_empty(self):
        self.client.stream_mock.return_value = aiter(())

        await self.client.update_state(())

        self.assertEqual(
            self.client.stream_mock.mock_calls,
            [call((), method="/Fred.Registry.Api.Keyset.Keyset/update_keyset_state", timeout=None)],
        )

    async def test_update_state(self):
        self.client.stream_mock.return_value = aiter(())

        await self.client.update_state(
            (StateUpdate(id=KeysetId("STARBUG-42"), set_flags=("POWER",), unset_flags=("ANCHOR",)),)
        )

        request = UpdateKeysetStateRequest()
        update = request.updates.add()
        update.keyset_id.uuid.value = "STARBUG-42"
        update.set_flags.append("POWER")
        update.unset_flags.append("ANCHOR")
        self.assertEqual(
            self.client.stream_mock.mock_calls,
            [call((request,), method="/Fred.Registry.Api.Keyset.Keyset/update_keyset_state", timeout=None)],
        )

    async def test_update_state_kwargs(self):
        self.client.stream_mock.return_value = aiter(())

        await self.client.update_state(
            (StateUpdate(id=KeysetId("STARBUG-42")),), snapshot_id=SnapshotId("BACKWARDS"), log_entry_id="DEAR-DIARY"
        )

        request = UpdateKeysetStateRequest()
        update = request.updates.add()
        update.keyset_id.uuid.value = "STARBUG-42"
        request.keyset_snapshot_id.value = "BACKWARDS"
        request.log_entry_id.value = "DEAR-DIARY"
        self.assertEqual(
            self.client.stream_mock.mock_calls,
            [call((request,), method="/Fred.Registry.Api.Keyset.Keyset/update_keyset_state", timeout=None)],
        )

    async def test_update_state_batch_size(self):
        self.client.stream_mock.return_value = aiter(())

        await self.client.update_state(
            (StateUpdate(id=KeysetId("STARBUG-42")), StateUpdate(id=KeysetId("STARBUG-43"))), batch_size=1
        )

        request_1 = UpdateKeysetStateRequest()
        update_1 = request_1.updates.add()
        update_1.keyset_id.uuid.value = "STARBUG-42"
        request_2 = UpdateKeysetStateRequest()
        update_2 = request_2.updates.add()
        update_2.keyset_id.uuid.value = "STARBUG-43"
        self.assertEqual(
            self.client.stream_mock.mock_calls,
            [call((request_1, request_2), method="/Fred.Registry.Api.Keyset.Keyset/update_keyset_state", timeout=None)],
        )

    async def test_add_auth_info_generate(self):
        reply = AddKeysetAuthInfoReply()
        reply.data.auth_info = "Gazpacho!"
        self.client.mock.return_value = make_awaitable(reply)

        auth_info = await self.client.add_auth_info(KeysetId("STARBUG-1"))

        self.assertEqual(auth_info, "Gazpacho!")

        request = AddKeysetAuthInfoRequest()
        request.keyset_id.uuid.value = "STARBUG-1"
        request.generate.CopyFrom(Empty())
        self.assertEqual(
            self.client.mock.mock_calls,
            [call(request, method="/Fred.Registry.Api.Keyset.Keyset/add_keyset_auth_info", timeout=None)],
        )

    async def test_add_auth_info_custom(self):
        reply = AddKeysetAuthInfoReply()
        self.client.mock.return_value = make_awaitable(reply)

        auth_info = await self.client.add_auth_info(KeysetId("STARBUG-1"), auth_info="Gazpacho!")  # type: ignore[func-returns-value]

        self.assertIsNone(auth_info)

        request = AddKeysetAuthInfoRequest()
        request.keyset_id.uuid.value = "STARBUG-1"
        request.auth_info = "Gazpacho!"
        self.assertEqual(
            self.client.mock.mock_calls,
            [call(request, method="/Fred.Registry.Api.Keyset.Keyset/add_keyset_auth_info", timeout=None)],
        )

    async def test_add_auth_info_extra_args(self):
        reply = AddKeysetAuthInfoReply()
        reply.data.auth_info = "Gazpacho!"
        self.client.mock.return_value = make_awaitable(reply)

        auth_info = await self.client.add_auth_info(
            KeysetId("STARBUG-1"), snapshot_id=SnapshotId("NOVA5"), ttl=42, log_entry_id="2X4B", by_registrar="HOLLY"
        )

        self.assertEqual(auth_info, "Gazpacho!")

        request = AddKeysetAuthInfoRequest()
        request.keyset_id.uuid.value = "STARBUG-1"
        request.keyset_snapshot_id.value = "NOVA5"
        request.generate.CopyFrom(Empty())
        request.ttl = 42
        request.log_entry_id.value = "2X4B"
        request.by_registrar.value = "HOLLY"
        self.assertEqual(
            self.client.mock.mock_calls,
            [call(request, method="/Fred.Registry.Api.Keyset.Keyset/add_keyset_auth_info", timeout=None)],
        )


class DnsKeyTest(TestCase):
    def test_ds_record(self):
        data = (
            (
                "example.com",
                DnsKey(
                    flags=257,
                    alg=13,
                    key="kXKkvWU3vGYfTJGl3qBd4qhiWp5aRs7YtkCJxD2d+t7KXqwahww5IgJtxJT2yFItlggazyfXqJEVOmMJ3qT0tQ==",
                ),
                DSRecord(
                    keytag=370,
                    alg=13,
                    digest_type=2,
                    digest="BE74359954660069D5C63D200C39F5603827D7DD02B56F120EE9F3A86764247C",
                ),
            ),
            # Example from RFC 4509, section 2.3
            # See https://datatracker.ietf.org/doc/html/rfc4509#section-2.3
            (
                "dskey.example.com",
                DnsKey(
                    flags=256,
                    alg=5,
                    key="AQOeiiR0GOMYkDshWoSKz9XzfwJr1AYtsmx3TGkJaNXVbfi/2pHm822aJ5iI9BMzNXxeYCmZDRD99WYwYqUSdjMmmAphXdvxegXd/M5+X7OrzKBaMbCVdFLUUh6DhweJBjEVv5f2wwjM9XzcnOf+EPbtG9DMBmADjFDc2w/rljwvFw==",
                ),
                DSRecord(
                    keytag=60485,
                    alg=5,
                    digest_type=2,
                    digest="D4B7D520E7BB5F0F67674A0CCEB1E3E0614B93C4F9E99B8383F6A1E4469DA50A",
                ),
            ),
            # Example of high algorithm number
            (
                "example.com",
                DnsKey(
                    flags=257,
                    alg=254,
                    key="Z2F6cGFjaG8h",
                ),
                DSRecord(
                    keytag=53667,
                    alg=254,
                    digest_type=2,
                    digest="65D86A34355F375FE8A4EA41F4619CCB52499EDA07CEBCF6D66DE4916101279C",
                ),
            ),
        )

        for fqdn, dnskey, dsrecord in data:
            with self.subTest(fqdn=fqdn, dnskey=dnskey, dsrecord=dsrecord):
                self.assertEqual(dnskey.ds_record(fqdn), dsrecord)

    def test_keytag_md5(self):
        data = (
            (DnsKey(flags=256, alg=1, key=base64.b64encode(b"abcdefgh").decode()), 25958),
            (DnsKey(flags=256, alg=1, key=base64.b64encode(b"abc").decode()), 0),
        )

        for dnskey, keytag in data:
            with self.subTest(dnskey=dnskey, keytag=keytag):
                self.assertEqual(dnskey._ds_record_keytag(), keytag)

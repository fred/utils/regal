from datetime import datetime
from unittest import IsolatedAsyncioTestCase
from unittest.mock import sentinel

import pytz
from fred_api.registry.domain.domain_history_types_pb2 import DomainHistoryRequest
from fred_api.registry.domain.domain_state_history_types_pb2 import DomainStateHistoryRequest
from fred_api.registry.nsset.nsset_history_types_pb2 import NssetHistoryRequest
from fred_api.registry.nsset.nsset_state_history_types_pb2 import NssetStateHistoryRequest
from frgal.utils import AsyncTestClientMixin

from regal.domain import DomainClient
from regal.nsset import NssetClient


class TestDomainClient(AsyncTestClientMixin, DomainClient):
    history_types = (DomainHistoryRequest, DomainStateHistoryRequest)


class TestNssetClient(AsyncTestClientMixin, NssetClient):
    history_types = (NssetHistoryRequest, NssetStateHistoryRequest)


class RegistryObjectClientTest(IsolatedAsyncioTestCase):
    """Test RegistryObjectClient's methods.

    Methods `get_object_info`, `get_object_state`,
    `get_object_history` and `get_object_state_history`
    only make sense for one of the registry object types.
    They are tested in respecive child classes of
    RegistryObjectClient.
    """

    def test_set_history_limits(self):
        for start in ("nova-5", datetime(2020, 1, 1, tzinfo=pytz.utc), None):
            for end in ("red-dwarf", datetime(2021, 11, 11, tzinfo=pytz.utc), None):
                for client_type in (TestDomainClient, TestNssetClient):
                    client = client_type(netloc=sentinel.netloc)
                    for request_type in client.history_types:
                        with self.subTest(client=client, request_type=request_type, start=start, end=end):
                            request = request_type()
                            client._set_history_limits(request, start=start, end=end)  # type: ignore[arg-type]

                            self.assertHistoryLimit(client.object_history_id, request.history.lower_limit, start)
                            self.assertHistoryLimit(client.object_history_id, request.history.upper_limit, end)

    def assertHistoryLimit(self, object_history_id, limit, instant):
        if isinstance(instant, datetime):
            self.assertEqual(pytz.utc.localize(limit.timestamp.ToDatetime()), instant)
        elif isinstance(instant, str):
            self.assertEqual(getattr(limit, object_history_id).uuid.value, instant)
        else:
            self.assertIsNone(limit.WhichOneof("OptionalTimeSpec"))

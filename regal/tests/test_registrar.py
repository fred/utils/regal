from datetime import datetime, timedelta, timezone
from decimal import Decimal
from functools import cached_property
from pathlib import Path
from unittest import IsolatedAsyncioTestCase, TestCase
from unittest.mock import call, sentinel

from asyncstdlib import tuple as atuple
from fred_api.registry.registrar.registrar_certification_types_pb2 import (
    GetRegistrarCertificationsReply,
    GetRegistrarCertificationsRequest,
)
from fred_api.registry.registrar.registrar_common_types_pb2 import (
    Credit,
    RegistrarCertification as RegistrarCertificationMessage,
    RegistrarHandle,
    SslCertificate as SslCertificateMessage,
)
from fred_api.registry.registrar.registrar_credit_types_pb2 import RegistrarCreditReply, RegistrarCreditRequest
from fred_api.registry.registrar.registrar_epp_credentials_types_pb2 import (
    GetRegistrarEppCredentialsReply,
    GetRegistrarEppCredentialsRequest,
)
from fred_api.registry.registrar.registrar_group_types_pb2 import (
    GetRegistrarGroupsMembershipReply,
    GetRegistrarGroupsMembershipRequest,
    GetRegistrarGroupsReply,
)
from fred_api.registry.registrar.registrar_info_types_pb2 import RegistrarInfoReply, RegistrarInfoRequest
from fred_api.registry.registrar.registrar_list_types_pb2 import RegistrarListReply
from fred_api.registry.registrar.registrar_zone_access_types_pb2 import (
    RegistrarZoneAccessReply,
    RegistrarZoneAccessRequest,
)
from fred_api.registry.registrar.service_admin_grpc_pb2 import (
    AddRegistrarCertificationReply,
    AddRegistrarCertificationRequest,
    AddRegistrarEppCredentialsReply,
    AddRegistrarEppCredentialsRequest,
    AddRegistrarGroupMembershipReply,
    AddRegistrarGroupMembershipRequest,
    AddRegistrarZoneAccessReply,
    AddRegistrarZoneAccessRequest,
    CreateRegistrarReply,
    CreateRegistrarRequest,
    DeleteRegistrarCertificationReply,
    DeleteRegistrarCertificationRequest,
    DeleteRegistrarEppCredentialsReply,
    DeleteRegistrarEppCredentialsRequest,
    DeleteRegistrarGroupMembershipReply,
    DeleteRegistrarGroupMembershipRequest,
    DeleteRegistrarZoneAccessReply,
    DeleteRegistrarZoneAccessRequest,
    UpdateRegistrarCertificationReply,
    UpdateRegistrarCertificationRequest,
    UpdateRegistrarEppCredentialsReply,
    UpdateRegistrarEppCredentialsRequest,
    UpdateRegistrarReply,
    UpdateRegistrarRequest,
    UpdateRegistrarZoneAccessReply,
    UpdateRegistrarZoneAccessRequest,
)
from fred_types import RegistrarId
from freezegun import freeze_time
from frgal.utils import AsyncTestClientMixin
from google.protobuf.empty_pb2 import Empty
from testfixtures import LogCapture

from regal.common import Address, FileId
from regal.exceptions import GroupDoesNotExist, InvalidData, RegistrarCertificationDoesNotExist, RegistrarDoesNotExist
from regal.registrar import (
    EppCredentials,
    EppCredentialsId,
    Registrar,
    RegistrarAdminClient,
    RegistrarCertification,
    RegistrarCertificationId,
    RegistrarClient,
    RegistrarDecoder,
    SslCertificate,
    ZoneAccess,
)

from .utils import make_awaitable

DATA_DIR = Path(__file__).parent / "data"


class RegistrarCertificationFactory:
    """Helper class to create registrar certifications."""

    @classmethod
    def make(cls, id: int, valid_days: int = 0) -> RegistrarCertification:
        # Python API model
        valid_from = datetime(2023, 1, 1, tzinfo=timezone.utc) + timedelta(days=id)
        return RegistrarCertification(
            certification_id=RegistrarCertificationId("ID " + str(id)),
            registrar_handle="REG-1",
            valid_from=valid_from,
            valid_to=None if valid_days <= 0 else valid_from + timedelta(days=valid_days),
            classification=50,
            file_id=FileId("File " + str(id)),
        )

    @classmethod
    def make_message(cls, id: int, valid_days: int = 0) -> RegistrarCertificationMessage:
        # gRPC API message
        time_from = datetime(2023, 1, 1, tzinfo=timezone.utc) + timedelta(days=id)
        result = RegistrarCertificationMessage()
        result.certification_id.value = "ID " + str(id)
        result.registrar_handle.value = "REG-1"
        result.valid_from.FromDatetime(time_from)
        if valid_days > 0:
            result.valid_to.FromDatetime(time_from + timedelta(days=valid_days))
        result.classification = 50
        result.file_id.value = "File " + str(id)
        return result


class TestRegistrarClient(AsyncTestClientMixin, RegistrarClient):
    """Test RegistrarClient."""


class TestRegistrarAdminClient(AsyncTestClientMixin, RegistrarAdminClient):
    """Test RegistrarAdminClient."""


class RegistrarTest(TestCase):
    def test_registrar_id_default(self):
        registrar = Registrar(registrar_id=RegistrarId("12345"), registrar_handle="JMC", name="Jupiter Mining Corp.")
        self.assertEqual(
            registrar.registrar_id,
            RegistrarId("12345"),
        )

        self.assertEqual(
            Registrar(registrar_handle="JMC", name="Jupiter Mining Corp.").registrar_id,  # type: ignore[call-arg]
            RegistrarId("JMC"),
        )

        self.assertEqual(Registrar.default_registrar_id(sentinel.data), sentinel.data)  # type: ignore[operator]


@freeze_time("2000-01-01T00:00:00Z")
class ZoneAccessTest(TestCase):
    def test_is_active(self):
        now = datetime.now(tz=timezone.utc)
        data = (
            (ZoneAccess(zone="cz"), True),
            (ZoneAccess(zone="cz", valid_from=now), True),
            (ZoneAccess(zone="cz", valid_from=now - timedelta(days=1)), True),
            (ZoneAccess(zone="cz", valid_from=now + timedelta(days=1)), False),
            (ZoneAccess(zone="cz", valid_to=now), False),
            (ZoneAccess(zone="cz", valid_to=now - timedelta(days=1)), False),
            (ZoneAccess(zone="cz", valid_to=now + timedelta(days=1)), True),
            (ZoneAccess(zone="cz", valid_from=now - timedelta(days=1), valid_to=now + timedelta(days=1)), True),
            (ZoneAccess(zone="cz", valid_from=now - timedelta(days=2), valid_to=now - timedelta(days=1)), False),
            (ZoneAccess(zone="cz", valid_from=now + timedelta(days=1), valid_to=now + timedelta(days=2)), False),
        )
        for access, is_valid in data:
            with self.subTest(access=access, is_valid=is_valid):
                self.assertIs(access.is_active(), is_valid)


class RegistrarDecoderTest(TestCase):
    def setUp(self):
        self.decoder = RegistrarDecoder()

    def test_decode_credit(self):
        self.assertEqual(self.decoder.decode(Credit(decimal_value="42.19")), Decimal("42.19"))
        self.assertIsNone(self.decoder.decode(Credit()))

    def test_decode_registrar_certification(self):
        decoded = self.decoder.decode(RegistrarCertificationFactory.make_message(1, 100))
        self.assertIsInstance(decoded, RegistrarCertification)
        self.assertEqual(decoded, RegistrarCertificationFactory.make(1, 100))

    def test_decode_ssl_certificate(self):
        self.assertEqual(
            self.decoder.decode(SslCertificateMessage(fingerprint=b"\x00\x01\x02\x03", cert_data_pem="")),
            SslCertificate(fingerprint="00:01:02:03", cert_data_pem=""),
        )


class SslCertificateTest(TestCase):
    def setUp(self):
        self.log_handler = LogCapture("regal.registrar")

    def tearDown(self):
        self.log_handler.uninstall()

    @cached_property
    def cert_data_pem(self):
        with open(DATA_DIR / "cert.pem") as fh:
            return fh.read()

    def test_from_pem(self):
        self.assertEqual(
            SslCertificate.from_pem(self.cert_data_pem).fingerprint,
            "6c:c5:55:3f:bf:eb:d8:d8:4e:dc:bb:74:16:96:7e:af",
        )
        self.log_handler.check()

    def test_from_pem_invalid_certificate(self):
        with self.assertRaises(ValueError):
            SslCertificate.from_pem("invalid")
        self.log_handler.check()

    def test_certificate_invalid(self):
        cert = SslCertificate(fingerprint="00:01:02:03", cert_data_pem="invalid")
        self.assertEqual(cert.fingerprint, "00:01:02:03")
        self.assertEqual(cert.cert_data_pem, "invalid")
        self.assertIsNone(cert.issuer)
        self.log_handler.check(
            (
                "regal.registrar",
                "WARNING",
                "Invalid certificate data with fingerprint 00:01:02:03",
            ),
        )

    def test_fingerprint_invalid(self):
        with self.assertRaises(ValueError):
            SslCertificate(fingerprint="invalid")
        self.log_handler.check()

    def test_fingerprint_mismatch(self):
        cert = SslCertificate(fingerprint="00:01:02:03", cert_data_pem=self.cert_data_pem)
        self.assertEqual(cert.fingerprint, "00:01:02:03")
        self.assertEqual(cert.cert_data_pem, self.cert_data_pem)
        self.log_handler.check(
            (
                "regal.registrar",
                "WARNING",
                (
                    "Fingerprint 00:01:02:03 does not match computed fingerprint "
                    "6c:c5:55:3f:bf:eb:d8:d8:4e:dc:bb:74:16:96:7e:af"
                ),
            )
        )

    def test_properties(self):
        cert = SslCertificate.from_pem(self.cert_data_pem)
        self.assertEqual(
            cert.issuer,
            "1.2.840.113549.1.9.1=rimmer@example.com,CN=Arnold Rimmer,"
            "OU=Red Dwarf,O=Jupiter Mining Corp.,L=Prague,ST=Some-State,C=CZ",
        )
        self.assertEqual(
            cert.subject,
            "1.2.840.113549.1.9.1=rimmer@example.com,CN=Arnold Rimmer,"
            "OU=Red Dwarf,O=Jupiter Mining Corp.,L=Prague,ST=Some-State,C=CZ",
        )
        self.assertEqual(cert.not_valid_before, datetime(2022, 11, 15, 8, 26, 20, tzinfo=timezone.utc))
        self.assertEqual(cert.not_valid_after, datetime(2023, 11, 15, 8, 26, 20, tzinfo=timezone.utc))

    def test_properties_none(self):
        cert = SslCertificate(fingerprint="01:02:03:04")
        self.assertIsNone(cert.issuer)
        self.assertIsNone(cert.subject)
        self.assertIsNone(cert.not_valid_before)
        self.assertIsNone(cert.not_valid_after)


class RegistrarClientTest(IsolatedAsyncioTestCase):
    """Test RegistrarClient's methods."""

    def setUp(self):
        self.client = TestRegistrarClient(netloc=sentinel.netloc)

    async def test_list_handles(self):
        registrar_handles = RegistrarListReply()
        registrar_handles.data.registrar_handles.extend(
            [
                RegistrarHandle(value="REG-SW"),
                RegistrarHandle(value="REG-ST"),
            ]
        )
        self.client.mock.return_value = make_awaitable(registrar_handles)

        self.assertEqual(await atuple(self.client.list_handles()), ("REG-SW", "REG-ST"))

        self.client.mock.assert_called_once_with(
            Empty(),
            method="/Fred.Registry.Api.Registrar.Registrar/get_registrar_handles",
            timeout=None,
        )

    async def test_get_registrar_handles(self):
        registrar_handles = RegistrarListReply()
        registrar_handles.data.registrar_handles.extend(
            [
                RegistrarHandle(value="REG-SW"),
                RegistrarHandle(value="REG-ST"),
            ]
        )
        self.client.mock.return_value = make_awaitable(registrar_handles)

        with self.assertWarnsRegex(
            DeprecationWarning, "Method get_registrar_handles is deprecated in favor of list_handles."
        ):
            self.assertEqual(await self.client.get_registrar_handles(), ["REG-SW", "REG-ST"])

        self.client.mock.assert_called_once_with(
            Empty(),
            method="/Fred.Registry.Api.Registrar.Registrar/get_registrar_handles",
            timeout=None,
        )

    async def test_get(self):
        registrar_info = RegistrarInfoReply()
        registrar_info.data.registrar_handle.value = "REG-SW"
        registrar_info.data.name = "Star Wars Inc."
        registrar_info.data.place.street[:] = ["Narrow street 12"]
        registrar_info.data.place.city = "Cloud City"
        registrar_info.data.place.postal_code.value = "12345"
        registrar_info.data.place.country_code.value = "GE"
        self.client.mock.return_value = make_awaitable(registrar_info)

        reply = await self.client.get("REG-SW")
        self.assertEqual(
            reply,
            Registrar(  # type: ignore[call-arg]
                registrar_handle="REG-SW",
                name="Star Wars Inc.",
                place=Address(
                    street=["Narrow street 12"],
                    city="Cloud City",
                    postal_code="12345",
                    country_code="GE",
                ),
            ),
        )

        request = RegistrarInfoRequest()
        request.registrar_handle.value = "REG-SW"
        self.client.mock.assert_called_once_with(
            request,
            method="/Fred.Registry.Api.Registrar.Registrar/get_registrar_info",
            timeout=None,
        )

    async def test_get_registrar_info(self):
        registrar_info = RegistrarInfoReply()
        registrar_info.data.registrar_handle.value = "REG-SW"
        registrar_info.data.name = "Star Wars Inc."
        registrar_info.data.place.street[:] = ["Narrow street 12"]
        registrar_info.data.place.city = "Cloud City"
        registrar_info.data.place.postal_code.value = "12345"
        registrar_info.data.place.country_code.value = "GE"
        self.client.mock.return_value = make_awaitable(registrar_info)

        with self.assertWarnsRegex(DeprecationWarning, "Method get_registrar_info is deprecated in favor of get."):
            reply = await self.client.get_registrar_info("REG-SW")
        self.assertEqual(
            reply,
            Registrar(  # type: ignore[call-arg]
                registrar_handle="REG-SW",
                name="Star Wars Inc.",
                place=Address(
                    street=["Narrow street 12"],
                    city="Cloud City",
                    postal_code="12345",
                    country_code="GE",
                ),
            ),
        )

        request = RegistrarInfoRequest()
        request.registrar_handle.value = "REG-SW"
        self.client.mock.assert_called_once_with(
            request,
            method="/Fred.Registry.Api.Registrar.Registrar/get_registrar_info",
            timeout=None,
        )

    async def test_get_credit(self):
        registrar_credit = RegistrarCreditReply()
        registrar_credit.data.credit_by_zone["cz"].decimal_value = "42000.40"
        registrar_credit.data.credit_by_zone["0.2.4.e164.arpa"].decimal_value = "0.00"
        self.client.mock.return_value = make_awaitable(registrar_credit)

        self.assertEqual(
            await self.client.get_credit("REG-SW"),
            {
                "cz": Decimal("42000.40"),
                "0.2.4.e164.arpa": Decimal("0.00"),
            },
        )

        request = RegistrarCreditRequest()
        request.registrar_handle.value = "REG-SW"
        self.client.mock.assert_called_once_with(
            request,
            method="/Fred.Registry.Api.Registrar.Registrar/get_registrar_credit",
            timeout=None,
        )

    async def test_get_registrar_credit(self):
        registrar_credit = RegistrarCreditReply()
        registrar_credit.data.credit_by_zone["cz"].decimal_value = "42000.40"
        registrar_credit.data.credit_by_zone["0.2.4.e164.arpa"].decimal_value = "0.00"
        self.client.mock.return_value = make_awaitable(registrar_credit)

        with self.assertWarnsRegex(
            DeprecationWarning, "Method get_registrar_credit is deprecated in favor of get_credit."
        ):
            self.assertEqual(
                await self.client.get_registrar_credit("REG-SW"),
                {
                    "cz": Decimal("42000.40"),
                    "0.2.4.e164.arpa": Decimal("0.00"),
                },
            )

        request = RegistrarCreditRequest()
        request.registrar_handle.value = "REG-SW"
        self.client.mock.assert_called_once_with(
            request,
            method="/Fred.Registry.Api.Registrar.Registrar/get_registrar_credit",
            timeout=None,
        )

    async def test_list_zone_access(self):
        registrar_zone_access = RegistrarZoneAccessReply()
        registrar_zone_access.data.access_by_zone["cz"].has_access_now = True
        history = registrar_zone_access.data.access_by_zone["cz"].history.add()
        history.valid_from.FromDatetime(datetime(2020, 1, 1, tzinfo=timezone.utc))
        history.valid_to.FromDatetime(datetime(2021, 1, 1, tzinfo=timezone.utc))
        history = registrar_zone_access.data.access_by_zone["cz"].history.add()
        history.valid_from.FromDatetime(datetime(2022, 1, 1, tzinfo=timezone.utc))
        registrar_zone_access.data.access_by_zone["0.2.4.e164.arpa"].has_access_now = False
        history = registrar_zone_access.data.access_by_zone["0.2.4.e164.arpa"].history.add()
        history.valid_from.FromDatetime(datetime(2020, 1, 1, tzinfo=timezone.utc))
        history.valid_to.FromDatetime(datetime(2021, 1, 1, tzinfo=timezone.utc))
        self.client.mock.return_value = make_awaitable(registrar_zone_access)

        self.assertEqual(
            await atuple(self.client.list_zone_access("REG-SW")),
            (
                ZoneAccess(
                    zone="cz",
                    valid_from=datetime(2020, 1, 1, tzinfo=timezone.utc),
                    valid_to=datetime(2021, 1, 1, tzinfo=timezone.utc),
                ),
                ZoneAccess(
                    zone="cz",
                    valid_from=datetime(2022, 1, 1, tzinfo=timezone.utc),
                    valid_to=None,
                ),
                ZoneAccess(
                    zone="0.2.4.e164.arpa",
                    valid_from=datetime(2020, 1, 1, tzinfo=timezone.utc),
                    valid_to=datetime(2021, 1, 1, tzinfo=timezone.utc),
                ),
            ),
        )

        request = RegistrarZoneAccessRequest()
        request.registrar_handle.value = "REG-SW"
        self.client.mock.assert_called_once_with(
            request,
            method="/Fred.Registry.Api.Registrar.Registrar/get_registrar_zone_access",
            timeout=None,
        )

    async def test_get_registrar_zone_access(self):
        registrar_zone_access = RegistrarZoneAccessReply()
        registrar_zone_access.data.access_by_zone["cz"].has_access_now = True
        registrar_zone_access.data.access_by_zone["0.2.4.e164.arpa"].has_access_now = False
        self.client.mock.return_value = make_awaitable(registrar_zone_access)

        with self.assertWarnsRegex(
            DeprecationWarning, "Method get_registrar_zone_access is deprecated in favor of list_zone_access."
        ):
            self.assertEqual(
                await self.client.get_registrar_zone_access("REG-SW"),
                {
                    "cz": True,
                    "0.2.4.e164.arpa": False,
                },
            )

        request = RegistrarZoneAccessRequest()
        request.registrar_handle.value = "REG-SW"
        self.client.mock.assert_called_once_with(
            request,
            method="/Fred.Registry.Api.Registrar.Registrar/get_registrar_zone_access",
            timeout=None,
        )

    async def test_get_registrar_zone_access_history(self):
        registrar_zone_access = RegistrarZoneAccessReply()
        registrar_zone_access.data.access_by_zone["cz"].has_access_now = True
        history = registrar_zone_access.data.access_by_zone["cz"].history.add()
        history.valid_from.FromDatetime(datetime(2020, 1, 1, tzinfo=timezone.utc))
        registrar_zone_access.data.access_by_zone["0.2.4.e164.arpa"].has_access_now = False
        history = registrar_zone_access.data.access_by_zone["0.2.4.e164.arpa"].history.add()
        history.valid_from.FromDatetime(datetime(2020, 1, 1, tzinfo=timezone.utc))
        history.valid_to.FromDatetime(datetime(2021, 1, 1, tzinfo=timezone.utc))
        self.client.mock.return_value = make_awaitable(registrar_zone_access)

        with self.assertWarnsRegex(
            DeprecationWarning, "Method get_registrar_zone_access_history is deprecated in favor of list_zone_access."
        ):
            self.assertEqual(
                await self.client.get_registrar_zone_access_history("REG-SW"),
                {
                    "cz": [
                        ZoneAccess(
                            zone="cz",
                            valid_from=datetime(2020, 1, 1, tzinfo=timezone.utc),
                            valid_to=None,
                        )
                    ],
                    "0.2.4.e164.arpa": [
                        ZoneAccess(
                            zone="0.2.4.e164.arpa",
                            valid_from=datetime(2020, 1, 1, tzinfo=timezone.utc),
                            valid_to=datetime(2021, 1, 1, tzinfo=timezone.utc),
                        )
                    ],
                },
            )

        request = RegistrarZoneAccessRequest()
        request.registrar_handle.value = "REG-SW"
        self.client.mock.assert_called_once_with(
            request,
            method="/Fred.Registry.Api.Registrar.Registrar/get_registrar_zone_access",
            timeout=None,
        )

    async def test_list_epp_credentials(self):
        reply = GetRegistrarEppCredentialsReply()
        reply.data.registrar_handle.value = "JMC"
        credentials = reply.data.credentials.add()
        credentials.credentials_id.value = "epp:1"
        credentials.create_time.FromDatetime(datetime(2022, 1, 1, tzinfo=timezone.utc))
        credentials.certificate.fingerprint = b"\x01\x02\x03\x04"
        credentials.certificate.cert_data_pem = ""
        self.client.mock.return_value = make_awaitable(reply)

        response = await atuple(self.client.list_epp_credentials("JMC"))

        self.assertEqual(
            response,
            (
                EppCredentials(
                    id=EppCredentialsId("epp:1"),
                    create_time=datetime(2022, 1, 1, tzinfo=timezone.utc),
                    certificate=SslCertificate(fingerprint="01:02:03:04", cert_data_pem=""),
                ),
            ),
        )

        request = GetRegistrarEppCredentialsRequest()
        request.registrar_handle.value = "JMC"
        self.client.mock.assert_called_once_with(
            request,
            method="/Fred.Registry.Api.Registrar.Registrar/get_registrar_epp_credentials",
            timeout=None,
        )

    async def test_get_registrar_epp_credentials(self):
        reply = GetRegistrarEppCredentialsReply()
        reply.data.registrar_handle.value = "JMC"
        credentials = reply.data.credentials.add()
        credentials.credentials_id.value = "epp:1"
        credentials.create_time.FromDatetime(datetime(2022, 1, 1, tzinfo=timezone.utc))
        credentials.certificate.fingerprint = b"\x01\x02\x03\x04"
        credentials.certificate.cert_data_pem = ""
        self.client.mock.return_value = make_awaitable(reply)

        with self.assertWarnsRegex(
            DeprecationWarning, "Method get_registrar_epp_credentials is deprecated in favor of list_epp_credentials."
        ):
            response = await self.client.get_registrar_epp_credentials("JMC")

        self.assertEqual(
            response,
            [
                {
                    "credentials_id": "epp:1",
                    "create_time": datetime(2022, 1, 1, tzinfo=timezone.utc),
                    "certificate": SslCertificate(fingerprint="01:02:03:04", cert_data_pem=""),
                },
            ],
        )

        request = GetRegistrarEppCredentialsRequest()
        request.registrar_handle.value = "JMC"
        self.client.mock.assert_called_once_with(
            request,
            method="/Fred.Registry.Api.Registrar.Registrar/get_registrar_epp_credentials",
            timeout=None,
        )

    async def test_list_groups_empty(self):
        self.client.mock.return_value = make_awaitable(GetRegistrarGroupsReply())

        response = await atuple(self.client.list_groups())

        self.assertEqual(response, ())
        self.client.mock.assert_called_once_with(
            Empty(),
            method="/Fred.Registry.Api.Registrar.Registrar/get_registrar_groups",
            timeout=None,
        )

    async def test_list_groups(self):
        groups = ("group 1", "group 2", "group 3", "group 4", "group 5")
        reply = GetRegistrarGroupsReply()
        reply.data.groups.extend(groups)
        self.client.mock.return_value = make_awaitable(reply)

        response = await atuple(self.client.list_groups())

        self.assertEqual(response, groups)

    async def test_get_registrar_groups_empty(self):
        self.client.mock.return_value = make_awaitable(GetRegistrarGroupsReply())

        with self.assertWarnsRegex(
            DeprecationWarning, "Method get_registrar_groups is deprecated in favor of list_groups."
        ):
            response = await self.client.get_registrar_groups()

        self.assertEqual(response, [])
        self.client.mock.assert_called_once_with(
            Empty(),
            method="/Fred.Registry.Api.Registrar.Registrar/get_registrar_groups",
            timeout=None,
        )

    async def test_get_registrar_groups(self):
        groups = ["group 1", "group 2", "group 3", "group 4", "group 5"]
        reply = GetRegistrarGroupsReply()
        reply.data.groups.extend(groups)
        self.client.mock.return_value = make_awaitable(reply)

        with self.assertWarnsRegex(
            DeprecationWarning, "Method get_registrar_groups is deprecated in favor of list_groups."
        ):
            response = await self.client.get_registrar_groups()

        self.assertEqual(response, groups)

    async def test_list_group_membership_empty(self):
        reply = GetRegistrarGroupsMembershipReply()
        reply.data.registrar_handle.value = "REG-1"
        self.client.mock.return_value = make_awaitable(reply)

        response = await atuple(self.client.list_group_membership("REG-1"))

        self.assertEqual(response, ())
        request = GetRegistrarGroupsMembershipRequest()
        request.registrar_handle.value = "REG-1"
        self.client.mock.assert_called_once_with(
            request,
            method="/Fred.Registry.Api.Registrar.Registrar/get_registrar_groups_membership",
            timeout=None,
        )

    async def test_list_group_membership(self):
        groups = ("group 1", "group 2", "group 3", "group 4", "group 5")
        reply = GetRegistrarGroupsMembershipReply()
        reply.data.registrar_handle.value = "REG-1"
        reply.data.groups.extend(groups)
        self.client.mock.return_value = make_awaitable(reply)

        response = await atuple(self.client.list_group_membership("REG-1"))

        self.assertEqual(response, groups)

    async def test_list_group_membership_not_found(self):
        reply = GetRegistrarGroupsMembershipReply()
        reply.exception.registrar_does_not_exist.CopyFrom(
            GetRegistrarGroupsMembershipReply.Exception.RegistrarDoesNotExist()
        )
        self.client.mock.return_value = make_awaitable(reply)
        with self.assertRaises(RegistrarDoesNotExist):
            await atuple(self.client.list_group_membership("REG-1"))

    async def test_get_registrar_groups_membership_empty(self):
        reply = GetRegistrarGroupsMembershipReply()
        reply.data.registrar_handle.value = "REG-1"
        self.client.mock.return_value = make_awaitable(reply)

        with self.assertWarnsRegex(
            DeprecationWarning,
            "Method get_registrar_groups_membership is deprecated in favor of list_group_membership.",
        ):
            response = await self.client.get_registrar_groups_membership("REG-1")

        self.assertEqual(response, [])
        request = GetRegistrarGroupsMembershipRequest()
        request.registrar_handle.value = "REG-1"
        self.client.mock.assert_called_once_with(
            request,
            method="/Fred.Registry.Api.Registrar.Registrar/get_registrar_groups_membership",
            timeout=None,
        )

    async def test_get_registrar_groups_membership(self):
        groups = ["group 1", "group 2", "group 3", "group 4", "group 5"]
        reply = GetRegistrarGroupsMembershipReply()
        reply.data.registrar_handle.value = "REG-1"
        reply.data.groups.extend(groups)
        self.client.mock.return_value = make_awaitable(reply)

        with self.assertWarnsRegex(
            DeprecationWarning,
            "Method get_registrar_groups_membership is deprecated in favor of list_group_membership.",
        ):
            response = await self.client.get_registrar_groups_membership("REG-1")

        self.assertEqual(response, groups)

    async def test_list_certifications_empty(self):
        reply = GetRegistrarCertificationsReply()
        self.client.mock.return_value = make_awaitable(reply)

        response = await atuple(self.client.list_certifications("REG-1"))

        request = GetRegistrarCertificationsRequest()
        request.registrar_handle.value = "REG-1"
        self.client.mock.assert_called_once_with(
            request,
            method="/Fred.Registry.Api.Registrar.Registrar/get_registrar_certifications",
            timeout=None,
        )
        self.assertEqual(response, ())

    async def test_list_certifications(self):
        certifications_pb = [RegistrarCertificationFactory.make_message(1, 100)]
        certifications_pb.extend([RegistrarCertificationFactory.make_message(i) for i in range(2, 6)])
        reply = GetRegistrarCertificationsReply()
        reply.data.certifications.extend(certifications_pb)
        self.client.mock.return_value = make_awaitable(reply)

        response = await atuple(self.client.list_certifications("REG-1"))

        certifications = [RegistrarCertificationFactory.make(1, 100)]
        certifications.extend([RegistrarCertificationFactory.make(i) for i in range(2, 6)])
        self.assertEqual(response, tuple(certifications))

    async def test_get_registrar_certifications_empty(self):
        reply = GetRegistrarCertificationsReply()
        self.client.mock.return_value = make_awaitable(reply)

        with self.assertWarnsRegex(
            DeprecationWarning, "Method get_registrar_certifications is deprecated in favor of list_certifications."
        ):
            response = await self.client.get_registrar_certifications("REG-1")

        request = GetRegistrarCertificationsRequest()
        request.registrar_handle.value = "REG-1"
        self.client.mock.assert_called_once_with(
            request,
            method="/Fred.Registry.Api.Registrar.Registrar/get_registrar_certifications",
            timeout=None,
        )
        self.assertEqual(response, [])

    async def test_get_registrar_certifications(self):
        certifications_pb = [RegistrarCertificationFactory.make_message(1, 100)]
        certifications_pb.extend([RegistrarCertificationFactory.make_message(i) for i in range(2, 6)])
        reply = GetRegistrarCertificationsReply()
        reply.data.certifications.extend(certifications_pb)
        self.client.mock.return_value = make_awaitable(reply)

        with self.assertWarnsRegex(
            DeprecationWarning, "Method get_registrar_certifications is deprecated in favor of list_certifications."
        ):
            response = await self.client.get_registrar_certifications("REG-1")

        certifications = [RegistrarCertificationFactory.make(1, 100)]
        certifications.extend([RegistrarCertificationFactory.make(i) for i in range(2, 6)])
        self.assertEqual(response, certifications)

    async def test_get_registrar_certifications_not_found(self):
        reply = GetRegistrarCertificationsReply()
        reply.exception.registrar_does_not_exist.CopyFrom(
            GetRegistrarCertificationsReply.Exception.RegistrarDoesNotExist()
        )
        self.client.mock.return_value = make_awaitable(reply)

        with self.assertWarnsRegex(
            DeprecationWarning, "Method get_registrar_certifications is deprecated in favor of list_certifications."
        ):
            with self.assertRaises(RegistrarDoesNotExist):
                await self.client.get_registrar_certifications("REG-1")


class RegistrarAdminClientTest(IsolatedAsyncioTestCase):
    """Test RegistrarAdminClient's methods."""

    def setUp(self):
        self.client = TestRegistrarAdminClient(netloc=sentinel.netloc)
        self.now = datetime(2022, 1, 1, tzinfo=timezone.utc)
        self.tomorrow = self.now + timedelta(days=1)

    async def test_create_minimal(self):
        registrar = Registrar(registrar_handle="HOLLY", name="Holly", organization="JMC")  # type: ignore[call-arg]
        reply = CreateRegistrarReply()
        reply.data.registrar_handle.value = "HOLLY"
        self.client.mock.return_value = make_awaitable(reply)

        await self.client.create(registrar)

        request = CreateRegistrarRequest()
        request.registrar_handle.value = "HOLLY"
        request.name = "Holly"
        request.organization = "JMC"
        calls = [call(request, method="/Fred.Registry.Api.Registrar.Admin/create_registrar", timeout=None)]
        self.assertEqual(self.client.mock.mock_calls, calls)

    async def test_create(self):
        place = Address(
            street=["Deck 16"], city="Red Dwarf", state_or_province="Deep Space", postal_code="JMC", country_code="JU"
        )
        registrar = Registrar(  # type: ignore[call-arg]
            registrar_handle="HOLLY",
            name="Holly",
            organization="JMC",
            place=place,
            telephone="+1.234",
            fax="+9.876",
            emails=["holly@example.org"],
            url="jmc.example.org",
            company_registration_number="JMC42",
            vat_identification_number="JU42",
            is_system_registrar=True,
            variable_symbol="42",
            payment_memo_regex=".*",
            is_vat_payer=True,
        )
        reply = CreateRegistrarReply()
        reply.data.registrar_handle.value = "HOLLY"
        self.client.mock.return_value = make_awaitable(reply)

        await self.client.create(registrar)

        request = CreateRegistrarRequest()
        request.registrar_handle.value = "HOLLY"
        request.name = "Holly"
        request.organization = "JMC"
        request.place.street.append("Deck 16")
        request.place.city = "Red Dwarf"
        request.place.state_or_province = "Deep Space"
        request.place.postal_code.value = "JMC"
        request.place.country_code.value = "JU"
        request.telephone.value = "+1.234"
        request.fax.value = "+9.876"
        request.emails.add(value="holly@example.org")
        request.url.value = "jmc.example.org"
        request.is_system_registrar = True
        request.company_registration_number.value = "JMC42"
        request.vat_identification_number.value = "JU42"
        request.variable_symbol = "42"
        request.payment_memo_regex = ".*"
        request.is_vat_payer = True
        calls = [call(request, method="/Fred.Registry.Api.Registrar.Admin/create_registrar", timeout=None)]
        self.assertEqual(self.client.mock.mock_calls, calls)

    async def test_create_registrar_minimal(self):
        registrar = Registrar(registrar_handle="HOLLY", name="Holly", organization="JMC")  # type: ignore[call-arg]
        reply = CreateRegistrarReply()
        reply.data.registrar_handle.value = "HOLLY"
        self.client.mock.return_value = make_awaitable(reply)

        with self.assertWarnsRegex(DeprecationWarning, "Method create_registrar is deprecated in favor of create."):
            await self.client.create_registrar(registrar)

        request = CreateRegistrarRequest()
        request.registrar_handle.value = "HOLLY"
        request.name = "Holly"
        request.organization = "JMC"
        calls = [call(request, method="/Fred.Registry.Api.Registrar.Admin/create_registrar", timeout=None)]
        self.assertEqual(self.client.mock.mock_calls, calls)

    async def test_create_registrar(self):
        place = Address(
            street=["Deck 16"], city="Red Dwarf", state_or_province="Deep Space", postal_code="JMC", country_code="JU"
        )
        registrar = Registrar(  # type: ignore[call-arg]
            registrar_handle="HOLLY",
            name="Holly",
            organization="JMC",
            place=place,
            telephone="+1.234",
            fax="+9.876",
            emails=["holly@example.org"],
            url="jmc.example.org",
            company_registration_number="JMC42",
            vat_identification_number="JU42",
            is_system_registrar=True,
            variable_symbol="42",
            payment_memo_regex=".*",
            is_vat_payer=True,
        )
        reply = CreateRegistrarReply()
        reply.data.registrar_handle.value = "HOLLY"
        self.client.mock.return_value = make_awaitable(reply)

        with self.assertWarnsRegex(DeprecationWarning, "Method create_registrar is deprecated in favor of create."):
            await self.client.create_registrar(registrar)

        request = CreateRegistrarRequest()
        request.registrar_handle.value = "HOLLY"
        request.name = "Holly"
        request.organization = "JMC"
        request.place.street.append("Deck 16")
        request.place.city = "Red Dwarf"
        request.place.state_or_province = "Deep Space"
        request.place.postal_code.value = "JMC"
        request.place.country_code.value = "JU"
        request.telephone.value = "+1.234"
        request.fax.value = "+9.876"
        request.emails.add(value="holly@example.org")
        request.url.value = "jmc.example.org"
        request.is_system_registrar = True
        request.company_registration_number.value = "JMC42"
        request.vat_identification_number.value = "JU42"
        request.variable_symbol = "42"
        request.payment_memo_regex = ".*"
        request.is_vat_payer = True
        calls = [call(request, method="/Fred.Registry.Api.Registrar.Admin/create_registrar", timeout=None)]
        self.assertEqual(self.client.mock.mock_calls, calls)

    async def test_update_minimal(self):
        reply = UpdateRegistrarReply()
        reply.data.registrar_handle.value = "HOLLY"
        self.client.mock.return_value = make_awaitable(reply)

        await self.client.update("HOLLY")

        request = UpdateRegistrarRequest()
        request.registrar_handle.value = "HOLLY"
        self.assertEqual(
            self.client.mock.mock_calls,
            [
                call(request, method="/Fred.Registry.Api.Registrar.Admin/update_registrar", timeout=None),
            ],
        )

    async def test_update_minimal_address(self):
        address = Address(street=["Deck 5"], city="Red Dwarf")
        reply = UpdateRegistrarReply()
        reply.data.registrar_handle.value = "HOLLY"
        self.client.mock.return_value = make_awaitable(reply)

        await self.client.update("HOLLY", place=address)

        request = UpdateRegistrarRequest()
        request.registrar_handle.value = "HOLLY"
        request.set_place.street[:] = ["Deck 5"]
        request.set_place.city = "Red Dwarf"
        self.assertEqual(
            self.client.mock.mock_calls,
            [
                call(request, method="/Fred.Registry.Api.Registrar.Admin/update_registrar", timeout=None),
            ],
        )

    async def test_update(self):
        address = Address(street=["Deck 5"], city="Red Dwarf", postal_code="123", country_code="JMC")
        reply = UpdateRegistrarReply()
        reply.data.registrar_handle.value = "HILLY"
        self.client.mock.return_value = make_awaitable(reply)

        await self.client.update(
            "HOLLY",
            registrar_handle="HILLY",
            name="Holly",
            organization="Jupiter Mining Corp.",
            place=address,
            telephone="+420.111222333",
            fax="+420.444555666",
            emails=["holly@example.com"],
            url="http://example.com",
            is_system_registrar=True,
            company_registration_number="12345",
            vat_identification_number="67890",
            variable_symbol="VAR",
            payment_memo_regex="REGEX",
            is_vat_payer=True,
            is_internal=False,
        )

        request = UpdateRegistrarRequest()
        request.registrar_handle.value = "HOLLY"
        request.set_registrar_handle.value = "HILLY"
        request.set_name = "Holly"
        request.set_organization = "Jupiter Mining Corp."
        request.set_place.street[:] = ["Deck 5"]
        request.set_place.city = "Red Dwarf"
        request.set_place.postal_code.value = "123"
        request.set_place.country_code.value = "JMC"
        request.set_telephone.value = "+420.111222333"
        request.set_fax.value = "+420.444555666"
        request.set_emails.values.add(value="holly@example.com")
        request.set_url.value = "http://example.com"
        request.set_is_system_registrar = True
        request.set_company_registration_number.value = "12345"
        request.set_vat_identification_number.value = "67890"
        request.set_variable_symbol = "VAR"
        request.set_payment_memo_regex = "REGEX"
        request.set_is_vat_payer = True
        request.set_is_internal = False
        self.assertEqual(
            self.client.mock.mock_calls,
            [
                call(request, method="/Fred.Registry.Api.Registrar.Admin/update_registrar", timeout=None),
            ],
        )

    async def test_update_registrar_minimal(self):
        reply = UpdateRegistrarReply()
        reply.data.registrar_handle.value = "HOLLY"
        self.client.mock.return_value = make_awaitable(reply)

        with self.assertWarnsRegex(DeprecationWarning, "Method update_registrar is deprecated in favor of update."):
            await self.client.update_registrar("HOLLY")

        request = UpdateRegistrarRequest()
        request.registrar_handle.value = "HOLLY"
        self.assertEqual(
            self.client.mock.mock_calls,
            [
                call(request, method="/Fred.Registry.Api.Registrar.Admin/update_registrar", timeout=None),
            ],
        )

    async def test_update_registrar_minimal_address(self):
        address = Address(street=["Deck 5"], city="Red Dwarf")
        reply = UpdateRegistrarReply()
        reply.data.registrar_handle.value = "HOLLY"
        self.client.mock.return_value = make_awaitable(reply)

        with self.assertWarnsRegex(DeprecationWarning, "Method update_registrar is deprecated in favor of update."):
            await self.client.update_registrar("HOLLY", place=address)

        request = UpdateRegistrarRequest()
        request.registrar_handle.value = "HOLLY"
        request.set_place.street[:] = ["Deck 5"]
        request.set_place.city = "Red Dwarf"
        self.assertEqual(
            self.client.mock.mock_calls,
            [
                call(request, method="/Fred.Registry.Api.Registrar.Admin/update_registrar", timeout=None),
            ],
        )

    async def test_update_registrar(self):
        address = Address(street=["Deck 5"], city="Red Dwarf", postal_code="123", country_code="JMC")
        reply = UpdateRegistrarReply()
        reply.data.registrar_handle.value = "HILLY"
        self.client.mock.return_value = make_awaitable(reply)

        with self.assertWarnsRegex(DeprecationWarning, "Method update_registrar is deprecated in favor of update."):
            await self.client.update_registrar(
                "HOLLY",
                registrar_handle="HILLY",
                name="Holly",
                organization="Jupiter Mining Corp.",
                place=address,
                telephone="+420.111222333",
                fax="+420.444555666",
                emails=["holly@example.com"],
                url="http://example.com",
                is_system_registrar=True,
                company_registration_number="12345",
                vat_identification_number="67890",
                variable_symbol="VAR",
                payment_memo_regex="REGEX",
                is_vat_payer=True,
                is_internal=False,
            )

        request = UpdateRegistrarRequest()
        request.registrar_handle.value = "HOLLY"
        request.set_registrar_handle.value = "HILLY"
        request.set_name = "Holly"
        request.set_organization = "Jupiter Mining Corp."
        request.set_place.street[:] = ["Deck 5"]
        request.set_place.city = "Red Dwarf"
        request.set_place.postal_code.value = "123"
        request.set_place.country_code.value = "JMC"
        request.set_telephone.value = "+420.111222333"
        request.set_fax.value = "+420.444555666"
        request.set_emails.values.add(value="holly@example.com")
        request.set_url.value = "http://example.com"
        request.set_is_system_registrar = True
        request.set_company_registration_number.value = "12345"
        request.set_vat_identification_number.value = "67890"
        request.set_variable_symbol = "VAR"
        request.set_payment_memo_regex = "REGEX"
        request.set_is_vat_payer = True
        request.set_is_internal = False
        self.assertEqual(
            self.client.mock.mock_calls,
            [
                call(request, method="/Fred.Registry.Api.Registrar.Admin/update_registrar", timeout=None),
            ],
        )

    def _get_add_zone_access_request(self, registrar="REG-SW", zone="cz", valid_from=None, valid_to=None):
        request = AddRegistrarZoneAccessRequest()
        request.registrar_handle.value = registrar
        request.zone = zone
        if valid_from:
            request.valid_from.FromDatetime(valid_from)
        if valid_to:
            request.valid_to.FromDatetime(valid_to)
        return request

    def _get_zone_access_reply(
        self,
        reply,
        registrar="REG-SW",
        zone="cz",
        valid_from=datetime(2022, 1, 1, tzinfo=timezone.utc),
        valid_to=None,
    ):
        reply.data.registrar_handle.value = "REG-SW"
        reply.data.zone = "cz"
        reply.data.valid_from.FromDatetime(valid_from)
        if valid_to:
            reply.data.valid_to.FromDatetime(valid_to)
        return reply

    def _get_add_zone_access_reply(self, *args, **kwargs):
        return self._get_zone_access_reply(AddRegistrarZoneAccessReply(), *args, **kwargs)

    def _get_update_zone_access_reply(self, *args, **kwargs):
        return self._get_zone_access_reply(UpdateRegistrarZoneAccessReply(), *args, **kwargs)

    async def test_add_zone_access_without_limits(self):
        self.client.mock.return_value = make_awaitable(self._get_add_zone_access_reply())

        self.assertEqual(
            await self.client.add_zone_access("REG-SW", ZoneAccess(zone="cz")),
            ZoneAccess(zone="cz", valid_from=self.now),
        )

        self.client.mock.assert_called_once_with(
            self._get_add_zone_access_request(),
            method="/Fred.Registry.Api.Registrar.Admin/add_registrar_zone_access",
            timeout=None,
        )

    async def test_add_zone_access_with_limits(self):
        self.client.mock.return_value = make_awaitable(self._get_add_zone_access_reply(valid_to=self.tomorrow))

        zone_access = ZoneAccess(zone="cz", valid_from=self.now, valid_to=self.tomorrow)
        self.assertEqual(await self.client.add_zone_access("REG-SW", zone_access), zone_access)

        self.client.mock.assert_called_once_with(
            self._get_add_zone_access_request(valid_from=self.now, valid_to=self.now + timedelta(days=1)),
            method="/Fred.Registry.Api.Registrar.Admin/add_registrar_zone_access",
            timeout=None,
        )

    async def test_update_zone_access_noop(self):
        self.client.mock.return_value = make_awaitable(self._get_update_zone_access_reply())

        self.assertEqual(
            await self.client.update_zone_access("REG-SW", ZoneAccess(zone="cz", valid_from=self.now)),
            ZoneAccess(zone="cz", valid_from=self.now),
        )

        request = UpdateRegistrarZoneAccessRequest()
        request.registrar_handle.value = "REG-SW"
        request.zone = "cz"
        request.valid_from.FromDatetime(self.now)
        self.client.mock.assert_called_once_with(
            request,
            method="/Fred.Registry.Api.Registrar.Admin/update_registrar_zone_access",
            timeout=None,
        )

    async def test_update_zone_access_null(self):
        self.client.mock.return_value = make_awaitable(self._get_update_zone_access_reply())

        self.assertEqual(
            await self.client.update_zone_access(
                "REG-SW",
                ZoneAccess(zone="cz", valid_from=self.tomorrow),
                valid_from=None,
                valid_to=None,
            ),
            ZoneAccess(zone="cz", valid_from=self.now),
        )

        request = UpdateRegistrarZoneAccessRequest()
        request.registrar_handle.value = "REG-SW"
        request.zone = "cz"
        request.valid_from.FromDatetime(self.tomorrow)
        request.set_valid_from.SetInParent()
        request.set_valid_to.SetInParent()
        self.client.mock.assert_called_once_with(
            request,
            method="/Fred.Registry.Api.Registrar.Admin/update_registrar_zone_access",
            timeout=None,
        )

    async def test_update_zone_access_datetime(self):
        self.client.mock.return_value = make_awaitable(self._get_update_zone_access_reply(valid_to=self.tomorrow))

        self.assertEqual(
            await self.client.update_zone_access(
                "REG-SW",
                ZoneAccess(zone="cz", valid_from=datetime(2020, 1, 1, tzinfo=timezone.utc)),
                valid_from=self.now,
                valid_to=self.tomorrow,
            ),
            ZoneAccess(zone="cz", valid_from=self.now, valid_to=self.tomorrow),
        )

        request = UpdateRegistrarZoneAccessRequest()
        request.registrar_handle.value = "REG-SW"
        request.zone = "cz"
        request.valid_from.FromDatetime(datetime(2020, 1, 1, tzinfo=timezone.utc))
        request.set_valid_from.FromDatetime(self.now)
        request.set_valid_to.FromDatetime(self.tomorrow)
        self.client.mock.assert_called_once_with(
            request,
            method="/Fred.Registry.Api.Registrar.Admin/update_registrar_zone_access",
            timeout=None,
        )

    async def test_delete_zone_access(self):
        reply = DeleteRegistrarZoneAccessReply()
        self.client.mock.return_value = make_awaitable(reply)

        await self.client.delete_zone_access("REG-SW", ZoneAccess(zone="cz", valid_from=self.now))

        request = DeleteRegistrarZoneAccessRequest()
        request.registrar_handle.value = "REG-SW"
        request.zone = "cz"
        request.valid_from.FromDatetime(self.now)
        self.client.mock.assert_called_once_with(
            request,
            method="/Fred.Registry.Api.Registrar.Admin/delete_registrar_zone_access",
            timeout=None,
        )

    async def test_add_epp_credentials_password(self):
        reply = AddRegistrarEppCredentialsReply()
        reply.data.credentials_id.value = "epp:1"
        self.client.mock.return_value = make_awaitable(reply)

        self.assertEqual(
            await self.client.add_epp_credentials("JMC", SslCertificate(fingerprint="01:02:03:04"), password="passwd"),
            EppCredentialsId("epp:1"),
        )

        request = AddRegistrarEppCredentialsRequest()
        request.registrar_handle.value = "JMC"
        request.certificate.fingerprint = b"\x01\x02\x03\x04"
        request.password = "passwd"
        self.client.mock.assert_called_once_with(
            request,
            method="/Fred.Registry.Api.Registrar.Admin/add_registrar_epp_credentials",
            timeout=None,
        )

    async def test_add_epp_credentials_template(self):
        reply = AddRegistrarEppCredentialsReply()
        reply.data.credentials_id.value = "epp:1"
        self.client.mock.return_value = make_awaitable(reply)

        self.assertEqual(
            await self.client.add_epp_credentials(
                "JMC",
                SslCertificate(fingerprint="01:02:03:04"),
                template_credentials_id=EppCredentialsId("epp:0"),
            ),
            EppCredentialsId("epp:1"),
        )

        request = AddRegistrarEppCredentialsRequest()
        request.registrar_handle.value = "JMC"
        request.certificate.fingerprint = b"\x01\x02\x03\x04"
        request.template_credentials_id.value = "epp:0"
        self.client.mock.assert_called_once_with(
            request,
            method="/Fred.Registry.Api.Registrar.Admin/add_registrar_epp_credentials",
            timeout=None,
        )

    async def test_add_epp_credentials_error(self):
        with self.assertRaises(ValueError):
            await self.client.add_epp_credentials(
                "JMC",
                SslCertificate(fingerprint="01:02:03:04"),
                password="passwd",
                template_credentials_id=EppCredentialsId("epp:0"),
            )

        self.client.mock.assert_not_called()

    async def test_update_epp_credentials(self):
        reply = UpdateRegistrarEppCredentialsReply()
        self.client.mock.return_value = make_awaitable(reply)

        await self.client.update_epp_credentials(EppCredentialsId("epp:0"), password="passwd")

        request = UpdateRegistrarEppCredentialsRequest()
        request.credentials_id.value = "epp:0"
        request.password = "passwd"
        self.client.mock.assert_called_once_with(
            request,
            method="/Fred.Registry.Api.Registrar.Admin/update_registrar_epp_credentials",
            timeout=None,
        )

    async def test_delete_epp_credentials(self):
        reply = DeleteRegistrarEppCredentialsReply()
        self.client.mock.return_value = make_awaitable(reply)

        await self.client.delete_epp_credentials(EppCredentialsId("epp:0"))

        request = DeleteRegistrarEppCredentialsRequest()
        request.credentials_id.value = "epp:0"
        self.client.mock.assert_called_once_with(
            request,
            method="/Fred.Registry.Api.Registrar.Admin/delete_registrar_epp_credentials",
            timeout=None,
        )

    async def test_add_group_membership(self):
        reply = AddRegistrarGroupMembershipReply()
        reply.data.registrar_handle.value = "REG-1"
        self.client.mock.return_value = make_awaitable(reply)

        await self.client.add_group_membership("REG-1", "group 5")

        request = AddRegistrarGroupMembershipRequest()
        request.registrar_handle.value = "REG-1"
        request.group = "group 5"
        self.client.mock.assert_called_once_with(
            request,
            method="/Fred.Registry.Api.Registrar.Admin/add_registrar_group_membership",
            timeout=None,
        )

    async def test_add_registrar_group_membership(self):
        groups = ["group 1", "group 2", "group 3", "group 4", "group 5"]
        reply = AddRegistrarGroupMembershipReply()
        reply.data.registrar_handle.value = "REG-1"
        reply.data.groups.extend(groups)
        self.client.mock.return_value = make_awaitable(reply)

        with self.assertWarnsRegex(
            DeprecationWarning, "Method add_registrar_group_membership is deprecated in favor of add_group_membership."
        ):
            result = await self.client.add_registrar_group_membership("REG-1", "group 5")

        self.assertEqual(result, groups)
        request = AddRegistrarGroupMembershipRequest()
        request.registrar_handle.value = "REG-1"
        request.group = "group 5"
        self.client.mock.assert_called_once_with(
            request,
            method="/Fred.Registry.Api.Registrar.Admin/add_registrar_group_membership",
            timeout=None,
        )

    async def test_add_registrar_group_membership_bad_reg(self):
        reply = AddRegistrarGroupMembershipReply()
        reply.exception.registrar_does_not_exist.CopyFrom(
            AddRegistrarGroupMembershipReply.Exception.RegistrarDoesNotExist()
        )
        self.client.mock.return_value = make_awaitable(reply)

        with self.assertWarnsRegex(
            DeprecationWarning, "Method add_registrar_group_membership is deprecated in favor of add_group_membership."
        ):
            with self.assertRaises(RegistrarDoesNotExist):
                await self.client.add_registrar_group_membership("REG-1", "group 5")

    async def test_add_registrar_group_membership_bad_grp(self):
        reply = AddRegistrarGroupMembershipReply()
        reply.exception.group_does_not_exist.CopyFrom(AddRegistrarGroupMembershipReply.Exception.GroupDoesNotExist())
        self.client.mock.return_value = make_awaitable(reply)

        with self.assertWarnsRegex(
            DeprecationWarning, "Method add_registrar_group_membership is deprecated in favor of add_group_membership."
        ):
            with self.assertRaises(GroupDoesNotExist):
                await self.client.add_registrar_group_membership("REG-1", "group 5")

    async def test_delete_group_membership(self):
        reply = DeleteRegistrarGroupMembershipReply()
        reply.data.registrar_handle.value = "REG-1"
        self.client.mock.return_value = make_awaitable(reply)

        await self.client.delete_group_membership("REG-1", "group 5")

        request = DeleteRegistrarGroupMembershipRequest()
        request.registrar_handle.value = "REG-1"
        request.group = "group 5"
        self.client.mock.assert_called_once_with(
            request,
            method="/Fred.Registry.Api.Registrar.Admin/delete_registrar_group_membership",
            timeout=None,
        )

    async def test_delete_registrar_group_membership(self):
        groups = ["group 1", "group 2", "group 3", "group 4"]
        reply = DeleteRegistrarGroupMembershipReply()
        reply.data.registrar_handle.value = "REG-1"
        reply.data.groups.extend(groups)
        self.client.mock.return_value = make_awaitable(reply)

        with self.assertWarnsRegex(
            DeprecationWarning,
            "Method delete_registrar_group_membership is deprecated in favor of delete_group_membership.",
        ):
            result = await self.client.delete_registrar_group_membership("REG-1", "group 5")

        self.assertEqual(result, groups)
        request = DeleteRegistrarGroupMembershipRequest()
        request.registrar_handle.value = "REG-1"
        request.group = "group 5"
        self.client.mock.assert_called_once_with(
            request,
            method="/Fred.Registry.Api.Registrar.Admin/delete_registrar_group_membership",
            timeout=None,
        )

    async def test_delete_registrar_group_membership_bad_reg(self):
        reply = DeleteRegistrarGroupMembershipReply()
        reply.exception.registrar_does_not_exist.CopyFrom(
            DeleteRegistrarGroupMembershipReply.Exception.RegistrarDoesNotExist()
        )
        self.client.mock.return_value = make_awaitable(reply)

        with self.assertWarnsRegex(
            DeprecationWarning,
            "Method delete_registrar_group_membership is deprecated in favor of delete_group_membership.",
        ):
            with self.assertRaises(RegistrarDoesNotExist):
                await self.client.delete_registrar_group_membership("REG-1", "group 5")

    async def test_delete_registrar_group_membership_bad_grp(self):
        reply = DeleteRegistrarGroupMembershipReply()
        reply.exception.group_does_not_exist.CopyFrom(DeleteRegistrarGroupMembershipReply.Exception.GroupDoesNotExist())
        self.client.mock.return_value = make_awaitable(reply)

        with self.assertWarnsRegex(
            DeprecationWarning,
            "Method delete_registrar_group_membership is deprecated in favor of delete_group_membership.",
        ):
            with self.assertRaises(GroupDoesNotExist):
                await self.client.delete_registrar_group_membership("REG-1", "group 5")

    async def test_add_certification(self):
        reply = AddRegistrarCertificationReply()
        self.client.mock.return_value = make_awaitable(reply)

        await self.client.add_certification("REG-1", 50, "File-1")

        request = AddRegistrarCertificationRequest()
        request.registrar_handle.value = "REG-1"
        request.classification = 50
        request.file_id.value = "File-1"
        self.client.mock.assert_called_once_with(
            request,
            method="/Fred.Registry.Api.Registrar.Admin/add_registrar_certification",
            timeout=None,
        )

    async def test_add_certification_with_dates(self):
        reply = AddRegistrarCertificationReply()
        self.client.mock.return_value = make_awaitable(reply)
        dt_from = datetime(2020, 6, 1, tzinfo=timezone.utc)
        dt_to = datetime(2021, 3, 25, tzinfo=timezone.utc)

        await self.client.add_certification("REG-1", 50, "File-1", valid_from=dt_from, valid_to=dt_to)

        request = AddRegistrarCertificationRequest()
        request.registrar_handle.value = "REG-1"
        request.valid_from.FromDatetime(dt_from)
        request.valid_to.FromDatetime(dt_to)
        request.classification = 50
        request.file_id.value = "File-1"
        self.client.mock.assert_called_once_with(
            request,
            method="/Fred.Registry.Api.Registrar.Admin/add_registrar_certification",
            timeout=None,
        )

    async def test_add_registrar_certification(self):
        reply = AddRegistrarCertificationReply()
        self.client.mock.return_value = make_awaitable(reply)

        with self.assertWarnsRegex(
            DeprecationWarning, "Method add_registrar_certification is deprecated in favor of add_certification."
        ):
            await self.client.add_registrar_certification("REG-1", 50, "File-1")

        request = AddRegistrarCertificationRequest()
        request.registrar_handle.value = "REG-1"
        request.classification = 50
        request.file_id.value = "File-1"
        self.client.mock.assert_called_once_with(
            request,
            method="/Fred.Registry.Api.Registrar.Admin/add_registrar_certification",
            timeout=None,
        )

    async def test_add_registrar_certification_with_dates(self):
        reply = AddRegistrarCertificationReply()
        self.client.mock.return_value = make_awaitable(reply)
        dt_from = datetime(2020, 6, 1, tzinfo=timezone.utc)
        dt_to = datetime(2021, 3, 25, tzinfo=timezone.utc)

        with self.assertWarnsRegex(
            DeprecationWarning, "Method add_registrar_certification is deprecated in favor of add_certification."
        ):
            await self.client.add_registrar_certification("REG-1", 50, "File-1", valid_from=dt_from, valid_to=dt_to)

        request = AddRegistrarCertificationRequest()
        request.registrar_handle.value = "REG-1"
        request.valid_from.FromDatetime(dt_from)
        request.valid_to.FromDatetime(dt_to)
        request.classification = 50
        request.file_id.value = "File-1"
        self.client.mock.assert_called_once_with(
            request,
            method="/Fred.Registry.Api.Registrar.Admin/add_registrar_certification",
            timeout=None,
        )

    async def test_add_registrar_certification_not_found(self):
        reply = AddRegistrarCertificationReply()
        reply.exception.registrar_does_not_exist.CopyFrom(
            AddRegistrarCertificationReply.Exception.RegistrarDoesNotExist()
        )
        self.client.mock.return_value = make_awaitable(reply)

        with self.assertWarnsRegex(
            DeprecationWarning, "Method add_registrar_certification is deprecated in favor of add_certification."
        ):
            with self.assertRaises(RegistrarDoesNotExist):
                await self.client.add_registrar_certification("REG-1", 50, "File-1")

    async def test_add_registrar_certification_invalid(self):
        reply = AddRegistrarCertificationReply()
        reply.exception.invalid_data.CopyFrom(AddRegistrarCertificationReply.Exception.InvalidData())
        reply.exception.invalid_data.fields.append("classification")
        self.client.mock.return_value = make_awaitable(reply)

        with self.assertWarnsRegex(
            DeprecationWarning, "Method add_registrar_certification is deprecated in favor of add_certification."
        ):
            with self.assertRaises(InvalidData):
                await self.client.add_registrar_certification("REG-1", -50, "File-1")

    async def test_update_certification(self):
        time_to = datetime(2023, 3, 1, tzinfo=timezone.utc)
        reply = UpdateRegistrarCertificationReply()
        self.client.mock.return_value = make_awaitable(reply)

        await self.client.update_certification("Cert-1", classification=50, file_id="File-1", valid_to=time_to)

        request = UpdateRegistrarCertificationRequest()
        request.certification_id.value = "Cert-1"
        request.set_valid_to.FromDatetime(time_to)
        request.set_classification = 50
        request.set_file_id.value = "File-1"
        self.client.mock.assert_called_once_with(
            request,
            method="/Fred.Registry.Api.Registrar.Admin/update_registrar_certification",
            timeout=None,
        )

    async def test_update_registrar_certification(self):
        time_to = datetime(2023, 3, 1, tzinfo=timezone.utc)
        reply = UpdateRegistrarCertificationReply()
        self.client.mock.return_value = make_awaitable(reply)

        with self.assertWarnsRegex(
            DeprecationWarning, "Method update_registrar_certification is deprecated in favor of update_certification."
        ):
            await self.client.update_registrar_certification("Cert-1", 50, "File-1", valid_to=time_to)

        request = UpdateRegistrarCertificationRequest()
        request.certification_id.value = "Cert-1"
        request.set_valid_to.FromDatetime(time_to)
        request.set_classification = 50
        request.set_file_id.value = "File-1"
        self.client.mock.assert_called_once_with(
            request,
            method="/Fred.Registry.Api.Registrar.Admin/update_registrar_certification",
            timeout=None,
        )

    async def test_update_registrar_certification_not_found(self):
        reply = UpdateRegistrarCertificationReply()
        reply.exception.registrar_certification_does_not_exist.CopyFrom(
            UpdateRegistrarCertificationReply.Exception.RegistrarCertificationDoesNotExist()
        )
        self.client.mock.return_value = make_awaitable(reply)

        with self.assertWarnsRegex(
            DeprecationWarning, "Method update_registrar_certification is deprecated in favor of update_certification."
        ):
            with self.assertRaises(RegistrarCertificationDoesNotExist):
                await self.client.update_registrar_certification("Cert-1", 50, "File-1")

    async def test_update_registrar_certification_invalid(self):
        reply = UpdateRegistrarCertificationReply()
        reply.exception.invalid_data.CopyFrom(UpdateRegistrarCertificationReply.Exception.InvalidData())
        reply.exception.invalid_data.fields.append("classification")
        self.client.mock.return_value = make_awaitable(reply)

        with self.assertWarnsRegex(
            DeprecationWarning, "Method update_registrar_certification is deprecated in favor of update_certification."
        ):
            with self.assertRaises(InvalidData):
                await self.client.update_registrar_certification("Cert-1")

        request = UpdateRegistrarCertificationRequest()
        request.certification_id.value = "Cert-1"
        self.client.mock.assert_called_once_with(
            request,
            method="/Fred.Registry.Api.Registrar.Admin/update_registrar_certification",
            timeout=None,
        )

    async def test_delete_certification(self):
        reply = DeleteRegistrarCertificationReply()
        self.client.mock.return_value = make_awaitable(reply)

        await self.client.delete_certification("Cert-1")

        request = DeleteRegistrarCertificationRequest()
        request.certification_id.value = "Cert-1"
        self.client.mock.assert_called_once_with(
            request,
            method="/Fred.Registry.Api.Registrar.Admin/delete_registrar_certification",
            timeout=None,
        )

    async def test_delete_registrar_certification(self):
        reply = DeleteRegistrarCertificationReply()
        self.client.mock.return_value = make_awaitable(reply)

        with self.assertWarnsRegex(
            DeprecationWarning, "Method delete_registrar_certification is deprecated in favor of delete_certification."
        ):
            await self.client.delete_registrar_certification("Cert-1")

        request = DeleteRegistrarCertificationRequest()
        request.certification_id.value = "Cert-1"
        self.client.mock.assert_called_once_with(
            request,
            method="/Fred.Registry.Api.Registrar.Admin/delete_registrar_certification",
            timeout=None,
        )

    async def test_delete_registrar_certification_not_found(self):
        reply = DeleteRegistrarCertificationReply()
        reply.exception.registrar_certification_does_not_exist.CopyFrom(
            DeleteRegistrarCertificationReply.Exception.RegistrarCertificationDoesNotExist()
        )
        self.client.mock.return_value = make_awaitable(reply)

        with self.assertWarnsRegex(
            DeprecationWarning, "Method delete_registrar_certification is deprecated in favor of delete_certification."
        ):
            with self.assertRaises(RegistrarCertificationDoesNotExist):
                await self.client.delete_registrar_certification("Cert-1")

    async def test_delete_registrar_certification_invalid(self):
        reply = DeleteRegistrarCertificationReply()
        reply.exception.invalid_data.CopyFrom(DeleteRegistrarCertificationReply.Exception.InvalidData())
        reply.exception.invalid_data.fields.append("certification_id")
        self.client.mock.return_value = make_awaitable(reply)

        with self.assertWarnsRegex(
            DeprecationWarning, "Method delete_registrar_certification is deprecated in favor of delete_certification."
        ):
            with self.assertRaises(InvalidData):
                await self.client.delete_registrar_certification("")

import sys
from abc import ABC
from datetime import datetime
from typing import Any, Generic, TypeVar, cast
from unittest import TestCase, skipIf

from fred_api.registry.common_types_pb2 import StateFlag
from fred_types import BaseObjectHistoryId, SnapshotId
from frgal.utils import AsyncTestClientMixin
from google.protobuf.empty_pb2 import Empty
from pytz import utc

from regal.common import ObjectHistory, ObjectHistoryItem, ObjectStateHistoryItem
from regal.object import ObjectStateFlag, RegistryObjectClient
from regal.utils import AbcAttribute

from .utils import make_awaitable

C = TypeVar("C", bound=RegistryObjectClient)


class ObjectClientTestMixin(ABC, Generic[C]):
    object_id = "kryten"
    client: C

    grpc_namespace: str = AbcAttribute
    get_state_request = AbcAttribute
    get_state_reply = AbcAttribute
    get_state_method: str = AbcAttribute
    get_history_request = AbcAttribute
    get_history_reply = AbcAttribute
    get_history_method: str = AbcAttribute
    get_history_record = AbcAttribute
    get_state_history_request = AbcAttribute
    get_state_history_reply = AbcAttribute
    get_state_history_method: str = AbcAttribute
    get_state_history_record = AbcAttribute
    get_state_flags_method: str = AbcAttribute
    get_state_flags_reply_cls = AbcAttribute

    def _get_history_record(self, history_id: str, valid_from: datetime) -> Any:
        record = self.get_history_record()
        getattr(record, self.client.object_history_id).uuid.value = history_id
        record.valid_from.FromDatetime(valid_from)
        return record

    def _get_state_history_record(self, valid_from: datetime, flags: dict[str, bool]) -> Any:
        record = self.get_state_history_record()
        record.valid_from.FromDatetime(valid_from)
        record.flags.update(flags)
        return record

    async def _test_get_state(self, result: frozenset[str], internal: bool = False) -> None:
        reply = self.get_state_reply()
        getattr(reply.data, self.client.object_id).uuid.value = self.object_id
        reply.data.state.flags.update({"internal": True, "external": True, "unset": False})
        flags_reply = self.get_state_flags_reply_cls()
        flags_reply.data.traits["internal"].CopyFrom(StateFlag.Traits(visibility=StateFlag.Visibility.Enum.internal))
        flags_reply.data.traits["external"].CopyFrom(StateFlag.Traits(visibility=StateFlag.Visibility.Enum.external))
        flags_reply.data.traits["unset"].CopyFrom(StateFlag.Traits(visibility=StateFlag.Visibility.Enum.external))
        if not internal:
            cast(AsyncTestClientMixin, self.client).mock.side_effect = [
                make_awaitable(reply),
                make_awaitable(flags_reply),
            ]
        else:
            cast(AsyncTestClientMixin, self.client).mock.side_effect = [make_awaitable(reply)]

        response = await self.client.get_state(**{self.client.object_id: self.object_id}, internal=internal)  # type: ignore[attr-defined]

        cast(TestCase, self).assertEqual(response, result)
        request = self.get_state_request()
        getattr(request, self.client.object_id).uuid.value = self.object_id
        cast(AsyncTestClientMixin, self.client).mock.assert_any_call(
            request,
            method="{}/{}".format(self.grpc_namespace, self.get_state_method),
            timeout=None,
        )

    async def test_get_state(self):
        await self._test_get_state(result=frozenset({"external"}))

    async def test_get_state_external(self):
        # Test only external states are returned.
        await self._test_get_state(result=frozenset({"internal", "external"}), internal=True)

    async def test_get_state_snapshot_id(self):
        # Test get_state with a snapshot_id.
        reply = self.get_state_reply()
        getattr(reply.data, self.client.object_id).uuid.value = self.object_id
        reply.data.state.flags.update({"external": True})
        flags_reply = self.get_state_flags_reply_cls()
        flags_reply.data.traits["external"].CopyFrom(StateFlag.Traits(visibility=StateFlag.Visibility.Enum.external))
        cast(AsyncTestClientMixin, self.client).mock.side_effect = [
            make_awaitable(reply),
            make_awaitable(flags_reply),
        ]

        response = await self.client.get_state(  # type: ignore[attr-defined]
            **{self.client.object_id: self.object_id}, internal=False, snapshot_id=SnapshotId("LAST-THURSDAY")
        )  # type: ignore[attr-defined]

        cast(TestCase, self).assertEqual(response, frozenset({"external"}))
        request = self.get_state_request()
        getattr(request, self.client.object_id).uuid.value = self.object_id
        getattr(request, self.client.object_snapshot_id).value = "LAST-THURSDAY"
        cast(AsyncTestClientMixin, self.client).mock.assert_any_call(
            request,
            method="{}/{}".format(self.grpc_namespace, self.get_state_method),
            timeout=None,
        )

    async def _test_get_object_state(self, result: dict[str, bool], internal: bool = True) -> None:
        reply = self.get_state_reply()
        getattr(reply.data, self.client.object_id).uuid.value = self.object_id
        reply.data.state.flags.update({"internal": True, "external": False})
        flags_reply = self.get_state_flags_reply_cls()
        flags_reply.data.traits["internal"].CopyFrom(StateFlag.Traits(visibility=StateFlag.Visibility.Enum.internal))
        flags_reply.data.traits["external"].CopyFrom(StateFlag.Traits(visibility=StateFlag.Visibility.Enum.external))
        cast(AsyncTestClientMixin, self.client).mock.side_effect = [make_awaitable(reply), make_awaitable(flags_reply)]

        with cast(TestCase, self).assertWarnsRegex(
            DeprecationWarning, "Method {} is deprecated in favor of get_state.".format(self.get_state_method)
        ):
            response = await getattr(self.client, self.get_state_method)(
                **{self.client.object_id: self.object_id}, internal=internal
            )

        cast(TestCase, self).assertEqual(response, result)
        request = self.get_state_request()
        getattr(request, self.client.object_id).uuid.value = self.object_id
        cast(AsyncTestClientMixin, self.client).mock.assert_any_call(
            request,
            method="{}/{}".format(self.grpc_namespace, self.get_state_method),
            timeout=None,
        )

    async def test_get_object_state(self):
        await self._test_get_object_state(result={"internal": True, "external": False})

    async def test_get_object_state_external(self):
        # Test only external states are returned.
        await self._test_get_object_state(result={"external": False}, internal=False)

    @skipIf(sys.version_info[:2] == (3, 11), "This test obscurely fails under Python 3.11 and Pydantic 2")
    async def test_get_object_state_deprecated(self):  # pragma: no cover
        # Test deprecated warning on internal flag.
        reply = self.get_state_reply()
        getattr(reply.data, self.client.object_id).uuid.value = self.object_id
        cast(AsyncTestClientMixin, self.client).mock.return_value = make_awaitable(reply)

        with cast(TestCase, self).assertWarnsRegex(
            DeprecationWarning, "Default value of internal flag will change to False in regal 2.0."
        ):
            await getattr(self.client, self.get_state_method)(
                **{self.client.object_id: self.object_id},
            )

    async def test_get_history(self):
        reply = self.get_history_reply()
        getattr(reply.data, self.client.object_id).uuid.value = self.object_id
        record = self.get_history_record()
        getattr(record, self.client.object_history_id).uuid.value = "red-dwarf"
        reply.data.timeline.extend(
            [
                self._get_history_record("augustus", datetime(2020, 1, 1, tzinfo=utc)),
                self._get_history_record("nova-5", datetime(2021, 1, 1, tzinfo=utc)),
            ]
        )
        reply.data.valid_to.FromDatetime(datetime(2022, 1, 1, tzinfo=utc))
        cast(AsyncTestClientMixin, self.client).mock.return_value = make_awaitable(reply)

        response = await self.client.get_history(**{self.client.object_id: self.object_id})  # type: ignore[attr-defined]
        cast(TestCase, self).assertEqual(
            response,
            ObjectHistory(
                timeline=[
                    ObjectHistoryItem(
                        history_id=BaseObjectHistoryId("augustus"),
                        valid_from=datetime(2020, 1, 1, tzinfo=utc),
                        log_entry_id=None,
                    ),
                    ObjectHistoryItem(
                        history_id=BaseObjectHistoryId("nova-5"),
                        valid_from=datetime(2021, 1, 1, tzinfo=utc),
                        log_entry_id=None,
                    ),
                ],
                valid_to=datetime(2022, 1, 1, tzinfo=utc),
            ),
        )

        request = self.get_history_request()
        getattr(request, self.client.object_id).uuid.value = self.object_id
        cast(AsyncTestClientMixin, self.client).mock.assert_called_once_with(
            request,
            method="{}/{}".format(self.grpc_namespace, self.get_history_method),
            timeout=None,
        )

    async def test_get_object_history(self):
        reply = self.get_history_reply()
        getattr(reply.data, self.client.object_id).uuid.value = self.object_id
        record = self.get_history_record()
        getattr(record, self.client.object_history_id).uuid.value = "red-dwarf"
        reply.data.timeline.extend(
            [
                self._get_history_record("augustus", datetime(2020, 1, 1, tzinfo=utc)),
                self._get_history_record("nova-5", datetime(2021, 1, 1, tzinfo=utc)),
            ]
        )
        reply.data.valid_to.FromDatetime(datetime(2022, 1, 1, tzinfo=utc))
        cast(AsyncTestClientMixin, self.client).mock.return_value = make_awaitable(reply)

        with cast(TestCase, self).assertWarnsRegex(
            DeprecationWarning, "Method {} is deprecated in favor of get_history.".format(self.get_history_method)
        ):
            response = await getattr(self.client, self.get_history_method)(**{self.client.object_id: self.object_id})
        cast(TestCase, self).assertEqual(
            response,
            {
                self.client.object_id: self.object_id,
                "timeline": [
                    {
                        self.client.object_history_id: "augustus",
                        "valid_from": datetime(2020, 1, 1, tzinfo=utc),
                        "log_entry_id": None,
                    },
                    {
                        self.client.object_history_id: "nova-5",
                        "valid_from": datetime(2021, 1, 1, tzinfo=utc),
                        "log_entry_id": None,
                    },
                ],
                "valid_to": datetime(2022, 1, 1, tzinfo=utc),
            },
        )

        request = self.get_history_request()
        getattr(request, self.client.object_id).uuid.value = self.object_id
        cast(AsyncTestClientMixin, self.client).mock.assert_called_once_with(
            request,
            method="{}/{}".format(self.grpc_namespace, self.get_history_method),
            timeout=None,
        )

    async def test_get_state_history_empty(self):
        reply = self.get_state_history_reply()
        getattr(reply.data, self.client.object_id).uuid.value = self.object_id
        reply.data.history.timeline.extend(
            [
                self._get_state_history_record(
                    datetime(2020, 1, 1, tzinfo=utc), {"linked": True, "serverBlocked": False}
                ),
                self._get_state_history_record(
                    datetime(2021, 1, 1, tzinfo=utc), {"linked": False, "serverBlocked": True}
                ),
            ]
        )
        reply.data.history.valid_to.FromDatetime(datetime(2022, 1, 1, tzinfo=utc))
        cast(AsyncTestClientMixin, self.client).mock.return_value = make_awaitable(reply)

        response = await self.client.get_state_history(**{self.client.object_id: self.object_id})  # type: ignore[attr-defined]
        cast(TestCase, self).assertEqual(
            response,
            ObjectHistory(
                timeline=[
                    ObjectStateHistoryItem(
                        valid_from=datetime(2020, 1, 1, tzinfo=utc), flags={"linked": True, "serverBlocked": False}
                    ),
                    ObjectStateHistoryItem(
                        valid_from=datetime(2021, 1, 1, tzinfo=utc), flags={"linked": False, "serverBlocked": True}
                    ),
                ],
                valid_to=datetime(2022, 1, 1, tzinfo=utc),
            ),
        )

        request = self.get_state_history_request()
        getattr(request, self.client.object_id).uuid.value = self.object_id
        cast(AsyncTestClientMixin, self.client).mock.assert_called_once_with(
            request,
            method="{}/{}".format(self.grpc_namespace, self.get_state_history_method),
            timeout=None,
        )

    async def test_get_object_state_history_empty(self):
        reply = self.get_state_history_reply()
        getattr(reply.data, self.client.object_id).uuid.value = self.object_id
        reply.data.history.timeline.extend(
            [
                self._get_state_history_record(
                    datetime(2020, 1, 1, tzinfo=utc), {"linked": True, "serverBlocked": False}
                ),
                self._get_state_history_record(
                    datetime(2021, 1, 1, tzinfo=utc), {"linked": False, "serverBlocked": True}
                ),
            ]
        )
        reply.data.history.valid_to.FromDatetime(datetime(2022, 1, 1, tzinfo=utc))
        cast(AsyncTestClientMixin, self.client).mock.return_value = make_awaitable(reply)

        with cast(TestCase, self).assertWarnsRegex(
            DeprecationWarning,
            "Method {} is deprecated in favor of get_state_history.".format(self.get_state_history_method),
        ):
            response = await getattr(self.client, self.get_state_history_method)(
                **{self.client.object_id: self.object_id}
            )
        cast(TestCase, self).assertEqual(
            response,
            {
                self.client.object_id: self.object_id,
                "timeline": [
                    {"valid_from": datetime(2020, 1, 1, tzinfo=utc), "flags": {"linked": True, "serverBlocked": False}},
                    {"valid_from": datetime(2021, 1, 1, tzinfo=utc), "flags": {"linked": False, "serverBlocked": True}},
                ],
                "valid_to": datetime(2022, 1, 1, tzinfo=utc),
            },
        )

        request = self.get_state_history_request()
        getattr(request, self.client.object_id).uuid.value = self.object_id
        cast(AsyncTestClientMixin, self.client).mock.assert_called_once_with(
            request,
            method="{}/{}".format(self.grpc_namespace, self.get_state_history_method),
            timeout=None,
        )

    async def test_get_object_state_flags(self):
        reply = self.get_state_flags_reply_cls()
        reply.data.traits["internal"].CopyFrom(StateFlag.Traits(visibility=StateFlag.Visibility.Enum.internal))
        reply.data.traits["external"].CopyFrom(StateFlag.Traits(visibility=StateFlag.Visibility.Enum.external))
        reply.data.traits["manual"].CopyFrom(StateFlag.Traits(how_to_set=StateFlag.Manipulation.Enum.manual))
        reply.data.traits["automatic"].CopyFrom(StateFlag.Traits(how_to_set=StateFlag.Manipulation.Enum.automatic))
        cast(AsyncTestClientMixin, self.client).mock.return_value = make_awaitable(reply)

        response = await self.client.get_state_flags()

        result = {
            "internal": ObjectStateFlag(manual=True, internal=True),
            "external": ObjectStateFlag(manual=True, internal=False),
            "manual": ObjectStateFlag(manual=True, internal=False),
            "automatic": ObjectStateFlag(manual=False, internal=False),
        }
        cast(TestCase, self).assertEqual(response, result)
        cast(AsyncTestClientMixin, self.client).mock.assert_called_once_with(
            Empty(),
            method="{}/{}".format(self.grpc_namespace, self.get_state_flags_method),
            timeout=None,
        )

from datetime import datetime
from typing import cast
from unittest import IsolatedAsyncioTestCase, TestCase
from unittest.mock import sentinel

import pytz
from fred_api.registry.contact.contact_common_types_pb2 import ContactHistoryId
from fred_api.registry.contact.service_search_contact_grpc_pb2 import (
    SearchContactHistoryReply,
    SearchContactHistoryRequest,
    SearchContactReply,
    SearchContactRequest,
)
from fred_api.registry.domain.service_search_domain_grpc_pb2 import (
    SearchDomainHistoryReply,
    SearchDomainHistoryRequest,
    SearchDomainReply,
    SearchDomainRequest,
)
from fred_api.registry.keyset.service_search_keyset_grpc_pb2 import (
    SearchKeysetHistoryReply,
    SearchKeysetHistoryRequest,
    SearchKeysetReply,
    SearchKeysetRequest,
)
from fred_api.registry.nsset.service_search_nsset_grpc_pb2 import (
    SearchNssetHistoryReply,
    SearchNssetHistoryRequest,
    SearchNssetReply,
    SearchNssetRequest,
)
from frgal.utils import AsyncTestClientMixin

from regal.common import FuzzyValue
from regal.domain import DomainHistoryId
from regal.search import (
    HistorySearchPeriod,
    SearchContactClient,
    SearchCurrentResult,
    SearchDomainClient,
    SearchHistoryResult,
    SearchKeysetClient,
    SearchNssetClient,
    SearchResults,
)
from regal.search.common import BaseSearchClient, SearchDecoder

from .utils import make_awaitable


class HistorySearchPeriodTest(TestCase):
    def test_from_message_dict(self):
        self.assertEqual(
            HistorySearchPeriod.from_message_dict({"valid_from": datetime(2020, 1, 1, tzinfo=pytz.utc)}),
            HistorySearchPeriod(valid_from=datetime(2020, 1, 1, tzinfo=pytz.utc)),
        )
        self.assertEqual(
            HistorySearchPeriod.from_message_dict(
                {
                    "valid_from": datetime(2020, 1, 1, tzinfo=pytz.utc),
                    "domain_history_ids": ["0", "1"],
                }
            ),
            HistorySearchPeriod(
                valid_from=datetime(2020, 1, 1, tzinfo=pytz.utc),
                history_ids=[DomainHistoryId("0"), DomainHistoryId("1")],
            ),
        )


class SearchDecoderTest(TestCase):
    def setUp(self):
        self.decoder: SearchDecoder = SearchDecoder()

    def test_decode_current_search(self):
        reply = SearchContactReply()
        reply.data.searched_items[:] = ["handle", "email"]
        reply.data.estimated_total.lower_estimate = 0
        reply.data.estimated_total.upper_estimate = 100

        result = SearchContactReply.Data.Result()
        result.object_id.uuid.value = "kryten"
        result.matched_items[:] = ["handle"]
        reply.data.results.extend([result])

        self.assertEqual(
            self.decoder.decode(reply),
            SearchResults(
                results=[SearchCurrentResult(object_id="kryten", matched_items=["handle"])],  # type: ignore
                searched_items=["handle", "email"],
                estimated_total=FuzzyValue(lower_estimate=0, upper_estimate=100),
            ),
        )

    def test_decode_current_search_empty(self):
        reply = SearchContactReply()
        reply.data.estimated_total.lower_estimate = 0
        reply.data.estimated_total.upper_estimate = 100

        self.assertEqual(
            self.decoder.decode(reply),
            SearchResults(
                results=[],
                searched_items=[],
                estimated_total=FuzzyValue(lower_estimate=0, upper_estimate=100),
            ),
        )

    def test_decode_history_search(self):
        reply = SearchContactHistoryReply()
        reply.data.searched_items[:] = ["handle", "email"]
        reply.data.estimated_total.lower_estimate = 0
        reply.data.estimated_total.upper_estimate = 100

        history = SearchContactHistoryReply.Data.Result.HistoryPeriod()
        history.matched_items[:] = ["handle"]
        history.valid_from.FromDatetime(datetime(2020, 1, 1, tzinfo=pytz.utc))
        history.contact_history_ids.extend(
            [
                self._get_contact_history_id("augustus"),
                self._get_contact_history_id("nova-5"),
            ]
        )

        result = SearchContactHistoryReply.Data.Result()
        result.object_id.uuid.value = "kryten"
        result.histories.extend([history])

        reply.data.results.extend([result])

        self.assertEqual(
            self.decoder.decode(reply)["data"],
            SearchResults(
                results=[
                    SearchHistoryResult(
                        object_id="kryten",  # type: ignore
                        histories=[
                            HistorySearchPeriod(
                                matched_items=["handle"],
                                valid_from=datetime(2020, 1, 1, tzinfo=pytz.utc),
                                history_ids=["augustus", "nova-5"],
                            )
                        ],
                    )
                ],
                searched_items=["handle", "email"],
                estimated_total=FuzzyValue(lower_estimate=0, upper_estimate=100),
            ),
        )

    def test_decode_history_search_empty(self):
        reply = SearchContactHistoryReply()
        reply.data.estimated_total.lower_estimate = 0
        reply.data.estimated_total.upper_estimate = 100

        self.assertEqual(
            self.decoder.decode(reply)["data"],
            SearchResults(
                results=[],
                searched_items=[],
                estimated_total=FuzzyValue(lower_estimate=0, upper_estimate=100),
            ),
        )

    @staticmethod
    def _get_contact_history_id(object_id: str) -> ContactHistoryId:
        contact_history_id = ContactHistoryId()
        contact_history_id.uuid.value = object_id
        return contact_history_id


class SearchClientTestMixin:
    TestSearchClient: type[BaseSearchClient]
    grpc_namespace: str

    search_request: type
    search_reply: type
    search_method: str

    search_history_request: type
    search_history_reply: type
    search_history_method: str

    def setUp(self):
        self.client = self.TestSearchClient(sentinel.netloc)

    async def test_search_minimal(self):
        reply = self.search_reply()
        reply.data.searched_items[:] = ["field1", "field2"]
        reply.data.estimated_total.lower_estimate = 0
        reply.data.estimated_total.upper_estimate = 100
        cast(AsyncTestClientMixin, self.client).mock.return_value = make_awaitable(reply)

        result = await self.client.search(query_values=["query1"])

        cast(TestCase, self).assertEqual(
            result,
            SearchResults(
                results=[],
                searched_items=["field1", "field2"],
                estimated_total=FuzzyValue(lower_estimate=0, upper_estimate=100),
            ),
        )

        request = self.search_request()
        request.query_values[:] = ["query1"]
        cast(AsyncTestClientMixin, self.client).mock.assert_called_once_with(
            request,
            method="{}/{}".format(self.grpc_namespace, self.search_method),
            timeout=None,
        )

    async def test_search_full(self):
        reply = self.search_reply()
        reply.data.searched_items[:] = ["field1", "field2"]
        reply.data.estimated_total.lower_estimate = 0
        reply.data.estimated_total.upper_estimate = 100

        result = self.search_reply.Data.Result()  # type: ignore[attr-defined]
        result.object_id.uuid.value = "kryten"
        result.matched_items[:] = ["field1"]
        reply.data.results.extend([result])
        cast(AsyncTestClientMixin, self.client).mock.return_value = make_awaitable(reply)

        result = await self.client.search(
            query_values=["query1", "query2"],
            searched_items=["field1", "field2"],
            limit=100,
        )

        cast(TestCase, self).assertEqual(
            result,
            SearchResults(
                results=[SearchCurrentResult(object_id="kryten", matched_items=["field1"])],  # type: ignore[arg-type,unused-ignore]
                searched_items=["field1", "field2"],
                estimated_total=FuzzyValue(lower_estimate=0, upper_estimate=100),
            ),
        )

        request = self.search_request()
        request.query_values[:] = ["query1", "query2"]
        request.searched_items[:] = ["field1", "field2"]
        request.limit = 100
        cast(AsyncTestClientMixin, self.client).mock.assert_called_once_with(
            request,
            method="{}/{}".format(self.grpc_namespace, self.search_method),
            timeout=None,
        )

    async def test_search_object_minimal(self):
        reply = self.search_reply()
        reply.data.searched_items[:] = ["field1", "field2"]
        reply.data.estimated_total.lower_estimate = 0
        reply.data.estimated_total.upper_estimate = 100
        cast(AsyncTestClientMixin, self.client).mock.return_value = make_awaitable(reply)

        with cast(TestCase, self).assertWarnsRegex(
            DeprecationWarning, "Method {} is deprecated in favor of search.".format(self.search_method)
        ):
            result = await getattr(self.client, self.search_method)(query_values=["query1"])

        cast(TestCase, self).assertEqual(
            result,
            SearchResults(
                results=[],
                searched_items=["field1", "field2"],
                estimated_total=FuzzyValue(lower_estimate=0, upper_estimate=100),
            ),
        )

        request = self.search_request()
        request.query_values[:] = ["query1"]
        cast(AsyncTestClientMixin, self.client).mock.assert_called_once_with(
            request,
            method="{}/{}".format(self.grpc_namespace, self.search_method),
            timeout=None,
        )

    async def test_search_object_full(self):
        reply = self.search_reply()
        reply.data.searched_items[:] = ["field1", "field2"]
        reply.data.estimated_total.lower_estimate = 0
        reply.data.estimated_total.upper_estimate = 100

        result = self.search_reply.Data.Result()  # type: ignore[attr-defined]
        result.object_id.uuid.value = "kryten"
        result.matched_items[:] = ["field1"]
        reply.data.results.extend([result])
        cast(AsyncTestClientMixin, self.client).mock.return_value = make_awaitable(reply)

        with cast(TestCase, self).assertWarnsRegex(
            DeprecationWarning, "Method {} is deprecated in favor of search.".format(self.search_method)
        ):
            result = await getattr(self.client, self.search_method)(
                query_values=["query1", "query2"],
                searched_items=["field1", "field2"],
                limit=100,
            )

        cast(TestCase, self).assertEqual(
            result,
            SearchResults(
                results=[SearchCurrentResult(object_id="kryten", matched_items=["field1"])],  # type: ignore
                searched_items=["field1", "field2"],
                estimated_total=FuzzyValue(lower_estimate=0, upper_estimate=100),
            ),
        )

        request = self.search_request()
        request.query_values[:] = ["query1", "query2"]
        request.searched_items[:] = ["field1", "field2"]
        request.limit = 100
        cast(AsyncTestClientMixin, self.client).mock.assert_called_once_with(
            request,
            method="{}/{}".format(self.grpc_namespace, self.search_method),
            timeout=None,
        )

    async def test_search_history_minimal(self):
        reply = self.search_history_reply()
        reply.data.searched_items[:] = ["field1", "field2"]
        reply.data.estimated_total.lower_estimate = 0
        reply.data.estimated_total.upper_estimate = 100
        cast(AsyncTestClientMixin, self.client).mock.return_value = make_awaitable(reply)

        result = await self.client.search_history(query_values=["query1"])

        cast(TestCase, self).assertEqual(
            result,
            SearchResults(
                results=[],
                searched_items=["field1", "field2"],
                estimated_total=FuzzyValue(lower_estimate=0, upper_estimate=100),
            ),
        )

        request = self.search_history_request()
        request.query_values[:] = ["query1"]
        cast(AsyncTestClientMixin, self.client).mock.assert_called_once_with(
            request,
            method="{}/{}".format(self.grpc_namespace, self.search_history_method),
            timeout=None,
        )

    async def test_search_history_full(self):
        reply = self.search_history_reply()
        reply.data.searched_items[:] = ["field1", "field2"]
        reply.data.estimated_total.lower_estimate = 0
        reply.data.estimated_total.upper_estimate = 100

        history = self.search_history_reply.Data.Result.HistoryPeriod()  # type: ignore[attr-defined]
        history.matched_items[:] = ["field1"]
        history.valid_from.FromDatetime(datetime(2020, 1, 1, tzinfo=pytz.utc))

        result = self.search_history_reply.Data.Result()  # type: ignore[attr-defined]
        result.object_id.uuid.value = "kryten"
        result.histories.extend([history])

        reply.data.results.extend([result])
        cast(AsyncTestClientMixin, self.client).mock.return_value = make_awaitable(reply)

        result = await self.client.search_history(
            query_values=["query1", "query2"],
            searched_items=["field1", "field2"],
            limit=50,
            start=datetime(2020, 1, 1, tzinfo=pytz.utc),
            end=datetime(2021, 1, 1, tzinfo=pytz.utc),
        )

        cast(TestCase, self).assertEqual(result, self.client.decoder.decode(reply)["data"])

        request = self.search_history_request()
        request.query_values[:] = ["query1", "query2"]
        request.searched_items[:] = ["field1", "field2"]
        setattr(request, self.client.limit_name, 50)
        request.data_valid_from.FromDatetime(datetime(2020, 1, 1, tzinfo=pytz.utc))
        request.data_valid_to.FromDatetime(datetime(2021, 1, 1, tzinfo=pytz.utc))
        cast(AsyncTestClientMixin, self.client).mock.assert_called_once_with(
            request,
            method="{}/{}".format(self.grpc_namespace, self.search_history_method),
            timeout=None,
        )

    async def test_search_object_history_minimal(self):
        reply = self.search_history_reply()
        reply.data.searched_items[:] = ["field1", "field2"]
        reply.data.estimated_total.lower_estimate = 0
        reply.data.estimated_total.upper_estimate = 100
        cast(AsyncTestClientMixin, self.client).mock.return_value = make_awaitable(reply)

        with cast(TestCase, self).assertWarnsRegex(
            DeprecationWarning, "Method {} is deprecated in favor of search_history.".format(self.search_history_method)
        ):
            result = await getattr(self.client, self.search_history_method)(query_values=["query1"])

        cast(TestCase, self).assertEqual(
            result,
            SearchResults(
                results=[],
                searched_items=["field1", "field2"],
                estimated_total=FuzzyValue(lower_estimate=0, upper_estimate=100),
            ),
        )

        request = self.search_history_request()
        request.query_values[:] = ["query1"]
        cast(AsyncTestClientMixin, self.client).mock.assert_called_once_with(
            request,
            method="{}/{}".format(self.grpc_namespace, self.search_history_method),
            timeout=None,
        )

    async def test_search_object_history_full(self):
        reply = self.search_history_reply()
        reply.data.searched_items[:] = ["field1", "field2"]
        reply.data.estimated_total.lower_estimate = 0
        reply.data.estimated_total.upper_estimate = 100

        history = self.search_history_reply.Data.Result.HistoryPeriod()  # type: ignore[attr-defined]
        history.matched_items[:] = ["field1"]
        history.valid_from.FromDatetime(datetime(2020, 1, 1, tzinfo=pytz.utc))

        result = self.search_history_reply.Data.Result()  # type: ignore[attr-defined]
        result.object_id.uuid.value = "kryten"
        result.histories.extend([history])

        reply.data.results.extend([result])
        cast(AsyncTestClientMixin, self.client).mock.return_value = make_awaitable(reply)

        with cast(TestCase, self).assertWarnsRegex(
            DeprecationWarning, "Method {} is deprecated in favor of search_history.".format(self.search_history_method)
        ):
            result = await getattr(self.client, self.search_history_method)(
                query_values=["query1", "query2"],
                searched_items=["field1", "field2"],
                limit=50,
                start=datetime(2020, 1, 1, tzinfo=pytz.utc),
                end=datetime(2021, 1, 1, tzinfo=pytz.utc),
            )

        cast(TestCase, self).assertEqual(result, self.client.decoder.decode(reply)["data"])

        request = self.search_history_request()
        request.query_values[:] = ["query1", "query2"]
        request.searched_items[:] = ["field1", "field2"]
        setattr(request, self.client.limit_name, 50)
        request.data_valid_from.FromDatetime(datetime(2020, 1, 1, tzinfo=pytz.utc))
        request.data_valid_to.FromDatetime(datetime(2021, 1, 1, tzinfo=pytz.utc))
        cast(AsyncTestClientMixin, self.client).mock.assert_called_once_with(
            request,
            method="{}/{}".format(self.grpc_namespace, self.search_history_method),
            timeout=None,
        )


class SearchContactClientTest(SearchClientTestMixin, IsolatedAsyncioTestCase):
    class TestSearchClient(AsyncTestClientMixin, SearchContactClient):
        """Test search contact client."""

    grpc_namespace = "/Fred.Registry.Api.Contact.SearchContact"

    search_request = SearchContactRequest
    search_reply = SearchContactReply
    search_method = "search_contact"

    search_history_request = SearchContactHistoryRequest
    search_history_reply = SearchContactHistoryReply
    search_history_method = "search_contact_history"


class SearchDomainClientTest(SearchClientTestMixin, IsolatedAsyncioTestCase):
    class TestSearchClient(AsyncTestClientMixin, SearchDomainClient):
        """Test search domain client."""

    grpc_namespace = "/Fred.Registry.Api.Domain.SearchDomain"

    search_request = SearchDomainRequest
    search_reply = SearchDomainReply
    search_method = "search_domain"

    search_history_request = SearchDomainHistoryRequest
    search_history_reply = SearchDomainHistoryReply
    search_history_method = "search_domain_history"


class SearchKeysetClientTest(SearchClientTestMixin, IsolatedAsyncioTestCase):
    class TestSearchClient(AsyncTestClientMixin, SearchKeysetClient):
        """Test search keyset client."""

    grpc_namespace = "/Fred.Registry.Api.Keyset.SearchKeyset"

    search_request = SearchKeysetRequest
    search_reply = SearchKeysetReply
    search_method = "search_keyset"

    search_history_request = SearchKeysetHistoryRequest
    search_history_reply = SearchKeysetHistoryReply
    search_history_method = "search_keyset_history"


class SearchNssetClientTest(SearchClientTestMixin, IsolatedAsyncioTestCase):
    class TestSearchClient(AsyncTestClientMixin, SearchNssetClient):
        """Test search nsset client."""

    grpc_namespace = "/Fred.Registry.Api.Nsset.SearchNsset"

    search_request = SearchNssetRequest
    search_reply = SearchNssetReply
    search_method = "search_nsset"

    search_history_request = SearchNssetHistoryRequest
    search_history_reply = SearchNssetHistoryReply
    search_history_method = "search_nsset_history"

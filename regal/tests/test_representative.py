from typing import Any
from unittest import IsolatedAsyncioTestCase
from unittest.mock import call, sentinel

from fred_api.registry.contact_representative.contact_representative_common_types_pb2 import ContactRepresentativeInfo
from fred_api.registry.contact_representative.service_contact_representative_grpc_pb2 import (
    ContactRepresentativeInfoReply,
    ContactRepresentativeInfoRequest,
    CreateContactRepresentativeReply,
    CreateContactRepresentativeRequest,
    DeleteContactRepresentativeReply,
    DeleteContactRepresentativeRequest,
    UpdateContactRepresentativeReply,
    UpdateContactRepresentativeRequest,
)
from fred_types import ContactId, LogEntryId
from frgal.utils import AsyncTestClientMixin

from regal.common import Address
from regal.representative import (
    Representative,
    RepresentativeClient,
    RepresentativeDecoder,
    RepresentativeHistoryId,
    RepresentativeId,
)

from .utils import make_awaitable


class RepresentativeDecoderTest(IsolatedAsyncioTestCase):
    """Test RepresentativeDecoder."""

    def setUp(self):
        self.decoder = RepresentativeDecoder()

    def test_decode_info(self):
        info_1 = ContactRepresentativeInfo()
        info_1.id.value = "RIMMER"
        info_1.contact_id.uuid.value = "SMEGHEAD"

        info_2 = ContactRepresentativeInfo()
        info_2.id.value = "RIMMER"
        info_2.contact_id.uuid.value = "SMEGHEAD"
        info_2.history_id.value = "ACE"

        info_3 = ContactRepresentativeInfo(name="Arnold Rimmer")
        info_3.id.value = "RIMMER"
        info_3.contact_id.uuid.value = "SMEGHEAD"

        info_4 = ContactRepresentativeInfo()
        info_4.id.value = "RIMMER"
        info_4.contact_id.uuid.value = "SMEGHEAD"
        info_4.name = "Arnold Rimmer"
        info_4.organization = "Jupiter Mining Corporation"
        info_4.place.street.append("Deck 16")
        info_4.place.city = "Red Dwarf"
        info_4.place.state_or_province = "Deep Space"
        info_4.place.postal_code.value = "JMC"
        info_4.place.country_code.value = "JU"
        info_4.telephone.value = "Chief!"
        info_4.email.value = "arnold@example.org"
        place = Address(
            street=["Deck 16"], city="Red Dwarf", state_or_province="Deep Space", postal_code="JMC", country_code="JU"
        )
        result_4 = Representative(
            id=RepresentativeId("RIMMER"),
            contact_id=ContactId("SMEGHEAD"),
            name="Arnold Rimmer",
            organization="Jupiter Mining Corporation",
            place=place,
            telephone="Chief!",
            email="arnold@example.org",
        )
        data = (
            # info, result
            (info_1, Representative(id=RepresentativeId("RIMMER"), contact_id=ContactId("SMEGHEAD"))),
            (
                info_2,
                Representative(
                    id=RepresentativeId("RIMMER"),
                    history_id=RepresentativeHistoryId("ACE"),
                    contact_id=ContactId("SMEGHEAD"),
                ),
            ),
            (
                info_3,
                Representative(id=RepresentativeId("RIMMER"), contact_id=ContactId("SMEGHEAD"), name="Arnold Rimmer"),
            ),
            (info_4, result_4),
        )
        for info, result in data:
            with self.subTest(info=info):
                self.assertEqual(self.decoder.decode(info), result)


class TestRepresentativeClient(AsyncTestClientMixin, RepresentativeClient):
    """Test RepresentativeClient."""


class RegistrarClientTest(IsolatedAsyncioTestCase):
    def setUp(self):
        self.client = TestRepresentativeClient(netloc=sentinel.netloc)

    async def _test_get(
        self,
        value: Any,
        request: ContactRepresentativeInfoRequest,
    ) -> None:
        reply = ContactRepresentativeInfoReply()
        reply.data.contact_representative.id.value = "RIMMER"
        reply.data.contact_representative.contact_id.uuid.value = "SMEGHEAD"
        self.client.mock.return_value = make_awaitable(reply)
        result = Representative(id=RepresentativeId("RIMMER"), contact_id=ContactId("SMEGHEAD"))

        self.assertEqual(await self.client.get(value), result)

        a_call = call(
            request,
            method="/Fred.Registry.Api.ContactRepresentative.ContactRepresentative/get_contact_representative_info",
            timeout=None,
        )
        self.assertEqual(self.client.mock.mock_calls, [a_call])

    async def test_get_plain(self):
        request = ContactRepresentativeInfoRequest()
        request.id.value = "RIMMER"
        await self._test_get("RIMMER", request)

    async def test_get_id(self):
        request = ContactRepresentativeInfoRequest()
        request.id.value = "RIMMER"
        await self._test_get(RepresentativeId("RIMMER"), request)

    async def test_get_history_id(self):
        request = ContactRepresentativeInfoRequest()
        request.history_id.value = "ACE"
        await self._test_get(RepresentativeHistoryId("ACE"), request)

    async def test_get_contact_id(self):
        request = ContactRepresentativeInfoRequest()
        request.contact_id.uuid.value = "SMEGHEAD"
        await self._test_get(ContactId("SMEGHEAD"), request)

    async def test_create_min(self):
        reply = CreateContactRepresentativeReply()
        reply.data.contact_representative.id.value = "RIMMER"
        reply.data.contact_representative.contact_id.uuid.value = "SMEGHEAD"
        self.client.mock.return_value = make_awaitable(reply)
        representative = Representative(id=RepresentativeId("PLACEHOLDER"), contact_id=ContactId("SMEGHEAD"))
        result = Representative(id=RepresentativeId("RIMMER"), contact_id=ContactId("SMEGHEAD"))

        self.assertEqual(await self.client.create(representative), result)

        request = CreateContactRepresentativeRequest()
        request.contact_id.uuid.value = "SMEGHEAD"
        a_call = call(
            request,
            method="/Fred.Registry.Api.ContactRepresentative.ContactRepresentative/create_contact_representative",
            timeout=None,
        )
        self.assertEqual(self.client.mock.mock_calls, [a_call])

    async def test_create_full(self):
        reply = CreateContactRepresentativeReply()
        reply.data.contact_representative.id.value = "RIMMER"
        reply.data.contact_representative.contact_id.uuid.value = "SMEGHEAD"
        self.client.mock.return_value = make_awaitable(reply)
        place = Address(
            street=["Deck 16"], city="Red Dwarf", state_or_province="Deep Space", postal_code="JMC", country_code="JU"
        )
        representative = Representative(
            id=RepresentativeId("PLACEHOLDER"),
            contact_id=ContactId("SMEGHEAD"),
            name="Arnold Rimmer",
            organization="Jupiter Mining Corporation",
            place=place,
            telephone="Chief!",
            email="arnold@example.org",
        )
        result = Representative(id=RepresentativeId("RIMMER"), contact_id=ContactId("SMEGHEAD"))

        self.assertEqual(await self.client.create(representative), result)

        request = CreateContactRepresentativeRequest()
        request.contact_id.uuid.value = "SMEGHEAD"
        request.name = "Arnold Rimmer"
        request.organization = "Jupiter Mining Corporation"
        request.place.street.append("Deck 16")
        request.place.city = "Red Dwarf"
        request.place.state_or_province = "Deep Space"
        request.place.postal_code.value = "JMC"
        request.place.country_code.value = "JU"
        request.telephone.value = "Chief!"
        request.email.value = "arnold@example.org"
        a_call = call(
            request,
            method="/Fred.Registry.Api.ContactRepresentative.ContactRepresentative/create_contact_representative",
            timeout=None,
        )
        self.assertEqual(self.client.mock.mock_calls, [a_call])

    async def test_create_log_entry_id(self):
        reply = CreateContactRepresentativeReply()
        reply.data.contact_representative.id.value = "RIMMER"
        reply.data.contact_representative.contact_id.uuid.value = "SMEGHEAD"
        self.client.mock.return_value = make_awaitable(reply)
        representative = Representative(
            id=RepresentativeId("PLACEHOLDER"), contact_id=ContactId("SMEGHEAD"), name="Arnold Rimmer"
        )
        result = Representative(id=RepresentativeId("RIMMER"), contact_id=ContactId("SMEGHEAD"))

        self.assertEqual(await self.client.create(representative, log_entry_id=LogEntryId("QUAGAARS")), result)

        request = CreateContactRepresentativeRequest()
        request.contact_id.uuid.value = "SMEGHEAD"
        request.name = "Arnold Rimmer"
        request.log_entry_id.value = "QUAGAARS"
        a_call = call(
            request,
            method="/Fred.Registry.Api.ContactRepresentative.ContactRepresentative/create_contact_representative",
            timeout=None,
        )
        self.assertEqual(self.client.mock.mock_calls, [a_call])

    async def test_update_min(self):
        reply = UpdateContactRepresentativeReply()
        reply.data.contact_representative.id.value = "RIMMER"
        reply.data.contact_representative.contact_id.uuid.value = "SMEGHEAD"
        self.client.mock.return_value = make_awaitable(reply)
        representative = Representative(id=RepresentativeId("SMEGHEAD"), contact_id=ContactId("PLACEHOLDER"))
        result = Representative(id=RepresentativeId("RIMMER"), contact_id=ContactId("SMEGHEAD"))

        self.assertEqual(await self.client.update(representative), result)

        request = UpdateContactRepresentativeRequest()
        request.id.value = "SMEGHEAD"
        a_call = call(
            request,
            method="/Fred.Registry.Api.ContactRepresentative.ContactRepresentative/update_contact_representative",
            timeout=None,
        )
        self.assertEqual(self.client.mock.mock_calls, [a_call])

    async def test_update_full(self):
        reply = UpdateContactRepresentativeReply()
        reply.data.contact_representative.id.value = "RIMMER"
        reply.data.contact_representative.contact_id.uuid.value = "SMEGHEAD"
        self.client.mock.return_value = make_awaitable(reply)
        place = Address(
            street=["Deck 16"], city="Red Dwarf", state_or_province="Deep Space", postal_code="JMC", country_code="JU"
        )
        representative = Representative(
            id=RepresentativeId("SMEGHEAD"),
            contact_id=ContactId("PLACEHOLDER"),
            name="Arnold Rimmer",
            organization="Jupiter Mining Corporation",
            place=place,
            telephone="Chief!",
            email="arnold@example.org",
        )
        result = Representative(id=RepresentativeId("RIMMER"), contact_id=ContactId("SMEGHEAD"))

        self.assertEqual(await self.client.update(representative), result)

        request = UpdateContactRepresentativeRequest()
        request.id.value = "SMEGHEAD"
        request.name = "Arnold Rimmer"
        request.organization = "Jupiter Mining Corporation"
        request.place.street.append("Deck 16")
        request.place.city = "Red Dwarf"
        request.place.state_or_province = "Deep Space"
        request.place.postal_code.value = "JMC"
        request.place.country_code.value = "JU"
        request.telephone.value = "Chief!"
        request.email.value = "arnold@example.org"
        a_call = call(
            request,
            method="/Fred.Registry.Api.ContactRepresentative.ContactRepresentative/update_contact_representative",
            timeout=None,
        )
        self.assertEqual(self.client.mock.mock_calls, [a_call])

    async def test_update_log_entry_id(self):
        reply = UpdateContactRepresentativeReply()
        reply.data.contact_representative.id.value = "RIMMER"
        reply.data.contact_representative.contact_id.uuid.value = "SMEGHEAD"
        self.client.mock.return_value = make_awaitable(reply)
        representative = Representative(
            id=RepresentativeId("SMEGHEAD"), contact_id=ContactId("PLACEHOLDER"), name="Arnold Rimmer"
        )
        result = Representative(id=RepresentativeId("RIMMER"), contact_id=ContactId("SMEGHEAD"))

        self.assertEqual(await self.client.update(representative, log_entry_id=LogEntryId("QUAGAARS")), result)

        request = UpdateContactRepresentativeRequest()
        request.id.value = "SMEGHEAD"
        request.name = "Arnold Rimmer"
        request.log_entry_id.value = "QUAGAARS"
        a_call = call(
            request,
            method="/Fred.Registry.Api.ContactRepresentative.ContactRepresentative/update_contact_representative",
            timeout=None,
        )
        self.assertEqual(self.client.mock.mock_calls, [a_call])

    async def test_delete(self):
        self.client.mock.return_value = make_awaitable(DeleteContactRepresentativeReply())

        await self.client.delete(RepresentativeId("SMEGHEAD"))

        request = DeleteContactRepresentativeRequest()
        request.id.value = "SMEGHEAD"
        a_call = call(
            request,
            method="/Fred.Registry.Api.ContactRepresentative.ContactRepresentative/delete_contact_representative",
            timeout=None,
        )
        self.assertEqual(self.client.mock.mock_calls, [a_call])

    async def test_delete_log_entry_id(self):
        self.client.mock.return_value = make_awaitable(DeleteContactRepresentativeReply())

        await self.client.delete(RepresentativeId("SMEGHEAD"), log_entry_id=LogEntryId("QUAGAARS"))

        request = DeleteContactRepresentativeRequest()
        request.id.value = "SMEGHEAD"
        request.log_entry_id.value = "QUAGAARS"
        a_call = call(
            request,
            method="/Fred.Registry.Api.ContactRepresentative.ContactRepresentative/delete_contact_representative",
            timeout=None,
        )
        self.assertEqual(self.client.mock.mock_calls, [a_call])

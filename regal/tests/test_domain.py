from collections.abc import Iterable, Sequence
from datetime import datetime
from typing import Any
from unittest import IsolatedAsyncioTestCase, TestCase
from unittest.mock import call, sentinel

import pytz
from asyncstdlib import iter as aiter, list as alist, tuple as atuple
from fred_api.registry.common_types_pb2 import DomainDoesNotExist as DomainDoesNotExistMessage, OrderByDirection
from fred_api.registry.domain.add_domain_auth_info_types_pb2 import AddDomainAuthInfoReply, AddDomainAuthInfoRequest
from fred_api.registry.domain.domain_common_types_pb2 import DomainContactRole as ProtoDomainContactRole
from fred_api.registry.domain.domain_history_types_pb2 import (
    DomainHistoryRecord,
    DomainHistoryReply,
    DomainHistoryRequest,
)
from fred_api.registry.domain.domain_info_types_pb2 import (
    BatchDomainInfoReply,
    BatchDomainInfoRequest,
    DomainIdReply,
    DomainIdRequest,
    DomainInfoReply,
    DomainInfoRequest,
)
from fred_api.registry.domain.domain_life_cycle_stage_types_pb2 import (
    DomainLifeCycleStageReply,
    DomainLifeCycleStageRequest,
)
from fred_api.registry.domain.domain_state_flags_types_pb2 import DomainStateFlagsReply
from fred_api.registry.domain.domain_state_history_types_pb2 import (
    DomainStateHistory,
    DomainStateHistoryReply,
    DomainStateHistoryRequest,
)
from fred_api.registry.domain.domain_state_types_pb2 import DomainStateReply, DomainStateRequest
from fred_api.registry.domain.domains_by_contact_types_pb2 import DomainsByContactReply, DomainsByContactRequest
from fred_api.registry.domain.list_domains_by_contact_types_pb2 import (
    ListDomainsByContactReply,
    ListDomainsByContactRequest,
)
from fred_api.registry.domain.list_domains_by_keyset_types_pb2 import (
    ListDomainsByKeysetReply,
    ListDomainsByKeysetRequest,
)
from fred_api.registry.domain.list_domains_by_nsset_types_pb2 import ListDomainsByNssetReply, ListDomainsByNssetRequest
from fred_api.registry.domain.list_domains_types_pb2 import ListDomainsReply, ListDomainsRequest
from fred_api.registry.domain.service_admin_grpc_pb2 import (
    BatchDeleteDomainsReply as BatchDeleteDomainsReplyMessage,
    BatchDeleteDomainsRequest,
    DomainContactInfo,
    GetDomainsDeleteNotifyInfoRequest,
    GetDomainsNotifyInfoReply as GetDomainsNotifyInfoReplyMessage,
    GetDomainsOutzoneNotifyInfoRequest,
    ManageDomainStateFlagsReply as ManageDomainStateFlagsReplyMessage,
    ManageDomainStateFlagsRequest,
    UpdateDomainsAdditionalNotifyInfoReply,
    UpdateDomainsDeleteAdditionalNotifyInfoRequest,
    UpdateDomainsOutzoneAdditionalNotifyInfoRequest,
)
from fred_api.registry.domain.update_domain_state_types_pb2 import UpdateDomainStateRequest
from fred_types import (
    ContactId,
    ContactRef,
    DomainHistoryId,
    DomainId,
    DomainRef,
    KeysetId,
    KeysetRef,
    NssetId,
    NssetRef,
    SnapshotId,
)
from frgal.utils import AsyncTestClientMixin
from google.protobuf.empty_pb2 import Empty

from regal.common import DatetimeRange, ListPage, PaginationReply
from regal.domain import (
    BatchDeleteDomainsReply,
    ContactInfo,
    ContactInfoType,
    Domain,
    DomainAdminClient,
    DomainAdminDecoder,
    DomainByRelation,
    DomainClient,
    DomainContactRole,
    DomainDecoder,
    DomainLifeCycleStage,
    DomainLifeCycleStageResult,
    GetDomainsNotifyInfoReply,
    ManageDomainStateFlagsReply,
    NotDeletedReason,
    NotifyInfoDomain,
)
from regal.domain_blacklist import BlockId
from regal.exceptions import DomainDoesNotExist
from regal.object import StateUpdate
from regal.tests.common import ObjectClientTestMixin

from .utils import make_awaitable


class TestDomainClient(AsyncTestClientMixin, DomainClient):
    """Test DomainClient."""

    exhaust_iterables = True


class TestDomainAdminClient(AsyncTestClientMixin, DomainAdminClient):
    """Test DomainAdminClient."""

    exhaust_iterables = True


class DomainDecoderTest(TestCase):
    def setUp(self):
        self.decoder = DomainDecoder()

    def test_decode_domain_by_contact_no_role(self):
        domain = DomainsByContactReply.Data.Domain()
        domain.domain.id.uuid.value = "domain.1"
        domain.domain.fqdn.value = "example.com"
        domain.domain_history_id.uuid.value = "domain.history.1"
        domain.is_deleted = True

        decoded = self.decoder.decode(domain)
        self.assertIsInstance(decoded["domain"], DomainRef)
        self.assertEqual(
            decoded,
            {
                "domain": DomainRef(id=DomainId("domain.1"), fqdn="example.com"),
                "domain_history_id": DomainHistoryId("domain.history.1"),
                "roles": set(),
                "is_deleted": True,
            },
        )

    def test_decode_domain_by_contact_one_role(self):
        data = {
            "is_holder": DomainContactRole.HOLDER,
            "is_admin_contact": DomainContactRole.ADMIN,
            "is_keyset_tech_contact": DomainContactRole.KEYSET_TECH,
            "is_nsset_tech_contact": DomainContactRole.NSSET_TECH,
        }

        for name, role in data.items():
            with self.subTest(name=name, role=role):
                domain = DomainsByContactReply.Data.Domain()
                domain.domain.id.uuid.value = "domain.1"
                domain.domain.fqdn.value = "example.com"
                domain.domain_history_id.uuid.value = "domain.history.1"
                setattr(domain, name, True)
                self.assertEqual(
                    self.decoder.decode(domain),
                    {
                        "domain": DomainRef(id=DomainId("domain.1"), fqdn="example.com"),
                        "domain_history_id": DomainHistoryId("domain.history.1"),
                        "roles": {role},
                        "is_deleted": False,
                    },
                )

    def test_decode_domain_by_contact_all_roles(self):
        domain = DomainsByContactReply.Data.Domain()
        domain.domain.id.uuid.value = "domain.1"
        domain.domain.fqdn.value = "example.com"
        domain.domain_history_id.uuid.value = "domain.history.1"
        domain.is_holder = True
        domain.is_admin_contact = True
        domain.is_keyset_tech_contact = True
        domain.is_nsset_tech_contact = True
        self.assertEqual(
            self.decoder.decode(domain),
            {
                "domain": DomainRef(id=DomainId("domain.1"), fqdn="example.com"),
                "domain_history_id": DomainHistoryId("domain.history.1"),
                "roles": set(DomainContactRole),
                "is_deleted": False,
            },
        )

    def test_decode_list_by_contact_domain(self):
        data: Iterable[tuple[tuple[int, ...], set[DomainContactRole]]] = (
            # roles, result
            ((), set()),
            ((ProtoDomainContactRole.role_registrant,), {DomainContactRole.HOLDER}),
            ((ProtoDomainContactRole.role_administrative,), {DomainContactRole.ADMIN}),
            ((ProtoDomainContactRole.role_keyset_technical,), {DomainContactRole.KEYSET_TECH}),
            ((ProtoDomainContactRole.role_nsset_technical,), {DomainContactRole.NSSET_TECH}),
            (
                (ProtoDomainContactRole.role_registrant, ProtoDomainContactRole.role_administrative),
                {DomainContactRole.HOLDER, DomainContactRole.ADMIN},
            ),
        )
        for roles, result in data:
            with self.subTest(roles=roles):
                domain = ListDomainsByContactReply.Data.Domain()
                domain.domain.id.uuid.value = "domain.1"
                domain.domain.fqdn.value = "example.com"
                domain.domain_history_id.uuid.value = "domain.history.1"
                domain.is_deleted = True
                domain.roles.extend(roles)

                decoded = self.decoder.decode(domain)
                self.assertEqual(
                    decoded,
                    {
                        "domain": DomainRef(id=DomainId("domain.1"), fqdn="example.com"),
                        "domain_history_id": DomainHistoryId("domain.history.1"),
                        "roles": result,
                        "is_deleted": True,
                    },
                )


class DomainAdminDecoderTest(TestCase):
    def setUp(self):
        self.decoder = DomainAdminDecoder()

    def test_decode_domain_contact_info(self):
        self.assertEqual(
            self.decoder.decode(DomainContactInfo(email="rimmer@example.org")),
            ContactInfo(type=ContactInfoType.EMAIL, value="rimmer@example.org"),
        )
        self.assertEqual(
            self.decoder.decode(DomainContactInfo(telephone="123456")),
            ContactInfo(type=ContactInfoType.TELEPHONE, value="123456"),
        )
        self.assertIsNone(self.decoder.decode(DomainContactInfo()))


class DomainClientTest(ObjectClientTestMixin[TestDomainClient], IsolatedAsyncioTestCase):
    """Test DomainClient's methods."""

    grpc_namespace = "/Fred.Registry.Api.Domain.Domain"
    get_state_request = DomainStateRequest
    get_state_reply = DomainStateReply
    get_state_method = "get_domain_state"
    get_history_request = DomainHistoryRequest
    get_history_reply = DomainHistoryReply
    get_history_method = "get_domain_history"
    get_history_record = DomainHistoryRecord
    get_state_history_request = DomainStateHistoryRequest
    get_state_history_reply = DomainStateHistoryReply
    get_state_history_method = "get_domain_state_history"
    get_state_history_record = DomainStateHistory.Record
    get_state_flags_method = "get_domain_state_flags"
    get_state_flags_reply_cls = DomainStateFlagsReply

    def setUp(self):
        self.client = TestDomainClient(netloc=sentinel.netloc)

    async def _test_get_id(self, fqdn: str, request_fqdn: str) -> None:
        domain_id = DomainId("kryten")
        reply = DomainIdReply()
        reply.data.domain_id.uuid.value = domain_id
        self.client.mock.return_value = make_awaitable(reply)

        response = await self.client.get_id(fqdn)
        self.assertEqual(response, domain_id)
        self.assertIsInstance(response, DomainId)

        request = DomainIdRequest()
        request.fqdn.value = request_fqdn
        self.client.mock.assert_called_once_with(
            request,
            method="/Fred.Registry.Api.Domain.Domain/get_domain_id",
            timeout=None,
        )

    async def test_get_id(self):
        await self._test_get_id("example.org", "example.org")

    async def test_get_id_idn(self):
        await self._test_get_id("háčkyčárky.cz", "xn--hkyrky-ptac70bc.cz")

    async def _test_get_domain_id(self, fqdn: str, request_fqdn: str) -> None:
        domain_id = DomainId("kryten")
        reply = DomainIdReply()
        reply.data.domain_id.uuid.value = domain_id
        self.client.mock.return_value = make_awaitable(reply)

        with self.assertWarnsRegex(DeprecationWarning, "Method get_domain_id is deprecated in favor of get_id."):
            response = await self.client.get_domain_id(fqdn)
        self.assertEqual(response, domain_id)
        self.assertIsInstance(response, DomainId)

        request = DomainIdRequest()
        request.fqdn.value = request_fqdn
        self.client.mock.assert_called_once_with(
            request,
            method="/Fred.Registry.Api.Domain.Domain/get_domain_id",
            timeout=None,
        )

    async def test_get_domain_id(self):
        await self._test_get_domain_id("example.org", "example.org")

    async def test_get_domain_id_idn(self):
        await self._test_get_domain_id("háčkyčárky.cz", "xn--hkyrky-ptac70bc.cz")

    async def _test_get(self, request: DomainInfoRequest, **kwargs: Any) -> None:
        reply = DomainInfoReply()
        reply.data.fqdn.value = "example.com"
        reply.data.domain_id.uuid.value = "holly"
        reply.data.domain_history_id.uuid.value = "hilly"

        reply.data.registrant.id.uuid.value = "JMC"
        reply.data.registrant.handle.value = "JMC-HANDLE"
        reply.data.registrant.name = "Arnold Rimmer"

        reply.data.nsset.id.uuid.value = "red-dwarf"
        reply.data.nsset.handle.value = "RED-DWARF"

        reply.data.keyset.id.uuid.value = "queeg-500"
        reply.data.keyset.handle.value = "QUEEG-500"

        reply.data.sponsoring_registrar.value = "REG-JMC"
        reply.data.expires_at.FromDatetime(datetime(2030, 1, 1, 10, 0, 4, tzinfo=pytz.utc))
        self.client.mock.return_value = make_awaitable(reply)

        response = await self.client.get(DomainId("holly"), **kwargs)

        self.assertEqual(
            response,
            Domain(
                fqdn="example.com",
                domain_id=DomainId("holly"),
                domain_history_id=DomainHistoryId("hilly"),
                registrant=ContactRef(id=ContactId("JMC"), handle="JMC-HANDLE", name="Arnold Rimmer"),
                nsset=NssetRef(id=NssetId("red-dwarf"), handle="RED-DWARF"),
                keyset=KeysetRef(id=KeysetId("queeg-500"), handle="QUEEG-500"),
                sponsoring_registrar="REG-JMC",
                expires_at=datetime(2030, 1, 1, 10, 0, 4, tzinfo=pytz.utc),
            ),
        )

        request.domain_id.uuid.value = "holly"
        self.client.mock.assert_called_once_with(
            request,
            method="/Fred.Registry.Api.Domain.Domain/get_domain_info",
            timeout=None,
        )

    async def test_get(self):
        request = DomainInfoRequest()
        await self._test_get(request)

    async def test_get_history_id(self):
        request = DomainInfoRequest()
        request.domain_history_id.uuid.value = "nova-5"
        await self._test_get(request, history_id=DomainHistoryId("nova-5"))

    async def test_get_snapshot_id(self):
        # Test get with a snapshot_id.
        request = DomainInfoRequest()
        request.domain_snapshot_id.value = "LAST-THURSDAY"
        await self._test_get(request, snapshot_id=SnapshotId("LAST-THURSDAY"))

    async def test_get_no_expiry_date(self):
        reply = DomainInfoReply()
        reply.data.fqdn.value = "example.com"
        reply.data.domain_id.uuid.value = "holly"
        reply.data.domain_history_id.uuid.value = "hilly"
        reply.data.registrant.id.uuid.value = "JMC"
        reply.data.registrant.handle.value = "JMC-HANDLE"
        reply.data.sponsoring_registrar.value = "REG-JMC"
        self.client.mock.return_value = make_awaitable(reply)

        response = await self.client.get(domain_id=DomainId("holly"))

        self.assertEqual(
            response,
            Domain(
                fqdn="example.com",
                domain_id=DomainId("holly"),
                domain_history_id=DomainHistoryId("hilly"),
                registrant=ContactRef(id=ContactId("JMC"), handle="JMC-HANDLE"),
                sponsoring_registrar="REG-JMC",
                expires_at=None,
            ),
        )

    async def test_get_lifecycle(self):
        events = {
            "expiration_warning_scheduled_at",
            "outzone_unguarded_warning_scheduled_at",
            "outzone_scheduled_at",
            "delete_warning_scheduled_at",
            "delete_candidate_scheduled_at",
            "validation_expires_at",
            "outzone_at",
            "delete_candidate_at",
        }
        for event in events:
            with self.subTest(event=event):
                timestamp = datetime(1988, 9, 6, 12, tzinfo=pytz.UTC)
                reply = DomainInfoReply()
                reply.data.fqdn.value = "example.com"
                reply.data.domain_id.uuid.value = "holly"
                getattr(reply.data, event).FromDatetime(timestamp)
                self.client.mock.return_value = make_awaitable(reply)

                response = await self.client.get(DomainId("holly"))

                domain = Domain(fqdn="example.com", domain_id=DomainId("holly"))
                setattr(domain, event, timestamp)
                self.assertEqual(response, domain)

    async def _test_get_domain_info(self, domain_history_id: bool = False) -> None:
        reply = DomainInfoReply()
        reply.data.fqdn.value = "example.com"
        reply.data.domain_id.uuid.value = "holly"
        reply.data.domain_history_id.uuid.value = "hilly"

        reply.data.registrant.id.uuid.value = "JMC"
        reply.data.registrant.handle.value = "JMC-HANDLE"
        reply.data.registrant.name = "Arnold Rimmer"

        reply.data.nsset.id.uuid.value = "red-dwarf"
        reply.data.nsset.handle.value = "RED-DWARF"

        reply.data.keyset.id.uuid.value = "queeg-500"
        reply.data.keyset.handle.value = "QUEEG-500"

        reply.data.sponsoring_registrar.value = "REG-JMC"
        reply.data.expires_at.FromDatetime(datetime(2030, 1, 1, 10, 0, 4, tzinfo=pytz.utc))
        self.client.mock.return_value = make_awaitable(reply)

        with self.assertWarnsRegex(DeprecationWarning, "Method get_domain_info is deprecated in favor of get."):
            response = await self.client.get_domain_info(
                DomainId("holly"), history_id=DomainHistoryId("hilly") if domain_history_id else None
            )

        self.assertEqual(
            response,
            Domain(
                fqdn="example.com",
                domain_id=DomainId("holly"),
                domain_history_id=DomainHistoryId("hilly"),
                registrant=ContactRef(id=ContactId("JMC"), handle="JMC-HANDLE", name="Arnold Rimmer"),
                nsset=NssetRef(id=NssetId("red-dwarf"), handle="RED-DWARF"),
                keyset=KeysetRef(id=KeysetId("queeg-500"), handle="QUEEG-500"),
                sponsoring_registrar="REG-JMC",
                expires_at=datetime(2030, 1, 1, 10, 0, 4, tzinfo=pytz.utc),
            ),
        )

        request = DomainInfoRequest()
        request.domain_id.uuid.value = "holly"
        if domain_history_id:
            request.domain_history_id.uuid.value = "hilly"
        self.client.mock.assert_called_once_with(
            request,
            method="/Fred.Registry.Api.Domain.Domain/get_domain_info",
            timeout=None,
        )

    async def test_get_domain_info(self):
        await self._test_get_domain_info(False)

    async def test_get_domain_info_with_history_id(self):
        await self._test_get_domain_info(True)

    async def test_get_domain_info_no_expiry_date(self):
        reply = DomainInfoReply()
        reply.data.fqdn.value = "example.com"
        reply.data.domain_id.uuid.value = "holly"
        reply.data.domain_history_id.uuid.value = "hilly"
        reply.data.registrant.id.uuid.value = "JMC"
        reply.data.registrant.handle.value = "JMC-HANDLE"
        reply.data.sponsoring_registrar.value = "REG-JMC"
        self.client.mock.return_value = make_awaitable(reply)

        with self.assertWarnsRegex(DeprecationWarning, "Method get_domain_info is deprecated in favor of get."):
            response = await self.client.get_domain_info(domain_id=DomainId("holly"))

        self.assertEqual(
            response,
            Domain(
                fqdn="example.com",
                domain_id=DomainId("holly"),
                domain_history_id=DomainHistoryId("hilly"),
                registrant=ContactRef(id=ContactId("JMC"), handle="JMC-HANDLE"),
                sponsoring_registrar="REG-JMC",
                expires_at=None,
            ),
        )

    async def test_get_domain_info_lifecycle(self):
        events = {
            "expiration_warning_scheduled_at",
            "outzone_unguarded_warning_scheduled_at",
            "outzone_scheduled_at",
            "delete_warning_scheduled_at",
            "delete_candidate_scheduled_at",
            "validation_expires_at",
            "outzone_at",
            "delete_candidate_at",
        }
        for event in events:
            with self.subTest(event=event):
                timestamp = datetime(1988, 9, 6, 12, tzinfo=pytz.UTC)
                reply = DomainInfoReply()
                reply.data.fqdn.value = "example.com"
                reply.data.domain_id.uuid.value = "holly"
                getattr(reply.data, event).FromDatetime(timestamp)
                self.client.mock.return_value = make_awaitable(reply)

                with self.assertWarnsRegex(DeprecationWarning, "Method get_domain_info is deprecated in favor of get."):
                    response = await self.client.get_domain_info(DomainId("holly"))

                domain = Domain(fqdn="example.com", domain_id=DomainId("holly"))
                setattr(domain, event, timestamp)
                self.assertEqual(response, domain)

    async def _test_batch_get(self, domains: tuple[Domain, ...]) -> None:
        result = self.client.batch_get((DomainId("starbug-42"),))

        self.assertEqual(await atuple(result), domains)
        request = BatchDomainInfoRequest()
        req_1 = request.requests.add()
        req_1.domain_id.uuid.value = "starbug-42"
        self.assertEqual(
            self.client.stream_mock.mock_calls,
            [call((request,), method="/Fred.Registry.Api.Domain.Domain/batch_domain_info", timeout=None)],
        )

    async def test_batch_get_empty_stream(self):
        self.client.stream_mock.return_value = aiter(())

        await self._test_batch_get(())

    async def test_batch_get_empty_response(self):
        reply = BatchDomainInfoReply()
        self.client.stream_mock.return_value = aiter([reply])

        await self._test_batch_get(())

    async def test_batch_get_empty_replies(self):
        reply = BatchDomainInfoReply()
        reply.data.replies.extend([])
        self.client.stream_mock.return_value = aiter([reply])

        await self._test_batch_get(())

    async def test_batch_get(self):
        reply = BatchDomainInfoReply()
        domain_reply = reply.data.replies.add()
        domain_reply.data.domain_id.uuid.value = "starbug-42"
        domain_reply.data.fqdn.value = "example.org"
        self.client.stream_mock.return_value = aiter([reply])

        domain = Domain(domain_id=DomainId("starbug-42"), fqdn="example.org")
        await self._test_batch_get((domain,))

    async def test_batch_get_snapshot_id(self):
        # Ensure snapshot is passed to each request.
        snapshot_id = SnapshotId("LAST-THURSDAY")
        self.client.stream_mock.return_value = aiter(())

        await atuple(self.client.batch_get((DomainId("starbug-42"), DomainId("starbug-43")), snapshot_id=snapshot_id))

        request = BatchDomainInfoRequest()
        req_1 = request.requests.add()
        req_1.domain_id.uuid.value = "starbug-42"
        req_1.domain_snapshot_id.value = snapshot_id
        req_2 = request.requests.add()
        req_2.domain_id.uuid.value = "starbug-43"
        req_2.domain_snapshot_id.value = snapshot_id
        self.assertEqual(
            self.client.stream_mock.mock_calls,
            [call((request,), method="/Fred.Registry.Api.Domain.Domain/batch_domain_info", timeout=None)],
        )

    async def test_batch_get_error_ignore(self):
        reply = BatchDomainInfoReply()
        domain_reply = reply.data.replies.add()
        domain_reply.error.exception.domain_does_not_exist.CopyFrom(DomainDoesNotExistMessage())
        self.client.stream_mock.return_value = aiter([reply])

        await self._test_batch_get(())

    async def test_batch_get_error_raise(self):
        reply = BatchDomainInfoReply()
        domain_reply = reply.data.replies.add()
        domain_reply.error.exception.domain_does_not_exist.CopyFrom(DomainDoesNotExistMessage())
        self.client.stream_mock.return_value = aiter([reply])

        with self.assertRaises(DomainDoesNotExist):
            await atuple(self.client.batch_get((DomainId("starbug-42"),), errors="raise"))

    async def test_batch_get_error_invalid(self):
        with self.assertRaisesRegex(ValueError, "Unknown errors value: 'invalid'"):
            await atuple(self.client.batch_get((), errors="invalid"))  # type: ignore[arg-type]

    async def test_batch_get_flatten(self):
        # Test responses are properly flattened.
        reply_1 = BatchDomainInfoReply()
        domain_reply_1 = reply_1.data.replies.add()
        domain_reply_1.data.domain_id.uuid.value = "42"
        domain_reply_1.data.fqdn.value = "lister.example.org"
        domain_reply_2 = reply_1.data.replies.add()
        domain_reply_2.data.domain_id.uuid.value = "43"
        domain_reply_2.data.fqdn.value = "rimmer.example.org"
        reply_2 = BatchDomainInfoReply()
        domain_reply_3 = reply_2.data.replies.add()
        domain_reply_3.data.domain_id.uuid.value = "44"
        domain_reply_3.data.fqdn.value = "kryten.example.org"
        domain_reply_4 = reply_2.data.replies.add()
        domain_reply_4.data.domain_id.uuid.value = "45"
        domain_reply_4.data.fqdn.value = "cat.example.org"
        self.client.stream_mock.return_value = aiter([reply_1, reply_2])

        domains = (
            Domain(domain_id=DomainId("42"), fqdn="lister.example.org"),
            Domain(domain_id=DomainId("43"), fqdn="rimmer.example.org"),
            Domain(domain_id=DomainId("44"), fqdn="kryten.example.org"),
            Domain(domain_id=DomainId("45"), fqdn="cat.example.org"),
        )
        await self._test_batch_get(domains)

    async def test_list_empty_stream(self):
        self.client.stream_mock.return_value = aiter(())

        domains = await atuple(self.client.list("example"))

        self.assertEqual(domains, ())
        request = ListDomainsRequest(zone="example")
        self.assertEqual(
            self.client.stream_mock.mock_calls,
            [call(request, method="/Fred.Registry.Api.Domain.Domain/list_domains", timeout=None)],
        )

    async def test_list_empty_response(self):
        reply = ListDomainsReply()
        reply.data.domains.extend([])
        self.client.stream_mock.return_value = aiter([reply])

        domains = await atuple(self.client.list("example"))

        self.assertEqual(domains, ())
        request = ListDomainsRequest(zone="example")
        self.assertEqual(
            self.client.stream_mock.mock_calls,
            [call(request, method="/Fred.Registry.Api.Domain.Domain/list_domains", timeout=None)],
        )

    async def test_list(self):
        reply = ListDomainsReply()
        domain_ref = reply.data.domains.add()
        domain_ref.id.uuid.value = "42"
        domain_ref.fqdn.value = "example.org"
        self.client.stream_mock.return_value = aiter([reply])

        domains = await atuple(self.client.list("example"))

        self.assertEqual(domains, (DomainRef(id=DomainId("42"), fqdn="example.org"),))
        request = ListDomainsRequest(zone="example")
        self.assertEqual(
            self.client.stream_mock.mock_calls,
            [call(request, method="/Fred.Registry.Api.Domain.Domain/list_domains", timeout=None)],
        )

    async def test_list_flatten(self):
        # Test responses are properly flattened.
        reply_1 = ListDomainsReply()
        domain_ref_1 = reply_1.data.domains.add()
        domain_ref_1.id.uuid.value = "42"
        domain_ref_1.fqdn.value = "1.example.org"
        domain_ref_2 = reply_1.data.domains.add()
        domain_ref_2.id.uuid.value = "43"
        domain_ref_2.fqdn.value = "2.example.org"
        reply_2 = ListDomainsReply()
        domain_ref_3 = reply_2.data.domains.add()
        domain_ref_3.id.uuid.value = "44"
        domain_ref_3.fqdn.value = "3.example.org"
        domain_ref_4 = reply_2.data.domains.add()
        domain_ref_4.id.uuid.value = "45"
        domain_ref_4.fqdn.value = "4.example.org"
        self.client.stream_mock.return_value = aiter([reply_1, reply_2])

        domains = await atuple(self.client.list("example"))

        result = (
            DomainRef(id=DomainId("42"), fqdn="1.example.org"),
            DomainRef(id=DomainId("43"), fqdn="2.example.org"),
            DomainRef(id=DomainId("44"), fqdn="3.example.org"),
            DomainRef(id=DomainId("45"), fqdn="4.example.org"),
        )
        self.assertEqual(domains, result)
        request = ListDomainsRequest(zone="example")
        self.assertEqual(
            self.client.stream_mock.mock_calls,
            [call(request, method="/Fred.Registry.Api.Domain.Domain/list_domains", timeout=None)],
        )

    async def _test_list_by_contact(self, result: tuple[DomainByRelation, ...]) -> None:
        self.assertEqual(await atuple(self.client.list_by_contact(ContactId("KRYTEN"))), result)

        request = ListDomainsByContactRequest()
        request.contact_id.uuid.value = "KRYTEN"
        self.assertEqual(
            self.client.stream_mock.mock_calls,
            [call(request, method="/Fred.Registry.Api.Domain.Domain/list_domains_by_contact", timeout=None)],
        )

    async def test_list_by_contact_empty_stream(self):
        self.client.stream_mock.return_value = aiter(())

        await self._test_list_by_contact(())

    async def test_list_by_contact_empty_response(self):
        reply = ListDomainsByContactReply()
        reply.data.domains.extend([])
        self.client.stream_mock.return_value = aiter([reply])

        await self._test_list_by_contact(())

    async def test_list_by_contact(self):
        reply = ListDomainsByContactReply()
        domain_ref = reply.data.domains.add()
        domain_ref.domain.id.uuid.value = "42"
        domain_ref.domain.fqdn.value = "example.org"
        domain_ref.domain_history_id.uuid.value = "Nova 5"
        domain_ref.is_deleted = True
        domain_ref.roles.extend((ProtoDomainContactRole.role_registrant, ProtoDomainContactRole.role_administrative))
        self.client.stream_mock.return_value = aiter([reply])

        domain = DomainByRelation(
            domain=DomainRef(id=DomainId("42"), fqdn="example.org"),
            domain_history_id=DomainHistoryId("Nova 5"),
            is_deleted=True,
            roles=frozenset((DomainContactRole.HOLDER, DomainContactRole.ADMIN)),
        )
        await self._test_list_by_contact((domain,))

    async def test_list_by_contact_snapshot_id(self):
        # Test snapshot_id is passed to the request correctly.
        self.client.stream_mock.return_value = aiter(())

        await atuple(self.client.list_by_contact(ContactId("KRYTEN"), snapshot_id=SnapshotId("LAST-THURSDAY")))

        request = ListDomainsByContactRequest()
        request.contact_id.uuid.value = "KRYTEN"
        request.contact_snapshot_id.value = "LAST-THURSDAY"
        self.assertEqual(
            self.client.stream_mock.mock_calls,
            [call(request, method="/Fred.Registry.Api.Domain.Domain/list_domains_by_contact", timeout=None)],
        )

    async def test_list_by_contact_aggregate(self):
        # Test aggregate_entire_history is passed to the request correctly.
        self.client.stream_mock.return_value = aiter(())

        await atuple(self.client.list_by_contact(ContactId("KRYTEN"), aggregate_entire_history=True))

        request = ListDomainsByContactRequest()
        request.contact_id.uuid.value = "KRYTEN"
        request.aggregate_entire_history = True
        self.assertEqual(
            self.client.stream_mock.mock_calls,
            [call(request, method="/Fred.Registry.Api.Domain.Domain/list_domains_by_contact", timeout=None)],
        )

    async def test_list_by_contact_fqdn_filter(self):
        # Test fqdn_filter is passed to the request correctly.
        self.client.stream_mock.return_value = aiter(())

        await atuple(self.client.list_by_contact(ContactId("KRYTEN"), fqdn_filter="example"))

        request = ListDomainsByContactRequest()
        request.contact_id.uuid.value = "KRYTEN"
        request.fqdn_filter = "example"
        self.assertEqual(
            self.client.stream_mock.mock_calls,
            [call(request, method="/Fred.Registry.Api.Domain.Domain/list_domains_by_contact", timeout=None)],
        )

    async def _test_list_by_contact_roles(self, roles: set[DomainContactRole], request_roles: set[int]) -> None:
        # Test roles are passed to the request correctly.
        self.client.stream_mock.return_value = aiter(())

        await atuple(self.client.list_by_contact(ContactId("KRYTEN"), roles=roles))

        request = ListDomainsByContactRequest()
        request.contact_id.uuid.value = "KRYTEN"
        request.role_filters.extend(request_roles)
        self.assertEqual(
            self.client.stream_mock.mock_calls,
            [call(request, method="/Fred.Registry.Api.Domain.Domain/list_domains_by_contact", timeout=None)],
        )

    async def test_list_by_contact_roles_holder(self):
        await self._test_list_by_contact_roles({DomainContactRole.HOLDER}, {ProtoDomainContactRole.role_registrant})

    async def test_list_by_contact_roles_admin(self):
        await self._test_list_by_contact_roles({DomainContactRole.ADMIN}, {ProtoDomainContactRole.role_administrative})

    async def test_list_by_contact_roles_keyset_tech(self):
        await self._test_list_by_contact_roles(
            {DomainContactRole.KEYSET_TECH}, {ProtoDomainContactRole.role_keyset_technical}
        )

    async def test_list_by_contact_roles_nsset_tech(self):
        await self._test_list_by_contact_roles(
            {DomainContactRole.NSSET_TECH}, {ProtoDomainContactRole.role_nsset_technical}
        )

    async def test_list_by_contact_roles_multiple(self):
        await self._test_list_by_contact_roles(
            {DomainContactRole.HOLDER, DomainContactRole.ADMIN},
            {ProtoDomainContactRole.role_registrant, ProtoDomainContactRole.role_administrative},
        )

    async def test_list_by_contact_order_by(self):
        # Test order_by is passed to the request correctly.
        self.client.stream_mock.return_value = aiter(())

        await atuple(self.client.list_by_contact(ContactId("KRYTEN"), order_by=("-crdate", "fqdn")))

        request = ListDomainsByContactRequest()
        request.contact_id.uuid.value = "KRYTEN"
        request.order_by.add(
            field=ListDomainsByContactRequest.OrderByField.order_by_field_crdate,
            direction=OrderByDirection.order_by_direction_descending,
        )
        request.order_by.add(field=ListDomainsByContactRequest.OrderByField.order_by_field_fqdn)
        self.assertEqual(
            self.client.stream_mock.mock_calls,
            [call(request, method="/Fred.Registry.Api.Domain.Domain/list_domains_by_contact", timeout=None)],
        )

    async def test_list_by_contact_flatten(self):
        # Test responses are properly flattened.
        reply_1 = ListDomainsByContactReply()
        domain_ref_1 = reply_1.data.domains.add()
        domain_ref_1.domain.id.uuid.value = "42"
        domain_ref_1.domain.fqdn.value = "1.example.org"
        domain_ref_2 = reply_1.data.domains.add()
        domain_ref_2.domain.id.uuid.value = "43"
        domain_ref_2.domain.fqdn.value = "2.example.org"
        reply_2 = ListDomainsByContactReply()
        domain_ref_3 = reply_2.data.domains.add()
        domain_ref_3.domain.id.uuid.value = "44"
        domain_ref_3.domain.fqdn.value = "3.example.org"
        domain_ref_4 = reply_2.data.domains.add()
        domain_ref_4.domain.id.uuid.value = "45"
        domain_ref_4.domain.fqdn.value = "4.example.org"
        self.client.stream_mock.return_value = aiter([reply_1, reply_2])

        domains = (
            DomainByRelation(domain=DomainRef(id=DomainId("42"), fqdn="1.example.org")),
            DomainByRelation(domain=DomainRef(id=DomainId("43"), fqdn="2.example.org")),
            DomainByRelation(domain=DomainRef(id=DomainId("44"), fqdn="3.example.org")),
            DomainByRelation(domain=DomainRef(id=DomainId("45"), fqdn="4.example.org")),
        )
        await self._test_list_by_contact(domains)

    async def _test_list_by_contact_page(self, page: ListPage[DomainByRelation]) -> None:
        result = await self.client.list_by_contact_page(ContactId("KRYTEN"))

        self.assertEqual(result, page)

        request = ListDomainsByContactRequest()
        request.contact_id.uuid.value = "KRYTEN"
        self.assertEqual(
            self.client.stream_mock.mock_calls,
            [call(request, method="/Fred.Registry.Api.Domain.Domain/list_domains_by_contact", timeout=None)],
        )

    async def test_list_by_contact_page_empty_stream(self):
        self.client.stream_mock.return_value = aiter(())

        await self._test_list_by_contact_page(ListPage(results=()))

    async def test_list_by_contact_page_empty_response(self):
        reply = ListDomainsByContactReply()
        reply.data.domains.extend([])
        self.client.stream_mock.return_value = aiter([reply])

        await self._test_list_by_contact_page(ListPage(results=()))

    async def test_list_by_contact_page_pagination_response(self):
        reply = ListDomainsByContactReply()
        reply.data.pagination.next_page_token = "quagaars"
        reply.data.pagination.items_left = 42
        self.client.stream_mock.return_value = aiter([reply])

        pagination = PaginationReply(next_page_token="quagaars", items_left=42)
        await self._test_list_by_contact_page(ListPage(results=(), pagination=pagination))

    async def test_list_by_contact_page(self):
        reply = ListDomainsByContactReply()
        domain_ref = reply.data.domains.add()
        domain_ref.domain.id.uuid.value = "42"
        domain_ref.domain.fqdn.value = "example.org"
        domain_ref.domain_history_id.uuid.value = "Nova 5"
        domain_ref.is_deleted = True
        domain_ref.roles.extend((ProtoDomainContactRole.role_registrant, ProtoDomainContactRole.role_administrative))
        self.client.stream_mock.return_value = aiter([reply])

        domain = DomainByRelation(
            domain=DomainRef(id=DomainId("42"), fqdn="example.org"),
            domain_history_id=DomainHistoryId("Nova 5"),
            is_deleted=True,
            roles=frozenset((DomainContactRole.HOLDER, DomainContactRole.ADMIN)),
        )
        await self._test_list_by_contact_page(ListPage(results=(domain,)))

    async def test_list_by_contact_page_pagination_request(self):
        # Test pagination args are passed to the request correctly.
        self.client.stream_mock.return_value = aiter(())

        await self.client.list_by_contact_page(ContactId("KRYTEN"), page_token="quagaars", page_size=42)

        request = ListDomainsByContactRequest()
        request.contact_id.uuid.value = "KRYTEN"
        request.pagination.page_token = "quagaars"
        request.pagination.page_size = 42
        self.assertEqual(
            self.client.stream_mock.mock_calls,
            [call(request, method="/Fred.Registry.Api.Domain.Domain/list_domains_by_contact", timeout=None)],
        )

    async def test_list_by_contact_page_snapshot_id(self):
        # Test snapshot_id is passed to the request correctly.
        self.client.stream_mock.return_value = aiter(())

        await self.client.list_by_contact_page(ContactId("KRYTEN"), snapshot_id=SnapshotId("LAST-THURSDAY"))

        request = ListDomainsByContactRequest()
        request.contact_id.uuid.value = "KRYTEN"
        request.contact_snapshot_id.value = "LAST-THURSDAY"
        self.assertEqual(
            self.client.stream_mock.mock_calls,
            [call(request, method="/Fred.Registry.Api.Domain.Domain/list_domains_by_contact", timeout=None)],
        )

    async def test_list_by_contact_page_aggregate(self):
        # Test aggregate_entire_history is passed to the request correctly.
        self.client.stream_mock.return_value = aiter(())

        await self.client.list_by_contact_page(ContactId("KRYTEN"), aggregate_entire_history=True)

        request = ListDomainsByContactRequest()
        request.contact_id.uuid.value = "KRYTEN"
        request.aggregate_entire_history = True
        self.assertEqual(
            self.client.stream_mock.mock_calls,
            [call(request, method="/Fred.Registry.Api.Domain.Domain/list_domains_by_contact", timeout=None)],
        )

    async def test_list_by_contact_page_fqdn_filter(self):
        # Test fqdn_filter is passed to the request correctly.
        self.client.stream_mock.return_value = aiter(())

        await self.client.list_by_contact_page(ContactId("KRYTEN"), fqdn_filter="example")

        request = ListDomainsByContactRequest()
        request.contact_id.uuid.value = "KRYTEN"
        request.fqdn_filter = "example"
        self.assertEqual(
            self.client.stream_mock.mock_calls,
            [call(request, method="/Fred.Registry.Api.Domain.Domain/list_domains_by_contact", timeout=None)],
        )

    async def _test_list_by_contact_page_roles(self, roles: set[DomainContactRole], request_roles: set[int]) -> None:
        # Test roles are passed to the request correctly.
        self.client.stream_mock.return_value = aiter(())

        await self.client.list_by_contact_page(ContactId("KRYTEN"), roles=roles)

        request = ListDomainsByContactRequest()
        request.contact_id.uuid.value = "KRYTEN"
        request.role_filters.extend(request_roles)
        self.assertEqual(
            self.client.stream_mock.mock_calls,
            [call(request, method="/Fred.Registry.Api.Domain.Domain/list_domains_by_contact", timeout=None)],
        )

    async def test_list_by_contact_page_roles_holder(self):
        await self._test_list_by_contact_page_roles(
            {DomainContactRole.HOLDER}, {ProtoDomainContactRole.role_registrant}
        )

    async def test_list_by_contact_page_roles_admin(self):
        await self._test_list_by_contact_page_roles(
            {DomainContactRole.ADMIN}, {ProtoDomainContactRole.role_administrative}
        )

    async def test_list_by_contact_page_roles_keyset_tech(self):
        await self._test_list_by_contact_page_roles(
            {DomainContactRole.KEYSET_TECH}, {ProtoDomainContactRole.role_keyset_technical}
        )

    async def test_list_by_contact_page_roles_nsset_tech(self):
        await self._test_list_by_contact_page_roles(
            {DomainContactRole.NSSET_TECH}, {ProtoDomainContactRole.role_nsset_technical}
        )

    async def test_list_by_contact_page_roles_multiple(self):
        await self._test_list_by_contact_page_roles(
            {DomainContactRole.HOLDER, DomainContactRole.ADMIN},
            {ProtoDomainContactRole.role_registrant, ProtoDomainContactRole.role_administrative},
        )

    async def test_list_by_contact_page_order_by(self):
        # Test order_by is passed to the request correctly.
        self.client.stream_mock.return_value = aiter(())

        await self.client.list_by_contact_page(ContactId("KRYTEN"), order_by=("-crdate", "fqdn"))

        request = ListDomainsByContactRequest()
        request.contact_id.uuid.value = "KRYTEN"
        request.order_by.add(
            field=ListDomainsByContactRequest.OrderByField.order_by_field_crdate,
            direction=OrderByDirection.order_by_direction_descending,
        )
        request.order_by.add(field=ListDomainsByContactRequest.OrderByField.order_by_field_fqdn)
        self.assertEqual(
            self.client.stream_mock.mock_calls,
            [call(request, method="/Fred.Registry.Api.Domain.Domain/list_domains_by_contact", timeout=None)],
        )

    async def test_list_by_contact_page_flatten(self):
        # Test responses are properly flattened.
        reply_1 = ListDomainsByContactReply()
        domain_ref_1 = reply_1.data.domains.add()
        domain_ref_1.domain.id.uuid.value = "42"
        domain_ref_1.domain.fqdn.value = "1.example.org"
        domain_ref_2 = reply_1.data.domains.add()
        domain_ref_2.domain.id.uuid.value = "43"
        domain_ref_2.domain.fqdn.value = "2.example.org"
        reply_2 = ListDomainsByContactReply()
        domain_ref_3 = reply_2.data.domains.add()
        domain_ref_3.domain.id.uuid.value = "44"
        domain_ref_3.domain.fqdn.value = "3.example.org"
        domain_ref_4 = reply_2.data.domains.add()
        domain_ref_4.domain.id.uuid.value = "45"
        domain_ref_4.domain.fqdn.value = "4.example.org"
        self.client.stream_mock.return_value = aiter([reply_1, reply_2])

        domains = (
            DomainByRelation(domain=DomainRef(id=DomainId("42"), fqdn="1.example.org")),
            DomainByRelation(domain=DomainRef(id=DomainId("43"), fqdn="2.example.org")),
            DomainByRelation(domain=DomainRef(id=DomainId("44"), fqdn="3.example.org")),
            DomainByRelation(domain=DomainRef(id=DomainId("45"), fqdn="4.example.org")),
        )
        await self._test_list_by_contact_page(ListPage(results=domains))

    async def _test_list_by_keyset(self, result: tuple[DomainByRelation, ...]) -> None:
        self.assertEqual(await atuple(self.client.list_by_keyset(KeysetId("gazpacho"))), result)

        request = ListDomainsByKeysetRequest()
        request.keyset_id.uuid.value = "gazpacho"
        self.assertEqual(
            self.client.stream_mock.mock_calls,
            [call(request, method="/Fred.Registry.Api.Domain.Domain/list_domains_by_keyset", timeout=None)],
        )

    async def test_list_by_keyset_empty_stream(self):
        self.client.stream_mock.return_value = aiter(())

        await self._test_list_by_keyset(())

    async def test_list_by_keyset_empty_response(self):
        reply = ListDomainsByKeysetReply()
        reply.data.domains.extend([])
        self.client.stream_mock.return_value = aiter([reply])

        await self._test_list_by_keyset(())

    async def test_list_by_keyset(self):
        reply = ListDomainsByKeysetReply()
        domain_ref = reply.data.domains.add()
        domain_ref.domain.id.uuid.value = "42"
        domain_ref.domain.fqdn.value = "example.org"
        domain_ref.domain_history_id.uuid.value = "Nova 5"
        domain_ref.is_deleted = True
        self.client.stream_mock.return_value = aiter([reply])

        domain = DomainByRelation(
            domain=DomainRef(id=DomainId("42"), fqdn="example.org"),
            domain_history_id=DomainHistoryId("Nova 5"),
            is_deleted=True,
        )
        await self._test_list_by_keyset((domain,))

    async def test_list_by_keyset_snapshot_id(self):
        # Test snapshot_id is passed to the request correctly.
        self.client.stream_mock.return_value = aiter(())

        await atuple(self.client.list_by_keyset(KeysetId("KRYTEN"), snapshot_id=SnapshotId("LAST-THURSDAY")))

        request = ListDomainsByKeysetRequest()
        request.keyset_id.uuid.value = "KRYTEN"
        request.keyset_snapshot_id.value = "LAST-THURSDAY"
        self.assertEqual(
            self.client.stream_mock.mock_calls,
            [call(request, method="/Fred.Registry.Api.Domain.Domain/list_domains_by_keyset", timeout=None)],
        )

    async def test_list_by_keyset_aggregate(self):
        # Test aggregate_entire_history is passed to the request correctly.
        self.client.stream_mock.return_value = aiter(())

        await atuple(self.client.list_by_keyset(KeysetId("KRYTEN"), aggregate_entire_history=True))

        request = ListDomainsByKeysetRequest()
        request.keyset_id.uuid.value = "KRYTEN"
        request.aggregate_entire_history = True
        self.assertEqual(
            self.client.stream_mock.mock_calls,
            [call(request, method="/Fred.Registry.Api.Domain.Domain/list_domains_by_keyset", timeout=None)],
        )

    async def test_list_by_keyset_order_by(self):
        # Test order_by is passed to the request correctly.
        self.client.stream_mock.return_value = aiter(())

        await atuple(self.client.list_by_keyset(KeysetId("KRYTEN"), order_by=("-crdate", "fqdn")))

        request = ListDomainsByKeysetRequest()
        request.keyset_id.uuid.value = "KRYTEN"
        request.order_by.add(
            field=ListDomainsByKeysetRequest.OrderByField.order_by_field_crdate,
            direction=OrderByDirection.order_by_direction_descending,
        )
        request.order_by.add(field=ListDomainsByKeysetRequest.OrderByField.order_by_field_fqdn)
        self.assertEqual(
            self.client.stream_mock.mock_calls,
            [call(request, method="/Fred.Registry.Api.Domain.Domain/list_domains_by_keyset", timeout=None)],
        )

    async def test_list_by_keyset_flatten(self):
        # Test responses are properly flattened.
        reply_1 = ListDomainsByKeysetReply()
        domain_ref_1 = reply_1.data.domains.add()
        domain_ref_1.domain.id.uuid.value = "42"
        domain_ref_1.domain.fqdn.value = "1.example.org"
        domain_ref_2 = reply_1.data.domains.add()
        domain_ref_2.domain.id.uuid.value = "43"
        domain_ref_2.domain.fqdn.value = "2.example.org"
        reply_2 = ListDomainsByKeysetReply()
        domain_ref_3 = reply_2.data.domains.add()
        domain_ref_3.domain.id.uuid.value = "44"
        domain_ref_3.domain.fqdn.value = "3.example.org"
        domain_ref_4 = reply_2.data.domains.add()
        domain_ref_4.domain.id.uuid.value = "45"
        domain_ref_4.domain.fqdn.value = "4.example.org"
        self.client.stream_mock.return_value = aiter([reply_1, reply_2])

        domains = (
            DomainByRelation(domain=DomainRef(id=DomainId("42"), fqdn="1.example.org")),
            DomainByRelation(domain=DomainRef(id=DomainId("43"), fqdn="2.example.org")),
            DomainByRelation(domain=DomainRef(id=DomainId("44"), fqdn="3.example.org")),
            DomainByRelation(domain=DomainRef(id=DomainId("45"), fqdn="4.example.org")),
        )
        await self._test_list_by_keyset(domains)

    async def _test_list_by_keyset_page(self, page: ListPage[DomainByRelation]) -> None:
        result = await self.client.list_by_keyset_page(KeysetId("gazpacho"))

        self.assertEqual(result, page)

        request = ListDomainsByKeysetRequest()
        request.keyset_id.uuid.value = "gazpacho"
        self.assertEqual(
            self.client.stream_mock.mock_calls,
            [call(request, method="/Fred.Registry.Api.Domain.Domain/list_domains_by_keyset", timeout=None)],
        )

    async def test_list_by_keyset_page_empty_stream(self):
        self.client.stream_mock.return_value = aiter(())

        await self._test_list_by_keyset_page(ListPage(results=()))

    async def test_list_by_keyset_page_empty_response(self):
        reply = ListDomainsByKeysetReply()
        reply.data.domains.extend([])
        self.client.stream_mock.return_value = aiter([reply])

        await self._test_list_by_keyset_page(ListPage(results=()))

    async def test_list_by_keyset_page_pagination_response(self):
        reply = ListDomainsByKeysetReply()
        reply.data.pagination.next_page_token = "quagaars"
        reply.data.pagination.items_left = 42
        self.client.stream_mock.return_value = aiter([reply])

        pagination = PaginationReply(next_page_token="quagaars", items_left=42)
        await self._test_list_by_keyset_page(ListPage(results=(), pagination=pagination))

    async def test_list_by_keyset_page(self):
        reply = ListDomainsByKeysetReply()
        domain_ref = reply.data.domains.add()
        domain_ref.domain.id.uuid.value = "42"
        domain_ref.domain.fqdn.value = "example.org"
        domain_ref.domain_history_id.uuid.value = "Nova 5"
        domain_ref.is_deleted = True
        self.client.stream_mock.return_value = aiter([reply])

        domain = DomainByRelation(
            domain=DomainRef(id=DomainId("42"), fqdn="example.org"),
            domain_history_id=DomainHistoryId("Nova 5"),
            is_deleted=True,
        )
        await self._test_list_by_keyset_page(ListPage(results=(domain,)))

    async def test_list_by_keyset_page_pagination_request(self):
        # Test pagination args are passed to the request correctly.
        self.client.stream_mock.return_value = aiter(())

        await self.client.list_by_keyset_page(KeysetId("gazpacho"), page_token="quagaars", page_size=42)

        request = ListDomainsByKeysetRequest()
        request.keyset_id.uuid.value = "gazpacho"
        request.pagination.page_token = "quagaars"
        request.pagination.page_size = 42
        self.assertEqual(
            self.client.stream_mock.mock_calls,
            [call(request, method="/Fred.Registry.Api.Domain.Domain/list_domains_by_keyset", timeout=None)],
        )

    async def test_list_by_keyset_page_snapshot_id(self):
        # Test snapshot_id is passed to the request correctly.
        self.client.stream_mock.return_value = aiter(())

        await self.client.list_by_keyset_page(KeysetId("KRYTEN"), snapshot_id=SnapshotId("LAST-THURSDAY"))

        request = ListDomainsByKeysetRequest()
        request.keyset_id.uuid.value = "KRYTEN"
        request.keyset_snapshot_id.value = "LAST-THURSDAY"
        self.assertEqual(
            self.client.stream_mock.mock_calls,
            [call(request, method="/Fred.Registry.Api.Domain.Domain/list_domains_by_keyset", timeout=None)],
        )

    async def test_list_by_keyset_page_aggregate(self):
        # Test aggregate_entire_history is passed to the request correctly.
        self.client.stream_mock.return_value = aiter(())

        await self.client.list_by_keyset_page(KeysetId("KRYTEN"), aggregate_entire_history=True)

        request = ListDomainsByKeysetRequest()
        request.keyset_id.uuid.value = "KRYTEN"
        request.aggregate_entire_history = True
        self.assertEqual(
            self.client.stream_mock.mock_calls,
            [call(request, method="/Fred.Registry.Api.Domain.Domain/list_domains_by_keyset", timeout=None)],
        )

    async def test_list_by_keyset_page_order_by(self):
        # Test order_by is passed to the request correctly.
        self.client.stream_mock.return_value = aiter(())

        await self.client.list_by_keyset_page(KeysetId("KRYTEN"), order_by=("-crdate", "fqdn"))

        request = ListDomainsByKeysetRequest()
        request.keyset_id.uuid.value = "KRYTEN"
        request.order_by.add(
            field=ListDomainsByKeysetRequest.OrderByField.order_by_field_crdate,
            direction=OrderByDirection.order_by_direction_descending,
        )
        request.order_by.add(field=ListDomainsByKeysetRequest.OrderByField.order_by_field_fqdn)
        self.assertEqual(
            self.client.stream_mock.mock_calls,
            [call(request, method="/Fred.Registry.Api.Domain.Domain/list_domains_by_keyset", timeout=None)],
        )

    async def test_list_by_keyset_page_flatten(self):
        # Test responses are properly flattened.
        reply_1 = ListDomainsByKeysetReply()
        domain_ref_1 = reply_1.data.domains.add()
        domain_ref_1.domain.id.uuid.value = "42"
        domain_ref_1.domain.fqdn.value = "1.example.org"
        domain_ref_2 = reply_1.data.domains.add()
        domain_ref_2.domain.id.uuid.value = "43"
        domain_ref_2.domain.fqdn.value = "2.example.org"
        reply_2 = ListDomainsByKeysetReply()
        domain_ref_3 = reply_2.data.domains.add()
        domain_ref_3.domain.id.uuid.value = "44"
        domain_ref_3.domain.fqdn.value = "3.example.org"
        domain_ref_4 = reply_2.data.domains.add()
        domain_ref_4.domain.id.uuid.value = "45"
        domain_ref_4.domain.fqdn.value = "4.example.org"
        self.client.stream_mock.return_value = aiter([reply_1, reply_2])

        domains = (
            DomainByRelation(domain=DomainRef(id=DomainId("42"), fqdn="1.example.org")),
            DomainByRelation(domain=DomainRef(id=DomainId("43"), fqdn="2.example.org")),
            DomainByRelation(domain=DomainRef(id=DomainId("44"), fqdn="3.example.org")),
            DomainByRelation(domain=DomainRef(id=DomainId("45"), fqdn="4.example.org")),
        )
        await self._test_list_by_keyset_page(ListPage(results=domains))

    async def _test_list_by_nsset(self, result: tuple[DomainByRelation, ...]) -> None:
        self.assertEqual(await atuple(self.client.list_by_nsset(NssetId("gazpacho"), refs_only=False)), result)

        request = ListDomainsByNssetRequest()
        request.nsset_id.uuid.value = "gazpacho"
        self.assertEqual(
            self.client.stream_mock.mock_calls,
            [call(request, method="/Fred.Registry.Api.Domain.Domain/list_domains_by_nsset", timeout=None)],
        )

    async def test_list_by_nsset_empty_stream(self):
        self.client.stream_mock.return_value = aiter(())

        await self._test_list_by_nsset(())

    async def test_list_by_nsset_empty_response(self):
        reply = ListDomainsByNssetReply()
        reply.data.domains.extend([])
        self.client.stream_mock.return_value = aiter([reply])

        await self._test_list_by_nsset(())

    async def test_list_by_nsset(self):
        reply = ListDomainsByNssetReply()
        domain_ref = reply.data.domains.add()
        domain_ref.domain.id.uuid.value = "42"
        domain_ref.domain.fqdn.value = "example.org"
        domain_ref.domain_history_id.uuid.value = "Nova 5"
        domain_ref.is_deleted = True
        self.client.stream_mock.return_value = aiter([reply])

        domain = DomainByRelation(
            domain=DomainRef(id=DomainId("42"), fqdn="example.org"),
            domain_history_id=DomainHistoryId("Nova 5"),
            is_deleted=True,
        )
        await self._test_list_by_nsset((domain,))

    async def test_list_by_nsset_refs_only(self):
        reply = ListDomainsByNssetReply()
        domain_ref = reply.data.domains.add()
        domain_ref.domain.id.uuid.value = "42"
        domain_ref.domain.fqdn.value = "example.org"
        domain_ref.domain_history_id.uuid.value = "Nova 5"
        domain_ref.is_deleted = True
        self.client.stream_mock.return_value = aiter([reply])

        with self.assertWarnsRegex(
            DeprecationWarning,
            "Argument refs_only will be removed and method will act as False in the future release.",
        ):
            results = await atuple(self.client.list_by_nsset(NssetId("gazpacho")))

        domain = DomainRef(id=DomainId("42"), fqdn="example.org")
        self.assertEqual(results, (domain,))
        request = ListDomainsByNssetRequest()
        request.nsset_id.uuid.value = "gazpacho"
        self.assertEqual(
            self.client.stream_mock.mock_calls,
            [call(request, method="/Fred.Registry.Api.Domain.Domain/list_domains_by_nsset", timeout=None)],
        )

    async def test_list_by_nsset_snapshot_id(self):
        # Test snapshot_id is passed to the request correctly.
        self.client.stream_mock.return_value = aiter(())

        await atuple(
            self.client.list_by_nsset(NssetId("KRYTEN"), snapshot_id=SnapshotId("LAST-THURSDAY"), refs_only=False)
        )

        request = ListDomainsByNssetRequest()
        request.nsset_id.uuid.value = "KRYTEN"
        request.nsset_snapshot_id.value = "LAST-THURSDAY"
        self.assertEqual(
            self.client.stream_mock.mock_calls,
            [call(request, method="/Fred.Registry.Api.Domain.Domain/list_domains_by_nsset", timeout=None)],
        )

    async def test_list_by_nsset_aggregate(self):
        # Test aggregate_entire_history is passed to the request correctly.
        self.client.stream_mock.return_value = aiter(())

        await atuple(self.client.list_by_nsset(NssetId("KRYTEN"), aggregate_entire_history=True, refs_only=False))

        request = ListDomainsByNssetRequest()
        request.nsset_id.uuid.value = "KRYTEN"
        request.aggregate_entire_history = True
        self.assertEqual(
            self.client.stream_mock.mock_calls,
            [call(request, method="/Fred.Registry.Api.Domain.Domain/list_domains_by_nsset", timeout=None)],
        )

    async def test_list_by_nsset_order_by(self):
        # Test order_by is passed to the request correctly.
        self.client.stream_mock.return_value = aiter(())

        await atuple(self.client.list_by_nsset(NssetId("KRYTEN"), order_by=("-crdate", "fqdn"), refs_only=False))

        request = ListDomainsByNssetRequest()
        request.nsset_id.uuid.value = "KRYTEN"
        request.order_by.add(
            field=ListDomainsByNssetRequest.OrderByField.order_by_field_crdate,
            direction=OrderByDirection.order_by_direction_descending,
        )
        request.order_by.add(field=ListDomainsByNssetRequest.OrderByField.order_by_field_fqdn)
        self.assertEqual(
            self.client.stream_mock.mock_calls,
            [call(request, method="/Fred.Registry.Api.Domain.Domain/list_domains_by_nsset", timeout=None)],
        )

    async def test_list_by_nsset_flatten(self):
        # Test responses are properly flattened.
        reply_1 = ListDomainsByNssetReply()
        domain_ref_1 = reply_1.data.domains.add()
        domain_ref_1.domain.id.uuid.value = "42"
        domain_ref_1.domain.fqdn.value = "1.example.org"
        domain_ref_2 = reply_1.data.domains.add()
        domain_ref_2.domain.id.uuid.value = "43"
        domain_ref_2.domain.fqdn.value = "2.example.org"
        reply_2 = ListDomainsByNssetReply()
        domain_ref_3 = reply_2.data.domains.add()
        domain_ref_3.domain.id.uuid.value = "44"
        domain_ref_3.domain.fqdn.value = "3.example.org"
        domain_ref_4 = reply_2.data.domains.add()
        domain_ref_4.domain.id.uuid.value = "45"
        domain_ref_4.domain.fqdn.value = "4.example.org"
        self.client.stream_mock.return_value = aiter([reply_1, reply_2])

        domains = (
            DomainByRelation(domain=DomainRef(id=DomainId("42"), fqdn="1.example.org")),
            DomainByRelation(domain=DomainRef(id=DomainId("43"), fqdn="2.example.org")),
            DomainByRelation(domain=DomainRef(id=DomainId("44"), fqdn="3.example.org")),
            DomainByRelation(domain=DomainRef(id=DomainId("45"), fqdn="4.example.org")),
        )
        await self._test_list_by_nsset(domains)

    async def _test_list_by_nsset_page(self, page: ListPage[DomainByRelation]) -> None:
        result = await self.client.list_by_nsset_page(NssetId("gazpacho"))

        self.assertEqual(result, page)

        request = ListDomainsByNssetRequest()
        request.nsset_id.uuid.value = "gazpacho"
        self.assertEqual(
            self.client.stream_mock.mock_calls,
            [call(request, method="/Fred.Registry.Api.Domain.Domain/list_domains_by_nsset", timeout=None)],
        )

    async def test_list_by_nsset_page_empty_stream(self):
        self.client.stream_mock.return_value = aiter(())

        await self._test_list_by_nsset_page(ListPage(results=()))

    async def test_list_by_nsset_page_empty_response(self):
        reply = ListDomainsByNssetReply()
        reply.data.domains.extend([])
        self.client.stream_mock.return_value = aiter([reply])

        await self._test_list_by_nsset_page(ListPage(results=()))

    async def test_list_by_nsset_page_pagination_response(self):
        reply = ListDomainsByNssetReply()
        reply.data.pagination.next_page_token = "quagaars"
        reply.data.pagination.items_left = 42
        self.client.stream_mock.return_value = aiter([reply])

        pagination = PaginationReply(next_page_token="quagaars", items_left=42)
        await self._test_list_by_nsset_page(ListPage(results=(), pagination=pagination))

    async def test_list_by_nsset_page(self):
        reply = ListDomainsByNssetReply()
        domain_ref = reply.data.domains.add()
        domain_ref.domain.id.uuid.value = "42"
        domain_ref.domain.fqdn.value = "example.org"
        domain_ref.domain_history_id.uuid.value = "Nova 5"
        domain_ref.is_deleted = True
        self.client.stream_mock.return_value = aiter([reply])

        domain = DomainByRelation(
            domain=DomainRef(id=DomainId("42"), fqdn="example.org"),
            domain_history_id=DomainHistoryId("Nova 5"),
            is_deleted=True,
        )
        await self._test_list_by_nsset_page(ListPage(results=(domain,)))

    async def test_list_by_nsset_page_pagination_request(self):
        # Test pagination args are passed to the request correctly.
        self.client.stream_mock.return_value = aiter(())

        await self.client.list_by_nsset_page(NssetId("gazpacho"), page_token="quagaars", page_size=42)

        request = ListDomainsByNssetRequest()
        request.nsset_id.uuid.value = "gazpacho"
        request.pagination.page_token = "quagaars"
        request.pagination.page_size = 42
        self.assertEqual(
            self.client.stream_mock.mock_calls,
            [call(request, method="/Fred.Registry.Api.Domain.Domain/list_domains_by_nsset", timeout=None)],
        )

    async def test_list_by_nsset_page_snapshot_id(self):
        # Test snapshot_id is passed to the request correctly.
        self.client.stream_mock.return_value = aiter(())

        await self.client.list_by_nsset_page(NssetId("KRYTEN"), snapshot_id=SnapshotId("LAST-THURSDAY"))

        request = ListDomainsByNssetRequest()
        request.nsset_id.uuid.value = "KRYTEN"
        request.nsset_snapshot_id.value = "LAST-THURSDAY"
        self.assertEqual(
            self.client.stream_mock.mock_calls,
            [call(request, method="/Fred.Registry.Api.Domain.Domain/list_domains_by_nsset", timeout=None)],
        )

    async def test_list_by_nsset_page_aggregate(self):
        # Test aggregate_entire_history is passed to the request correctly.
        self.client.stream_mock.return_value = aiter(())

        await self.client.list_by_nsset_page(NssetId("KRYTEN"), aggregate_entire_history=True)

        request = ListDomainsByNssetRequest()
        request.nsset_id.uuid.value = "KRYTEN"
        request.aggregate_entire_history = True
        self.assertEqual(
            self.client.stream_mock.mock_calls,
            [call(request, method="/Fred.Registry.Api.Domain.Domain/list_domains_by_nsset", timeout=None)],
        )

    async def test_list_by_nsset_page_order_by(self):
        # Test order_by is passed to the request correctly.
        self.client.stream_mock.return_value = aiter(())

        await self.client.list_by_nsset_page(NssetId("KRYTEN"), order_by=("-crdate", "fqdn"))

        request = ListDomainsByNssetRequest()
        request.nsset_id.uuid.value = "KRYTEN"
        request.order_by.add(
            field=ListDomainsByNssetRequest.OrderByField.order_by_field_crdate,
            direction=OrderByDirection.order_by_direction_descending,
        )
        request.order_by.add(field=ListDomainsByNssetRequest.OrderByField.order_by_field_fqdn)
        self.assertEqual(
            self.client.stream_mock.mock_calls,
            [call(request, method="/Fred.Registry.Api.Domain.Domain/list_domains_by_nsset", timeout=None)],
        )

    async def test_list_by_nsset_page_flatten(self):
        # Test responses are properly flattened.
        reply_1 = ListDomainsByNssetReply()
        domain_ref_1 = reply_1.data.domains.add()
        domain_ref_1.domain.id.uuid.value = "42"
        domain_ref_1.domain.fqdn.value = "1.example.org"
        domain_ref_2 = reply_1.data.domains.add()
        domain_ref_2.domain.id.uuid.value = "43"
        domain_ref_2.domain.fqdn.value = "2.example.org"
        reply_2 = ListDomainsByNssetReply()
        domain_ref_3 = reply_2.data.domains.add()
        domain_ref_3.domain.id.uuid.value = "44"
        domain_ref_3.domain.fqdn.value = "3.example.org"
        domain_ref_4 = reply_2.data.domains.add()
        domain_ref_4.domain.id.uuid.value = "45"
        domain_ref_4.domain.fqdn.value = "4.example.org"
        self.client.stream_mock.return_value = aiter([reply_1, reply_2])

        domains = (
            DomainByRelation(domain=DomainRef(id=DomainId("42"), fqdn="1.example.org")),
            DomainByRelation(domain=DomainRef(id=DomainId("43"), fqdn="2.example.org")),
            DomainByRelation(domain=DomainRef(id=DomainId("44"), fqdn="3.example.org")),
            DomainByRelation(domain=DomainRef(id=DomainId("45"), fqdn="4.example.org")),
        )
        await self._test_list_by_nsset_page(ListPage(results=domains))

    async def test_get_domains_by_contact_empty(self):
        reply = DomainsByContactReply()
        reply.data.domains.extend([])
        self.client.stream_mock.return_value = aiter([reply])

        response = await self.client.get_domains_by_contact(contact_id=ContactId("contact.1"), roles=set())

        self.assertEqual(response, {"domains": []})
        request = DomainsByContactRequest()
        request.contact_id.uuid.value = "contact.1"
        self.assertEqual(
            self.client.stream_mock.mock_calls,
            [
                call(request, method="/Fred.Registry.Api.Domain.Domain/get_domains_by_contact", timeout=None),
            ],
        )

    async def test_get_domains_by_contact_empty_with_pagination(self):
        reply = DomainsByContactReply()
        reply.data.domains.extend([])
        reply.data.pagination.items_left = 0
        self.client.stream_mock.return_value = aiter([reply])

        response = await self.client.get_domains_by_contact(
            contact_id=ContactId("contact.1"),
            roles=set(),
            page_size=10,
        )

        self.assertEqual(response, {"domains": [], "pagination": {"next_page_token": "", "items_left": 0}})
        request = DomainsByContactRequest()
        request.contact_id.uuid.value = "contact.1"
        request.pagination.page_size = 10
        self.assertEqual(
            self.client.stream_mock.mock_calls,
            [
                call(request, method="/Fred.Registry.Api.Domain.Domain/get_domains_by_contact", timeout=None),
            ],
        )

    async def test_get_domains_by_contact_full(self):
        reply = DomainsByContactReply()
        domain = reply.data.domains.add()
        domain.domain.id.uuid.value = "domain.1"
        domain.domain.fqdn.value = "domain.example.com"
        domain.domain_history_id.uuid.value = "domain.history.1"
        domain.is_deleted = True
        domain.is_holder = True
        domain.is_admin_contact = True
        reply.data.pagination.items_left = 1
        reply.data.pagination.next_page_token = "token.2"
        self.client.stream_mock.return_value = aiter([reply])

        response = await self.client.get_domains_by_contact(
            contact_id=ContactId("contact.1"),
            roles=set(DomainContactRole),
            include_deleted=True,
            order_by="-fqdn",
            fqdn_filter="*dom*",
            page_token="token.1",
            page_size=1,
        )

        self.assertEqual(
            response,
            {
                "domains": [
                    {
                        "domain": DomainRef(id=DomainId("domain.1"), fqdn="domain.example.com"),
                        "domain_history_id": DomainHistoryId("domain.history.1"),
                        "roles": {DomainContactRole.HOLDER, DomainContactRole.ADMIN},
                        "is_deleted": True,
                    }
                ],
                "pagination": {"next_page_token": "token.2", "items_left": 1},
            },
        )
        request = DomainsByContactRequest()
        request.contact_id.uuid.value = "contact.1"
        request.include_deleted_domains = True
        request.order_by = "-fqdn"
        request.fqdn_filter = "*dom*"
        request.include_holder = True
        request.include_admin_contact = True
        request.include_keyset_tech_contact = True
        request.include_nsset_tech_contact = True
        request.pagination.page_token = "token.1"
        request.pagination.page_size = 1
        self.assertEqual(
            self.client.stream_mock.mock_calls,
            [
                call(request, method="/Fred.Registry.Api.Domain.Domain/get_domains_by_contact", timeout=None),
            ],
        )

    async def test_get_domains_by_contact_chunks(self):
        reply_1 = DomainsByContactReply()
        domain_1 = reply_1.data.domains.add()
        domain_1.domain.id.uuid.value = "domain.1"
        domain_1.domain.fqdn.value = "domain.example.com"
        domain_1.domain_history_id.uuid.value = "domain.history.1"
        domain_1.is_deleted = True
        domain_1.is_holder = True
        domain_1.is_admin_contact = True
        reply_1.data.pagination.items_left = 1
        reply_1.data.pagination.next_page_token = "token.2"
        reply_2 = DomainsByContactReply()
        domain_2 = reply_2.data.domains.add()
        domain_2.domain.id.uuid.value = "domain.2"
        domain_2.domain.fqdn.value = "domain.example.org"
        domain_2.domain_history_id.uuid.value = "domain.history.2"
        domain_2.is_deleted = False
        domain_2.is_holder = False
        domain_2.is_admin_contact = True
        self.client.stream_mock.return_value = aiter([reply_1, reply_2])

        response = await self.client.get_domains_by_contact(
            contact_id=ContactId("contact.1"),
            roles=set(DomainContactRole),
            include_deleted=True,
            order_by="-fqdn",
            fqdn_filter="*dom*",
            page_token="token.1",
            page_size=1,
        )

        self.assertEqual(
            response,
            {
                "domains": [
                    {
                        "domain": DomainRef(id=DomainId("domain.1"), fqdn="domain.example.com"),
                        "domain_history_id": DomainHistoryId("domain.history.1"),
                        "roles": {DomainContactRole.HOLDER, DomainContactRole.ADMIN},
                        "is_deleted": True,
                    },
                    {
                        "domain": DomainRef(id=DomainId("domain.2"), fqdn="domain.example.org"),
                        "domain_history_id": DomainHistoryId("domain.history.2"),
                        "roles": {DomainContactRole.ADMIN},
                        "is_deleted": False,
                    },
                ],
                "pagination": {"next_page_token": "token.2", "items_left": 1},
            },
        )

    async def test_update_state_empty(self):
        self.client.stream_mock.return_value = aiter(())

        await self.client.update_state(())

        self.assertEqual(
            self.client.stream_mock.mock_calls,
            [call((), method="/Fred.Registry.Api.Domain.Domain/update_domain_state", timeout=None)],
        )

    async def test_update_state(self):
        self.client.stream_mock.return_value = aiter(())

        await self.client.update_state(
            (StateUpdate(id=DomainId("STARBUG-42"), set_flags=("POWER",), unset_flags=("ANCHOR",)),)
        )

        request = UpdateDomainStateRequest()
        update = request.updates.add()
        update.domain_id.uuid.value = "STARBUG-42"
        update.set_flags.append("POWER")
        update.unset_flags.append("ANCHOR")
        self.assertEqual(
            self.client.stream_mock.mock_calls,
            [call((request,), method="/Fred.Registry.Api.Domain.Domain/update_domain_state", timeout=None)],
        )

    async def test_update_state_kwargs(self):
        self.client.stream_mock.return_value = aiter(())

        await self.client.update_state(
            (StateUpdate(id=DomainId("STARBUG-42")),), snapshot_id=SnapshotId("BACKWARDS"), log_entry_id="DEAR-DIARY"
        )

        request = UpdateDomainStateRequest()
        update = request.updates.add()
        update.domain_id.uuid.value = "STARBUG-42"
        request.domain_snapshot_id.value = "BACKWARDS"
        request.log_entry_id.value = "DEAR-DIARY"
        self.assertEqual(
            self.client.stream_mock.mock_calls,
            [call((request,), method="/Fred.Registry.Api.Domain.Domain/update_domain_state", timeout=None)],
        )

    async def test_update_state_batch_size(self):
        self.client.stream_mock.return_value = aiter(())

        await self.client.update_state(
            (StateUpdate(id=DomainId("STARBUG-42")), StateUpdate(id=DomainId("STARBUG-43"))), batch_size=1
        )

        request_1 = UpdateDomainStateRequest()
        update_1 = request_1.updates.add()
        update_1.domain_id.uuid.value = "STARBUG-42"
        request_2 = UpdateDomainStateRequest()
        update_2 = request_2.updates.add()
        update_2.domain_id.uuid.value = "STARBUG-43"
        self.assertEqual(
            self.client.stream_mock.mock_calls,
            [call((request_1, request_2), method="/Fred.Registry.Api.Domain.Domain/update_domain_state", timeout=None)],
        )

    async def _test_get_domain_life_cycle_stage(self, fqdn: str, request_fqdn: str) -> None:
        reply = DomainLifeCycleStageReply()
        reply.data.domain_id.uuid.value = "42"
        self.client.mock.return_value = make_awaitable(reply)

        result = await self.client.get_domain_life_cycle_stage(fqdn)

        self.assertEqual(result, "42")
        request = DomainLifeCycleStageRequest()
        request.fqdn.value = request_fqdn
        self.assertEqual(
            self.client.mock.mock_calls,
            [call(request, method="/Fred.Registry.Api.Domain.Domain/get_domain_life_cycle_stage", timeout=None)],
        )

    async def test_get_domain_life_cycle_stage(self):
        await self._test_get_domain_life_cycle_stage("example.org", "example.org")

    async def test_get_domain_life_cycle_stage_idn(self):
        await self._test_get_domain_life_cycle_stage("háčkyčárky.cz", "xn--hkyrky-ptac70bc.cz")

    async def test_get_domain_life_cycle_stage_delete_candidate(self):
        reply = DomainLifeCycleStageReply()
        reply.data.delete_candidate.CopyFrom(Empty())
        self.client.mock.return_value = make_awaitable(reply)

        result = await self.client.get_domain_life_cycle_stage("example.org")

        self.assertIsNone(result)
        request = DomainLifeCycleStageRequest()
        request.fqdn.value = "example.org"
        self.assertEqual(
            self.client.mock.mock_calls,
            [call(request, method="/Fred.Registry.Api.Domain.Domain/get_domain_life_cycle_stage", timeout=None)],
        )

    async def test_get_domain_life_cycle_stage_not_exist(self):
        reply = DomainLifeCycleStageReply()
        reply.data.not_registered.CopyFrom(Empty())
        self.client.mock.return_value = make_awaitable(reply)

        with self.assertRaises(DomainDoesNotExist):
            await self.client.get_domain_life_cycle_stage("example.org")

        request = DomainLifeCycleStageRequest()
        request.fqdn.value = "example.org"
        self.assertEqual(
            self.client.mock.mock_calls,
            [call(request, method="/Fred.Registry.Api.Domain.Domain/get_domain_life_cycle_stage", timeout=None)],
        )

    async def test_get_domain_life_cycle_in_auction(self):
        reply = DomainLifeCycleStageReply()
        reply.data.in_auction.CopyFrom(Empty())
        self.client.mock.return_value = make_awaitable(reply)

        with self.assertRaises(DomainDoesNotExist):
            await self.client.get_domain_life_cycle_stage("example.org")

        request = DomainLifeCycleStageRequest()
        request.fqdn.value = "example.org"
        self.assertEqual(
            self.client.mock.mock_calls,
            [call(request, method="/Fred.Registry.Api.Domain.Domain/get_domain_life_cycle_stage", timeout=None)],
        )

    async def _test_get_life_cycle_stage(
        self,
        reply: DomainLifeCycleStageReply,
        result: DomainLifeCycleStageResult,
        *,
        fqdn: str = "example.org",
        request_fqdn: str = "example.org",
    ) -> None:
        self.client.mock.return_value = make_awaitable(reply)

        self.assertEqual(await self.client.get_life_cycle_stage(fqdn), result)

        request = DomainLifeCycleStageRequest()
        request.fqdn.value = request_fqdn
        self.assertEqual(
            self.client.mock.mock_calls,
            [call(request, method="/Fred.Registry.Api.Domain.Domain/get_domain_life_cycle_stage", timeout=None)],
        )

    async def test_get_life_cycle_stage(self):
        reply = DomainLifeCycleStageReply()
        reply.data.domain_id.uuid.value = "42"
        result = DomainLifeCycleStageResult(DomainLifeCycleStage.REGISTERED, DomainId("42"))
        await self._test_get_life_cycle_stage(reply, result)

    async def test_get_life_cycle_stage_idn(self):
        reply = DomainLifeCycleStageReply()
        reply.data.domain_id.uuid.value = "42"
        result = DomainLifeCycleStageResult(DomainLifeCycleStage.REGISTERED, DomainId("42"))
        await self._test_get_life_cycle_stage(
            reply, result, fqdn="háčkyčárky.cz", request_fqdn="xn--hkyrky-ptac70bc.cz"
        )

    async def test_get_life_cycle_stage_delete_candidate(self):
        reply = DomainLifeCycleStageReply()
        reply.data.delete_candidate.CopyFrom(Empty())
        result = DomainLifeCycleStageResult(DomainLifeCycleStage.DELETE_CANDIDATE, None)
        await self._test_get_life_cycle_stage(reply, result)

    async def test_get_life_cycle_stage_not_exist(self):
        reply = DomainLifeCycleStageReply()
        reply.data.not_registered.CopyFrom(Empty())
        result = DomainLifeCycleStageResult(DomainLifeCycleStage.NOT_REGISTERED, None)
        await self._test_get_life_cycle_stage(reply, result)

    async def test_get_life_cycle_in_auction(self):
        reply = DomainLifeCycleStageReply()
        reply.data.in_auction.CopyFrom(Empty())
        result = DomainLifeCycleStageResult(DomainLifeCycleStage.IN_AUCTION, None)
        await self._test_get_life_cycle_stage(reply, result)

    async def test_get_life_cycle_blacklisted(self):
        reply = DomainLifeCycleStageReply()
        reply.data.blacklisted.CopyFrom(DomainLifeCycleStageReply.Data.Blacklist())
        result = DomainLifeCycleStageResult(DomainLifeCycleStage.BLACKLISTED, None)
        await self._test_get_life_cycle_stage(reply, result)

    async def test_get_life_cycle_blacklisted_block_ids(self):
        reply = DomainLifeCycleStageReply()
        blacklist = DomainLifeCycleStageReply.Data.Blacklist()
        blacklist.block_ids.add(value="42")
        blacklist.block_ids.add(value="43")
        reply.data.blacklisted.CopyFrom(blacklist)
        result = DomainLifeCycleStageResult(
            DomainLifeCycleStage.BLACKLISTED, None, block_ids=(BlockId("42"), BlockId("43"))
        )
        await self._test_get_life_cycle_stage(reply, result)

    async def test_get_life_cycle_stage_empty(self):
        reply = DomainLifeCycleStageReply()
        result = DomainLifeCycleStageResult(DomainLifeCycleStage.NOT_REGISTERED, None)
        await self._test_get_life_cycle_stage(reply, result)

    async def test_add_auth_info_generate(self):
        reply = AddDomainAuthInfoReply()
        reply.data.auth_info = "Gazpacho!"
        self.client.mock.return_value = make_awaitable(reply)

        auth_info = await self.client.add_auth_info(DomainId("STARBUG-1"))

        self.assertEqual(auth_info, "Gazpacho!")

        request = AddDomainAuthInfoRequest()
        request.domain_id.uuid.value = "STARBUG-1"
        request.generate.CopyFrom(Empty())
        self.assertEqual(
            self.client.mock.mock_calls,
            [call(request, method="/Fred.Registry.Api.Domain.Domain/add_domain_auth_info", timeout=None)],
        )

    async def test_add_auth_info_custom(self):
        reply = AddDomainAuthInfoReply()
        self.client.mock.return_value = make_awaitable(reply)

        auth_info = await self.client.add_auth_info(DomainId("STARBUG-1"), auth_info="Gazpacho!")  # type: ignore[func-returns-value]

        self.assertIsNone(auth_info)

        request = AddDomainAuthInfoRequest()
        request.domain_id.uuid.value = "STARBUG-1"
        request.auth_info = "Gazpacho!"
        self.assertEqual(
            self.client.mock.mock_calls,
            [call(request, method="/Fred.Registry.Api.Domain.Domain/add_domain_auth_info", timeout=None)],
        )

    async def test_add_auth_info_extra_args(self):
        reply = AddDomainAuthInfoReply()
        reply.data.auth_info = "Gazpacho!"
        self.client.mock.return_value = make_awaitable(reply)

        auth_info = await self.client.add_auth_info(
            DomainId("STARBUG-1"), snapshot_id=SnapshotId("NOVA5"), ttl=42, log_entry_id="2X4B", by_registrar="HOLLY"
        )

        self.assertEqual(auth_info, "Gazpacho!")

        request = AddDomainAuthInfoRequest()
        request.domain_id.uuid.value = "STARBUG-1"
        request.domain_snapshot_id.value = "NOVA5"
        request.generate.CopyFrom(Empty())
        request.ttl = 42
        request.log_entry_id.value = "2X4B"
        request.by_registrar.value = "HOLLY"
        self.assertEqual(
            self.client.mock.mock_calls,
            [call(request, method="/Fred.Registry.Api.Domain.Domain/add_domain_auth_info", timeout=None)],
        )


class DomainAdminClientTest(IsolatedAsyncioTestCase):
    """Test DomainAdminClient's methods."""

    def setUp(self):
        self.client = TestDomainAdminClient(netloc=sentinel.netloc)

    async def _test_batch_delete(
        self,
        domains: Sequence[Domain],
        reply: BatchDeleteDomainsReplyMessage,
        results: list[BatchDeleteDomainsReply],
        requests: list[BatchDeleteDomainsRequest],
        **kwargs: Any,
    ) -> None:
        self.client.stream_mock.return_value = aiter([reply])

        undeleted = [i async for i in self.client.batch_delete(domains, **kwargs)]

        self.assertEqual(undeleted, results)
        self.client.stream_mock.assert_called_once_with(
            tuple(requests),
            method="/Fred.Registry.Api.Domain.Admin/batch_delete_domains",
            timeout=None,
        )

    async def test_batch_delete_empty_result(self):
        domains = [
            Domain(
                domain_id=DomainId("holly{}".format(i)),
                fqdn="example.org",
                domain_history_id=DomainHistoryId("hilly{}".format(i)),
            )
            for i in range(3)
        ]
        request = BatchDeleteDomainsRequest()
        for domain in domains:
            message = BatchDeleteDomainsRequest.Domain()
            message.domain_id.uuid.value = domain.domain_id
            message.domain_history_id.uuid.value = domain.domain_history_id
            request.domains.append(message)

        await self._test_batch_delete(
            domains=domains,
            reply=BatchDeleteDomainsReplyMessage(),
            results=[],
            requests=[request],
        )

    async def test_batch_delete(self):
        reply = BatchDeleteDomainsReplyMessage()
        domain = BatchDeleteDomainsReplyMessage.NotDeleted()
        domain.domain_id.uuid.value = "holly"
        domain.reason = NotDeletedReason.DOMAIN_BLOCKED.value
        reply.data.not_deleted_domains.append(domain)
        domains = [
            Domain(
                domain_id=DomainId("holly{}".format(i)),
                fqdn="example.org",
                domain_history_id=DomainHistoryId("hilly{}".format(i)),
            )
            for i in range(3)
        ]
        request = BatchDeleteDomainsRequest()
        for domain in domains:
            message = BatchDeleteDomainsRequest.Domain()
            message.domain_id.uuid.value = domain.domain_id
            message.domain_history_id.uuid.value = domain.domain_history_id
            request.domains.append(message)

        await self._test_batch_delete(
            domains=domains,
            reply=reply,
            results=[BatchDeleteDomainsReply(domain_id=DomainId("holly"), reason=NotDeletedReason.DOMAIN_BLOCKED)],
            requests=[request],
        )

    async def test_batch_delete_batch_size(self):
        domains = [
            Domain(
                domain_id=DomainId("holly{}".format(i)),
                fqdn="example.org",
                domain_history_id=DomainHistoryId("hilly{}".format(i)),
            )
            for i in range(3)
        ]
        requests = []
        for domain in domains:
            request = BatchDeleteDomainsRequest()
            message = BatchDeleteDomainsRequest.Domain()
            message.domain_id.uuid.value = domain.domain_id
            message.domain_history_id.uuid.value = domain.domain_history_id
            request.domains.append(message)
            requests.append(request)

        await self._test_batch_delete(
            domains=domains,
            reply=BatchDeleteDomainsReplyMessage(),
            results=[],
            requests=requests,
            batch_size=1,
        )

    async def test_batch_delete_invalid(self):
        # Test call with invalid Domain object - missing domain_history_id.
        domains = [Domain(domain_id=DomainId("holly"), fqdn="example.org", domain_history_id=None)]

        with self.assertRaisesRegex(ValueError, "is missing a domain_history_id"):
            await alist(self.client.batch_delete(domains))

        self.client.stream_mock.assert_not_called()

    async def _test_batch_delete_domains(
        self,
        domains: Sequence[Domain],
        reply: BatchDeleteDomainsReplyMessage,
        results: list[dict],
        requests: list[BatchDeleteDomainsRequest],
        **kwargs: Any,
    ) -> None:
        self.client.stream_mock.return_value = aiter([reply])

        with self.assertWarnsRegex(
            DeprecationWarning, "Method batch_delete_domains is deprecated in favor of batch_delete."
        ):
            undeleted = [i async for i in self.client.batch_delete_domains(domains, **kwargs)]

        self.assertEqual(undeleted, results)
        self.client.stream_mock.assert_called_once_with(
            tuple(requests),
            method="/Fred.Registry.Api.Domain.Admin/batch_delete_domains",
            timeout=None,
        )

    async def test_batch_delete_domains_empty_result(self):
        domains = [
            Domain(
                domain_id=DomainId("holly{}".format(i)),
                fqdn="example.org",
                domain_history_id=DomainHistoryId("hilly{}".format(i)),
            )
            for i in range(3)
        ]
        request = BatchDeleteDomainsRequest()
        for domain in domains:
            message = BatchDeleteDomainsRequest.Domain()
            message.domain_id.uuid.value = domain.domain_id
            message.domain_history_id.uuid.value = domain.domain_history_id
            request.domains.append(message)

        await self._test_batch_delete_domains(
            domains=domains,
            reply=BatchDeleteDomainsReplyMessage(),
            results=[],
            requests=[request],
        )

    async def test_batch_delete_domains(self):
        reply = BatchDeleteDomainsReplyMessage()
        domain = BatchDeleteDomainsReplyMessage.NotDeleted()
        domain.domain_id.uuid.value = "holly"
        domain.reason = NotDeletedReason.DOMAIN_BLOCKED.value
        reply.data.not_deleted_domains.append(domain)
        domains = [
            Domain(
                domain_id=DomainId("holly{}".format(i)),
                fqdn="example.org",
                domain_history_id=DomainHistoryId("hilly{}".format(i)),
            )
            for i in range(3)
        ]
        request = BatchDeleteDomainsRequest()
        for domain in domains:
            message = BatchDeleteDomainsRequest.Domain()
            message.domain_id.uuid.value = domain.domain_id
            message.domain_history_id.uuid.value = domain.domain_history_id
            request.domains.append(message)

        await self._test_batch_delete_domains(
            domains=domains,
            reply=reply,
            results=[{"domain_id": "holly", "reason": NotDeletedReason.DOMAIN_BLOCKED}],
            requests=[request],
        )

    async def test_batch_delete_domains_batch_size(self):
        domains = [
            Domain(
                domain_id=DomainId("holly{}".format(i)),
                fqdn="example.org",
                domain_history_id=DomainHistoryId("hilly{}".format(i)),
            )
            for i in range(3)
        ]
        requests = []
        for domain in domains:
            request = BatchDeleteDomainsRequest()
            message = BatchDeleteDomainsRequest.Domain()
            message.domain_id.uuid.value = domain.domain_id
            message.domain_history_id.uuid.value = domain.domain_history_id
            request.domains.append(message)
            requests.append(request)

        await self._test_batch_delete_domains(
            domains=domains,
            reply=BatchDeleteDomainsReplyMessage(),
            results=[],
            requests=requests,
            batch_size=1,
        )

    async def test_batch_delete_domains_invalid(self):
        # Test call with invalid Domain object - missing domain_history_id.
        domains = [Domain(domain_id=DomainId("holly"), fqdn="example.org", domain_history_id=None)]
        self.client.stream_mock.return_value = aiter([BatchDeleteDomainsReplyMessage()])

        with self.assertWarnsRegex(
            DeprecationWarning, "Method batch_delete_domains is deprecated in favor of batch_delete."
        ):
            with self.assertRaisesRegex(ValueError, "is missing a domain_history_id"):
                await alist(self.client.batch_delete_domains(domains))

        self.client.stream_mock.assert_not_called()

    async def test_manage_state_flags_empty_domains(self):
        self.client.stream_mock.return_value = aiter([])

        response = self.client.manage_state_flags(domains=[], state_flags={"linked": None})

        self.assertEqual(await alist(response), [])
        self.client.stream_mock.assert_called_once_with(
            (),
            method="/Fred.Registry.Api.Domain.Admin/manage_domain_state_flags",
            timeout=None,
        )

    async def test_manage_state_flags_empty_state_flags(self):
        self.client.stream_mock.return_value = aiter([])
        domains = [
            Domain(domain_id=DomainId("holly:1"), fqdn="holly.example.org"),
        ]

        response = self.client.manage_state_flags(domains=domains, state_flags={})

        self.assertEqual(await alist(response), [])

        request = ManageDomainStateFlagsRequest()
        for domain in domains:
            dom = request.state_flag_requests_to_create.domains.add()
            dom.domain_id.uuid.value = domain.domain_id
        self.client.stream_mock.assert_called_once_with(
            (request,),
            method="/Fred.Registry.Api.Domain.Admin/manage_domain_state_flags",
            timeout=None,
        )

    async def test_manage_state_flags_close(self):
        request_ids = ["request:1", "request:2"]
        self.client.stream_mock.return_value = aiter([ManageDomainStateFlagsReplyMessage()])

        response = self.client.manage_state_flags(close_requests=request_ids)

        self.assertEqual(await alist(response), [])
        request = ManageDomainStateFlagsRequest()
        for rid in request_ids:
            flag = request.state_flag_requests_to_close.add()
            flag.uuid.value = rid
        self.client.stream_mock.assert_called_once_with(
            (request,),
            method="/Fred.Registry.Api.Domain.Admin/manage_domain_state_flags",
            timeout=None,
        )

    async def test_manage_state_flags_set(self):
        valid_from = datetime(2022, 1, 1, tzinfo=pytz.utc)
        valid_to = datetime(2022, 2, 1, tzinfo=pytz.utc)
        domains = [
            Domain(domain_id=DomainId("holly:1"), fqdn="holly.example.org"),
            Domain(
                domain_id=DomainId("hilly:1"), fqdn="hilly.example.org", domain_history_id=DomainHistoryId("hilly:2")
            ),
        ]
        state_flags = {
            "linked": None,
            "serverBlocked": DatetimeRange(start=valid_from, end=valid_to),
        }
        reply = ManageDomainStateFlagsReplyMessage()
        for i, domain in enumerate(domains):
            for j, flag in enumerate(state_flags.keys()):
                info = reply.data.state_flag_requests_created.add()
                info.request_id.uuid.value = f"request:{i * 10 + j}"
                info.domain_id.uuid.value = domain.domain_id
                info.state_flag = flag
        self.client.stream_mock.return_value = aiter([reply])

        response = await alist(self.client.manage_state_flags(domains=domains, state_flags=state_flags))

        self.assertEqual(
            response,
            [
                ManageDomainStateFlagsReply(domain_id=DomainId("holly:1"), request_id="request:0", state_flag="linked"),
                ManageDomainStateFlagsReply(
                    domain_id=DomainId("holly:1"), request_id="request:1", state_flag="serverBlocked"
                ),
                ManageDomainStateFlagsReply(
                    domain_id=DomainId("hilly:1"), request_id="request:10", state_flag="linked"
                ),
                ManageDomainStateFlagsReply(
                    domain_id=DomainId("hilly:1"), request_id="request:11", state_flag="serverBlocked"
                ),
            ],
        )

        request = ManageDomainStateFlagsRequest()
        for domain in domains:
            dom = request.state_flag_requests_to_create.domains.add()
            dom.domain_id.uuid.value = domain.domain_id
            if domain.domain_history_id:
                dom.domain_history_id.uuid.value = domain.domain_history_id
        request.state_flag_requests_to_create.state_flags.get_or_create("linked")
        validity = request.state_flag_requests_to_create.state_flags.get_or_create("serverBlocked")
        validity.valid_from.FromDatetime(valid_from)
        validity.valid_to.FromDatetime(valid_to)

        self.client.stream_mock.assert_called_once_with(
            (request,),
            method="/Fred.Registry.Api.Domain.Admin/manage_domain_state_flags",
            timeout=None,
        )

    async def test_manage_state_flags_mismatch(self):
        domains = [Domain(domain_id=DomainId("holly"), fqdn="example.org")]
        state_flags = {"linked": None}

        with self.assertRaises(ValueError):
            await alist(self.client.manage_state_flags(domains=domains))  # type: ignore[call-overload]
        with self.assertRaises(ValueError):
            await alist(self.client.manage_state_flags(state_flags=state_flags))  # type: ignore[call-overload]

    async def test_manage_domain_state_flags_empty_domains(self):
        self.client.stream_mock.return_value = aiter([])

        response = self.client.manage_domain_state_flags(domains=[], state_flags={"linked": None})

        with self.assertWarnsRegex(
            DeprecationWarning, "Method manage_domain_state_flags is deprecated in favor of manage_state_flags."
        ):
            self.assertEqual(await alist(response), [])
        self.client.stream_mock.assert_called_once_with(
            (),
            method="/Fred.Registry.Api.Domain.Admin/manage_domain_state_flags",
            timeout=None,
        )

    async def test_manage_domain_state_flags_empty_state_flags(self):
        self.client.stream_mock.return_value = aiter([])
        domains = [
            Domain(domain_id=DomainId("holly:1"), fqdn="holly.example.org"),
        ]

        response = self.client.manage_domain_state_flags(domains=domains, state_flags={})

        with self.assertWarnsRegex(
            DeprecationWarning, "Method manage_domain_state_flags is deprecated in favor of manage_state_flags."
        ):
            self.assertEqual(await alist(response), [])

        request = ManageDomainStateFlagsRequest()
        for domain in domains:
            dom = request.state_flag_requests_to_create.domains.add()
            dom.domain_id.uuid.value = domain.domain_id
        self.client.stream_mock.assert_called_once_with(
            (request,),
            method="/Fred.Registry.Api.Domain.Admin/manage_domain_state_flags",
            timeout=None,
        )

    async def test_manage_domain_state_flags_close(self):
        request_ids = ["request:1", "request:2"]
        self.client.stream_mock.return_value = aiter([ManageDomainStateFlagsReplyMessage()])

        response = self.client.manage_domain_state_flags(close_requests=request_ids)

        with self.assertWarnsRegex(
            DeprecationWarning, "Method manage_domain_state_flags is deprecated in favor of manage_state_flags."
        ):
            self.assertEqual(await alist(response), [])
        request = ManageDomainStateFlagsRequest()
        for rid in request_ids:
            flag = request.state_flag_requests_to_close.add()
            flag.uuid.value = rid
        self.client.stream_mock.assert_called_once_with(
            (request,),
            method="/Fred.Registry.Api.Domain.Admin/manage_domain_state_flags",
            timeout=None,
        )

    async def test_manage_domain_state_flags_set(self):
        valid_from = datetime(2022, 1, 1, tzinfo=pytz.utc)
        valid_to = datetime(2022, 2, 1, tzinfo=pytz.utc)
        domains = [
            Domain(domain_id=DomainId("holly:1"), fqdn="holly.example.org"),
            Domain(
                domain_id=DomainId("hilly:1"), fqdn="hilly.example.org", domain_history_id=DomainHistoryId("hilly:2")
            ),
        ]
        state_flags = {
            "linked": None,
            "serverBlocked": DatetimeRange(start=valid_from, end=valid_to),
        }
        reply = ManageDomainStateFlagsReplyMessage()
        for i, domain in enumerate(domains):
            for j, flag in enumerate(state_flags.keys()):
                info = reply.data.state_flag_requests_created.add()
                info.request_id.uuid.value = f"request:{i * 10 + j}"
                info.domain_id.uuid.value = domain.domain_id
                info.state_flag = flag
        self.client.stream_mock.return_value = aiter([reply])

        with self.assertWarnsRegex(
            DeprecationWarning, "Method manage_domain_state_flags is deprecated in favor of manage_state_flags."
        ):
            response = await alist(self.client.manage_domain_state_flags(domains=domains, state_flags=state_flags))

        self.assertEqual(
            response,
            [
                {"domain_id": DomainId("holly:1"), "request_id": "request:0", "state_flag": "linked"},
                {"domain_id": DomainId("holly:1"), "request_id": "request:1", "state_flag": "serverBlocked"},
                {"domain_id": DomainId("hilly:1"), "request_id": "request:10", "state_flag": "linked"},
                {"domain_id": DomainId("hilly:1"), "request_id": "request:11", "state_flag": "serverBlocked"},
            ],
        )
        self.assertIsInstance(response[0]["domain_id"], DomainId)
        self.assertIsInstance(response[1]["domain_id"], DomainId)

        request = ManageDomainStateFlagsRequest()
        for domain in domains:
            dom = request.state_flag_requests_to_create.domains.add()
            dom.domain_id.uuid.value = domain.domain_id
            if domain.domain_history_id:
                dom.domain_history_id.uuid.value = domain.domain_history_id
        request.state_flag_requests_to_create.state_flags.get_or_create("linked")
        validity = request.state_flag_requests_to_create.state_flags.get_or_create("serverBlocked")
        validity.valid_from.FromDatetime(valid_from)
        validity.valid_to.FromDatetime(valid_to)

        self.client.stream_mock.assert_called_once_with(
            (request,),
            method="/Fred.Registry.Api.Domain.Admin/manage_domain_state_flags",
            timeout=None,
        )

    async def test_manage_domain_state_flags_mismatch(self):
        domains = [Domain(domain_id=DomainId("holly"), fqdn="example.org")]
        state_flags = {"linked": None}

        with self.assertWarnsRegex(
            DeprecationWarning, "Method manage_domain_state_flags is deprecated in favor of manage_state_flags."
        ):
            with self.assertRaises(ValueError):
                await alist(self.client.manage_domain_state_flags(domains=domains))  # type: ignore[call-overload]
        with self.assertWarnsRegex(
            DeprecationWarning, "Method manage_domain_state_flags is deprecated in favor of manage_state_flags."
        ):
            with self.assertRaises(ValueError):
                await alist(self.client.manage_domain_state_flags(state_flags=state_flags))  # type: ignore[call-overload]

    async def _test_get_notify_info(self, method: str, start: datetime, end: datetime, page_size: int) -> None:
        reply = GetDomainsNotifyInfoReplyMessage()
        for i in range(page_size):
            domain = reply.data.domains.add()
            domain.domain_id.uuid.value = f"domain.{i}"
            domain.fqdn = f"domain-{i}.cz"
            domain.registrant_id.uuid.value = f"contact.{i}"
            domain.registrant_handle.value = f"CONTACT.{i}"
            domain.registrant_name = f"Mr. {i}"
            domain.registrant_organization = f"{i} corp."
            domain.contacts.add(email=f"contact@domain-{i}.cz")
            domain.additional_contacts.add(email=f"additional@domain-{i}.cz")
            domain.additional_contacts.add(telephone=f"+420.{i}")
        reply.data.pagination.next_page_token = "NEXT"
        reply.data.pagination.items_left = 7

        self.client.stream_mock.return_value = aiter([reply])
        response = await getattr(self.client, method)(
            start=start,
            end=end,
            page_size=page_size,
        )

        notify_infos = [
            NotifyInfoDomain(
                domain_id=DomainId(f"domain.{i}"),
                fqdn=f"domain-{i}.cz",
                registrant_id=ContactId(f"contact.{i}"),
                registrant_handle=f"CONTACT.{i}",
                registrant_name=f"Mr. {i}",
                registrant_organization=f"{i} corp.",
                contacts=[ContactInfo(type=ContactInfoType.EMAIL, value=f"contact@domain-{i}.cz")],
                additional_contacts=[
                    ContactInfo(type=ContactInfoType.EMAIL, value=f"additional@domain-{i}.cz"),
                    ContactInfo(type=ContactInfoType.TELEPHONE, value=f"+420.{i}"),
                ],
            )
            for i in range(page_size)
        ]
        self.assertEqual(
            response,
            GetDomainsNotifyInfoReply(
                domains=notify_infos,
                pagination=PaginationReply(next_page_token="NEXT", items_left=7),
            ),
        )

    async def test_get_outzone_notify_info(self):
        start = datetime(2022, 1, 1, tzinfo=pytz.utc)
        end = datetime(2022, 1, 2, tzinfo=pytz.utc)

        await self._test_get_notify_info("get_outzone_notify_info", start, end, 5)

        request = GetDomainsOutzoneNotifyInfoRequest()
        request.outzone_from.FromDatetime(start)
        request.outzone_to.FromDatetime(end)
        request.pagination.page_size = 5

        self.assertEqual(
            self.client.stream_mock.mock_calls,
            [
                call(request, method="/Fred.Registry.Api.Domain.Admin/get_domains_outzone_notify_info", timeout=None),
            ],
        )

    async def test_get_delete_notify_info(self):
        start = datetime(2022, 1, 1, tzinfo=pytz.utc)
        end = datetime(2022, 1, 2, tzinfo=pytz.utc)

        await self._test_get_notify_info("get_delete_notify_info", start, end, 10)

        request = GetDomainsDeleteNotifyInfoRequest()
        request.delete_from.FromDatetime(start)
        request.delete_to.FromDatetime(end)
        request.pagination.page_size = 10

        self.assertEqual(
            self.client.stream_mock.mock_calls,
            [
                call(request, method="/Fred.Registry.Api.Domain.Admin/get_domains_delete_notify_info", timeout=None),
            ],
        )

    async def _test_get_domains_notify_info(self, method: str, start: datetime, end: datetime, page_size: int) -> None:
        reply = GetDomainsNotifyInfoReplyMessage()
        for i in range(page_size):
            domain = reply.data.domains.add()
            domain.domain_id.uuid.value = f"domain.{i}"
            domain.fqdn = f"domain-{i}.cz"
            domain.registrant_id.uuid.value = f"contact.{i}"
            domain.registrant_handle.value = f"CONTACT.{i}"
            domain.registrant_name = f"Mr. {i}"
            domain.registrant_organization = f"{i} corp."
            domain.contacts.add(email=f"contact@domain-{i}.cz")
            domain.additional_contacts.add(email=f"additional@domain-{i}.cz")
            domain.additional_contacts.add(telephone=f"+420.{i}")
        reply.data.pagination.next_page_token = "NEXT"
        reply.data.pagination.items_left = 7

        self.client.stream_mock.return_value = aiter([reply])
        response = await getattr(self.client, method)(
            start=start,
            end=end,
            page_size=page_size,
        )
        self.assertEqual(
            response,
            {
                "domains": [
                    {
                        "domain_id": f"domain.{i}",
                        "fqdn": f"domain-{i}.cz",
                        "registrant_id": f"contact.{i}",
                        "registrant_handle": f"CONTACT.{i}",
                        "registrant_name": f"Mr. {i}",
                        "registrant_organization": f"{i} corp.",
                        "contacts": [ContactInfo(type=ContactInfoType.EMAIL, value=f"contact@domain-{i}.cz")],
                        "additional_contacts": [
                            ContactInfo(type=ContactInfoType.EMAIL, value=f"additional@domain-{i}.cz"),
                            ContactInfo(type=ContactInfoType.TELEPHONE, value=f"+420.{i}"),
                        ],
                    }
                    for i in range(page_size)
                ],
                "pagination": {"next_page_token": "NEXT", "items_left": 7},
            },
        )

    async def test_get_domains_outzone_notify_info(self):
        start = datetime(2022, 1, 1, tzinfo=pytz.utc)
        end = datetime(2022, 1, 2, tzinfo=pytz.utc)

        with self.assertWarnsRegex(
            DeprecationWarning,
            "Method get_domains_outzone_notify_info is deprecated in favor of get_outzone_notify_info.",
        ):
            await self._test_get_domains_notify_info("get_domains_outzone_notify_info", start, end, 5)

        request = GetDomainsOutzoneNotifyInfoRequest()
        request.outzone_from.FromDatetime(start)
        request.outzone_to.FromDatetime(end)
        request.pagination.page_size = 5

        self.assertEqual(
            self.client.stream_mock.mock_calls,
            [
                call(request, method="/Fred.Registry.Api.Domain.Admin/get_domains_outzone_notify_info", timeout=None),
            ],
        )

    async def test_get_domains_delete_notify_info(self):
        start = datetime(2022, 1, 1, tzinfo=pytz.utc)
        end = datetime(2022, 1, 2, tzinfo=pytz.utc)

        with self.assertWarnsRegex(
            DeprecationWarning,
            "Method get_domains_delete_notify_info is deprecated in favor of get_delete_notify_info.",
        ):
            await self._test_get_domains_notify_info("get_domains_delete_notify_info", start, end, 10)

        request = GetDomainsDeleteNotifyInfoRequest()
        request.delete_from.FromDatetime(start)
        request.delete_to.FromDatetime(end)
        request.pagination.page_size = 10

        self.assertEqual(
            self.client.stream_mock.mock_calls,
            [
                call(request, method="/Fred.Registry.Api.Domain.Admin/get_domains_delete_notify_info", timeout=None),
            ],
        )

    async def test_update_outzone_additional_notify_info(self):
        self.client.mock.return_value = make_awaitable(UpdateDomainsAdditionalNotifyInfoReply())

        start = datetime(2022, 1, 1, tzinfo=pytz.utc)
        end = datetime(2022, 1, 2, tzinfo=pytz.utc)
        domains = {
            DomainId(f"domain.{i}"): [
                ContactInfo(type=ContactInfoType.EMAIL, value="email@example.com"),
                ContactInfo(type=ContactInfoType.TELEPHONE, value="+420.000000"),
            ]
            for i in range(3)
        }

        await self.client.update_outzone_additional_notify_info(
            domains,
            start=start,
            end=end,
            batch_size=2,
        )

        requests = []
        domain_items = tuple(domains.items())
        for request_domains in domain_items[:2], domain_items[2:]:
            request = UpdateDomainsOutzoneAdditionalNotifyInfoRequest()
            for domain_id, _contacts in request_domains:
                domain = request.domains_additional_info.add()
                domain.domain_id.uuid.value = domain_id
                domain.outzone_from.FromDatetime(start)
                domain.outzone_to.FromDatetime(end)
                domain.additional_contacts.add(email="email@example.com")
                domain.additional_contacts.add(telephone="+420.000000")
            requests.append(request)
        self.assertEqual(
            self.client.mock.mock_calls,
            [
                call(
                    tuple(requests),
                    method="/Fred.Registry.Api.Domain.Admin/update_domains_outzone_additional_notify_info",
                    timeout=None,
                ),
            ],
        )

    async def test_update_delete_additional_notify_info(self):
        self.client.mock.return_value = make_awaitable(UpdateDomainsAdditionalNotifyInfoReply())

        start = datetime(2022, 1, 1, tzinfo=pytz.utc)
        end = datetime(2022, 1, 2, tzinfo=pytz.utc)
        domains = {
            DomainId(f"domain.{i}"): [
                ContactInfo(type=ContactInfoType.EMAIL, value="email@example.com"),
                ContactInfo(type=ContactInfoType.TELEPHONE, value="+420.000000"),
            ]
            for i in range(3)
        }

        await self.client.update_delete_additional_notify_info(
            domains,
            start=start,
            end=end,
            batch_size=2,
        )

        requests = []
        domain_items = tuple(domains.items())
        for request_domains in domain_items[:2], domain_items[2:]:
            request = UpdateDomainsDeleteAdditionalNotifyInfoRequest()
            for domain_id, _contacts in request_domains:
                domain = request.domains_additional_info.add()
                domain.domain_id.uuid.value = domain_id
                domain.delete_from.FromDatetime(start)
                domain.delete_to.FromDatetime(end)
                domain.additional_contacts.add(email="email@example.com")
                domain.additional_contacts.add(telephone="+420.000000")
            requests.append(request)
        self.assertEqual(
            self.client.mock.mock_calls,
            [
                call(
                    tuple(requests),
                    method="/Fred.Registry.Api.Domain.Admin/update_domains_delete_additional_notify_info",
                    timeout=None,
                ),
            ],
        )

    async def test_update_domains_outzone_additional_notify_info(self):
        self.client.mock.return_value = make_awaitable(UpdateDomainsAdditionalNotifyInfoReply())

        start = datetime(2022, 1, 1, tzinfo=pytz.utc)
        end = datetime(2022, 1, 2, tzinfo=pytz.utc)
        domains = {
            DomainId(f"domain.{i}"): [
                ContactInfo(type=ContactInfoType.EMAIL, value="email@example.com"),
                ContactInfo(type=ContactInfoType.TELEPHONE, value="+420.000000"),
            ]
            for i in range(3)
        }

        with self.assertWarnsRegex(
            DeprecationWarning,
            "Method update_domains_outzone_additional_notify_info is deprecated in favor of update_outzone_additional_notify_info.",
        ):
            await self.client.update_domains_outzone_additional_notify_info(
                domains,
                start=start,
                end=end,
                batch_size=2,
            )

        requests = []
        domain_items = tuple(domains.items())
        for request_domains in domain_items[:2], domain_items[2:]:
            request = UpdateDomainsOutzoneAdditionalNotifyInfoRequest()
            for domain_id, _contacts in request_domains:
                domain = request.domains_additional_info.add()
                domain.domain_id.uuid.value = domain_id
                domain.outzone_from.FromDatetime(start)
                domain.outzone_to.FromDatetime(end)
                domain.additional_contacts.add(email="email@example.com")
                domain.additional_contacts.add(telephone="+420.000000")
            requests.append(request)
        self.assertEqual(
            self.client.mock.mock_calls,
            [
                call(
                    tuple(requests),
                    method="/Fred.Registry.Api.Domain.Admin/update_domains_outzone_additional_notify_info",
                    timeout=None,
                ),
            ],
        )

    async def test_update_domains_delete_additional_notify_info(self):
        self.client.mock.return_value = make_awaitable(UpdateDomainsAdditionalNotifyInfoReply())

        start = datetime(2022, 1, 1, tzinfo=pytz.utc)
        end = datetime(2022, 1, 2, tzinfo=pytz.utc)
        domains = {
            DomainId(f"domain.{i}"): [
                ContactInfo(type=ContactInfoType.EMAIL, value="email@example.com"),
                ContactInfo(type=ContactInfoType.TELEPHONE, value="+420.000000"),
            ]
            for i in range(3)
        }

        with self.assertWarnsRegex(
            DeprecationWarning,
            "Method update_domains_delete_additional_notify_info is deprecated in favor of update_delete_additional_notify_info.",
        ):
            await self.client.update_domains_delete_additional_notify_info(
                domains,
                start=start,
                end=end,
                batch_size=2,
            )

        requests = []
        domain_items = tuple(domains.items())
        for request_domains in domain_items[:2], domain_items[2:]:
            request = UpdateDomainsDeleteAdditionalNotifyInfoRequest()
            for domain_id, _contacts in request_domains:
                domain = request.domains_additional_info.add()
                domain.domain_id.uuid.value = domain_id
                domain.delete_from.FromDatetime(start)
                domain.delete_to.FromDatetime(end)
                domain.additional_contacts.add(email="email@example.com")
                domain.additional_contacts.add(telephone="+420.000000")
            requests.append(request)
        self.assertEqual(
            self.client.mock.mock_calls,
            [
                call(
                    tuple(requests),
                    method="/Fred.Registry.Api.Domain.Admin/update_domains_delete_additional_notify_info",
                    timeout=None,
                ),
            ],
        )

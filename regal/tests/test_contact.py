from datetime import date
from typing import Any, Optional
from unittest import IsolatedAsyncioTestCase, TestCase
from unittest.mock import call, sentinel

from asyncstdlib import iter as aiter, tuple as atuple
from fred_api.registry.common_types_pb2 import ContactDoesNotExist as ContactDoesNotExistMessage, OrderByDirection
from fred_api.registry.contact.add_contact_auth_info_types_pb2 import AddContactAuthInfoReply, AddContactAuthInfoRequest
from fred_api.registry.contact.contact_common_types_pb2 import (
    Birthdate,
    ContactAdditionalIdentifier as AdditionalIdentifierMessage,
    WarningLetter as WarningLetterMessage,
)
from fred_api.registry.contact.contact_history_types_pb2 import (
    ContactHistoryRecord,
    ContactHistoryReply,
    ContactHistoryRequest,
)
from fred_api.registry.contact.contact_info_types_pb2 import (
    ContactIdReply,
    ContactIdRequest,
    ContactInfoReply,
    ContactInfoRequest,
)
from fred_api.registry.contact.contact_state_flags_types_pb2 import ContactStateFlagsReply
from fred_api.registry.contact.contact_state_history_types_pb2 import (
    ContactStateHistory,
    ContactStateHistoryReply,
    ContactStateHistoryRequest,
)
from fred_api.registry.contact.contact_state_types_pb2 import ContactStateReply, ContactStateRequest
from fred_api.registry.contact.create_contact_types_pb2 import CreateContactReply, CreateContactRequest
from fred_api.registry.contact.list_contacts_types_pb2 import ListContactsReply
from fred_api.registry.contact.list_merge_candidates_types_pb2 import (
    ListMergeCandidatesReply,
    ListMergeCandidatesRequest,
)
from fred_api.registry.contact.merge_contacts_types_pb2 import MergeContactsReply, MergeContactsRequest
from fred_api.registry.contact.update_contact_state_stream_types_pb2 import UpdateContactStateStreamRequest
from fred_api.registry.contact.update_contact_state_types_pb2 import UpdateContactStateReply, UpdateContactStateRequest
from fred_api.registry.contact.update_contact_types_pb2 import UpdateContactReply, UpdateContactRequest
from fred_types import ContactHistoryId, ContactId, ContactRef, LogEntryId, RegistrarId, RegistrarRef, SnapshotId
from frgal.utils import AsyncTestClientMixin
from google.protobuf.empty_pb2 import Empty

from regal.common import Address, ListPage, PaginationReply
from regal.constants import DELETE
from regal.contact import (
    AdditionalIdentifier,
    Contact,
    ContactClient,
    ContactDecoder,
    ContactIdentifierType,
    MergeCandidate,
    WarningLetterPreference,
)
from regal.exceptions import ContactDoesNotExist, NotCurrentVersion
from regal.object import StateUpdate
from regal.tests.common import ObjectClientTestMixin

from .utils import make_awaitable


class AdditionalIdentifierTest(TestCase):
    def test_birthdate(self):
        data = (
            # ident, birthdate
            (AdditionalIdentifier(type=ContactIdentifierType.BIRTHDATE, value="1988-02-15"), date(1988, 2, 15)),
            (AdditionalIdentifier(type=ContactIdentifierType.BIRTHDATE, value=""), None),
            (AdditionalIdentifier(type=ContactIdentifierType.PASSPORT_NUMBER, value="1988-02-15"), None),
        )
        for ident, result in data:
            with self.subTest(ident=ident):
                self.assertEqual(ident.birthdate, result)

    def test_birthdate_invalid(self):
        ident = AdditionalIdentifier(type=ContactIdentifierType.BIRTHDATE, value="JUNK")
        with self.assertRaisesRegex(ValueError, "'JUNK' is not a valid date."):
            ident.birthdate


class ContactDecoderTest(IsolatedAsyncioTestCase):
    """Test ContactDecoder."""

    def setUp(self):
        self.decoder = ContactDecoder()

    def test_additional_identifier(self):
        self.assertEqual(
            self.decoder.decode(AdditionalIdentifierMessage(birthdate=Birthdate(value="2000-01-01"))),
            AdditionalIdentifier(type=ContactIdentifierType.BIRTHDATE, value="2000-01-01"),
        )
        self.assertIsNone(self.decoder.decode(AdditionalIdentifierMessage()))

    def test_decode_warning_letter(self):
        self.assertEqual(self.decoder.decode(WarningLetterMessage(preference=0)), WarningLetterPreference.TO_SEND)
        self.assertEqual(self.decoder.decode(WarningLetterMessage(preference=1)), WarningLetterPreference.NOT_TO_SEND)
        self.assertEqual(self.decoder.decode(WarningLetterMessage(preference=2)), WarningLetterPreference.NOT_SPECIFIED)


class TestContactClient(AsyncTestClientMixin, ContactClient):
    """Test ContactClient."""

    exhaust_iterables = True


class ContactClientTest(ObjectClientTestMixin[TestContactClient], IsolatedAsyncioTestCase):
    """Test ContactClient's methods."""

    grpc_namespace = "/Fred.Registry.Api.Contact.Contact"
    get_state_request = ContactStateRequest
    get_state_reply = ContactStateReply
    get_state_method = "get_contact_state"
    get_history_request = ContactHistoryRequest
    get_history_reply = ContactHistoryReply
    get_history_method = "get_contact_history"
    get_history_record = ContactHistoryRecord
    get_state_history_request = ContactStateHistoryRequest
    get_state_history_reply = ContactStateHistoryReply
    get_state_history_method = "get_contact_state_history"
    get_state_history_record = ContactStateHistory.Record
    get_state_flags_method = "get_contact_state_flags"
    get_state_flags_reply_cls = ContactStateFlagsReply

    def setUp(self):
        self.client = TestContactClient(netloc=sentinel.netloc)

    async def test_get_id(self):
        contact_id = ContactId("kryten")
        reply = ContactIdReply()
        reply.data.contact_id.uuid.value = contact_id
        self.client.mock.return_value = make_awaitable(reply)

        response = await self.client.get_id("HANDLE")
        self.assertEqual(response, contact_id)
        self.assertIsInstance(response, ContactId)

        request = ContactIdRequest()
        request.contact_handle.value = "HANDLE"
        self.client.mock.assert_called_once_with(
            request,
            method="/Fred.Registry.Api.Contact.Contact/get_contact_id",
            timeout=None,
        )

    async def test_get_contact_id(self):
        contact_id = ContactId("kryten")
        reply = ContactIdReply()
        reply.data.contact_id.uuid.value = contact_id
        self.client.mock.return_value = make_awaitable(reply)

        with self.assertWarnsRegex(DeprecationWarning, "Method get_contact_id is deprecated in favor of get_id."):
            response = await self.client.get_contact_id("HANDLE")
        self.assertEqual(response, contact_id)
        self.assertIsInstance(response, ContactId)

        request = ContactIdRequest()
        request.contact_handle.value = "HANDLE"
        self.client.mock.assert_called_once_with(
            request,
            method="/Fred.Registry.Api.Contact.Contact/get_contact_id",
            timeout=None,
        )

    async def _test_get(self, request: ContactInfoRequest, **kwargs: Any) -> None:
        reply = ContactInfoReply()
        reply.data.contact_id.uuid.value = "kryten"
        reply.data.contact_handle.value = "HANDLE"
        reply.data.warning_letter.preference = WarningLetterPreference.NOT_TO_SEND.value
        reply.data.additional_identifier.birthdate.value = "2000-01-01"
        self.client.mock.return_value = make_awaitable(reply)

        response = await self.client.get(ContactId("kryten"), **kwargs)

        self.assertEqual(
            response,
            Contact(
                contact_id=ContactId("kryten"),
                contact_handle="HANDLE",
                additional_identifier=AdditionalIdentifier(type=ContactIdentifierType.BIRTHDATE, value="2000-01-01"),
                warning_letter=WarningLetterPreference.NOT_TO_SEND,
            ),
        )

        request.contact_id.uuid.value = "kryten"
        self.client.mock.assert_called_once_with(
            request,
            method="/Fred.Registry.Api.Contact.Contact/get_contact_info",
            timeout=None,
        )

    async def test_get(self):
        request = ContactInfoRequest()
        await self._test_get(request)

    async def test_get_history_id(self):
        request = ContactInfoRequest()
        request.contact_history_id.uuid.value = "nova-5"
        await self._test_get(request, history_id=ContactHistoryId("nova-5"))

    async def test_get_snapshot_id(self):
        # Test get with a snapshot_id.
        request = ContactInfoRequest()
        request.contact_snapshot_id.value = "LAST-THURSDAY"
        await self._test_get(request, snapshot_id=SnapshotId("LAST-THURSDAY"))

    async def _test_get_contact_info(self, contact_history_id: Optional[ContactHistoryId] = None) -> None:
        reply = ContactInfoReply()
        reply.data.contact_id.uuid.value = "kryten"
        if contact_history_id:
            reply.data.contact_history_id.uuid.value = contact_history_id
        reply.data.contact_handle.value = "HANDLE"
        reply.data.warning_letter.preference = WarningLetterPreference.NOT_TO_SEND.value
        reply.data.additional_identifier.birthdate.value = "2000-01-01"
        self.client.mock.return_value = make_awaitable(reply)

        with self.assertWarnsRegex(DeprecationWarning, "Method get_contact_info is deprecated in favor of get."):
            response = await self.client.get_contact_info(ContactId("kryten"), history_id=contact_history_id)

        self.assertEqual(
            response,
            Contact(
                contact_id=ContactId("kryten"),
                contact_history_id=contact_history_id,
                contact_handle="HANDLE",
                additional_identifier=AdditionalIdentifier(type=ContactIdentifierType.BIRTHDATE, value="2000-01-01"),
                warning_letter=WarningLetterPreference.NOT_TO_SEND,
            ),
        )

        request = ContactInfoRequest()
        request.contact_id.uuid.value = "kryten"
        if contact_history_id:
            request.contact_history_id.uuid.value = contact_history_id
        self.client.mock.assert_called_once_with(
            request,
            method="/Fred.Registry.Api.Contact.Contact/get_contact_info",
            timeout=None,
        )

    async def test_get_contact_info(self):
        await self._test_get_contact_info()

    async def test_get_contact_info_with_history_id(self):
        await self._test_get_contact_info(contact_history_id=ContactHistoryId("nova-5"))

    async def test_list_empty_stream(self):
        self.client.stream_mock.return_value = aiter(())

        contacts = await atuple(self.client.list())

        self.assertEqual(contacts, ())
        self.assertEqual(
            self.client.stream_mock.mock_calls,
            [call(Empty(), method="/Fred.Registry.Api.Contact.Contact/list_contacts", timeout=None)],
        )

    async def test_list_empty_response(self):
        reply = ListContactsReply()
        reply.data.contacts.extend([])
        self.client.stream_mock.return_value = aiter([reply])

        contacts = await atuple(self.client.list())

        self.assertEqual(contacts, ())
        self.assertEqual(
            self.client.stream_mock.mock_calls,
            [call(Empty(), method="/Fred.Registry.Api.Contact.Contact/list_contacts", timeout=None)],
        )

    async def test_list(self):
        reply = ListContactsReply()
        contact_ref = reply.data.contacts.add()
        contact_ref.id.uuid.value = "42"
        contact_ref.handle.value = "KRYTEN"
        self.client.stream_mock.return_value = aiter([reply])

        contacts = await atuple(self.client.list())

        self.assertEqual(contacts, (ContactRef(id=ContactId("42"), handle="KRYTEN"),))
        self.assertEqual(
            self.client.stream_mock.mock_calls,
            [call(Empty(), method="/Fred.Registry.Api.Contact.Contact/list_contacts", timeout=None)],
        )

    async def test_list_flatten(self):
        # Test responses are properly flattened.
        reply_1 = ListContactsReply()
        contact_ref_1 = reply_1.data.contacts.add()
        contact_ref_1.id.uuid.value = "42"
        contact_ref_1.handle.value = "KRYTEN"
        contact_ref_2 = reply_1.data.contacts.add()
        contact_ref_2.id.uuid.value = "43"
        contact_ref_2.handle.value = "LISTER"
        reply_2 = ListContactsReply()
        contact_ref_3 = reply_2.data.contacts.add()
        contact_ref_3.id.uuid.value = "44"
        contact_ref_3.handle.value = "RIMMER"
        contact_ref_4 = reply_2.data.contacts.add()
        contact_ref_4.id.uuid.value = "45"
        contact_ref_4.handle.value = "CAT"
        self.client.stream_mock.return_value = aiter([reply_1, reply_2])

        contacts = await atuple(self.client.list())

        result = (
            ContactRef(id=ContactId("42"), handle="KRYTEN"),
            ContactRef(id=ContactId("43"), handle="LISTER"),
            ContactRef(id=ContactId("44"), handle="RIMMER"),
            ContactRef(id=ContactId("45"), handle="CAT"),
        )
        self.assertEqual(contacts, result)
        self.assertEqual(
            self.client.stream_mock.mock_calls,
            [call(Empty(), method="/Fred.Registry.Api.Contact.Contact/list_contacts", timeout=None)],
        )

    async def _test_create_contact(self, contact: Contact, request: CreateContactRequest) -> None:
        reply = CreateContactReply()
        reply.data.contact_id.uuid.value = "42"
        self.client.mock.return_value = make_awaitable(reply)

        contact_id = await self.client.create(contact)

        self.assertEqual(contact_id, ContactId("42"))

        grpc_call = call(request, method="/Fred.Registry.Api.Contact.Contact/create_contact", timeout=None)
        self.assertEqual(self.client.mock.mock_calls, [grpc_call])

    async def test_create_contact_minimal(self):
        contact = Contact(contact_id=ContactId("PLACEHOLDER"), contact_handle="KRYTEN")
        request = CreateContactRequest()
        request.contact_handle.value = "KRYTEN"
        request.warning_letter.preference = 2
        await self._test_create_contact(contact, request)

    async def test_create_contact_minimal_address(self):
        place = Address(street=["Deck 16"], city="Red Dwarf")
        contact = Contact(contact_id=ContactId("PLACEHOLDER"), contact_handle="KRYTEN", place=place)
        request = CreateContactRequest()
        request.contact_handle.value = "KRYTEN"
        request.place.street.append("Deck 16")
        request.place.city = "Red Dwarf"
        request.warning_letter.preference = 2
        await self._test_create_contact(contact, request)

    async def test_create_contact(self):
        place = Address(
            street=["Deck 16"], city="Red Dwarf", state_or_province="Deep Space", postal_code="JMC", country_code="JU"
        )
        contact = Contact(
            contact_id=ContactId("PLACEHOLDER"),
            contact_handle="KRYTEN",
            name="Kryten 2X4B-523P",
            organization="Jupiter Mining Corporation",
            place=place,
            telephone="123456789",
            fax="987654321",
            emails=["kryten@example.org"],
            notify_emails=["holly@example.org"],
            vat_identification_number="JMC42",
            additional_identifier=AdditionalIdentifier(
                type=ContactIdentifierType.NATIONAL_IDENTITY_NUMBER, value="196"
            ),
            publish={"emails": True},
            warning_letter=WarningLetterPreference.TO_SEND,
        )

        request = CreateContactRequest()
        request.contact_handle.value = "KRYTEN"
        request.name = "Kryten 2X4B-523P"
        request.organization = "Jupiter Mining Corporation"
        request.place.street.append("Deck 16")
        request.place.city = "Red Dwarf"
        request.place.state_or_province = "Deep Space"
        request.place.postal_code.value = "JMC"
        request.place.country_code.value = "JU"
        request.telephone.value = "123456789"
        request.fax.value = "987654321"
        request.emails.add(value="kryten@example.org")
        request.notify_emails.add(value="holly@example.org")
        request.vat_identification_number.value = "JMC42"
        request.additional_identifier.national_identity_number.value = "196"
        request.disclose.update({"emails": True})
        request.warning_letter.preference = 0
        await self._test_create_contact(contact, request)

    async def test_create_contact_mailing_address(self):
        place = Address(
            street=["Deck 16"],
            city="Red Dwarf",
            state_or_province="Deep Space",
            postal_code="JMC",
            country_code="JU",
            company="Jupiter Mining Corporation",
        )
        contact = Contact(contact_id=ContactId("PLACEHOLDER"), contact_handle="KRYTEN", mailing_address=place)
        request = CreateContactRequest()
        request.contact_handle.value = "KRYTEN"
        request.warning_letter.preference = 2
        request.mailing_address.street.append("Deck 16")
        request.mailing_address.city = "Red Dwarf"
        request.mailing_address.state_or_province = "Deep Space"
        request.mailing_address.postal_code.value = "JMC"
        request.mailing_address.country_code.value = "JU"
        request.mailing_address.company = "Jupiter Mining Corporation"
        await self._test_create_contact(contact, request)

    async def test_create_contact_billing_address(self):
        place = Address(
            street=["Deck 16"],
            city="Red Dwarf",
            state_or_province="Deep Space",
            postal_code="JMC",
            country_code="JU",
            company="Jupiter Mining Corporation",
        )
        contact = Contact(contact_id=ContactId("PLACEHOLDER"), contact_handle="KRYTEN", billing_address=place)
        request = CreateContactRequest()
        request.contact_handle.value = "KRYTEN"
        request.warning_letter.preference = 2
        request.billing_address.street.append("Deck 16")
        request.billing_address.city = "Red Dwarf"
        request.billing_address.state_or_province = "Deep Space"
        request.billing_address.postal_code.value = "JMC"
        request.billing_address.country_code.value = "JU"
        request.billing_address.company = "Jupiter Mining Corporation"
        await self._test_create_contact(contact, request)

    async def test_create_contact_shipping_address1(self):
        place = Address(
            street=["Deck 16"],
            city="Red Dwarf",
            state_or_province="Deep Space",
            postal_code="JMC",
            country_code="JU",
            company="Jupiter Mining Corporation",
        )
        contact = Contact(contact_id=ContactId("PLACEHOLDER"), contact_handle="KRYTEN", shipping_address1=place)
        request = CreateContactRequest()
        request.contact_handle.value = "KRYTEN"
        request.warning_letter.preference = 2
        request.shipping_address1.street.append("Deck 16")
        request.shipping_address1.city = "Red Dwarf"
        request.shipping_address1.state_or_province = "Deep Space"
        request.shipping_address1.postal_code.value = "JMC"
        request.shipping_address1.country_code.value = "JU"
        request.shipping_address1.company = "Jupiter Mining Corporation"
        await self._test_create_contact(contact, request)

    async def test_create_contact_shipping_address2(self):
        place = Address(
            street=["Deck 16"],
            city="Red Dwarf",
            state_or_province="Deep Space",
            postal_code="JMC",
            country_code="JU",
            company="Jupiter Mining Corporation",
        )
        contact = Contact(contact_id=ContactId("PLACEHOLDER"), contact_handle="KRYTEN", shipping_address2=place)
        request = CreateContactRequest()
        request.contact_handle.value = "KRYTEN"
        request.warning_letter.preference = 2
        request.shipping_address2.street.append("Deck 16")
        request.shipping_address2.city = "Red Dwarf"
        request.shipping_address2.state_or_province = "Deep Space"
        request.shipping_address2.postal_code.value = "JMC"
        request.shipping_address2.country_code.value = "JU"
        request.shipping_address2.company = "Jupiter Mining Corporation"
        await self._test_create_contact(contact, request)

    async def test_create_contact_shipping_address3(self):
        place = Address(
            street=["Deck 16"],
            city="Red Dwarf",
            state_or_province="Deep Space",
            postal_code="JMC",
            country_code="JU",
            company="Jupiter Mining Corporation",
        )
        contact = Contact(contact_id=ContactId("PLACEHOLDER"), contact_handle="KRYTEN", shipping_address3=place)
        request = CreateContactRequest()
        request.contact_handle.value = "KRYTEN"
        request.warning_letter.preference = 2
        request.shipping_address3.street.append("Deck 16")
        request.shipping_address3.city = "Red Dwarf"
        request.shipping_address3.state_or_province = "Deep Space"
        request.shipping_address3.postal_code.value = "JMC"
        request.shipping_address3.country_code.value = "JU"
        request.shipping_address3.company = "Jupiter Mining Corporation"
        await self._test_create_contact(contact, request)

    async def test_create_contact_log_entry_id(self):
        reply = CreateContactReply()
        reply.data.contact_id.uuid.value = "42"
        self.client.mock.return_value = make_awaitable(reply)

        contact = Contact(contact_id=ContactId("PLACEHOLDER"), contact_handle="KRYTEN")
        contact_id = await self.client.create(contact, log_entry_id=LogEntryId("McGruder"))

        self.assertEqual(contact_id, ContactId("42"))

        request = CreateContactRequest()
        request.contact_handle.value = "KRYTEN"
        request.warning_letter.preference = 2
        request.log_entry_id.value = "McGruder"
        grpc_call = call(request, method="/Fred.Registry.Api.Contact.Contact/create_contact", timeout=None)
        self.assertEqual(self.client.mock.mock_calls, [grpc_call])

    async def test_create_contact_by_registrar(self):
        reply = CreateContactReply()
        reply.data.contact_id.uuid.value = "42"
        self.client.mock.return_value = make_awaitable(reply)

        contact = Contact(contact_id=ContactId("PLACEHOLDER"), contact_handle="KRYTEN")
        contact_id = await self.client.create(contact, by_registrar="HOLLY")

        self.assertEqual(contact_id, ContactId("42"))

        request = CreateContactRequest()
        request.contact_handle.value = "KRYTEN"
        request.warning_letter.preference = 2
        request.by_registrar.value = "HOLLY"
        grpc_call = call(request, method="/Fred.Registry.Api.Contact.Contact/create_contact", timeout=None)
        self.assertEqual(self.client.mock.mock_calls, [grpc_call])

    async def test_update_minimal(self):
        reply = UpdateContactReply()
        reply.data.contact_history_id.uuid.value = "523P"
        self.client.mock.return_value = make_awaitable(reply)

        self.assertEqual(await self.client.update(ContactId("2X4B")), "523P")

        request = UpdateContactRequest()
        request.contact_id.uuid.value = "2X4B"
        grpc_call = call(request, method="/Fred.Registry.Api.Contact.Contact/update_contact", timeout=None)
        self.assertEqual(self.client.mock.mock_calls, [grpc_call])

    async def test_update_full(self):
        reply = UpdateContactReply()
        reply.data.contact_history_id.uuid.value = "523P"
        self.client.mock.return_value = make_awaitable(reply)

        result = await self.client.update(
            ContactId("2X4B"),
            snapshot_id=SnapshotId("NOVA5"),
            name="Kryten 2X4B-523P",
            organization="Jupiter Mining Corporation",
            place=Address(
                street=["Deck 16"], city="Red Dwarf", state_or_province="Starbug", postal_code="JMC", country_code="JU"
            ),
            telephone="+1.2345",
            fax="+9.8765",
            emails=["kryten@example.org"],
            notify_emails=["2x4b@example.org"],
            vat_identification_number="2X4B-523P",
            additional_identifier=AdditionalIdentifier(type=ContactIdentifierType.BIRTHDATE, value="1988-09-06"),
            publish={"name": True, "emails": False},
            mailing_address=Address(
                street=["Deck 17"],
                city="Red Dwarf",
                state_or_province="Deep space",
                postal_code="JMC",
                country_code="JU",
                company="Lister",
            ),
            billing_address=Address(
                street=["Deck 18"],
                city="Red Dwarf",
                state_or_province="Deep space",
                postal_code="JMC",
                country_code="JU",
                company="Rimmer",
            ),
            shipping_address1=Address(
                street=["Deck 19"],
                city="Red Dwarf",
                state_or_province="Deep space",
                postal_code="JMC",
                country_code="JU",
                company="Holly",
            ),
            shipping_address2=Address(
                street=["Deck 20"],
                city="Red Dwarf",
                state_or_province="Deep space",
                postal_code="JMC",
                country_code="JU",
                company="Hilly",
            ),
            shipping_address3=Address(
                street=["Service deck"],
                city="Red Dwarf",
                state_or_province="Deep space",
                postal_code="JMC",
                country_code="JU",
                company="The Skutters",
            ),
            warning_letter=WarningLetterPreference.NOT_TO_SEND,
            log_entry_id="42",
            by_registrar="HOLLY",
        )

        self.assertEqual(result, "523P")

        request = UpdateContactRequest()
        request.contact_id.uuid.value = "2X4B"
        request.contact_snapshot_id.value = "NOVA5"
        request.set_name = "Kryten 2X4B-523P"
        request.set_organization = "Jupiter Mining Corporation"
        request.set_place.street.append("Deck 16")
        request.set_place.city = "Red Dwarf"
        request.set_place.state_or_province = "Starbug"
        request.set_place.postal_code.value = "JMC"
        request.set_place.country_code.value = "JU"
        request.set_telephone.value = "+1.2345"
        request.set_fax.value = "+9.8765"
        request.set_emails.values.add(value="kryten@example.org")
        request.set_notify_emails.values.add(value="2x4b@example.org")
        request.set_vat_identification_number.value = "2X4B-523P"
        request.set_additional_identifier.birthdate.value = "1988-09-06"
        request.set_publish.update({"name": True, "emails": False})
        request.set_mailing_address.street.append("Deck 17")
        request.set_mailing_address.city = "Red Dwarf"
        request.set_mailing_address.state_or_province = "Deep space"
        request.set_mailing_address.postal_code.value = "JMC"
        request.set_mailing_address.country_code.value = "JU"
        request.set_mailing_address.company = "Lister"
        request.set_billing_address.street.append("Deck 18")
        request.set_billing_address.city = "Red Dwarf"
        request.set_billing_address.state_or_province = "Deep space"
        request.set_billing_address.postal_code.value = "JMC"
        request.set_billing_address.country_code.value = "JU"
        request.set_billing_address.company = "Rimmer"
        request.set_shipping_address1.street.append("Deck 19")
        request.set_shipping_address1.city = "Red Dwarf"
        request.set_shipping_address1.state_or_province = "Deep space"
        request.set_shipping_address1.postal_code.value = "JMC"
        request.set_shipping_address1.country_code.value = "JU"
        request.set_shipping_address1.company = "Holly"
        request.set_shipping_address2.street.append("Deck 20")
        request.set_shipping_address2.city = "Red Dwarf"
        request.set_shipping_address2.state_or_province = "Deep space"
        request.set_shipping_address2.postal_code.value = "JMC"
        request.set_shipping_address2.country_code.value = "JU"
        request.set_shipping_address2.company = "Hilly"
        request.set_shipping_address3.street.append("Service deck")
        request.set_shipping_address3.city = "Red Dwarf"
        request.set_shipping_address3.state_or_province = "Deep space"
        request.set_shipping_address3.postal_code.value = "JMC"
        request.set_shipping_address3.country_code.value = "JU"
        request.set_shipping_address3.company = "The Skutters"
        request.set_warning_letter.preference = WarningLetterMessage.SendingPreference.not_to_send
        request.log_entry_id.value = "42"
        request.by_registrar.value = "HOLLY"
        grpc_call = call(request, method="/Fred.Registry.Api.Contact.Contact/update_contact", timeout=None)
        self.assertEqual(self.client.mock.mock_calls, [grpc_call])

    async def test_update_delete_addresses(self):
        reply = UpdateContactReply()
        reply.data.contact_history_id.uuid.value = "523P"
        self.client.mock.return_value = make_awaitable(reply)

        result = await self.client.update(
            ContactId("2X4B"),
            mailing_address=DELETE,
            billing_address=DELETE,
            shipping_address1=DELETE,
            shipping_address2=DELETE,
            shipping_address3=DELETE,
        )
        self.assertEqual(result, "523P")

        request = UpdateContactRequest()
        request.contact_id.uuid.value = "2X4B"
        request.delete_mailing_address.CopyFrom(Empty())
        request.delete_billing_address.CopyFrom(Empty())
        request.delete_shipping_address1.CopyFrom(Empty())
        request.delete_shipping_address2.CopyFrom(Empty())
        request.delete_shipping_address3.CopyFrom(Empty())
        grpc_call = call(request, method="/Fred.Registry.Api.Contact.Contact/update_contact", timeout=None)
        self.assertEqual(self.client.mock.mock_calls, [grpc_call])

    async def test_update_state_empty(self):
        self.client.stream_mock.return_value = aiter(())

        await self.client.update_state(())

        self.assertEqual(
            self.client.stream_mock.mock_calls,
            [call((), method="/Fred.Registry.Api.Contact.Contact/update_contact_state_stream", timeout=None)],
        )

    async def test_update_state(self):
        self.client.stream_mock.return_value = aiter(())

        await self.client.update_state(
            (StateUpdate(id=ContactId("STARBUG-42"), set_flags=("POWER",), unset_flags=("ANCHOR",)),)
        )

        request = UpdateContactStateStreamRequest()
        update = request.updates.add()
        update.contact_id.uuid.value = "STARBUG-42"
        update.set_flags.append("POWER")
        update.unset_flags.append("ANCHOR")
        self.assertEqual(
            self.client.stream_mock.mock_calls,
            [call((request,), method="/Fred.Registry.Api.Contact.Contact/update_contact_state_stream", timeout=None)],
        )

    async def test_update_state_kwargs(self):
        self.client.stream_mock.return_value = aiter(())

        await self.client.update_state(
            (StateUpdate(id=ContactId("STARBUG-42")),), snapshot_id=SnapshotId("BACKWARDS"), log_entry_id="DEAR-DIARY"
        )

        request = UpdateContactStateStreamRequest()
        update = request.updates.add()
        update.contact_id.uuid.value = "STARBUG-42"
        request.contact_snapshot_id.value = "BACKWARDS"
        request.log_entry_id.value = "DEAR-DIARY"
        self.assertEqual(
            self.client.stream_mock.mock_calls,
            [call((request,), method="/Fred.Registry.Api.Contact.Contact/update_contact_state_stream", timeout=None)],
        )

    async def test_update_state_batch_size(self):
        self.client.stream_mock.return_value = aiter(())

        await self.client.update_state(
            (StateUpdate(id=ContactId("STARBUG-42")), StateUpdate(id=ContactId("STARBUG-43"))), batch_size=1
        )

        request_1 = UpdateContactStateStreamRequest()
        update_1 = request_1.updates.add()
        update_1.contact_id.uuid.value = "STARBUG-42"
        request_2 = UpdateContactStateStreamRequest()
        update_2 = request_2.updates.add()
        update_2.contact_id.uuid.value = "STARBUG-43"
        calls = [
            call(
                (request_1, request_2),
                method="/Fred.Registry.Api.Contact.Contact/update_contact_state_stream",
                timeout=None,
            )
        ]
        self.assertEqual(self.client.stream_mock.mock_calls, calls)

    async def test_update_state_kwarg_updates(self):
        # Ensure the method can be called with the udpated passed as kwarg.
        # Remove when the old behaviour will be removed as well.
        self.client.stream_mock.return_value = aiter(())

        await self.client.update_state(updates=())

        self.assertEqual(
            self.client.stream_mock.mock_calls,
            [call((), method="/Fred.Registry.Api.Contact.Contact/update_contact_state_stream", timeout=None)],
        )

    async def test_update_state_old(self):
        flags = {"state 1": True, "state 2": True, "State 3": False}
        reply = UpdateContactStateReply()
        self.client.mock.return_value = make_awaitable(reply)

        with self.assertWarnsRegex(
            DeprecationWarning, "Argument `contact_id` is deprecated in favor of `StateUpdate` object."
        ):
            await self.client.update_state(
                ContactId("Id-1"), ContactHistoryId("Hid-1"), flags, log_entry_id=LogEntryId("Log-1")
            )

        request = UpdateContactStateRequest()
        request.contact_id.uuid.value = "Id-1"
        request.contact_history_id.uuid.value = "Hid-1"
        request.log_entry_id.value = "Log-1"
        for key, value in flags.items():
            request.set_flags[key] = value
        self.client.mock.assert_called_once_with(
            request,
            method="/Fred.Registry.Api.Contact.Contact/update_contact_state",
            timeout=None,
        )

    async def test_update_state_not_found(self):
        reply = UpdateContactStateReply()
        reply.exception.contact_does_not_exist.CopyFrom(ContactDoesNotExistMessage())
        self.client.mock.return_value = make_awaitable(reply)

        with self.assertWarnsRegex(
            DeprecationWarning, "Argument `contact_id` is deprecated in favor of `StateUpdate` object."
        ):
            with self.assertRaises(ContactDoesNotExist):
                await self.client.update_state(ContactId("Id-1"), ContactHistoryId("Hid-1"), {})

    async def test_update_state_not_current(self):
        reply = UpdateContactStateReply()
        reply.exception.not_current_version.CopyFrom(UpdateContactStateReply.Exception.NotCurrentVersion())
        self.client.mock.return_value = make_awaitable(reply)

        with self.assertWarnsRegex(
            DeprecationWarning, "Argument `contact_id` is deprecated in favor of `StateUpdate` object."
        ):
            with self.assertRaises(NotCurrentVersion):
                await self.client.update_state(ContactId("Id-1"), ContactHistoryId("Hid-1"), {})

    async def test_update_contact_state(self):
        flags = {"state 1": True, "state 2": True, "State 3": False}
        reply = UpdateContactStateReply()
        self.client.mock.return_value = make_awaitable(reply)

        with self.assertWarnsRegex(
            DeprecationWarning, "Method update_contact_state is deprecated in favor of update_state."
        ):
            await self.client.update_contact_state(
                ContactId("Id-1"), ContactHistoryId("Hid-1"), flags, LogEntryId("Log-1")
            )

        request = UpdateContactStateRequest()
        request.contact_id.uuid.value = "Id-1"
        request.contact_history_id.uuid.value = "Hid-1"
        request.log_entry_id.value = "Log-1"
        for key, value in flags.items():
            request.set_flags[key] = value
        self.client.mock.assert_called_once_with(
            request,
            method="/Fred.Registry.Api.Contact.Contact/update_contact_state",
            timeout=None,
        )

    async def test_update_contact_state_not_found(self):
        reply = UpdateContactStateReply()
        reply.exception.contact_does_not_exist.CopyFrom(ContactDoesNotExistMessage())
        self.client.mock.return_value = make_awaitable(reply)

        with self.assertWarnsRegex(
            DeprecationWarning, "Method update_contact_state is deprecated in favor of update_state."
        ):
            with self.assertRaises(ContactDoesNotExist):
                await self.client.update_contact_state(ContactId("Id-1"), ContactHistoryId("Hid-1"), {})

    async def test_update_contact_state_not_current(self):
        reply = UpdateContactStateReply()
        reply.exception.not_current_version.CopyFrom(UpdateContactStateReply.Exception.NotCurrentVersion())
        self.client.mock.return_value = make_awaitable(reply)

        with self.assertWarnsRegex(
            DeprecationWarning, "Method update_contact_state is deprecated in favor of update_state."
        ):
            with self.assertRaises(NotCurrentVersion):
                await self.client.update_contact_state(ContactId("Id-1"), ContactHistoryId("Hid-1"), {})

    async def test_add_auth_info_generate(self):
        reply = AddContactAuthInfoReply()
        reply.data.auth_info = "Gazpacho!"
        self.client.mock.return_value = make_awaitable(reply)

        auth_info = await self.client.add_auth_info(ContactId("STARBUG-1"))

        self.assertEqual(auth_info, "Gazpacho!")

        request = AddContactAuthInfoRequest()
        request.contact_id.uuid.value = "STARBUG-1"
        request.generate.CopyFrom(Empty())
        self.assertEqual(
            self.client.mock.mock_calls,
            [call(request, method="/Fred.Registry.Api.Contact.Contact/add_contact_auth_info", timeout=None)],
        )

    async def test_add_auth_info_custom(self):
        reply = AddContactAuthInfoReply()
        self.client.mock.return_value = make_awaitable(reply)

        auth_info = await self.client.add_auth_info(ContactId("STARBUG-1"), auth_info="Gazpacho!")  # type: ignore[func-returns-value]

        self.assertIsNone(auth_info)

        request = AddContactAuthInfoRequest()
        request.contact_id.uuid.value = "STARBUG-1"
        request.auth_info = "Gazpacho!"
        self.assertEqual(
            self.client.mock.mock_calls,
            [call(request, method="/Fred.Registry.Api.Contact.Contact/add_contact_auth_info", timeout=None)],
        )

    async def test_add_auth_info_extra_args(self):
        reply = AddContactAuthInfoReply()
        reply.data.auth_info = "Gazpacho!"
        self.client.mock.return_value = make_awaitable(reply)

        auth_info = await self.client.add_auth_info(
            ContactId("STARBUG-1"), snapshot_id=SnapshotId("NOVA5"), ttl=42, log_entry_id="2X4B", by_registrar="HOLLY"
        )

        self.assertEqual(auth_info, "Gazpacho!")

        request = AddContactAuthInfoRequest()
        request.contact_id.uuid.value = "STARBUG-1"
        request.contact_snapshot_id.value = "NOVA5"
        request.generate.CopyFrom(Empty())
        request.ttl = 42
        request.log_entry_id.value = "2X4B"
        request.by_registrar.value = "HOLLY"
        self.assertEqual(
            self.client.mock.mock_calls,
            [call(request, method="/Fred.Registry.Api.Contact.Contact/add_contact_auth_info", timeout=None)],
        )

    async def _test_list_merge_candidates(self, candidates: tuple[MergeCandidate, ...]) -> None:
        result = self.client.list_merge_candidates(ContactId("LEGION"))

        self.assertEqual(await atuple(result), candidates)
        request = ListMergeCandidatesRequest()
        request.contact_id.uuid.value = "LEGION"
        grpc_call = call(request, method="/Fred.Registry.Api.Contact.Contact/list_merge_candidates", timeout=None)
        self.assertEqual(self.client.stream_mock.mock_calls, [grpc_call])

    async def test_list_merge_candidates_empty_stream(self):
        self.client.stream_mock.return_value = aiter(())

        await self._test_list_merge_candidates(())

    async def test_list_merge_candidates_empty_response(self):
        reply = ListMergeCandidatesReply()
        reply.data.merge_candidates.extend([])
        self.client.stream_mock.return_value = aiter([reply])

        await self._test_list_merge_candidates(())

    async def test_list_merge_candidates(self):
        reply = ListMergeCandidatesReply()
        candidate_msg = reply.data.merge_candidates.add()
        candidate_msg.contact.id.uuid.value = "2X4B-523P"
        candidate_msg.contact.handle.value = "KRYTEN"
        candidate_msg.domain_count = 17
        candidate_msg.nsset_count = 18
        candidate_msg.keyset_count = 19
        candidate_msg.sponsoring_registrar.id.value = "QUEEG-500"
        candidate_msg.sponsoring_registrar.handle.value = "HOLLY"
        self.client.stream_mock.return_value = aiter([reply])

        candidate = MergeCandidate(
            contact=ContactRef(id=ContactId("2X4B-523P"), handle="KRYTEN"),
            domain_count=17,
            nsset_count=18,
            keyset_count=19,
            registrar=RegistrarRef(id=RegistrarId("QUEEG-500"), handle="HOLLY"),
        )
        await self._test_list_merge_candidates((candidate,))

    async def test_list_merge_candidates_order_by(self):
        # Test order_by is passed to the request correctly.
        self.client.stream_mock.return_value = aiter(())

        await atuple(self.client.list_merge_candidates(ContactId("KRYTEN"), order_by=("-crdate", "handle")))

        request = ListMergeCandidatesRequest()
        request.contact_id.uuid.value = "KRYTEN"
        request.order_by.add(
            field=ListMergeCandidatesRequest.OrderByField.order_by_field_crdate,
            direction=OrderByDirection.order_by_direction_descending,
        )
        request.order_by.add(field=ListMergeCandidatesRequest.OrderByField.order_by_field_handle)
        self.assertEqual(
            self.client.stream_mock.mock_calls,
            [call(request, method="/Fred.Registry.Api.Contact.Contact/list_merge_candidates", timeout=None)],
        )

    async def test_list_merge_candidates_flatten(self):
        # Test responses are properly flattened.
        reply_1 = ListMergeCandidatesReply()
        candidate_msg_1 = reply_1.data.merge_candidates.add()
        candidate_msg_1.contact.id.uuid.value = "42"
        candidate_msg_1.contact.handle.value = "LISTER"
        candidate_msg_1.sponsoring_registrar.id.value = "QUEEG-500"
        candidate_msg_1.sponsoring_registrar.handle.value = "HOLLY"
        candidate_msg_2 = reply_1.data.merge_candidates.add()
        candidate_msg_2.contact.id.uuid.value = "43"
        candidate_msg_2.contact.handle.value = "RIMMER"
        candidate_msg_2.sponsoring_registrar.id.value = "QUEEG-500"
        candidate_msg_2.sponsoring_registrar.handle.value = "HOLLY"
        reply_2 = ListMergeCandidatesReply()
        candidate_msg_3 = reply_2.data.merge_candidates.add()
        candidate_msg_3.contact.id.uuid.value = "44"
        candidate_msg_3.contact.handle.value = "KRYTEN"
        candidate_msg_3.sponsoring_registrar.id.value = "QUEEG-500"
        candidate_msg_3.sponsoring_registrar.handle.value = "HOLLY"
        candidate_msg_4 = reply_2.data.merge_candidates.add()
        candidate_msg_4.contact.id.uuid.value = "45"
        candidate_msg_4.contact.handle.value = "CAT"
        candidate_msg_4.sponsoring_registrar.id.value = "QUEEG-500"
        candidate_msg_4.sponsoring_registrar.handle.value = "HOLLY"
        self.client.stream_mock.return_value = aiter([reply_1, reply_2])

        registrar = RegistrarRef(id=RegistrarId("QUEEG-500"), handle="HOLLY")
        candidates = (
            MergeCandidate(contact=ContactRef(id=ContactId("42"), handle="LISTER"), registrar=registrar),
            MergeCandidate(contact=ContactRef(id=ContactId("43"), handle="RIMMER"), registrar=registrar),
            MergeCandidate(contact=ContactRef(id=ContactId("44"), handle="KRYTEN"), registrar=registrar),
            MergeCandidate(contact=ContactRef(id=ContactId("45"), handle="CAT"), registrar=registrar),
        )
        await self._test_list_merge_candidates(candidates)

    async def _test_list_merge_candidates_page(self, page: ListPage[MergeCandidate]) -> None:
        result = await self.client.list_merge_candidates_page(ContactId("LEGION"))

        self.assertEqual(result, page)
        request = ListMergeCandidatesRequest()
        request.contact_id.uuid.value = "LEGION"
        grpc_call = call(request, method="/Fred.Registry.Api.Contact.Contact/list_merge_candidates", timeout=None)
        self.assertEqual(self.client.stream_mock.mock_calls, [grpc_call])

    async def test_list_merge_candidates_page_empty_stream(self):
        self.client.stream_mock.return_value = aiter(())

        await self._test_list_merge_candidates_page(ListPage(results=()))

    async def test_list_merge_candidates_page_empty_response(self):
        reply = ListMergeCandidatesReply()
        reply.data.merge_candidates.extend([])
        self.client.stream_mock.return_value = aiter([reply])

        await self._test_list_merge_candidates_page(ListPage(results=()))

    async def test_list_merge_candidates_page_pagination_response(self):
        reply = ListMergeCandidatesReply()
        reply.data.pagination.next_page_token = "quagaars"
        reply.data.pagination.items_left = 42
        self.client.stream_mock.return_value = aiter([reply])

        pagination = PaginationReply(next_page_token="quagaars", items_left=42)
        await self._test_list_merge_candidates_page(ListPage(results=(), pagination=pagination))

    async def test_list_merge_candidates_page(self):
        reply = ListMergeCandidatesReply()
        candidate_msg = reply.data.merge_candidates.add()
        candidate_msg.contact.id.uuid.value = "2X4B-523P"
        candidate_msg.contact.handle.value = "KRYTEN"
        candidate_msg.domain_count = 17
        candidate_msg.nsset_count = 18
        candidate_msg.keyset_count = 19
        candidate_msg.sponsoring_registrar.id.value = "QUEEG-500"
        candidate_msg.sponsoring_registrar.handle.value = "HOLLY"
        self.client.stream_mock.return_value = aiter([reply])

        candidate = MergeCandidate(
            contact=ContactRef(id=ContactId("2X4B-523P"), handle="KRYTEN"),
            domain_count=17,
            nsset_count=18,
            keyset_count=19,
            registrar=RegistrarRef(id=RegistrarId("QUEEG-500"), handle="HOLLY"),
        )
        await self._test_list_merge_candidates_page(ListPage(results=(candidate,)))

    async def test_list_merge_candidates_page_pagination_request(self):
        # Test pagination args are passed to the request correctly.
        self.client.stream_mock.return_value = aiter(())

        await self.client.list_merge_candidates_page(ContactId("LEGION"), page_token="quagaars", page_size=42)

        request = ListMergeCandidatesRequest()
        request.contact_id.uuid.value = "LEGION"
        request.pagination.page_token = "quagaars"
        request.pagination.page_size = 42
        self.assertEqual(
            self.client.stream_mock.mock_calls,
            [call(request, method="/Fred.Registry.Api.Contact.Contact/list_merge_candidates", timeout=None)],
        )

    async def test_list_merge_candidates_page_order_by(self):
        # Test order_by is passed to the request correctly.
        self.client.stream_mock.return_value = aiter(())

        await self.client.list_merge_candidates_page(ContactId("KRYTEN"), order_by=("-crdate", "handle"))

        request = ListMergeCandidatesRequest()
        request.contact_id.uuid.value = "KRYTEN"
        request.order_by.add(
            field=ListMergeCandidatesRequest.OrderByField.order_by_field_crdate,
            direction=OrderByDirection.order_by_direction_descending,
        )
        request.order_by.add(field=ListMergeCandidatesRequest.OrderByField.order_by_field_handle)
        self.assertEqual(
            self.client.stream_mock.mock_calls,
            [call(request, method="/Fred.Registry.Api.Contact.Contact/list_merge_candidates", timeout=None)],
        )

    async def test_list_merge_candidates_page_flatten(self):
        # Test responses are properly flattened.
        reply_1 = ListMergeCandidatesReply()
        candidate_msg_1 = reply_1.data.merge_candidates.add()
        candidate_msg_1.contact.id.uuid.value = "42"
        candidate_msg_1.contact.handle.value = "LISTER"
        candidate_msg_1.sponsoring_registrar.id.value = "QUEEG-500"
        candidate_msg_1.sponsoring_registrar.handle.value = "HOLLY"
        candidate_msg_2 = reply_1.data.merge_candidates.add()
        candidate_msg_2.contact.id.uuid.value = "43"
        candidate_msg_2.contact.handle.value = "RIMMER"
        candidate_msg_2.sponsoring_registrar.id.value = "QUEEG-500"
        candidate_msg_2.sponsoring_registrar.handle.value = "HOLLY"
        reply_2 = ListMergeCandidatesReply()
        candidate_msg_3 = reply_2.data.merge_candidates.add()
        candidate_msg_3.contact.id.uuid.value = "44"
        candidate_msg_3.contact.handle.value = "KRYTEN"
        candidate_msg_3.sponsoring_registrar.id.value = "QUEEG-500"
        candidate_msg_3.sponsoring_registrar.handle.value = "HOLLY"
        candidate_msg_4 = reply_2.data.merge_candidates.add()
        candidate_msg_4.contact.id.uuid.value = "45"
        candidate_msg_4.contact.handle.value = "CAT"
        candidate_msg_4.sponsoring_registrar.id.value = "QUEEG-500"
        candidate_msg_4.sponsoring_registrar.handle.value = "HOLLY"
        self.client.stream_mock.return_value = aiter([reply_1, reply_2])

        registrar = RegistrarRef(id=RegistrarId("QUEEG-500"), handle="HOLLY")
        candidates = (
            MergeCandidate(contact=ContactRef(id=ContactId("42"), handle="LISTER"), registrar=registrar),
            MergeCandidate(contact=ContactRef(id=ContactId("43"), handle="RIMMER"), registrar=registrar),
            MergeCandidate(contact=ContactRef(id=ContactId("44"), handle="KRYTEN"), registrar=registrar),
            MergeCandidate(contact=ContactRef(id=ContactId("45"), handle="CAT"), registrar=registrar),
        )
        await self._test_list_merge_candidates_page(ListPage(results=candidates))

    async def test_merge(self):
        request = MergeContactsRequest()
        request.target_contact_id.uuid.value = "LEGION"
        source_1 = request.source_contact_ids.add()
        source_1.uuid.value = "LISTER"
        source_2 = request.source_contact_ids.add()
        source_2.uuid.value = "RIMMER"
        self.client.mock.return_value = make_awaitable(MergeContactsReply())

        await self.client.merge(ContactId("LEGION"), (ContactId("LISTER"), ContactId("RIMMER")))

        grpc_call = call(request, method="/Fred.Registry.Api.Contact.Contact/merge_contacts", timeout=None)
        self.assertEqual(self.client.mock.mock_calls, [grpc_call])

    async def test_merge_log_entry_id(self):
        request = MergeContactsRequest()
        request.target_contact_id.uuid.value = "LEGION"
        source_1 = request.source_contact_ids.add()
        source_1.uuid.value = "LISTER"
        source_2 = request.source_contact_ids.add()
        source_2.uuid.value = "RIMMER"
        request.log_entry_id.value = "Ionian-Nerve-Grip"
        self.client.mock.return_value = make_awaitable(MergeContactsReply())

        await self.client.merge(
            ContactId("LEGION"),
            (ContactId("LISTER"), ContactId("RIMMER")),
            log_entry_id=LogEntryId("Ionian-Nerve-Grip"),
        )

        grpc_call = call(request, method="/Fred.Registry.Api.Contact.Contact/merge_contacts", timeout=None)
        self.assertEqual(self.client.mock.mock_calls, [grpc_call])

from collections.abc import Iterable
from datetime import datetime
from typing import Any, Optional
from unittest import IsolatedAsyncioTestCase, TestCase

import pytz
from asyncstdlib import iter as aiter, tuple as atuple
from fred_api.registry.common_types_pb2 import OrderByDirection, PlaceAddress, StateFlag
from fred_api.registry.contact.contact_common_types_pb2 import ContactAddress, ContactRef as ContactRefMessage
from fred_api.registry.contact.service_search_contact_grpc_pb2 import SearchContactHistoryReply, SearchContactReply
from fred_api.registry.domain.domain_common_types_pb2 import DomainRef as DomainRefMessage
from fred_api.registry.domain.domains_by_contact_types_pb2 import DomainsByContactReply
from fred_api.registry.keyset.keyset_common_types_pb2 import KeysetRef as KeysetRefMessage
from fred_api.registry.nsset.list_nssets_by_contact_types_pb2 import (
    ListNssetsByContactRequest,
)
from fred_api.registry.nsset.nsset_common_types_pb2 import NssetRef as NssetRefMessage
from fred_api.registry.object_events_pb2 import ObjectEvents as ObjectEventsMessage
from fred_api.registry.registrar.registrar_common_types_pb2 import RegistrarRef as RegistrarRefMessage
from fred_types import (
    ContactId,
    ContactRef,
    DomainId,
    DomainRef,
    KeysetId,
    KeysetRef,
    NssetId,
    NssetRef,
    RegistrarId,
    RegistrarRef,
)

from regal.common import (
    Address,
    FuzzyValue,
    ObjectEvent,
    ObjectEvents,
    PaginationReply,
    RegistryDecoder,
    _encode_order_by,
    _extract_pagination,
)
from regal.object import ObjectStateFlag


class AddressTest(TestCase):
    def test_str(self):
        data = (
            (Address(street=["Deck 5"], city="Red Dwarf"), "Deck 5, Red Dwarf"),
            (Address(street=["Deck 5", "Bulkhead 3"], city="Red Dwarf"), "Deck 5, Bulkhead 3, Red Dwarf"),
            (
                Address(
                    company="Jupiter Mining Corp.",
                    street=["Deck 5"],
                    city="Red Dwarf",
                    state_or_province="Quadrant Alpha",
                    postal_code="12345",
                    country_code="JMC",
                ),
                "Jupiter Mining Corp., Deck 5, Red Dwarf, Quadrant Alpha, 12345, JMC",
            ),
        )

        for address, string in data:
            with self.subTest(address=address, string=string):
                self.assertEqual(str(address), string)


class RegistryDecoderTest(TestCase):
    datetime = datetime(2020, 1, 1, tzinfo=pytz.utc)

    def setUp(self):
        self.decoder = RegistryDecoder()
        self.address = Address(
            street=["Floor 42"],
            city="Red Dwarf",
            state_or_province="JMC",
            postal_code="11111",
            country_code="XY",
        )

    def test_decode_fuzzy_value(self):
        self.assertEqual(
            self.decoder.decode(SearchContactReply.Data.FuzzyValue(lower_estimate=10, upper_estimate=100)),
            FuzzyValue(lower_estimate=10, upper_estimate=100),
        )
        self.assertEqual(
            self.decoder.decode(SearchContactHistoryReply.Data.FuzzyValue(lower_estimate=10, upper_estimate=100)),
            FuzzyValue(lower_estimate=10, upper_estimate=100),
        )
        self.assertEqual(
            self.decoder.decode(SearchContactReply.Data.FuzzyValue()), FuzzyValue(lower_estimate=0, upper_estimate=0)
        )

    def test_decode_place_address(self):
        address = PlaceAddress()
        address.street[:] = self.address.street
        address.city = self.address.city
        address.state_or_province = self.address.state_or_province
        address.postal_code.value = self.address.postal_code
        address.country_code.value = self.address.country_code

        self.assertEqual(self.decoder.decode(address), self.address)

    def test_decode_contact_address(self):
        self.address.company = "Jupiter Mining Company"

        address = ContactAddress()
        address.company = self.address.company
        address.street[:] = self.address.street
        address.city = self.address.city
        address.state_or_province = self.address.state_or_province
        address.postal_code.value = self.address.postal_code
        address.country_code.value = self.address.country_code

        self.assertEqual(self.decoder.decode(address), self.address)

    def test_decode_object_event(self):
        self.assertEqual(
            self.decoder.decode(self._make_event("REG-JMC", self.datetime)),
            ObjectEvent(registrar_handle="REG-JMC", timestamp=self.datetime),
        )

    def test_decode_object_events(self):
        events = ObjectEventsMessage()
        events.registered.CopyFrom(self._make_event("REG-JMC", self.datetime))
        events.transferred.CopyFrom(self._make_event("REG-JMC", self.datetime))
        events.updated.CopyFrom(self._make_event("REG-JMC", self.datetime))
        event = ObjectEvent(registrar_handle="REG-JMC", timestamp=self.datetime)

        self.assertEqual(
            self.decoder.decode(events),
            ObjectEvents(
                registered=event,
                transferred=event,
                updated=event,
                unregistered=None,
            ),
        )

    @staticmethod
    def _make_event(handle, datetime):
        event = ObjectEventsMessage.EventData()
        event.registrar_handle.value = handle
        event.timestamp.FromDatetime(datetime)
        return event

    def test_decode_traits(self):
        data = (
            # Traits, ObjectStateFlag
            (StateFlag.Traits(), ObjectStateFlag(manual=True, internal=False)),
            (
                StateFlag.Traits(how_to_set=StateFlag.Manipulation.Enum.manual),
                ObjectStateFlag(manual=True, internal=False),
            ),
            (
                StateFlag.Traits(how_to_set=StateFlag.Manipulation.Enum.automatic),
                ObjectStateFlag(manual=False, internal=False),
            ),
            (
                StateFlag.Traits(visibility=StateFlag.Visibility.Enum.external),
                ObjectStateFlag(manual=True, internal=False),
            ),
            (
                StateFlag.Traits(visibility=StateFlag.Visibility.Enum.internal),
                ObjectStateFlag(manual=True, internal=True),
            ),
        )
        for traits, state_flag in data:
            with self.subTest(traits=traits):
                self.assertEqual(self.decoder.decode(traits), state_flag)

    def test_decode_contact_ref(self):
        ref = ContactRefMessage()
        ref.id.uuid.value = "2X4B"
        ref.handle.value = "KRYTEN"

        decoded = self.decoder.decode(ref)
        self.assertIsInstance(decoded, ContactRef)
        self.assertEqual(decoded, ContactRef(id=ContactId("2X4B"), handle="KRYTEN"))

    def test_decode_contact_ref_full(self):
        ref = ContactRefMessage()
        ref.id.uuid.value = "2X4B"
        ref.handle.value = "KRYTEN"
        ref.name = "Kryten"
        ref.organization = "JMC"

        decoded = self.decoder.decode(ref)
        self.assertIsInstance(decoded, ContactRef)
        self.assertEqual(decoded, ContactRef(id=ContactId("2X4B"), handle="KRYTEN", name="Kryten", organization="JMC"))

    def test_decode_domain_ref(self):
        ref = DomainRefMessage()
        ref.id.uuid.value = "2X4B"
        ref.fqdn.value = "example.org"

        decoded = self.decoder.decode(ref)
        self.assertIsInstance(decoded, DomainRef)
        self.assertEqual(decoded, DomainRef(id=DomainId("2X4B"), fqdn="example.org"))

    def test_decode_keyset_ref(self):
        ref = KeysetRefMessage()
        ref.id.uuid.value = "2X4B"
        ref.handle.value = "KRYTEN"

        decoded = self.decoder.decode(ref)
        self.assertIsInstance(decoded, KeysetRef)
        self.assertEqual(decoded, KeysetRef(id=KeysetId("2X4B"), handle="KRYTEN"))

    def test_decode_nsset_ref(self):
        ref = NssetRefMessage()
        ref.id.uuid.value = "2X4B"
        ref.handle.value = "KRYTEN"

        decoded = self.decoder.decode(ref)
        self.assertIsInstance(decoded, NssetRef)
        self.assertEqual(decoded, NssetRef(id=NssetId("2X4B"), handle="KRYTEN"))

    def test_decode_registrar_ref(self):
        ref = RegistrarRefMessage()
        ref.id.value = "QUEEG-500"
        ref.handle.value = "HOLLY"
        ref.name = "Holly"
        ref.organization = "JMC"

        decoded = self.decoder.decode(ref)
        self.assertIsInstance(decoded, RegistrarRef)
        self.assertEqual(
            decoded,
            RegistrarRef(id=RegistrarId("QUEEG-500"), handle="HOLLY", name="Holly", organization="JMC"),
        )


class ExtractPaginationTest(IsolatedAsyncioTestCase):
    async def test_empty(self):
        # Use DomainsByContactReply as a message class for tests.
        page_msg = DomainsByContactReply()
        page_msg.data.pagination.next_page_token = "quagaars"
        page_msg.data.pagination.items_left = 42

        data: Iterable[tuple[Iterable[dict[str, Any]], Optional[PaginationReply]]] = (
            # stream, result
            # No stream
            ((), None),
            # No pagination
            ([{}], None),
            ([{}] * 5, None),
            # Pagination
            (
                [{"pagination": {"next_page_token": "quagaars", "items_left": 42}}],
                PaginationReply(next_page_token="quagaars", items_left=42),
            ),
            (
                [{"pagination": {"next_page_token": "quagaars", "items_left": 42}}] * 5,
                PaginationReply(next_page_token="quagaars", items_left=42),
            ),
        )
        for stream, result in data:
            with self.subTest(stream=stream):
                self.assertEqual(await _extract_pagination(aiter(stream)), result)


class EncodeOrderByTest(IsolatedAsyncioTestCase):
    async def test_valid(self):
        # Test _encode_order_by on `ListNssetsByContactRequest.OrderBy`.
        data = (
            # order_by, orders
            ((), ()),
            (
                ("crdate",),
                (
                    ListNssetsByContactRequest.OrderBy(
                        field=ListNssetsByContactRequest.OrderByField.order_by_field_crdate
                    ),
                ),
            ),
            (
                ("+crdate",),
                (
                    ListNssetsByContactRequest.OrderBy(
                        field=ListNssetsByContactRequest.OrderByField.order_by_field_crdate
                    ),
                ),
            ),
            (
                ("-crdate",),
                (
                    ListNssetsByContactRequest.OrderBy(
                        field=ListNssetsByContactRequest.OrderByField.order_by_field_crdate,
                        direction=OrderByDirection.order_by_direction_descending,
                    ),
                ),
            ),
            (
                ("handle",),
                (
                    ListNssetsByContactRequest.OrderBy(
                        field=ListNssetsByContactRequest.OrderByField.order_by_field_handle
                    ),
                ),
            ),
            (
                ("-handle",),
                (
                    ListNssetsByContactRequest.OrderBy(
                        field=ListNssetsByContactRequest.OrderByField.order_by_field_handle,
                        direction=OrderByDirection.order_by_direction_descending,
                    ),
                ),
            ),
            (
                ("domains_count",),
                (
                    ListNssetsByContactRequest.OrderBy(
                        field=ListNssetsByContactRequest.OrderByField.order_by_field_domains_count
                    ),
                ),
            ),
            (
                ("-domains_count",),
                (
                    ListNssetsByContactRequest.OrderBy(
                        field=ListNssetsByContactRequest.OrderByField.order_by_field_domains_count,
                        direction=OrderByDirection.order_by_direction_descending,
                    ),
                ),
            ),
            (
                ("sponsoring_registrar_handle",),
                (
                    ListNssetsByContactRequest.OrderBy(
                        field=ListNssetsByContactRequest.OrderByField.order_by_field_sponsoring_registrar_handle
                    ),
                ),
            ),
            (
                ("-sponsoring_registrar_handle",),
                (
                    ListNssetsByContactRequest.OrderBy(
                        field=ListNssetsByContactRequest.OrderByField.order_by_field_sponsoring_registrar_handle,
                        direction=OrderByDirection.order_by_direction_descending,
                    ),
                ),
            ),
            (
                ("crdate", "-handle"),
                (
                    ListNssetsByContactRequest.OrderBy(
                        field=ListNssetsByContactRequest.OrderByField.order_by_field_crdate
                    ),
                    ListNssetsByContactRequest.OrderBy(
                        field=ListNssetsByContactRequest.OrderByField.order_by_field_handle,
                        direction=OrderByDirection.order_by_direction_descending,
                    ),
                ),
            ),
        )
        for order_by, result in data:
            with self.subTest(order_by=order_by):
                self.assertEqual(await atuple(_encode_order_by(order_by, ListNssetsByContactRequest.OrderBy)), result)

    async def test_invalid(self):
        data = (
            # order_by, msg
            (("-",), "Unknown order_by field: ''"),
            (("+",), "Unknown order_by field: ''"),
            (("unspecified",), "Ordering by 'unspecified' is forbidden."),
            (("invalid",), "Unknown order_by field: 'invalid'"),
        )
        for order_by, msg in data:
            with self.subTest(order_by=order_by):
                with self.assertRaisesRegex(ValueError, msg):
                    await atuple(_encode_order_by(order_by, ListNssetsByContactRequest.OrderBy))

from typing import Any
from unittest.mock import AsyncMock

from frgal.utils import AsyncMock as FrgalAsyncMock

if FrgalAsyncMock == AsyncMock:
    # Frgal 4.X, make_awaitable is useless
    def make_awaitable(value: Any) -> Any:
        return value
else:
    # Frgal 3.X, use its make_awaitable
    from frgal.utils import make_awaitable  # noqa: F401  # pragma: no cover

from ipaddress import IPv4Address
from typing import Any, Optional
from unittest import IsolatedAsyncioTestCase
from unittest.mock import call, sentinel

from asyncstdlib import iter as aiter, tuple as atuple
from fred_api.registry.common_types_pb2 import NssetDoesNotExist as NssetDoesNotExistMessage, OrderByDirection
from fred_api.registry.nsset.add_nsset_auth_info_types_pb2 import AddNssetAuthInfoReply, AddNssetAuthInfoRequest
from fred_api.registry.nsset.list_nssets_by_contact_types_pb2 import (
    ListNssetsByContactReply,
    ListNssetsByContactRequest,
)
from fred_api.registry.nsset.list_nssets_types_pb2 import ListNssetsReply, ListNssetsRequest
from fred_api.registry.nsset.nsset_history_types_pb2 import NssetHistoryRecord, NssetHistoryReply, NssetHistoryRequest
from fred_api.registry.nsset.nsset_info_types_pb2 import (
    BatchNssetInfoReply,
    BatchNssetInfoRequest,
    CheckDnsHostReply,
    CheckDnsHostRequest,
    DnsHost as DnsHostMessage,
    NssetIdReply,
    NssetIdRequest,
    NssetInfoReply,
    NssetInfoRequest,
)
from fred_api.registry.nsset.nsset_state_flags_types_pb2 import NssetStateFlagsReply
from fred_api.registry.nsset.nsset_state_history_types_pb2 import (
    NssetStateHistory,
    NssetStateHistoryReply,
    NssetStateHistoryRequest,
)
from fred_api.registry.nsset.nsset_state_types_pb2 import NssetStateReply, NssetStateRequest
from fred_api.registry.nsset.update_nsset_state_types_pb2 import UpdateNssetStateRequest
from fred_types import ContactId, NssetRef, SnapshotId
from frgal.utils import AsyncTestClientMixin
from google.protobuf.empty_pb2 import Empty

from regal.common import ListPage, PaginationReply
from regal.exceptions import NssetDoesNotExist
from regal.nsset import (
    DnsHost,
    Nsset,
    NssetByRelation,
    NssetClient,
    NssetHistoryId,
    NssetId,
)
from regal.object import StateUpdate
from regal.tests.common import ObjectClientTestMixin

from .utils import make_awaitable


class TestNssetClient(AsyncTestClientMixin, NssetClient):
    """Test NssetClient."""

    exhaust_iterables = True


class NssetClientTest(ObjectClientTestMixin[TestNssetClient], IsolatedAsyncioTestCase):
    """Test NssetClient's methods."""

    grpc_namespace = "/Fred.Registry.Api.Nsset.Nsset"
    get_state_request = NssetStateRequest
    get_state_reply = NssetStateReply
    get_state_method = "get_nsset_state"
    get_history_request = NssetHistoryRequest
    get_history_reply = NssetHistoryReply
    get_history_method = "get_nsset_history"
    get_history_record = NssetHistoryRecord
    get_state_history_request = NssetStateHistoryRequest
    get_state_history_reply = NssetStateHistoryReply
    get_state_history_method = "get_nsset_state_history"
    get_state_history_record = NssetStateHistory.Record
    get_state_flags_method = "get_nsset_state_flags"
    get_state_flags_reply_cls = NssetStateFlagsReply

    def setUp(self):
        self.client = TestNssetClient(netloc=sentinel.netloc)

    async def test_list_empty_stream(self):
        self.client.stream_mock.return_value = aiter(())

        response = await atuple(self.client.list())

        self.assertEqual(response, ())
        self.assertEqual(
            self.client.stream_mock.mock_calls,
            [call(ListNssetsRequest(), method="/Fred.Registry.Api.Nsset.Nsset/list_nssets", timeout=None)],
        )

    async def test_list_empty_response(self):
        reply = ListNssetsReply()
        reply.data.nssets.extend([])
        self.client.stream_mock.return_value = aiter([reply])

        response = await atuple(self.client.list())

        self.assertEqual(response, ())
        self.assertEqual(
            self.client.stream_mock.mock_calls,
            [call(ListNssetsRequest(), method="/Fred.Registry.Api.Nsset.Nsset/list_nssets", timeout=None)],
        )

    async def test_list(self):
        reply = ListNssetsReply()
        nsset_ref = reply.data.nssets.add()
        nsset_ref.id.uuid.value = "42"
        nsset_ref.handle.value = "GAZPACHO"
        self.client.stream_mock.return_value = aiter([reply])

        response = await atuple(self.client.list())

        self.assertEqual(response, (NssetRef(id=NssetId("42"), handle="GAZPACHO"),))
        self.assertEqual(
            self.client.stream_mock.mock_calls,
            [call(ListNssetsRequest(), method="/Fred.Registry.Api.Nsset.Nsset/list_nssets", timeout=None)],
        )

    async def test_list_flatten(self):
        # Test responses are properly flattened.
        reply_1 = ListNssetsReply()
        nsset_ref_1 = reply_1.data.nssets.add()
        nsset_ref_1.id.uuid.value = "42"
        nsset_ref_1.handle.value = "GAZPACHO.1"
        nsset_ref_2 = reply_1.data.nssets.add()
        nsset_ref_2.id.uuid.value = "43"
        nsset_ref_2.handle.value = "GAZPACHO.2"
        reply_2 = ListNssetsReply()
        nsset_ref_3 = reply_2.data.nssets.add()
        nsset_ref_3.id.uuid.value = "44"
        nsset_ref_3.handle.value = "GAZPACHO.3"
        nsset_ref_4 = reply_2.data.nssets.add()
        nsset_ref_4.id.uuid.value = "45"
        nsset_ref_4.handle.value = "GAZPACHO.4"
        self.client.stream_mock.return_value = aiter([reply_1, reply_2])

        response = await atuple(self.client.list())

        expected = (
            NssetRef(id=NssetId("42"), handle="GAZPACHO.1"),
            NssetRef(id=NssetId("43"), handle="GAZPACHO.2"),
            NssetRef(id=NssetId("44"), handle="GAZPACHO.3"),
            NssetRef(id=NssetId("45"), handle="GAZPACHO.4"),
        )
        self.assertEqual(response, expected)
        self.assertEqual(
            self.client.stream_mock.mock_calls,
            [call(ListNssetsRequest(), method="/Fred.Registry.Api.Nsset.Nsset/list_nssets", timeout=None)],
        )

    async def test_list_exclude_not_linked(self):
        self.client.stream_mock.return_value = aiter(())

        response = await atuple(self.client.list(exclude_not_linked=True))

        self.assertEqual(response, ())
        self.assertEqual(
            self.client.stream_mock.mock_calls,
            [
                call(
                    ListNssetsRequest(exclude_nssets_without_domain=True),
                    method="/Fred.Registry.Api.Nsset.Nsset/list_nssets",
                    timeout=None,
                )
            ],
        )

    async def test_get_id(self):
        nsset_id = NssetId("kryten")
        reply = NssetIdReply()
        reply.data.nsset_id.uuid.value = nsset_id
        self.client.mock.return_value = make_awaitable(reply)

        response = await self.client.get_id("HANDLE")
        self.assertEqual(response, nsset_id)
        self.assertIsInstance(response, NssetId)

        request = NssetIdRequest()
        request.nsset_handle.value = "HANDLE"
        self.client.mock.assert_called_once_with(
            request,
            method="/Fred.Registry.Api.Nsset.Nsset/get_nsset_id",
            timeout=None,
        )

    async def test_get_nsset_id(self):
        nsset_id = NssetId("kryten")
        reply = NssetIdReply()
        reply.data.nsset_id.uuid.value = str(nsset_id)
        self.client.mock.return_value = make_awaitable(reply)

        with self.assertWarnsRegex(DeprecationWarning, "Method get_nsset_id is deprecated in favor of get_id."):
            response = await self.client.get_nsset_id("HANDLE")
        self.assertEqual(response, nsset_id)
        self.assertIsInstance(response, NssetId)

        request = NssetIdRequest()
        request.nsset_handle.value = "HANDLE"
        self.client.mock.assert_called_once_with(
            request,
            method="/Fred.Registry.Api.Nsset.Nsset/get_nsset_id",
            timeout=None,
        )

    async def _test_get(self, request: NssetInfoRequest, **kwargs: Any) -> None:
        dns_host = DnsHostMessage()
        dns_host.fqdn = "dns.example.com"
        dns_host.ip_addresses[:] = ["127.0.0.1"]
        reply = NssetInfoReply()
        reply.data.nsset_id.uuid.value = "kryten"
        reply.data.nsset_handle.value = "HANDLE"
        reply.data.dns_hosts.extend([dns_host])
        self.client.mock.return_value = make_awaitable(reply)

        response = await self.client.get(NssetId("kryten"), **kwargs)

        self.assertEqual(
            response,
            Nsset(
                nsset_id=NssetId("kryten"),
                nsset_handle="HANDLE",
                dns_hosts=[DnsHost(fqdn="dns.example.com", ip_addresses=[IPv4Address("127.0.0.1")])],
            ),
        )

        request.nsset_id.uuid.value = "kryten"
        self.client.mock.assert_called_once_with(
            request,
            method="/Fred.Registry.Api.Nsset.Nsset/get_nsset_info",
            timeout=None,
        )

    async def test_get(self):
        request = NssetInfoRequest()
        await self._test_get(request)

    async def test_get_history_id(self):
        request = NssetInfoRequest()
        request.nsset_history_id.uuid.value = "nova-5"
        await self._test_get(request, history_id=NssetHistoryId("nova-5"))

    async def test_get_snapshot_id(self):
        # Test get with a snapshot_id.
        request = NssetInfoRequest()
        request.nsset_snapshot_id.value = "LAST-THURSDAY"
        await self._test_get(request, snapshot_id=SnapshotId("LAST-THURSDAY"))

    async def test_get_include_domains_count(self):
        # Test include_domains_count is passed to request correctly.
        reply = NssetInfoReply()
        reply.data.nsset_id.uuid.value = "starbug-42"
        reply.data.nsset_handle.value = "STARBUG"
        self.client.mock.return_value = make_awaitable(reply)

        await self.client.get(NssetId("starbug-42"), include_domains_count=True)

        request = NssetInfoRequest()
        request.nsset_id.uuid.value = "starbug-42"
        request.include_domains_count = True
        self.client.mock.assert_called_once_with(
            request,
            method="/Fred.Registry.Api.Nsset.Nsset/get_nsset_info",
            timeout=None,
        )

    async def _test_get_nsset_info(self, nsset_history_id: Optional[NssetHistoryId] = None) -> None:
        dns_host = DnsHostMessage()
        dns_host.fqdn = "dns.example.com"
        dns_host.ip_addresses[:] = ["127.0.0.1"]
        reply = NssetInfoReply()
        reply.data.nsset_id.uuid.value = "kryten"
        if nsset_history_id:
            reply.data.nsset_history_id.uuid.value = nsset_history_id
        reply.data.nsset_handle.value = "HANDLE"
        reply.data.dns_hosts.extend([dns_host])
        self.client.mock.return_value = make_awaitable(reply)

        with self.assertWarnsRegex(DeprecationWarning, "Method get_nsset_info is deprecated in favor of get."):
            response = await self.client.get_nsset_info(NssetId("kryten"), history_id=nsset_history_id)

        self.assertEqual(
            response,
            Nsset(
                nsset_id=NssetId("kryten"),
                nsset_history_id=nsset_history_id,
                nsset_handle="HANDLE",
                dns_hosts=[DnsHost(fqdn="dns.example.com", ip_addresses=[IPv4Address("127.0.0.1")])],
            ),
        )

        request = NssetInfoRequest()
        request.nsset_id.uuid.value = "kryten"
        if nsset_history_id:
            request.nsset_history_id.uuid.value = nsset_history_id
        self.client.mock.assert_called_once_with(
            request,
            method="/Fred.Registry.Api.Nsset.Nsset/get_nsset_info",
            timeout=None,
        )

    async def test_get_nsset_info(self):
        await self._test_get_nsset_info()

    async def test_get_nsset_info_with_history_id(self):
        await self._test_get_nsset_info(nsset_history_id=NssetHistoryId("nova-5"))

    async def test_get_nsset_info_include_domains_count(self):
        # Test include_domains_count is passed to request correctly.
        reply = NssetInfoReply()
        reply.data.nsset_id.uuid.value = "starbug-42"
        reply.data.nsset_handle.value = "STARBUG"
        self.client.mock.return_value = make_awaitable(reply)

        with self.assertWarnsRegex(DeprecationWarning, "Method get_nsset_info is deprecated in favor of get."):
            await self.client.get_nsset_info(NssetId("starbug-42"), include_domains_count=True)

        request = NssetInfoRequest()
        request.nsset_id.uuid.value = "starbug-42"
        request.include_domains_count = True
        self.client.mock.assert_called_once_with(
            request,
            method="/Fred.Registry.Api.Nsset.Nsset/get_nsset_info",
            timeout=None,
        )

    async def _test_batch_get(self, nssets: tuple[Nsset, ...]) -> None:
        result = self.client.batch_get((NssetId("starbug-42"),))

        self.assertEqual(await atuple(result), nssets)
        request = BatchNssetInfoRequest()
        req_1 = request.requests.add()
        req_1.nsset_id.uuid.value = "starbug-42"
        self.assertEqual(
            self.client.stream_mock.mock_calls,
            [call((request,), method="/Fred.Registry.Api.Nsset.Nsset/batch_nsset_info", timeout=None)],
        )

    async def test_batch_get_empty_stream(self):
        self.client.stream_mock.return_value = aiter(())

        await self._test_batch_get(())

    async def test_batch_get_empty_response(self):
        reply = BatchNssetInfoReply()
        self.client.stream_mock.return_value = aiter([reply])

        await self._test_batch_get(())

    async def test_batch_get_empty_replies(self):
        reply = BatchNssetInfoReply()
        reply.data.replies.extend([])
        self.client.stream_mock.return_value = aiter([reply])

        await self._test_batch_get(())

    async def test_batch_get(self):
        reply = BatchNssetInfoReply()
        nsset_reply = reply.data.replies.add()
        nsset_reply.data.nsset_id.uuid.value = "starbug-42"
        nsset_reply.data.nsset_handle.value = "STARBUG"
        self.client.stream_mock.return_value = aiter([reply])

        nsset = Nsset(nsset_id=NssetId("starbug-42"), nsset_handle="STARBUG")
        await self._test_batch_get((nsset,))

    async def test_batch_get_snapshot_id(self):
        # Ensure snapshot is passed to each request.
        snapshot_id = SnapshotId("LAST-THURSDAY")
        self.client.stream_mock.return_value = aiter(())

        await atuple(self.client.batch_get((NssetId("starbug-42"), NssetId("starbug-43")), snapshot_id=snapshot_id))

        request = BatchNssetInfoRequest()
        req_1 = request.requests.add()
        req_1.nsset_id.uuid.value = "starbug-42"
        req_1.nsset_snapshot_id.value = snapshot_id
        req_2 = request.requests.add()
        req_2.nsset_id.uuid.value = "starbug-43"
        req_2.nsset_snapshot_id.value = snapshot_id
        self.assertEqual(
            self.client.stream_mock.mock_calls,
            [call((request,), method="/Fred.Registry.Api.Nsset.Nsset/batch_nsset_info", timeout=None)],
        )

    async def test_batch_get_error_ignore(self):
        reply = BatchNssetInfoReply()
        nsset_reply = reply.data.replies.add()
        nsset_reply.error.exception.nsset_does_not_exist.CopyFrom(NssetDoesNotExistMessage())
        self.client.stream_mock.return_value = aiter([reply])

        await self._test_batch_get(())

    async def test_batch_get_error_raise(self):
        reply = BatchNssetInfoReply()
        nsset_reply = reply.data.replies.add()
        nsset_reply.error.exception.nsset_does_not_exist.CopyFrom(NssetDoesNotExistMessage())
        self.client.stream_mock.return_value = aiter([reply])

        with self.assertRaises(NssetDoesNotExist):
            await atuple(self.client.batch_get((NssetId("starbug-42"),), errors="raise"))

    async def test_batch_get_error_invalid(self):
        with self.assertRaisesRegex(ValueError, "Unknown errors value: 'invalid'"):
            await atuple(self.client.batch_get((), errors="invalid"))  # type: ignore[arg-type]

    async def test_batch_get_include_domains_count(self):
        # Test include_domains_count is passed to the request correctly.
        self.client.stream_mock.return_value = aiter(())

        await atuple(self.client.batch_get((NssetId("starbug-42"),), include_domains_count=True))

        request = BatchNssetInfoRequest()
        req_1 = request.requests.add()
        req_1.nsset_id.uuid.value = "starbug-42"
        req_1.include_domains_count = True
        self.assertEqual(
            self.client.stream_mock.mock_calls,
            [call((request,), method="/Fred.Registry.Api.Nsset.Nsset/batch_nsset_info", timeout=None)],
        )

    async def test_batch_get_flatten(self):
        # Test responses are properly flattened.
        reply_1 = BatchNssetInfoReply()
        nsset_reply_1 = reply_1.data.replies.add()
        nsset_reply_1.data.nsset_id.uuid.value = "42"
        nsset_reply_1.data.nsset_handle.value = "STARBUG-42"
        nsset_reply_2 = reply_1.data.replies.add()
        nsset_reply_2.data.nsset_id.uuid.value = "43"
        nsset_reply_2.data.nsset_handle.value = "STARBUG-43"
        reply_2 = BatchNssetInfoReply()
        nsset_reply_3 = reply_2.data.replies.add()
        nsset_reply_3.data.nsset_id.uuid.value = "44"
        nsset_reply_3.data.nsset_handle.value = "STARBUG-44"
        nsset_reply_4 = reply_2.data.replies.add()
        nsset_reply_4.data.nsset_id.uuid.value = "45"
        nsset_reply_4.data.nsset_handle.value = "STARBUG-45"
        self.client.stream_mock.return_value = aiter([reply_1, reply_2])

        nssets = (
            Nsset(nsset_id=NssetId("42"), nsset_handle="STARBUG-42"),
            Nsset(nsset_id=NssetId("43"), nsset_handle="STARBUG-43"),
            Nsset(nsset_id=NssetId("44"), nsset_handle="STARBUG-44"),
            Nsset(nsset_id=NssetId("45"), nsset_handle="STARBUG-45"),
        )
        await self._test_batch_get(nssets)

    async def _test_list_by_contact(self, result: tuple[NssetByRelation, ...]) -> None:
        self.assertEqual(await atuple(self.client.list_by_contact(ContactId("KRYTEN"))), result)

        request = ListNssetsByContactRequest()
        request.contact_id.uuid.value = "KRYTEN"
        self.assertEqual(
            self.client.stream_mock.mock_calls,
            [call(request, method="/Fred.Registry.Api.Nsset.Nsset/list_nssets_by_contact", timeout=None)],
        )

    async def test_list_by_contact_empty_stream(self):
        self.client.stream_mock.return_value = aiter(())

        await self._test_list_by_contact(())

    async def test_list_by_contact_empty_response(self):
        reply = ListNssetsByContactReply()
        reply.data.nssets.extend([])
        self.client.stream_mock.return_value = aiter([reply])

        await self._test_list_by_contact(())

    async def test_list_by_contact(self):
        reply = ListNssetsByContactReply()
        nsset_ref = reply.data.nssets.add()
        nsset_ref.nsset.id.uuid.value = "starbug-42"
        nsset_ref.nsset.handle.value = "STARBUG"
        nsset_ref.nsset_history_id.uuid.value = "Nova 5"
        nsset_ref.is_deleted = True
        self.client.stream_mock.return_value = aiter([reply])

        nsset = NssetByRelation(
            nsset=NssetRef(id=NssetId("starbug-42"), handle="STARBUG"),
            nsset_history_id=NssetHistoryId("Nova 5"),
            is_deleted=True,
        )
        await self._test_list_by_contact((nsset,))

    async def test_list_by_contact_snapshot_id(self):
        # Test snapshot_id is passed to the request correctly.
        self.client.stream_mock.return_value = aiter(())

        await atuple(self.client.list_by_contact(ContactId("KRYTEN"), snapshot_id=SnapshotId("LAST-THURSDAY")))

        request = ListNssetsByContactRequest()
        request.contact_id.uuid.value = "KRYTEN"
        request.contact_snapshot_id.value = SnapshotId("LAST-THURSDAY")
        self.assertEqual(
            self.client.stream_mock.mock_calls,
            [call(request, method="/Fred.Registry.Api.Nsset.Nsset/list_nssets_by_contact", timeout=None)],
        )

    async def test_list_by_contact_aggregate(self):
        # Test aggregate_entire_history is passed to the request correctly.
        self.client.stream_mock.return_value = aiter(())

        await atuple(self.client.list_by_contact(ContactId("KRYTEN"), aggregate_entire_history=True))

        request = ListNssetsByContactRequest()
        request.contact_id.uuid.value = "KRYTEN"
        request.aggregate_entire_history = True
        self.assertEqual(
            self.client.stream_mock.mock_calls,
            [call(request, method="/Fred.Registry.Api.Nsset.Nsset/list_nssets_by_contact", timeout=None)],
        )

    async def test_list_by_contact_order_by(self):
        # Test order_by is passed to the request correctly.
        self.client.stream_mock.return_value = aiter(())

        await atuple(self.client.list_by_contact(ContactId("KRYTEN"), order_by=("-crdate", "handle")))

        request = ListNssetsByContactRequest()
        request.contact_id.uuid.value = "KRYTEN"
        request.order_by.add(
            field=ListNssetsByContactRequest.OrderByField.order_by_field_crdate,
            direction=OrderByDirection.order_by_direction_descending,
        )
        request.order_by.add(field=ListNssetsByContactRequest.OrderByField.order_by_field_handle)
        self.assertEqual(
            self.client.stream_mock.mock_calls,
            [call(request, method="/Fred.Registry.Api.Nsset.Nsset/list_nssets_by_contact", timeout=None)],
        )

    async def test_list_by_contact_flatten(self):
        # Test responses are properly flattened.
        reply_1 = ListNssetsByContactReply()
        nsset_ref_1 = reply_1.data.nssets.add()
        nsset_ref_1.nsset.id.uuid.value = "42"
        nsset_ref_1.nsset.handle.value = "STARBUG-42"
        nsset_ref_2 = reply_1.data.nssets.add()
        nsset_ref_2.nsset.id.uuid.value = "43"
        nsset_ref_2.nsset.handle.value = "STARBUG-43"
        reply_2 = ListNssetsByContactReply()
        nsset_ref_3 = reply_2.data.nssets.add()
        nsset_ref_3.nsset.id.uuid.value = "44"
        nsset_ref_3.nsset.handle.value = "STARBUG-44"
        nsset_ref_4 = reply_2.data.nssets.add()
        nsset_ref_4.nsset.id.uuid.value = "45"
        nsset_ref_4.nsset.handle.value = "STARBUG-45"
        self.client.stream_mock.return_value = aiter([reply_1, reply_2])

        nssets = (
            NssetByRelation(nsset=NssetRef(id=NssetId("42"), handle="STARBUG-42")),
            NssetByRelation(nsset=NssetRef(id=NssetId("43"), handle="STARBUG-43")),
            NssetByRelation(nsset=NssetRef(id=NssetId("44"), handle="STARBUG-44")),
            NssetByRelation(nsset=NssetRef(id=NssetId("45"), handle="STARBUG-45")),
        )
        await self._test_list_by_contact(nssets)

    async def _test_list_by_contact_page(self, page: ListPage[NssetByRelation]) -> None:
        result = await self.client.list_by_contact_page(ContactId("KRYTEN"))

        self.assertEqual(result, page)

        request = ListNssetsByContactRequest()
        request.contact_id.uuid.value = "KRYTEN"
        self.assertEqual(
            self.client.stream_mock.mock_calls,
            [call(request, method="/Fred.Registry.Api.Nsset.Nsset/list_nssets_by_contact", timeout=None)],
        )

    async def test_list_by_contact_page_empty_stream(self):
        self.client.stream_mock.return_value = aiter(())

        await self._test_list_by_contact_page(ListPage(results=()))

    async def test_list_by_contact_page_empty_response(self):
        reply = ListNssetsByContactReply()
        reply.data.nssets.extend([])
        self.client.stream_mock.return_value = aiter([reply])

        await self._test_list_by_contact_page(ListPage(results=()))

    async def test_list_by_contact_page_pagination_response(self):
        reply = ListNssetsByContactReply()
        reply.data.pagination.next_page_token = "quagaars"
        reply.data.pagination.items_left = 42
        self.client.stream_mock.return_value = aiter([reply])

        pagination = PaginationReply(next_page_token="quagaars", items_left=42)
        await self._test_list_by_contact_page(ListPage(results=(), pagination=pagination))

    async def test_list_by_contact_page(self):
        reply = ListNssetsByContactReply()
        nsset_ref = reply.data.nssets.add()
        nsset_ref.nsset.id.uuid.value = "starbug-42"
        nsset_ref.nsset.handle.value = "STARBUG"
        nsset_ref.nsset_history_id.uuid.value = "Nova 5"
        nsset_ref.is_deleted = True
        self.client.stream_mock.return_value = aiter([reply])

        nsset = NssetByRelation(
            nsset=NssetRef(id=NssetId("starbug-42"), handle="STARBUG"),
            nsset_history_id=NssetHistoryId("Nova 5"),
            is_deleted=True,
        )
        await self._test_list_by_contact_page(ListPage(results=(nsset,)))

    async def test_list_by_contact_page_pagination_request(self):
        # Test pagination args are passed to the request correctly.
        self.client.stream_mock.return_value = aiter(())

        await self.client.list_by_contact_page(ContactId("KRYTEN"), page_token="quagaars", page_size=42)

        request = ListNssetsByContactRequest()
        request.contact_id.uuid.value = "KRYTEN"
        request.pagination.page_token = "quagaars"
        request.pagination.page_size = 42
        self.assertEqual(
            self.client.stream_mock.mock_calls,
            [call(request, method="/Fred.Registry.Api.Nsset.Nsset/list_nssets_by_contact", timeout=None)],
        )

    async def test_list_by_contact_page_snapshot_id(self):
        # Test snapshot_id is passed to the request correctly.
        self.client.stream_mock.return_value = aiter(())

        await self.client.list_by_contact_page(ContactId("KRYTEN"), snapshot_id=SnapshotId("LAST-THURSDAY"))

        request = ListNssetsByContactRequest()
        request.contact_id.uuid.value = "KRYTEN"
        request.contact_snapshot_id.value = SnapshotId("LAST-THURSDAY")
        self.assertEqual(
            self.client.stream_mock.mock_calls,
            [call(request, method="/Fred.Registry.Api.Nsset.Nsset/list_nssets_by_contact", timeout=None)],
        )

    async def test_list_by_contact_page_aggregate(self):
        # Test aggregate_entire_history is passed to the request correctly.
        self.client.stream_mock.return_value = aiter(())

        await self.client.list_by_contact_page(ContactId("KRYTEN"), aggregate_entire_history=True)

        request = ListNssetsByContactRequest()
        request.contact_id.uuid.value = "KRYTEN"
        request.aggregate_entire_history = True
        self.assertEqual(
            self.client.stream_mock.mock_calls,
            [call(request, method="/Fred.Registry.Api.Nsset.Nsset/list_nssets_by_contact", timeout=None)],
        )

    async def test_list_by_contact_page_order_by(self):
        # Test order_by is passed to the request correctly.
        self.client.stream_mock.return_value = aiter(())

        await self.client.list_by_contact_page(ContactId("KRYTEN"), order_by=("-crdate", "handle"))

        request = ListNssetsByContactRequest()
        request.contact_id.uuid.value = "KRYTEN"
        request.order_by.add(
            field=ListNssetsByContactRequest.OrderByField.order_by_field_crdate,
            direction=OrderByDirection.order_by_direction_descending,
        )
        request.order_by.add(field=ListNssetsByContactRequest.OrderByField.order_by_field_handle)
        self.assertEqual(
            self.client.stream_mock.mock_calls,
            [call(request, method="/Fred.Registry.Api.Nsset.Nsset/list_nssets_by_contact", timeout=None)],
        )

    async def test_list_by_contact_page_flatten(self):
        # Test responses are properly flattened.
        reply_1 = ListNssetsByContactReply()
        nsset_ref_1 = reply_1.data.nssets.add()
        nsset_ref_1.nsset.id.uuid.value = "42"
        nsset_ref_1.nsset.handle.value = "STARBUG-42"
        nsset_ref_2 = reply_1.data.nssets.add()
        nsset_ref_2.nsset.id.uuid.value = "43"
        nsset_ref_2.nsset.handle.value = "STARBUG-43"
        reply_2 = ListNssetsByContactReply()
        nsset_ref_3 = reply_2.data.nssets.add()
        nsset_ref_3.nsset.id.uuid.value = "44"
        nsset_ref_3.nsset.handle.value = "STARBUG-44"
        nsset_ref_4 = reply_2.data.nssets.add()
        nsset_ref_4.nsset.id.uuid.value = "45"
        nsset_ref_4.nsset.handle.value = "STARBUG-45"
        self.client.stream_mock.return_value = aiter([reply_1, reply_2])

        nssets = (
            NssetByRelation(nsset=NssetRef(id=NssetId("42"), handle="STARBUG-42")),
            NssetByRelation(nsset=NssetRef(id=NssetId("43"), handle="STARBUG-43")),
            NssetByRelation(nsset=NssetRef(id=NssetId("44"), handle="STARBUG-44")),
            NssetByRelation(nsset=NssetRef(id=NssetId("45"), handle="STARBUG-45")),
        )
        await self._test_list_by_contact_page(ListPage(results=nssets))

    async def test_update_state_empty(self):
        self.client.stream_mock.return_value = aiter(())

        await self.client.update_state(())

        self.assertEqual(
            self.client.stream_mock.mock_calls,
            [call((), method="/Fred.Registry.Api.Nsset.Nsset/update_nsset_state", timeout=None)],
        )

    async def test_update_state(self):
        self.client.stream_mock.return_value = aiter(())

        await self.client.update_state(
            (StateUpdate(id=NssetId("STARBUG-42"), set_flags=("POWER",), unset_flags=("ANCHOR",)),)
        )

        request = UpdateNssetStateRequest()
        update = request.updates.add()
        update.nsset_id.uuid.value = "STARBUG-42"
        update.set_flags.append("POWER")
        update.unset_flags.append("ANCHOR")
        self.assertEqual(
            self.client.stream_mock.mock_calls,
            [call((request,), method="/Fred.Registry.Api.Nsset.Nsset/update_nsset_state", timeout=None)],
        )

    async def test_update_state_kwargs(self):
        self.client.stream_mock.return_value = aiter(())

        await self.client.update_state(
            (StateUpdate(id=NssetId("STARBUG-42")),), snapshot_id=SnapshotId("BACKWARDS"), log_entry_id="DEAR-DIARY"
        )

        request = UpdateNssetStateRequest()
        update = request.updates.add()
        update.nsset_id.uuid.value = "STARBUG-42"
        request.nsset_snapshot_id.value = "BACKWARDS"
        request.log_entry_id.value = "DEAR-DIARY"
        self.assertEqual(
            self.client.stream_mock.mock_calls,
            [call((request,), method="/Fred.Registry.Api.Nsset.Nsset/update_nsset_state", timeout=None)],
        )

    async def test_update_state_batch_size(self):
        self.client.stream_mock.return_value = aiter(())

        await self.client.update_state(
            (StateUpdate(id=NssetId("STARBUG-42")), StateUpdate(id=NssetId("STARBUG-43"))), batch_size=1
        )

        request_1 = UpdateNssetStateRequest()
        update_1 = request_1.updates.add()
        update_1.nsset_id.uuid.value = "STARBUG-42"
        request_2 = UpdateNssetStateRequest()
        update_2 = request_2.updates.add()
        update_2.nsset_id.uuid.value = "STARBUG-43"
        self.assertEqual(
            self.client.stream_mock.mock_calls,
            [call((request_1, request_2), method="/Fred.Registry.Api.Nsset.Nsset/update_nsset_state", timeout=None)],
        )

    async def _test_check_dns_host(self, fqdn: str, request_fqdn: str) -> None:
        reply = CheckDnsHostReply()
        reply.data.dns_host_exists = True
        self.client.mock.return_value = make_awaitable(reply)

        self.assertTrue(await self.client.check_dns_host(fqdn))

        request = CheckDnsHostRequest()
        request.fqdn = request_fqdn
        self.assertEqual(
            self.client.mock.mock_calls,
            [call(request, method="/Fred.Registry.Api.Nsset.Nsset/check_dns_host", timeout=None)],
        )

    async def test_check_dns_host(self):
        await self._test_check_dns_host("example.org", "example.org")

    async def test_check_dns_host_idn(self):
        await self._test_check_dns_host("háčkyčárky.cz", "xn--hkyrky-ptac70bc.cz")

    async def test_add_auth_info_generate(self):
        reply = AddNssetAuthInfoReply()
        reply.data.auth_info = "Gazpacho!"
        self.client.mock.return_value = make_awaitable(reply)

        auth_info = await self.client.add_auth_info(NssetId("STARBUG-1"))

        self.assertEqual(auth_info, "Gazpacho!")

        request = AddNssetAuthInfoRequest()
        request.nsset_id.uuid.value = "STARBUG-1"
        request.generate.CopyFrom(Empty())
        self.assertEqual(
            self.client.mock.mock_calls,
            [call(request, method="/Fred.Registry.Api.Nsset.Nsset/add_nsset_auth_info", timeout=None)],
        )

    async def test_add_auth_info_custom(self):
        reply = AddNssetAuthInfoReply()
        self.client.mock.return_value = make_awaitable(reply)

        auth_info = await self.client.add_auth_info(NssetId("STARBUG-1"), auth_info="Gazpacho!")  # type: ignore[func-returns-value]

        self.assertIsNone(auth_info)

        request = AddNssetAuthInfoRequest()
        request.nsset_id.uuid.value = "STARBUG-1"
        request.auth_info = "Gazpacho!"
        self.assertEqual(
            self.client.mock.mock_calls,
            [call(request, method="/Fred.Registry.Api.Nsset.Nsset/add_nsset_auth_info", timeout=None)],
        )

    async def test_add_auth_info_extra_args(self):
        reply = AddNssetAuthInfoReply()
        reply.data.auth_info = "Gazpacho!"
        self.client.mock.return_value = make_awaitable(reply)

        auth_info = await self.client.add_auth_info(
            NssetId("STARBUG-1"), snapshot_id=SnapshotId("NOVA5"), ttl=42, log_entry_id="2X4B", by_registrar="HOLLY"
        )

        self.assertEqual(auth_info, "Gazpacho!")

        request = AddNssetAuthInfoRequest()
        request.nsset_id.uuid.value = "STARBUG-1"
        request.nsset_snapshot_id.value = "NOVA5"
        request.generate.CopyFrom(Empty())
        request.ttl = 42
        request.log_entry_id.value = "2X4B"
        request.by_registrar.value = "HOLLY"
        self.assertEqual(
            self.client.mock.mock_calls,
            [call(request, method="/Fred.Registry.Api.Nsset.Nsset/add_nsset_auth_info", timeout=None)],
        )

"""Contact representatives and their client API."""

from typing import Optional, Union, cast

from fred_api.registry.contact_representative.contact_representative_common_types_pb2 import ContactRepresentativeInfo
from fred_api.registry.contact_representative.service_contact_representative_grpc_pb2 import (
    ContactRepresentativeInfoReply,
    ContactRepresentativeInfoRequest,
    CreateContactRepresentativeReply,
    CreateContactRepresentativeRequest,
    DeleteContactRepresentativeReply,
    DeleteContactRepresentativeRequest,
    UpdateContactRepresentativeReply,
    UpdateContactRepresentativeRequest,
)
from fred_api.registry.contact_representative.service_contact_representative_grpc_pb2_grpc import (
    ContactRepresentativeStub,
)
from fred_types import BaseModel, ContactId, RepresentativeHistoryId, RepresentativeId
from frgal.aio import AsyncGrpcClient

from .common import Address, RegistryDecoder
from .exceptions import ContactDoesNotExist, InvalidData, RepresentativeAlreadyExists, RepresentativeDoesNotExist


class Representative(BaseModel):
    """Represents a contact representative."""

    id: RepresentativeId
    history_id: Optional[RepresentativeHistoryId] = None
    contact_id: ContactId
    name: str = ""
    organization: str = ""
    place: Optional[Address] = None
    telephone: str = ""
    email: str = ""


class RepresentativeDecoder(RegistryDecoder):
    """Contact representative service decoder."""

    def __init__(self) -> None:
        super().__init__()
        self.set_decoder(ContactRepresentativeInfo, self._decode_contact_representative_info)

        self.set_exception_decoder(
            ContactRepresentativeInfoReply.Exception.ContactRepresentativeDoesNotExist, RepresentativeDoesNotExist
        )
        self.set_exception_decoder(
            CreateContactRepresentativeReply.Exception.ContactRepresentativeAlreadyExists, RepresentativeAlreadyExists
        )
        self.set_exception_decoder(CreateContactRepresentativeReply.Exception.ContactDoesNotExist, ContactDoesNotExist)
        self.set_exception_decoder(CreateContactRepresentativeReply.Exception.InvalidData, InvalidData)
        self.set_exception_decoder(
            UpdateContactRepresentativeReply.Exception.ContactRepresentativeDoesNotExist, RepresentativeDoesNotExist
        )
        self.set_exception_decoder(UpdateContactRepresentativeReply.Exception.InvalidData, InvalidData)
        self.set_exception_decoder(
            DeleteContactRepresentativeReply.Exception.ContactRepresentativeDoesNotExist, RepresentativeDoesNotExist
        )

    def _decode_contact_representative_info(self, value: ContactRepresentativeInfo) -> Representative:
        """Decode ContactRepresentativeInfo."""
        data = self._decode_message(value)
        data["telephone"] = data["telephone"] or ""
        data["email"] = data["email"] or ""
        return Representative(**data)


class RepresentativeClient(AsyncGrpcClient):
    """Contact representative service gRPC client."""

    stub_cls = ContactRepresentativeStub
    decoder_cls = RepresentativeDecoder
    service = "ContactRepresentative"

    async def get(
        self,
        value: Union[RepresentativeId, RepresentativeHistoryId, ContactId],
        /,
    ) -> Representative:
        """Return contact representative based on its identifier.

        Args:
            value: Contact representative ID, contact representative history ID or contact ID.

        Returns:
            Contact representative info.

        Raises:
            RepresentativeDoesNotExist: Identifier doesn't reference any known representative.
        """
        request = ContactRepresentativeInfoRequest()
        if isinstance(value, ContactId):
            request.contact_id.uuid.value = value
        elif isinstance(value, RepresentativeHistoryId):
            request.history_id.value = value
        else:
            request.id.value = value
        return cast(Representative, await self.call("get_contact_representative_info", request))

    async def create(self, representative: Representative, *, log_entry_id: Optional[str] = None) -> Representative:
        """Create and return contact representative.

        Args:
            representative: Contact representative.
            log_entry_id: An identifier of the related log entry.

        Returns:
            Contact representative info.

        Raises:
            ContactDoesNotExist: Referenced contact does not exist.
            RepresentativeAlreadyExists: Contact representative already exists.
            InvalidData: Contact representative data are invalid.
        """
        request = CreateContactRepresentativeRequest()
        request.contact_id.uuid.value = representative.contact_id
        request.name = representative.name
        request.organization = representative.organization
        if representative.place:
            request.place.street.extend(representative.place.street)
            request.place.city = representative.place.city
            request.place.state_or_province = representative.place.state_or_province
            request.place.postal_code.value = representative.place.postal_code
            request.place.country_code.value = representative.place.country_code
        if representative.telephone:
            request.telephone.value = representative.telephone
        if representative.email:
            request.email.value = representative.email
        if log_entry_id:
            request.log_entry_id.value = log_entry_id
        return cast(Representative, await self.call("create_contact_representative", request))

    async def update(self, representative: Representative, *, log_entry_id: Optional[str] = None) -> Representative:
        """Update and return contact representative.

        Args:
            representative: Contact representative.
            log_entry_id: An identifier of the related log entry.

        Returns:
            Contact representative info.

        Raises:
            RepresentativeDoesNotExist: Contact representative does not exist.
            InvalidData: Contact representative data are invalid.
        """
        request = UpdateContactRepresentativeRequest()
        request.id.value = representative.id
        request.name = representative.name
        request.organization = representative.organization
        if representative.place:
            request.place.street.extend(representative.place.street)
            request.place.city = representative.place.city
            request.place.state_or_province = representative.place.state_or_province
            request.place.postal_code.value = representative.place.postal_code
            request.place.country_code.value = representative.place.country_code
        if representative.telephone:
            request.telephone.value = representative.telephone
        if representative.email:
            request.email.value = representative.email
        if log_entry_id:
            request.log_entry_id.value = log_entry_id
        return cast(Representative, await self.call("update_contact_representative", request))

    async def delete(self, id: RepresentativeId, *, log_entry_id: Optional[str] = None) -> None:
        """Delete contact representative.

        Args:
            id: Contact representative ID.
            log_entry_id: An identifier of the related log entry.

        Raises:
            RepresentativeDoesNotExist: Contact representative does not exist.
        """
        request = DeleteContactRepresentativeRequest()
        request.id.value = id
        if log_entry_id:
            request.log_entry_id.value = log_entry_id
        await self.call("delete_contact_representative", request)
